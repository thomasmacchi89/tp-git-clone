#language: fr

@locaux
Fonctionnalité: Visualisation des locaux
	Pour pouvoir utiliser deveco
	En tant que DevEco
	Je veux pouvoir consulter les locaux

Scénario: Ajout et recherche de locaux
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Immobilier
	Alors je suis sur la page Immobilier
	Quand je clique sur "Créer un local"
	Quand je renseigne "Ancienne pizzeria" dans le champ "Titre"
	Quand je renseigne "30 Rue du 125ème Régiment d’Infanterie 86000 Poitiers" dans le champ "Adresse"
	Quand je clique sur "#option-0"
	Quand je choisis "#locaux-statut-option-vacant"
	Quand je choisis "#locaux-types-option-commerce"
	Quand je renseigne "173" dans le champ "Surface total (m²)"
	Quand je renseigne "66000" dans le champ "Loyer annuel (€)"
	Quand je renseigne "Centre-ville" dans le champ "Zone géographique"
	Quand je clique sur "#option-999"
	Quand je renseigne "Lorem ipsum, etc." dans le champ "Commentaires"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Alors je vois "à l'instant par"
	Quand je clique sur "Immobilier"
	Alors je suis sur la page "/locaux"
	Alors je vois "Rechercher dans mes locaux"
	Quand je renseigne "Ancienne" dans le champ "Rechercher dans mes locaux"
	Quand je clique sur "Rechercher"
	Alors je vois "pizzeria"
	Quand je choisis "#filtres-local-statut-option-occupe"
	Alors je ne vois pas "pizzeria"
	Quand je choisis "#filtres-local-statut-option-vacant"
	Alors je vois "pizzeria"
	Quand je clique sur "Bureau"
	Alors je ne vois pas "pizzeria"
	Quand je clique sur "sans filtre"
	Alors je vois "pizzeria"
	Quand je clique sur "Commerce"
	Alors je vois "pizzeria"
	Quand je clique sur "Ancienne pizzeria"
	Alors je suis sur l'url "/locaux/1"

Scénario: Ajout et suppression de propriétaire du territoire
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Immobilier
	Alors je suis sur la page Immobilier
	Quand je clique sur "Créer un local"
	Quand je renseigne "Ancienne pizzeria" dans le champ "Titre"
	Quand je renseigne "30 Rue du 125ème Régiment d’Infanterie 86000 Poitiers" dans le champ "Adresse"
	Quand je clique sur "#option-0"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je clique sur "Ajouter un propriétaire"
	Alors je vois "Rechercher dans les Établissements de mon territoire"
	Quand je renseigne "00572060200043" dans le champ "#proprietaire-recherche-siret"
	Quand je clique sur "Rechercher"
	Alors je vois "ABBEVILLE"
	Quand je clique sur "OK"
	Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
	Alors je vois "ABBEVILLE"
	Quand j'attends 2 secondes
	Quand je vais sur la page du Local 1
	Alors je vois "Ancienne pizzeria"
	Alors je vois "ABBEVILLE"
	Quand je clique sur "Supprimer"
	Alors je vois "Supprimer un propriétaire du local Ancienne pizzeria"
	Quand je clique sur "OK"
	Alors je ne vois pas "Supprimer un propriétaire du local Ancienne pizzeria"
	Alors je ne vois pas "ABBEVILLE"

Scénario: Ajout et suppression de propriétaire hors territoire
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Immobilier
	Alors je suis sur la page Immobilier
	Quand je clique sur "Créer un local"
	Quand je renseigne "Ancienne pizzeria" dans le champ "Titre"
	Quand je renseigne "30 Rue du 125ème Régiment d’Infanterie 86000 Poitiers" dans le champ "Adresse"
	Quand je clique sur "#option-0"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je clique sur "Ajouter un propriétaire"
	Alors je vois "Rechercher dans les Établissements de mon territoire"
	Quand je renseigne "77567227234537" dans le champ "#proprietaire-recherche-siret"
	Quand je clique sur "Rechercher"
	Alors je vois "CROIX ROUGE"
	Quand je clique sur "OK"
	Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
	Alors je vois "CROIX ROUGE"
	Quand j'attends 2 secondes
	Quand je vais sur la page du Local 1
	Alors je vois "Ancienne pizzeria"
	Alors je vois "CROIX ROUGE"
	Quand je clique sur "Supprimer"
	Alors je vois "Supprimer un propriétaire du local Ancienne pizzeria"
	Quand je clique sur "OK"
	Alors je ne vois pas "Supprimer un propriétaire du local Ancienne pizzeria"
	Alors je ne vois pas "CROIX ROUGE"
	Quand je clique sur l'onglet Établissements
	Alors je vois "Date de création"
	Quand je clique sur "Localisation"
	Quand je clique sur "Hors du territoire"
	Quand je clique sur "Appliquer les filtres"
	Alors je vois "CROIX ROUGE"
	Alors je vois "EXOGÈNE"

Scénario: Ajout et suppression de propriétaire particulier
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Immobilier
	Alors je suis sur la page Immobilier
	Quand je clique sur "Créer un local"
	Quand je renseigne "Ancienne pizzeria" dans le champ "Titre"
	Quand je renseigne "30 Rue du 125ème Régiment d’Infanterie 86000 Poitiers" dans le champ "Adresse"
	Quand je clique sur "#option-0"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je clique sur "Ajouter un propriétaire"
	Alors je vois "Rechercher dans les Établissements de mon territoire"
	Quand je clique sur "Créer un propriétaire"
	Alors je vois "Téléphone"
	Quand je renseigne "Gustin" dans le champ "Nom"
	Quand je renseigne "Proprio" dans le champ "Prénom"
	Quand je clique sur "OK"
	Alors je ne vois pas "Ajouter un propriétaire au local Pizza"
	Alors je vois "Proprio Gustin"
	Quand j'attends 2 secondes
	Quand je vais sur la page du Local 1
	Alors je vois "Ancienne pizzeria"
	Alors je vois "Proprio Gustin"
	Quand je clique sur "Supprimer"
	Alors je vois "Supprimer un propriétaire du local Ancienne pizzeria"
	Quand je clique sur "OK"
	Alors je ne vois pas "Supprimer un propriétaire du local Ancienne pizzeria"
	Alors je ne vois pas "Proprio Gustin"
