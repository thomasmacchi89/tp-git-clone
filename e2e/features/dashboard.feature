#language: fr

@dashboard
Fonctionnalité: Dashboard
	Pour pouvoir avoir une vue d'ensemble de mon Territoire
	En tant que Deveco
	Je veux pouvoir consulter le Dashboard

Scénario: Dashboard
	Quand j'ai un compte "DevEco" avec l'email "truc@test.fr" et la clef "bleh"
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je vois "Territoire de référence"
	Alors je vois "Mes collaborateurs"
	Alors je ne vois pas "truc@test.fr"
	Quand je clique sur "#collaborateurs--button"
	Alors je vois "truc@test.fr"
	Alors je vois "sur le territoire"
	Quand je clique sur la carte qui contient "sur le territoire"
	Alors je suis sur la page Établissements
	Quand je clique sur l'onglet Accueil
	Alors je vois "dans le portefeuille"
	Quand je clique sur la carte qui contient "dans le portefeuille"
	Alors je suis sur la page "/etablissements?suivi=portefeuille"
	Quand je clique sur l'onglet Accueil
	Alors je vois "locaux dont"
	Quand je clique sur la carte qui contient "locaux dont"
	Alors je suis sur la page Immobilier
	Quand je clique sur l'onglet Accueil
	Alors je vois "créateur d'entreprise"
	Quand je clique sur la carte qui contient "créateur d'entreprise"
	Alors je suis sur la page Créateurs
