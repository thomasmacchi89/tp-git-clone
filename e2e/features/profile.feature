#language: fr

@profile
Fonctionnalité: Page de profil
	Pour pouvoir consulter et modifier mes informations personnelles
	En tant qu'utilisateur de la plateforme
	Je veux pouvoir consulter ma page profil et modifier mes informations

Scénario: Informations personnelles
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Mes informations personnelles"
	Alors je suis sur la page "/profil"
	Alors je vois "Mes informations personnelles"
	Alors je vois "Adresse de courriel"
	Alors je vois "machin@test.fr"
	Quand je clique sur "Modifier mes informations"
	Alors l'élément nommé "input-email" est "désactivé"
	Alors l'élément nommé "submit-profile" est "désactivé"
	Quand je renseigne "Machin" dans le champ "Prénom"
	Quand je renseigne "Truc" dans le champ "Nom"
	Quand je clique sur "Enregistrer"
	Alors je vois "Mise à jour effectuée"
	Alors je vois "Modifier mes informations"
	Alors je vois "Machin"
	Alors je vois "Truc"

Scénario: Gestion du mot de passe
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Mes informations personnelles"
	Alors je suis sur la page "/profil"
	Alors je vois "Gestion du mot de passe"
	Quand je clique sur "Modifier mon mot de passe"
	Alors l'élément nommé "submit-password" est "désactivé"
	Quand je renseigne "test" dans le champ "Mot de passe"
	Quand je renseigne "confirmation" dans le champ "Confirmation"
	Alors l'élément nommé "submit-password" est "désactivé"
	Quand je renseigne "test" dans le champ "Confirmation"
	Alors l'élément nommé "submit-password" est "activé"
	Quand je clique sur "Enregistrer"
	Alors je vois "Mise à jour effectuée"
	Alors je vois "Modifier mon mot de passe"
