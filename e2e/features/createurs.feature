#language: fr
#

@createurs
Fonctionnalité: Visualisation des créateurs
	Pour pouvoir utiliser deveco
	En tant que DevEco
	Je veux pouvoir consulter mes Créateurs d'entreprise

Scénario: Ajout et recherche de Créateur
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Quand je clique sur "Créer la fiche"
	Quand j'attends 2 secondes
	Alors je vois "Dernière modification"
	Alors je suis sur la page du Créateur 1
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Alors je vois "Rechercher dans les créateurs d'entreprise"
	Alors je vois "Danielle Regner"
	Quand je renseigne "Test" dans le champ "Rechercher dans les créateurs d'entreprise"
	Quand je clique sur "Rechercher"
	Alors je ne vois pas "Danielle Regner"

Scénario: Modification de fiche
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Quand je clique sur "Créer"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Alors je vois "par machin@test.fr"
	Quand je clique sur "Modifier"
	Alors je suis sur la page d'édition du Créateur 1
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je clique sur "Enregistrer"
	Alors je suis sur la page du Créateur 1
	Alors je vois "Maurice"
	Alors je ne vois pas "Danielle"

Scénario: Qualification de créateur - Activités réelles
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je clique sur "Modifier"
	Quand je renseigne "Test" dans le champ "#champ-selection-activites-input"
	Quand je clique sur le texte "Créer"
	Quand je clique sur "Enregistrer"
	Alors je ne vois pas "Enregistrer"
	Alors je vois "Test"
	Quand je vais sur la page Créateurs
	Alors je vois "Danielle"
	Alors je vois "Maurice"
	Quand je clique sur "Champs qualifiés"
	Quand je clique sur "#champ-selection-activites-input"
	Quand je clique sur le texte "Test"
	Quand je clique sur "Appliquer les filtres"
	Alors je ne vois pas "Danielle"
	Alors je vois "Maurice"

Scénario: Qualification de créateur - Zones géographiques
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je clique sur "Modifier"
	Quand je renseigne "Test" dans le champ "#champ-selection-zones-input"
	Quand je clique sur le texte "Créer"
	Quand je clique sur "Enregistrer"
	Alors je ne vois pas "Enregistrer"
	Alors je vois "Test"
	Quand je vais sur la page Créateurs
	Alors je vois "Danielle"
	Alors je vois "Maurice"
	Quand je clique sur "Champs qualifiés"
	Quand je clique sur "#champ-selection-zones-input"
	Quand je clique sur le texte "Test"
	Quand je clique sur "Appliquer les filtres"
	Alors je ne vois pas "Danielle"
	Alors je vois "Maurice"

Scénario: Qualification de créateur - Mots-clés
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je clique sur "Modifier"
	Quand je renseigne "Test" dans le champ "#champ-selection-mots-input"
	Quand je clique sur le texte "Créer"
	Quand je clique sur "Enregistrer"
	Alors je ne vois pas "Enregistrer"
	Alors je vois "Test"
	Quand je vais sur la page Créateurs
	Alors je vois "Danielle"
	Alors je vois "Maurice"
	Quand je clique sur "Champs qualifiés"
	Quand je clique sur "#champ-selection-mots-input"
	Quand je clique sur le texte "Test"
	Quand je clique sur "Appliquer les filtres"
	Alors je ne vois pas "Danielle"
	Alors je vois "Maurice"

Scénario: Modification d'échange
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Quand je clique sur "Créer"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Alors je vois "par machin@test.fr"
	Quand je clique sur "Modifier l'échange"
	Alors je vois "Modifier un élément de suivi"
	Alors je vois "Qualification de l'échange"
	Quand je renseigne "Premier échange (modifié)" dans le champ "Titre de l'échange"
	Quand je clique sur "Enregistrer"
	Alors je ne vois pas "Modifier un élément de suivi"
	Alors je vois "Premier échange (modifié)"
	Alors je vois "Modifié"
	Alors l'élément contenant "Modifié" a dans son titre "Dernière modification de l'échange le "
	Alors l'élément contenant "Modifié" a dans son titre " par machin@test.fr"

Scénario: Ajout de demande et filtre
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Quand je clique sur "Créer"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je clique sur "Ajouter un élément de suivi"
	Alors je vois "Qualification de l'échange"
	Quand je renseigne "Premier échange" dans le champ "Titre de l'échange"
	Quand je clique sur "Accompagnement"
	Quand je clique sur "Enregistrer"
	Alors je vois "Premier échange"
	Alors je vois "Accompagnement"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Demandes"
	Quand je clique sur "#champ-selection-demandes-input"
	Quand je clique sur le texte "Accompagnement"
	Quand je clique sur "Appliquer les filtres"
	Alors je vois "Maurice"
	Alors je ne vois pas "Danielle"
	Quand je clique sur le texte "Accompagnement"
	Quand je clique sur "Appliquer les filtres"
	Alors je vois "Maurice"
	Alors je vois "Danielle"

Scénario: Clôture de demande
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Quand je clique sur "Créer"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je clique sur "Ajouter un élément de suivi"
	Alors je vois "Qualification de l'échange"
	Quand je renseigne "Premier échange" dans le champ "Titre de l'échange"
	Quand je clique sur "Accompagnement"
	Quand je clique sur "Enregistrer"
	Alors je vois "Premier échange"
	Alors je vois "Accompagnement"
	Quand je clique sur "Ajouter un élément de suivi"
	Quand je clique sur le texte "Clôturer"
	Alors je vois "Accompagnement"
	Quand je clique sur "Enregistrer"
	Quand je ne vois pas "Clôturer une demande"
	Alors je vois "Accompagnement"
	Alors je vois "clôturée"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Demandes"
	Quand je clique sur "#champ-selection-demandes-input"
	Quand je ne vois pas "Accompagnement"

Scénario: Ajout, modification, et clôture de rappel
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Quand je clique sur "Créer"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Quand je clique sur "Créer la fiche"
	Alors je vois "Dernière modification"
	Quand je clique sur "Ajouter un élément de suivi"
	Quand je clique sur "Ajouter un rappel"
	Quand je renseigne "Rappel important" dans le champ "Titre du rappel"
	Quand je clique sur "Enregistrer"
	Alors je ne vois pas "Ajouter un rappel"
	Alors je vois "Rappel important"
	Quand je vais sur la page Actions
	Alors je vois "Rappel important"
	Quand je clique sur "Danielle"
	Alors je suis sur la page du Créateur 1
	Quand je clique sur "Modifier le rappel"
	Alors je vois "Modifier un élément de suivi"
	Alors je vois "Modification d'un rappel"
	Quand je renseigne "Rappel très important !" dans le champ "Titre du rappel"
	Quand je clique sur "Enregistrer"
	Alors je ne vois pas "Modifier un élément de suivi"
	Alors je vois "Rappel très important !"
	Quand je clique sur "Clôturer le rappel"
	Alors je vois "Clôturer un rappel"
	Quand je clique sur "Annuler"
	Alors je ne vois pas "Clôturer un rappel"
	Quand je clique sur "Clôturer le rappel"
	Alors je vois "Clôturer un rappel"
	Quand je clique sur "Confirmer"
	Alors je ne vois pas "Clôturer un rappel"
	Alors je ne vois pas "Rappel très important !"
	Quand je clique sur "voir les rappels clôturés"
	Alors je vois "Rappel très important !"
	Quand je vais sur la page Actions
	Alors je ne vois pas "Rappel très important !"

Scénario: Sélection de masse
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Sélectionner les fiches de cette page"
	Alors je vois "2 fiches"
	Alors je vois "dans la sélection"
	Alors je vois "Vider la sélection"
	Quand je clique sur "Vider la sélection"
	Alors je ne vois pas "dans la sélection"

Scénario: Qualification de créateurs en masse - Activités réelles
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Sélectionner les fiches de cette page"
	Alors je vois "2 fiches"
	Alors je vois "dans la sélection"
	Quand je clique sur "Actions"
	Quand je clique sur "Qualifier la sélection"
	Alors je vois "Qualifier 2 fiches"
	Quand je renseigne "Test" dans le champ "#select-activites-input"
	Quand je clique sur le texte "Créer"
	Quand je clique sur "Confirmer"
	Alors je ne vois pas "Confirmer"
	Alors je vois "OK"
	Quand je clique sur "OK"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Champs qualifiés"
	Quand je clique sur "#champ-selection-activites-input"
	Quand je clique sur le texte "Test"
	Quand je clique sur "Appliquer les filtres"
	Alors je vois "Résultats de la recherche (2)"

Scénario: Qualification de créateurs en masse - Zones géographiques
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Sélectionner les fiches de cette page"
	Alors je vois "2 fiches"
	Alors je vois "dans la sélection"
	Quand je clique sur "Actions"
	Quand je clique sur "Qualifier la sélection"
	Alors je vois "Qualifier 2 fiches"
	Quand je renseigne "Test" dans le champ "#select-zones-input"
	Quand je clique sur le texte "Créer"
	Quand je clique sur "Confirmer"
	Alors je ne vois pas "Confirmer"
	Alors je vois "OK"
	Quand je clique sur "OK"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Champs qualifiés"
	Quand je clique sur "#champ-selection-zones-input"
	Quand je clique sur le texte "Test"
	Quand je clique sur "Appliquer les filtres"
	Alors je vois "Résultats de la recherche (2)"

Scénario: Qualification de créateurs en masse - Mots clés
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Danielle" dans le champ "Prénom"
	Quand je renseigne "Regner" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Actions"
	Quand je clique sur "Créer une fiche"
	Quand je renseigne "Maurice" dans le champ "Prénom"
	Quand je renseigne "Bidule" dans le champ "Nom"
	Alors je clique sur "Créer la fiche"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Sélectionner les fiches de cette page"
	Alors je vois "2 fiches"
	Alors je vois "dans la sélection"
	Quand je clique sur "Actions"
	Quand je clique sur "Qualifier la sélection"
	Alors je vois "Qualifier 2 fiches"
	Quand je renseigne "Test" dans le champ "#select-mots-input"
	Quand je clique sur le texte "Créer"
	Quand je clique sur "Confirmer"
	Alors je ne vois pas "Confirmer"
	Alors je vois "OK"
	Quand je clique sur "OK"
	Quand je vais sur la page Créateurs
	Quand je clique sur "Champs qualifiés"
	Quand je clique sur "#champ-selection-mots-input"
	Quand je clique sur le texte "Test"
	Quand je clique sur "Appliquer les filtres"
	Alors je vois "Résultats de la recherche (2)"
