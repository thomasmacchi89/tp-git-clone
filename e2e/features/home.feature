#language: fr

@home
Fonctionnalité: Page d'accueil
	Pour pouvoir me renseigner sur deveco
	En tant que visiteur
	Je veux pouvoir consulter la page d'accueil

Scénario: Home deveco
	Quand je vais sur la page d'accueil
	Alors je vois "Deveco"
	Alors je vois "Bienvenue sur la version beta de Deveco"
	Alors je vois "Gagnez en efficacité pour mieux accompagner le développement économique de votre territoire."
	Alors je vois "Me connecter"
	Quand je clique sur "Accéder à mon compte"
	Alors je suis sur la page "/connexion"
