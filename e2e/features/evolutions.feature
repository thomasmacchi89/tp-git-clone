#language: fr

@évolutions
Fonctionnalité: Information sur les évolutions de Deveco
	Pour pouvoir utiliser deveco
	En tant que DevEco
	Je veux pouvoir connaître les évolutions majeures du produit

Scénario: Affichage à la connexion et au rafraîchissement
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand le Deveco avec l'email "machin@test.fr" doit voir la fonctionnalité "fusion"
	Quand je vais sur le lien magique pour "blah"
	Alors je vois "ÉVOLUTIONS DANS DEVECO"
	Alors je vois "Retrouvez les Créateurs d’entreprise dans un onglet spécifique."
	Quand je clique sur "Suivant"
	Alors je vois "Retrouvez toutes les entreprises de votre territoire, que vous les connaissiez ou non"
	Quand je clique sur "Suivant"
	Alors je vois "établissements présents dans la base SIRENE de votre territoire de référence"
	Quand je clique sur "Suivant"
	Alors je vois "bulle bleue"
	Quand je clique sur "ai compris"
	Alors je ne vois pas "ÉVOLUTIONS DANS DEVECO"
	Quand je rafraîchis la page
	Alors je ne vois pas "ÉVOLUTIONS DANS DEVECO"
	Alors je vois "Territoire de référence"
	Quand le Deveco avec l'email "machin@test.fr" doit voir la fonctionnalité "fusion"
	Quand je rafraîchis la page
	Alors je vois "ÉVOLUTIONS DANS DEVECO"
