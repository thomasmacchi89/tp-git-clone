// for help on locators, see this: https://codecept.io/locators/#locator-builder
const { Alors, Quand, Soit } = require("./fr");

const { I } = inject();

Quand("je vais sur la page d'accueil", () => {
	I.amOnPage("/");
});

Quand("je vais sur la page {string}", (page) => {
	I.amOnPage(`${page}`);
});

const assertUrlMatches = (regex) => (url) => {
	let path = new URL(url).pathname;
	if (!new RegExp(`^${regex}$`).test(path)) {
		require("assert").fail(
			`Test failed: expected current url to match ${regex} but it is ${path}`
		);
	}
};

Alors("je suis sur la page d'accueil", async () => {
	I.waitUrlEquals("/", 2);
});

Alors("je suis sur la page {string}", async (page) => {
	I.waitUrlEquals(`${page}`, 2);
});

//
Quand(
	"je clique sur {string} sous le titre {string}",
	async (target, header) => {
		const item = locate("*")
			.after(locate("h2").withText(header))
			.find(`//*[text()[contains(.,'${target}')]]`);

		I.click(item);
	}
);

Quand("je clique sur la carte qui contient {string}", async (target) => {
	const card = locate(".fr-card").withText(target);

	I.click(card);
});

// TODO rewrite locators with Adrien
Alors(
	"je vois {string} sous le titre {string} de niveau {int}",
	async (target, header, level) => {
		const item = locate("*")
			.after(locate(`h${level}`).withText(header))
			.find(`//*[text()[contains(.,'${target}')]]`);

		I.see(item);
	}
);

Quand("j'attends {int} secondes", (num) => {
	I.wait(num);
});

Quand("je pause le test", () => {
	pause();
});

Quand("je renseigne {string} dans le champ {string}", (text, input) => {
	I.fillField(input, text);
});

Alors("je vois {string} dans le champ {string}", async (value, fieldLabel) => {
	const fieldId = await I.grabAttributeFrom(
		`//label[contains(., "${fieldLabel}")]`,
		"for"
	);
	I.seeInField(`#${fieldId}`, value);
});

Alors("je vois {string} {int} fois", async (value, amount) => {
	// I.seeNumberOfElements(value, amount);
	I.seeNumberOfVisibleElements(`//*[contains(text(), "${value}")]`, amount);
});

Quand("je clique sur {string}", (text) => {
	I.click(text);
});

Quand("je force le clic sur {string}", (text) => {
	I.forceClick(text);
});

Quand("je clique sur le texte {string}", async (text) => {
	const item = `//*[text()[contains(., "${text}")]]`;

	I.click(item);
});

Quand("je choisis {string}", (text) => {
	I.checkOption(text);
});

Quand("je ferme la modale", () => {
	I.click('button[title="fermer la modale"]');
});

Quand("j'attends que les suggestions apparaissent", () => {
	I.waitForElement("//ul[@role='listbox']", 3);
});

Quand("j'attends que les résultats de recherche apparaissent", () => {
	I.waitForElement("[aria-label^='Résultats de recherche']", 10);
});

Quand("j'attends que le titre de page {string} apparaisse", (title) => {
	I.scrollPageToTop();
	I.waitForElement(`//h1[contains(., "${title}")]`, 10);
});

Quand("j'attend que le texte {string} apparaisse", (text) => {
	I.waitForText(text, 5);
	I.scrollTo(`//*[text()[starts-with(., "${text}")]]`, 0, -100);
});

Quand("je scroll à {string}", (text) => {
	I.scrollTo(`//*[text()[starts-with(., "${text}")]]`, 0, -140);
});

Quand("je télécharge en cliquant sur {string}", (downloadText) => {
	I.handleDownloads();
	I.click(`//*[text()[starts-with(., "${downloadText}")]]`);
});

Quand(
	`je selectionne l'option {string} dans la liste {string}`,
	(option, select) => {
		I.selectOption(select, option);
	}
);

Quand("j'appuie sur Entrée", () => {
	I.pressKey("Enter");
});

Quand("j'appuie sur {string}", (key) => {
	I.pressKey(key);
});

//

Alors("je vois {string}", (text) => {
	I.see(text);
});

Alors("je ne vois pas {string}", (text) => {
	I.dontSee(text);
});

Alors("je vois le bouton {string}", (text) => {
	I.seeElement(`//button[text()="${text}"]`);
});

Alors("je vois le lien {string}", (text) => {
	I.seeElement(`//a[contains(., "${text}")]`);
});

Alors("je vois l'icône {string}", (iconClass) => {
	I.seeElement(`span.${iconClass}`);
});

Alors("je vois que bouton {string} est désactivé", (text) => {
	I.seeElement(`//button[text()="${text}" and @disabled]`);
});

Alors("le lien {string} pointe sur {string}", (text, url) => {
	I.seeElement(`//a[contains(., "${text}") and contains(@href, "${url}")]`);
});

Alors("je vois {string} fois le {string} {string}", (num, element, text) => {
	I.seeNumberOfVisibleElements(
		`//${element}[contains(., "${text}")]`,
		parseInt(num, 10)
	);
});

Alors("je vois {string} suggestions", (num) => {
	I.seeNumberOfVisibleElements("//ul[@role='listbox']//li", parseInt(num, 10));
});

Alors("je vois {string} résultats sous le texte {string}", (num, title) => {
	const target = `following-sibling::*//li//a`;
	const textMatcher = `text()[starts-with(., "${title}")]`;
	I.seeNumberOfVisibleElements(
		`//header[*[${textMatcher}]]/${target} | //div/*[${textMatcher}]/${target}`,
		parseInt(num, 10)
	);
});

Alors("je vois {int} tuiles sous le texte {string}", (num, title) => {
	const target = `following-sibling::*//div//a`;
	const textMatcher = `text()[starts-with(., "${title}")]`;
	I.seeNumberOfVisibleElements(
		`//header[*[${textMatcher}]]/${target} | //div/*[${textMatcher}]/${target}`,
		num
	);
});

Alors("je suis redirigé vers la page : {string}", (url) => {
	// also check search and hash
	I.waitForFunction(
		(url) =>
			window.location.pathname +
				window.location.search +
				window.location.hash ===
			url,
		[url],
		10
	);
});

Alors("j'ai téléchargé le fichier {string}", (filename) => {
	I.amInPath("output/downloads");
	I.seeFile(filename);
});

Quand("je téléverse le fichier {string}", (filename) => {
	I.attachFile(".dropzone input[type=file]", filename);
});

Quand("j'ai un compte {string}", (email) => {
	I.sendPostRequest("upsert/account/", { email });
});

Quand(
	"j'ai un compte {string} avec l'email {string} et la clef {string}",
	(accountType, email, accessKey) => {
		let entity = "none";
		if (/deveco/i.test(accountType)) {
			entity = "account";
		} else {
			entity = "superadmin";
		}
		I.sendPostRequest(`upsert/${entity}/`, { email, accessKey });
	}
);

Quand(
	"j'ai un compte {string} avec l'email {string} et le mot de passe {string}",
	(accountType, email, password) => {
		let entity = "none";
		if (/deveco/i.test(accountType)) {
			entity = "account";
		} else {
			entity = "superadmin";
		}
		I.sendPostRequest(`upsert/${entity}/?password=${password}`, { email });
	}
);

Quand(
	"le Deveco avec l'email {string} doit voir la fonctionnalité {string}",
	(email, newFeatures) => {
		I.sendPostRequest(`upsert/account/`, { email, newFeatures });
	}
);

Quand("je rafraîchis la page", () => I.refreshPage());

Quand("je vais sur le lien magique pour {string}", (accessKey) => {
	I.amOnPage(`/auth/jwt/${accessKey}`);
});

Alors("je suis sur l'url {string}", (url) => {
	I.waitUrlEquals(url);
});

Quand("je clique sur le bouton de déconnexion", () => {
	I.click("#logout");
});

Alors("l'élément nommé {string} est {string}", async (name, prop) => {
	let props = prop.split(", ");
	let selectors = {};
	switch (prop) {
		case "readonly":
			selectors = { ...selectors, readonly: true };
			break;
		case "en lecture seule":
			selectors = { ...selectors, readonly: true };
			break;
		case "éditable":
			selectors = { ...selectors, readonly: false };
			break;
		case "disabled":
			selectors = { ...selectors, disabled: true };
			break;
		case "désactivé":
			selectors = { ...selectors, disabled: true };
			break;
		case "enable":
			selectors = { ...selectors, disabled: false };
			break;
		case "activé":
			selectors = { ...selectors, disabled: false };
			break;
	}
	await I.seeElement(`[name="${name}"]`, selectors);
});

Before(async ({ title }) => {
	await I.sendGetRequest("resetAndSeed");
});

Alors(
	"l'élément contenant {string} a dans son titre {string}",
	async (txt, title) => {
		I.seeElement(locate(`[title*="${title}"]`).withText(txt));
	}
);

After(({ title }) => {});

// Définitions métier

// Créateurs
const createursPrefix = "createurs";

Quand("je clique sur l'onglet Créateurs", () => {
	I.click(`Créateurs d'entreprise`);
});

Quand("je vais sur la page Créateurs", () => {
	I.amOnPage(`/${createursPrefix}`);
});

Alors("je suis sur la page Créateurs", async () => {
	I.waitUrlEquals(`/${createursPrefix}`, 2);
});

Quand("je vais sur la page du Créateur {int}", async (id) => {
	I.amOnPage(`/${createursPrefix}/${id}`);
});

Alors("je suis sur la page du Créateur {int}", async (id) => {
	I.waitUrlEquals(`/${createursPrefix}/${id}`, 2);
});

Alors("je suis sur la page d'édition du Créateur {int}", async (id) => {
	I.waitUrlEquals(`/${createursPrefix}/${id}/modifier`, 2);
});

// Établissements
const etablissementsPrefix = "etablissements";

Quand("je clique sur l'onglet Établissements", () => {
	I.click(`Établissements`);
});

Quand("je vais sur la page Établissements", () => {
	I.amOnPage(`/${etablissementsPrefix}`);
});

Quand("je vais sur la page de l'Établissement {string}", async (siret) => {
	I.amOnPage(`/${etablissementsPrefix}/${siret}`);
});

Alors("je suis sur la page Établissements", async () => {
	I.waitUrlEquals(`/${etablissementsPrefix}`);
});

Alors("je suis sur la page de l'Établissement {string}", async (siret) => {
	I.waitUrlEquals(`/${etablissementsPrefix}/${siret}`);
});

Alors(
	"je suis sur la page d'édition de l'Établissement {string}",
	async (id) => {
		I.waitUrlEquals(`/${etablissementsPrefix}/${id}/modifier`);
	}
);

// Immobilier
const immobilierPrefix = "locaux";

Quand("je clique sur l'onglet Immobilier", () => {
	I.click(`Immobilier`);
});

Quand("je vais sur la page Immobilier", () => {
	I.amOnPage(`/${immobilierPrefix}`);
});

Alors("je suis sur la page Immobilier", async () => {
	I.waitUrlEquals(`/${immobilierPrefix}`);
});

Alors("je suis sur la page du Local {int}", async (id) => {
	I.waitUrlEquals(`/${immobilierPrefix}/${id}`);
});

Quand("je vais sur la page du Local {int}", async (id) => {
	I.amOnPage(`/${immobilierPrefix}/${id}`);
});

// Actions
Quand("je clique sur l'onglet Actions", () => {
	I.click(`Mes actions`);
});

Quand("je vais sur la page Actions", () => {
	I.amOnPage(`/actions`);
});

Alors("je suis sur la page Actions", async () => {
	I.waitUrlEquals("/actions");
});

// Accueil
Quand("je clique sur l'onglet Accueil", () => {
	I.click(`Accueil`);
});

Quand("je vais sur la page Accueil", () => {
	I.amOnPage(`/`);
});

Alors("je suis sur la page Accueil", async () => {
	I.waitUrlEquals("/");
});
