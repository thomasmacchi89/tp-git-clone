module Lib.Variables exposing (contactEmail, hintActivites, hintMotsCles, hintSuiviPortefeuille, hintSuiviTous, hintZoneGeographique)


contactEmail : String
contactEmail =
    "contact@deveco.incubateur.anct.gouv.fr"


hintActivites : String
hintActivites =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise d'une même activité ou filière dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres tags d'activité\n\nExemples\u{00A0}:\nNumérique, ESS, Commerces de proximité, Automobile, Médical, Service aux particuliers, Association, etc."


hintZoneGeographique : String
hintZoneGeographique =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise d'une même zone géographique dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres tags de zone géographique\n\nExemples\u{00A0}:\nCentre-ville, ZAC Les Peupliers, Rive droite, QPV, Ancien marché, etc."


hintMotsCles : String
hintMotsCles =
    "La qualification de ce champ vous permet de filtrer les établissements et créateurs d'entreprise ayant le même mot-clé dans les onglets Établissements et Créateurs d'entreprise\nVotre collectivité peut créer ses propres tags de mots-clés en fonction de ses spécificités\n\nExemples\u{00A0}:\nFête du pain, Jury de l'industrie, Redevable taxe Y, Charte signée, etc."


hintSuiviTous : String
hintSuiviTous =
    "Cherche parmi tous les établissements enregistrés dans Deveco\u{00A0}:\n- établissements de la base SIRENE du territoire de référence\n- établissements exogènes rajoutés manuellement"


hintSuiviPortefeuille : String
hintSuiviPortefeuille =
    "Cherche parmi les établissements dont la fiche a été enrichie par\u{00A0}:\n- une qualification (tags Activité réelle, Zone géographique, Mots-clés)\n- et/ou\n- un contact\n- un échange (lié ou non à une demande)"
