module Lib.UI exposing (capitalizeName, displayPhone, formatNumberWithThousandSpacing, infoLine, infoLineWrapper, plural, scrollElementTo, textWithDefault, withEmptyAs)

import Accessibility exposing (Html, div, span, text)
import Browser.Dom as Dom
import DSFR.Typography as Typo
import Html.Attributes
import Html.Attributes.Extra exposing (empty)
import List.Extra
import Task


textWithDefault : String -> String -> String
textWithDefault default content =
    case content of
        "" ->
            default

        _ ->
            content


displayPhone : String -> String
displayPhone phone =
    phone
        |> String.replace " " ""
        |> String.toList
        |> List.reverse
        |> List.Extra.greedyGroupsOf 2
        |> List.reverse
        |> List.map (List.reverse >> String.fromList)
        |> String.join "\u{2009}"


capitalizeName : String -> String
capitalizeName name =
    let
        separators =
            [ " ", "-", "'", "\u{00A0}" ]
    in
    separators
        |> List.foldl
            (\separator result ->
                result
                    |> String.split separator
                    |> List.map capitalizeWord
                    |> String.join separator
            )
            (String.toLower name)


capitalizeWord : String -> String
capitalizeWord word =
    case String.toList word of
        [] ->
            ""

        first :: rest ->
            Char.toLocaleUpper first
                :: rest
                |> String.fromList


withEmptyAs : String -> String -> String
withEmptyAs default txt =
    if txt == "" then
        default

    else
        txt


infoLine : Maybe Int -> String -> String -> Html msg
infoLine maxLine lab content =
    infoLineWrapper maxLine lab content (text content)


infoLineWrapper : Maybe Int -> String -> String -> Html msg -> Html msg
infoLineWrapper maxLine lab title content =
    let
        ( clamp, height ) =
            case maxLine of
                Just 1 ->
                    ( Html.Attributes.class "line-clamp-1", Html.Attributes.style "min-height" "1.5rem" )

                Just 2 ->
                    ( Html.Attributes.class "line-clamp-2", Html.Attributes.style "min-height" "3rem" )

                _ ->
                    ( empty, empty )
    in
    div [ clamp, height, Html.Attributes.title <| title ]
        [ span [] [ text lab ]
        , span
            [ Typo.textBold
            ]
            [ content ]
        ]


plural : Int -> String
plural t =
    case t of
        0 ->
            ""

        1 ->
            ""

        _ ->
            "s"


formatNumberWithThousandSpacing : Int -> String
formatNumberWithThousandSpacing num =
    num
        |> String.fromInt
        |> String.toList
        |> List.reverse
        |> List.Extra.greedyGroupsOf 3
        |> List.reverse
        |> List.map (List.reverse >> String.fromList)
        |> String.join "\u{00A0}"


scrollElementTo : (Result Dom.Error () -> msg) -> Maybe String -> ( Float, Float ) -> Cmd msg
scrollElementTo toMsg elementId ( x, y ) =
    case elementId of
        Nothing ->
            Task.attempt toMsg <| Dom.setViewport x y

        Just id ->
            Task.attempt toMsg <| Dom.setViewportOf id x y
