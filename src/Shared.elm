module Shared exposing
    ( Msg(..)
    , Redirect
    , Shared
    , User
    , goBack
    , identity
    , init
    , pageChangeEffects
    , subscriptions
    , update
    )

import Api
import Api.EntityId exposing (EntityId)
import Browser.Navigation as Nav
import Data.Entite exposing (EntiteId)
import Dict
import Effect
import Http
import Lib.UI
import RemoteData as RD
import Route
import Task
import Time exposing (Posix, Zone)
import TimeZone
import UI.Crisp exposing (toggleCrisp)
import UI.NewFeatures
import UI.Theme exposing (Theme(..), stringToTheme, switchTheme)
import User


type alias Shared =
    { key : Nav.Key
    , identity : Maybe User
    , headerMenuOpen : Bool
    , now : Posix
    , timezone : Zone
    , theme : Theme
    , entiteSelection : List (EntityId EntiteId)
    , etablissementSelection : List String
    , appUrl : String
    , newFeatures : UI.NewFeatures.Model
    }


type alias Flags =
    { now : Int
    , timezone : String
    , currentTheme : String
    , appUrl : String
    }


identity : Shared -> Maybe User
identity =
    .identity


type alias User =
    User.User


type alias Redirect =
    Maybe String


type Msg
    = LoggedIn User.User Redirect (Maybe UI.NewFeatures.Category)
    | LoggedOut Redirect
    | ConfirmedLoggedOut (RD.WebData ())
    | ToggleHeaderMenu
    | CloseHeaderMenu
    | Navigate Route.Route
    | ReplaceUrl Route.Route
    | GotTime Posix
    | SetTheme Theme
    | ScrollUp
    | GoBack
    | NoOp
    | BackUpEntitesSelection (List (EntityId EntiteId))
    | BackUpEtablissementsSelection (List String)
    | NewFeatures UI.NewFeatures.Msg


init : Flags -> Nav.Key -> ( Shared, Cmd Msg )
init { now, timezone, currentTheme, appUrl } key =
    ( { identity = Nothing
      , key = key
      , headerMenuOpen = False
      , now = Time.millisToPosix now
      , timezone = Dict.get timezone TimeZone.zones |> Maybe.withDefault TimeZone.europe__paris |> (\z -> z ())
      , theme = currentTheme |> stringToTheme |> Maybe.withDefault System
      , entiteSelection = []
      , etablissementSelection = []
      , appUrl = appUrl
      , newFeatures = UI.NewFeatures.init
      }
    , Cmd.none
    )


update : Msg -> Shared -> ( Shared, Cmd Msg )
update msg shared =
    case msg of
        GotTime now ->
            ( { shared | now = now }, Cmd.none )

        ToggleHeaderMenu ->
            ( { shared | headerMenuOpen = not shared.headerMenuOpen }, Cmd.none )

        CloseHeaderMenu ->
            ( { shared | headerMenuOpen = False }, Cmd.none )

        LoggedIn id redirect category ->
            ( { shared | identity = Just id, headerMenuOpen = False }
            , Cmd.batch
                [ redirect
                    |> Maybe.map (Nav.replaceUrl shared.key)
                    |> Maybe.withDefault Cmd.none
                , Task.perform Basics.identity <|
                    Task.succeed <|
                        NewFeatures <|
                            UI.NewFeatures.openCategory <|
                                category
                , toggleCrisp True
                ]
            )

        LoggedOut redirect ->
            ( shared
            , if shared.identity == Nothing then
                redirect
                    |> Maybe.map
                        (Nav.replaceUrl shared.key
                            << Route.toUrl
                            << Route.Connexion
                            << Just
                        )
                    |> Maybe.withDefault Cmd.none

              else
                Cmd.batch [ logout, toggleCrisp False ]
            )

        ConfirmedLoggedOut result ->
            case result of
                RD.Success () ->
                    ( { shared | identity = Nothing }
                    , Nav.replaceUrl shared.key <|
                        Route.toUrl <|
                            Route.Dashboard
                    )

                _ ->
                    ( shared, Cmd.none )

        Navigate route ->
            ( { shared | headerMenuOpen = False }
            , Nav.pushUrl shared.key <|
                Route.toUrl <|
                    route
            )

        ReplaceUrl route ->
            ( { shared | headerMenuOpen = False }
            , Nav.replaceUrl shared.key <|
                Route.toUrl <|
                    route
            )

        SetTheme theme ->
            ( { shared | theme = theme }, switchTheme <| UI.Theme.themeToString theme )

        ScrollUp ->
            ( shared
            , Lib.UI.scrollElementTo (\_ -> NoOp) Nothing ( 0, 0 )
            )

        GoBack ->
            ( shared
            , Nav.back shared.key 1
            )

        NoOp ->
            ( shared, Cmd.none )

        BackUpEntitesSelection entiteSelection ->
            ( { shared | entiteSelection = entiteSelection }, Cmd.none )

        BackUpEtablissementsSelection etablissementSelection ->
            ( { shared | etablissementSelection = etablissementSelection }, Cmd.none )

        NewFeatures subMsg ->
            let
                ( newFeatures, cmd ) =
                    UI.NewFeatures.update subMsg shared.newFeatures
            in
            ( { shared | newFeatures = newFeatures }, Cmd.map NewFeatures cmd )


logout : Cmd Msg
logout =
    Http.post
        { url = Api.logout
        , body = Http.emptyBody
        , expect = Http.expectWhatever (RD.fromResult >> ConfirmedLoggedOut)
        }


pageChangeEffects : ( model, Effect.Effect Msg msg ) -> ( model, Effect.Effect Msg msg )
pageChangeEffects =
    Effect.addBatch
        [ closeHeaderMenu
        , scrollUp
        ]


closeHeaderMenu : Effect.Effect Msg msg
closeHeaderMenu =
    Effect.fromShared CloseHeaderMenu


scrollUp : Effect.Effect Msg msg
scrollUp =
    Effect.fromShared <| ScrollUp


goBack : Msg
goBack =
    GoBack


subscriptions : Shared -> Sub Msg
subscriptions _ =
    Time.every 30000 GotTime
