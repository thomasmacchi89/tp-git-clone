module UI.Entite exposing (badgeExogene, badgeInactif, icon, iconeEtablissement)

import Accessibility exposing (Html, span, text)
import DSFR.Badge
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.User
import Data.Entite exposing (EntiteType(..))
import Data.Entreprise exposing (EtatAdministratif)
import Html.Attributes
import Html.Extra exposing (nothing)


createur : String
createur =
    "Créateur d'entreprise"


etablissement : String
etablissement =
    "Établissement"


toDisplay : EntiteType -> String
toDisplay ficheType =
    case ficheType of
        EntiteP _ ->
            createur

        EntiteE _ ->
            etablissement


iconeEtablissement : DSFR.Icons.IconName
iconeEtablissement =
    DSFR.Icons.Buildings.hotelLine


icon : EntiteType -> Html msg
icon ficheType =
    (case ficheType of
        EntiteP _ ->
            DSFR.Icons.User.userLine

        EntiteE _ ->
            iconeEtablissement
    )
        |> DSFR.Icons.iconLG
        |> List.singleton
        |> span [ Html.Attributes.title <| toDisplay ficheType ]


badgeInactif : Maybe EtatAdministratif -> Html msg
badgeInactif maybeEtatAdministratif =
    maybeEtatAdministratif
        |> Maybe.andThen
            (\etatAdministratif ->
                case etatAdministratif of
                    Data.Entreprise.Inactif ->
                        Just "Fermé"

                    _ ->
                        Nothing
            )
        |> Maybe.map (text >> DSFR.Badge.system { context = DSFR.Badge.Error, withIcon = False } >> DSFR.Badge.badgeMD)
        |> Maybe.withDefault nothing


badgeExogene : Bool -> Html msg
badgeExogene exogene =
    if exogene then
        text "Exogène"
            |> DSFR.Badge.system { context = DSFR.Badge.New, withIcon = False }
            |> DSFR.Badge.badgeMD

    else
        nothing
