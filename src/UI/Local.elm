module UI.Local exposing (cardPlaceholder, localTypeTags, localVacantBadge)

import Accessibility exposing (Html, div, span, text)
import DSFR.Badge
import DSFR.Color
import DSFR.Grid as Grid
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Local as Local
import Html.Attributes exposing (class, style)
import Html.Extra exposing (nothing)


localVacantBadge : Local.LocalStatut -> Html msg
localVacantBadge statut =
    (case statut of
        Local.Vacant ->
            Just "VACANT"

        _ ->
            Nothing
    )
        |> Maybe.map (text >> DSFR.Badge.default >> DSFR.Badge.withColor DSFR.Color.purpleGlycine >> DSFR.Badge.badgeMD)
        |> Maybe.withDefault nothing


localTypeTags : List Local.LocalType -> Html msg
localTypeTags types =
    types
        |> List.map
            (\type_ ->
                { data = type_, toString = Local.localTypeToDisplay }
                    |> DSFR.Tag.unclickable
            )
        |> DSFR.Tag.medium


cardPlaceholder : Html msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2" ]
            [ div
                [ class "fr-card--white flex flex-col h-[270px] p-4 custom-hover overflow-y-auto gap-2"
                , style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> List.repeat length
        |> String.join ""
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]
