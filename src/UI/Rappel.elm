module UI.Rappel exposing (Field(..), RappelInput, default, form, isValid, rappelInputToRappel, rappelToRappelInput, update)

import Accessibility exposing (Html, div, text)
import Api.EntityId exposing (EntityId)
import DSFR.Input
import Data.Rappel exposing (Rappel, RappelId)
import Date exposing (Date)
import Dict exposing (Dict)
import Html.Attributes exposing (class)


type Field
    = DateEchange
    | TitreEchange


type alias RappelInput =
    { id : EntityId RappelId
    , date : String
    , titre : String
    , dateCloture : Maybe String
    }


default : String -> Date -> RappelInput
default title today =
    { id = Api.EntityId.newId
    , date = today |> Date.toIsoString
    , titre = title
    , dateCloture = Nothing
    }


update : Field -> String -> RappelInput -> RappelInput
update field value echange =
    case field of
        DateEchange ->
            { echange | date = value }

        TitreEchange ->
            { echange | titre = value }


form : (Field -> String -> msg) -> RappelInput -> Html msg
form updateField echange =
    div [ class "flex flex-col gap-4" ]
        [ div [ class "fr-form-group w-[180px]" ]
            [ DSFR.Input.new
                { value = echange.date
                , onInput = updateField DateEchange
                , label = text "Date du rappel"
                , name = "nouvelle-fiche-particulier-date-rappel"
                }
                |> DSFR.Input.date
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = echange.titre
                , onInput = updateField TitreEchange
                , label = text "Titre du rappel *"
                , name = "nouvelle-fiche-particulier-titre-rappel"
                }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


isValid : RappelInput -> Bool
isValid =
    rappelInputToRappel >> Result.map (\_ -> True) >> Result.withDefault False


rappelInputToRappel : RappelInput -> Result (Dict String (List String)) Rappel
rappelInputToRappel input =
    let
        validateDate =
            ( .date
            , "date"
            , \date ->
                date |> Date.fromIsoString |> Result.mapError (\_ -> "Date invalide")
            )

        validateTitre =
            ( .titre
            , "titre"
            , \titre ->
                if "" /= String.trim titre then
                    Ok titre

                else
                    Err "Titre vide"
            )
    in
    Ok (\id date titre dateCloture -> Data.Rappel.createRappel <| { id = id, titre = titre, date = date, dateCloture = dateCloture })
        |> keepErrors (Ok <| .id <| input)
        |> keepErrors (validate validateDate input)
        |> keepErrors (validate validateTitre input)
        |> keepErrors (Ok <| Maybe.andThen (Date.fromIsoString >> Result.toMaybe) <| .dateCloture <| input)


rappelToRappelInput : Rappel -> RappelInput
rappelToRappelInput rappel =
    { id = Data.Rappel.id rappel
    , date = Data.Rappel.date rappel |> Date.toIsoString
    , titre = Data.Rappel.titre rappel
    , dateCloture = Data.Rappel.dateCloture rappel |> Maybe.map Date.toIsoString
    }


validate : ( input -> field, String, field -> Result error value ) -> input -> Result (Dict String (List error)) value
validate ( accessor, name, validator ) object =
    case validator <| accessor object of
        Ok v ->
            Ok v

        Err err ->
            Err <| Dict.insert name [ err ] Dict.empty


keepErrors : Result (Dict String (List error)) value -> Result (Dict String (List error)) (value -> nextStep) -> Result (Dict String (List error)) nextStep
keepErrors value function =
    case ( value, function ) of
        ( Ok v, Ok f ) ->
            Ok (f v)

        ( Err errs, Ok _ ) ->
            Err errs

        ( Ok _, Err errs ) ->
            Err errs

        ( Err errV, Err errF ) ->
            Err <| Dict.merge (\k v d -> Dict.insert k v d) (\k v1 v2 d -> Dict.insert k (v1 ++ v2) d) (\k v d -> Dict.insert k v d) errV errF Dict.empty
