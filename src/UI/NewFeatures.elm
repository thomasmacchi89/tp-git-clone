module UI.NewFeatures exposing (Category, Model, Msg, decodeCategory, init, openCategory, openFusion, update, viewModal)

import Accessibility exposing (Html, br, decorativeImg, div, h2, li, map, p, span, text, ul)
import DSFR.Button
import DSFR.CallOut
import DSFR.Icons
import DSFR.Icons.Buildings
import DSFR.Icons.System
import DSFR.Media
import DSFR.Modal
import DSFR.Typography as Typo
import Html.Attributes exposing (class, src)
import Html.Extra exposing (nothing)
import Json.Decode as Decode exposing (Decoder)
import Lib.UI


type Model
    = Model (Maybe SlidingList)


init : Model
init =
    Model Nothing


type Msg
    = NoOp
    | Open Category
    | Close
    | Next
    | Prev


type Category
    = Fusion


type alias SlidingList =
    ( List (Html Never)
    , Html Never
    , List (Html Never)
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model model) =
    Tuple.mapFirst Model <|
        case msg of
            NoOp ->
                ( model, Cmd.none )

            Open cat ->
                ( Just (initCat cat), Cmd.none )

            Close ->
                ( Nothing, Cmd.none )

            Next ->
                case model of
                    Nothing ->
                        ( Nothing, Cmd.none )

                    Just ( _, _, [] ) ->
                        ( Nothing, Cmd.none )

                    Just ( prev, cur, next :: rest ) ->
                        ( Just ( prev ++ [ cur ], next, rest )
                        , Lib.UI.scrollElementTo
                            (\_ -> NoOp)
                            (Just <| ("modal-" ++ modalId ++ "--body"))
                            ( 0, 0 )
                        )

            Prev ->
                case model of
                    Nothing ->
                        ( Nothing, Cmd.none )

                    Just ( prev, cur, next ) ->
                        case List.reverse prev of
                            [] ->
                                ( Nothing, Cmd.none )

                            p :: rest ->
                                ( Just ( List.reverse rest, p, cur :: next )
                                , Lib.UI.scrollElementTo
                                    (\_ -> NoOp)
                                    (Just <| ("modal-" ++ modalId ++ "--body"))
                                    ( 0, 0 )
                                )


initCat : Category -> SlidingList
initCat category =
    case category of
        Fusion ->
            ( [], initFusion, stepsFusion )


initFusion : Html msg
initFusion =
    fusionStep1


stepsFusion : List (Html msg)
stepsFusion =
    [ fusionStep2
    , fusionStep3
    , fusionStep4
    ]


fusionStep1 : Html msg
fusionStep1 =
    div [ class "flex flex-col" ]
        [ header "ÉVOLUTIONS DANS DEVECO"
        , subHeader 4 1 "Un onglet dédié pour les Créateurs d’entreprise"
        , DSFR.Media.img "Capture d'écran du menu de Deveco avec l'onglet Créateurs d'entreprise sélectionné" [ src "/assets/deveco-createurs.png", class "!max-w-[50%]" ] <|
            DSFR.Media.withResponsive False <|
                DSFR.Media.withCaption Nothing
        , p [] [ span [ Typo.textBold ] [ text "Retrouvez les Créateurs d’entreprise dans un onglet spécifique." ], text " ", text "Cette nouvelle répartition ne modifie pas les informations que vous avez entrées jusqu’à présent dans Deveco." ]
        , p [] [ text "Continuez à chercher les Créateurs que vous accompagnez par leur nom ou leur nom d’enseigne pressentie. Vous pouvez aussi filtrer les Créateurs en fonction de leur activité, de la zone d’implantation souhaitée ou des demandes en cours." ]
        , DSFR.Media.img "Capture d'écran de la page Créateurs d'entreprise" [ src "/assets/deveco-page-createurs.png", class "!max-w-[50%]" ] <|
            DSFR.Media.withResponsive False <|
                DSFR.Media.withCaption Nothing
        ]


fusionStep2 : Html msg
fusionStep2 =
    div [ class "flex flex-col" ]
        [ header "ÉVOLUTIONS DANS DEVECO"
        , subHeader 4 2 "Un seul onglet pour rassembler toutes vos entreprises"
        , DSFR.Media.img "Capture d'écran du menu de Deveco avec l'onglet Établissements sélectionné" [ src "/assets/deveco-etablissements.png", class "!max-w-[50%]" ] <|
            DSFR.Media.withResponsive False <|
                DSFR.Media.withCaption Nothing
        , p [ Typo.textBold ] [ text "Les onglets Mon territoire et Mon portefeuille fusionnent\u{00A0}!" ]
        , p [] [ text "Retrouvez toutes les entreprises de votre territoire, que vous les connaissiez ou non, les accompagniez ou non, au même endroit\u{00A0}!" ]
        , p [] [ text "Vous pouvez ainsi qualifier n’importe quels établissements pour les retrouver ensuite plus facilement (tags Activités réelles, Zone géographique, Mots-clés)." ]
        , p [] [ text "Cette nouvelle répartition ne modifie pas les informations que vous avez entrées jusqu’à présent dans Deveco." ]
        , DSFR.Media.img "Capture d'écran de la page Établissements" [ src "/assets/deveco-page-etablissements.png", class "!max-w-[50%]" ] <|
            DSFR.Media.withResponsive False <|
                DSFR.Media.withCaption Nothing
        , DSFR.CallOut.callout "" <|
            div []
                [ p []
                    [ span [ class "underline" ] [ text "Astuce" ]
                    , text " : "
                    , span [ Typo.textBold ] [ text "Ajoutez un établissement exogène" ]
                    , text " (qui se situe hors de votre territoire de référence) depuis l’onglet Établissement en cliquant sur le bouton Actions (en haut à droite) puis “+ Ajouter un Établissement exogène”."
                    ]
                , br []
                , p []
                    [ text "> Vous aurez besoin du numéro de SIRET de l’établissement pour l'ajouter."
                    ]
                ]
        ]


fusionStep3 : Html msg
fusionStep3 =
    div [ class "flex flex-col" ]
        [ header "ÉVOLUTIONS DANS DEVECO"
        , subHeader 4 3 "Continuez à distinguer les établissements du Portefeuille de votre service"
        , DSFR.Media.img "Capture d'écran du menu de Deveco avec l'onglet Établissements sélectionné" [ src "/assets/deveco-etablissements.png", class "!max-w-[50%]" ] <|
            DSFR.Media.withResponsive False <|
                DSFR.Media.withCaption Nothing
        , p [] [ text "Recherchez un établissement parmi :" ]
        , p []
            [ span [ Typo.textBold ] [ DSFR.Icons.icon DSFR.Icons.Buildings.hotelFill ]
            , text " "
            , span [ Typo.textBold, class "underline" ] [ text "Tous" ]
            , br []
            , span [ Typo.textBold ] [ text "Tous les établissements présents dans Deveco" ]
            , ul []
                [ li [] [ text "établissements présents dans la base SIRENE de votre territoire de référence" ]
                , li [] [ text "établissements exogènes (qui se trouvent hors de votre territoire de référence) - rajoutés manuellement" ]
                ]
            ]
        , p []
            [ span [ Typo.textBold ] [ DSFR.Icons.icon DSFR.Icons.System.eyeLine ]
            , text " "
            , span [ Typo.textBold, class "underline" ] [ text "Portefeuille" ]
            , br []
            , span [ Typo.textBold ] [ text "Les établissements du Portefeuille" ]
            , text " de l’ensemble de votre service deveco correspondent aux établissements connus du service, que vous suivez, avec lesquelles le service deveco a une relation."
            , br []
            , text "Un établissement est automatiquement mis dans le Portefeuille quand vous ajoutez l’une des informations suivantes\u{00A0}:"
            , ul []
                [ li [] [ text "qualification (tags Activité réelle, Zone géographique, Mots-clés)" ]
                , li [] [ text "contact (email, téléphone ou fonction)" ]
                , li [] [ text "compte-rendu d’un échange" ]
                , li [] [ text "rappel" ]
                ]
            ]
        , DSFR.Media.img "Capture d'écran du menu de filtres de la page Établissements" [ src "/assets/deveco-filtres-etablissements.png", class "!max-w-[25%]" ] <|
            DSFR.Media.withResponsive False <|
                DSFR.Media.withCaption Nothing
        ]


crispBubble : Html msg
crispBubble =
    decorativeImg [ src "/assets/deveco-crisp.png", class "!w-[1rem] inline" ]


fusionStep4 : Html msg
fusionStep4 =
    div [ class "flex flex-col" ]
        [ header "ÉVOLUTIONS DANS DEVECO"
        , subHeader 4 4 "Vous avez tout lu\u{00A0}? Tout compris\u{00A0}?"
        , p [ class "!mt-4" ] [ text "👍 Super\u{00A0}! Utilisez la bulle bleue ", crispBubble, text " en bas à droite de l'écran pour nous faire vos retours à chaud." ]
        , p [] [ text "👎 Pas de problème\u{00A0}: utilisez ", crispBubble, text " pour poser toutes vos questions lors de votre utilisation." ]
        , p [] [ crispBubble, text ", c'est nos chargés de déploiement en chair et en os qui répondent à toutes vos questions et vous accompagnent sur l'outil. N'hésitez pas à cliquer sur ", crispBubble, text " pour tchater en direct avec eux." ]
        , p [] [ decorativeImg [ src "/assets/deveco-vero.jpeg", class "!w-[4rem] inline" ], text " C'est pas un robot... c'est Véro\u{00A0}!" ]
        ]


header : String -> Html msg
header titleText =
    Accessibility.h1 [ Typo.fr_h4, class "!m-0" ]
        [ DSFR.Icons.iconLG DSFR.Icons.System.arrowRightLine
        , text " "
        , text titleText
        ]


subHeader : Int -> Int -> String -> Html msg
subHeader total current titleText =
    h2 [ Typo.fr_h5, class "blue-text !m-0" ]
        [ text titleText
        , text " "
        , text "("
        , text <| String.fromInt current
        , text "/"
        , text <| String.fromInt total
        , text ")"
        ]


viewModal : Model -> Html Msg
viewModal (Model maybeNewFeaturesModal) =
    case maybeNewFeaturesModal of
        Nothing ->
            nothing

        Just ( prev, cur, next ) ->
            DSFR.Modal.view
                { id = modalId
                , label = modalId
                , openMsg = NoOp
                , closeMsg = Just Close
                , title = nothing
                , opened = True
                }
                (div [ class "flex flex-col gap-4" ] [ map never cur, footer prev next ])
                Nothing
                |> Tuple.first


footer : List (Html Never) -> List (Html Never) -> Html Msg
footer prev next =
    let
        hasNext =
            List.length next > 0
    in
    DSFR.Button.group
        [ DSFR.Button.new
            { onClick = Just Next
            , label =
                if hasNext then
                    "Suivant"

                else
                    "J'ai compris\u{00A0}!"
            }
        , DSFR.Button.new { onClick = Just Prev, label = "Précédent" }
            |> DSFR.Button.secondary
            |> DSFR.Button.withDisabled (List.length prev == 0)
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


modalId : String
modalId =
    "new-features-modal"


openFusion : Msg
openFusion =
    openCategory <| Just Fusion


openCategory : Maybe Category -> Msg
openCategory cat =
    cat
        |> Maybe.map Open
        |> Maybe.withDefault NoOp


decodeCategory : Decoder (Maybe Category)
decodeCategory =
    Decode.oneOf
        [ Decode.string
            |> Decode.andThen
                (\s ->
                    case s of
                        "fusion" ->
                            Decode.succeed <| Just Fusion

                        _ ->
                            Decode.fail <| "Unknown category " ++ s
                )
        , Decode.succeed Nothing
        ]
