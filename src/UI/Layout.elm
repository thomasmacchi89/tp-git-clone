module UI.Layout exposing (MenuItem, layout, lazyBody, pageTitle)

import Accessibility exposing (Html, a, br, button, div, header, li, main_, map, nav, p, text, ul)
import Accessibility.Aria as Aria
import Accessibility.Landmark exposing (navigation)
import Accessibility.Role as Role
import DSFR.Badge
import DSFR.Color
import DSFR.Footer
import DSFR.Grid as Grid
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Typography as Typo
import Data.Role
import Html.Attributes as Attr exposing (class, style)
import Html.Attributes.Extra
import Html.Events as Events
import Html.Lazy
import Route exposing (Route)
import Shared exposing (Msg, Shared)
import Spa
import UI.NewFeatures
import User


lazyBody : (model -> Html msg) -> model -> List (Html msg)
lazyBody body model =
    Html.Lazy.lazy body model
        |> List.singleton


pageTitle : String -> String
pageTitle t =
    t ++ " - Deveco"


layout : Shared -> Route -> List (Html (Spa.Msg Msg msg)) -> List (Html (Spa.Msg Msg msg))
layout shared currentRoute children =
    [ head shared <|
        menuForUser currentRoute shared.identity
    , container children
    , map Spa.mapSharedMsg <| DSFR.Footer.simple shared
    , UI.NewFeatures.viewModal shared.newFeatures
        |> map (Shared.NewFeatures >> Spa.mapSharedMsg)
    ]


container : List (Html msg) -> Html msg
container =
    main_
        [ Grid.container
        , class "fr-p-2w"
        , style "min-height" "calc(100vh - 400px)"
        ]


menuForUser : Route -> Maybe Shared.User -> List (MenuItem msg)
menuForUser current maybeUser =
    let
        isCurrent route =
            route
                == current
                || (case ( current, route ) of
                        ( Route.Locaux _, Route.Locaux _ ) ->
                            True

                        ( Route.Local _, Route.Locaux _ ) ->
                            True

                        ( Route.LocalNew, Route.Locaux _ ) ->
                            True

                        ( Route.Etablissements _, Route.Etablissements _ ) ->
                            True

                        ( Route.Etablissement _, Route.Etablissements _ ) ->
                            True

                        ( Route.EtablissementNew, Route.Etablissements _ ) ->
                            True

                        ( Route.Createurs _, Route.Createurs _ ) ->
                            True

                        ( Route.Createur _, Route.Createurs _ ) ->
                            True

                        ( Route.CreateurNew, Route.Createurs _ ) ->
                            True

                        ( Route.CreateurEdit _, Route.Createurs _ ) ->
                            True

                        ( Route.AdminStats _, Route.AdminStats _ ) ->
                            True

                        ( Route.Stats _, Route.Stats _ ) ->
                            True

                        _ ->
                            False
                   )
    in
    case maybeUser of
        Nothing ->
            [ MenuItem (text "Accueil") (isCurrent <| Route.Dashboard) <| Route.toUrl <| Route.Dashboard
            , MenuItem (text "Statistiques") (isCurrent <| Route.Stats Nothing) <| Route.toUrl <| Route.Stats Nothing
            ]

        Just user ->
            case User.role user of
                Data.Role.Superadmin ->
                    [ MenuItem (text "Utilisateurs") (isCurrent <| Route.AdminUtilisateurs) <| Route.toUrl <| Route.AdminUtilisateurs
                    , MenuItem (text "Tags") (isCurrent <| Route.AdminTags) <| Route.toUrl <| Route.AdminTags
                    , MenuItem (text "Stats Admin") (isCurrent <| Route.AdminStats Nothing) <| Route.toUrl <| Route.AdminStats Nothing
                    ]

                Data.Role.Deveco _ ->
                    [ MenuItem (text "Accueil") (isCurrent <| Route.Dashboard) <| Route.toUrl <| Route.Dashboard
                    , MenuItem (text "Établissements") (isCurrent <| Route.Etablissements Nothing) <| Route.toUrl <| Route.Etablissements Nothing
                    , MenuItem (text "Créateurs d'entreprise") (isCurrent <| Route.Createurs Nothing) <| Route.toUrl <| Route.Createurs Nothing
                    , MenuItem (text "Immobilier") (isCurrent <| Route.Locaux Nothing) <| Route.toUrl <| Route.Locaux Nothing
                    , MenuItem (text "Mes actions") (isCurrent <| Route.Actions) <| Route.toUrl <| Route.Actions
                    ]


head : Shared -> List (MenuItem (Spa.Msg Msg msg)) -> Html (Spa.Msg Msg msg)
head shared menuItems =
    header
        [ Attr.attribute "role" "banner"
        , Attr.id "top"
        , class "fr-header"
        ]
        [ div
            [ class "fr-header__body"
            ]
            [ div
                [ Grid.container
                ]
                [ div
                    [ class "fr-header__body-row"
                    ]
                    [ div
                        [ class "fr-header__brand fr-enlarge-link"
                        ]
                        [ div
                            [ class "fr-header__brand-top"
                            ]
                            [ div
                                [ class "fr-header__logo"
                                ]
                                [ Typo.linkStandalone Typo.linkNoIcon
                                    (Route.toUrl <| Route.Dashboard)
                                    [ Attr.title "Accueil - Deveco"
                                    ]
                                    [ p
                                        [ class "fr-logo"
                                        ]
                                        [ text "Agence"
                                        , br []
                                        , text "Nationale"
                                        , br []
                                        , text "de la Cohésion"
                                        , br []
                                        , text "des Territoires"
                                        ]
                                    ]
                                ]
                            , div
                                [ class "fr-header__operator"
                                ]
                                []
                            , div
                                [ class "fr-header__navbar"
                                ]
                                [ button
                                    [ class "fr-btn--menu fr-btn"
                                    , Attr.attribute "aria-controls" modalMenuId
                                    , Attr.attribute "aria-haspopup" "menu"
                                    , Attr.title "Menu"
                                    , Attr.id modalMenuMobileTitle
                                    , Events.onClick <| Spa.mapSharedMsg <| Shared.ToggleHeaderMenu
                                    ]
                                    [ text "Menu" ]
                                ]
                            ]
                        , div
                            [ class "fr-header__service"
                            ]
                            [ Typo.linkStandalone Typo.linkNoIcon
                                (Route.toUrl <| Route.Dashboard)
                                [ Attr.title "Accueil - Deveco"
                                ]
                                [ p
                                    [ class "fr-header__service-title"
                                    ]
                                    [ text "Deveco"
                                    , text "BETA"
                                        |> DSFR.Badge.default
                                        |> DSFR.Badge.withColor DSFR.Color.GreenEmeraude
                                        |> DSFR.Badge.badgeMD
                                    ]
                                ]
                            , p
                                [ class "fr-header__service-tagline"
                                ]
                                [ text "Faciliter l'accès et la gestion des données entreprises pour les collectivités" ]
                            ]
                        ]
                    , div
                        [ class "fr-header__tools"
                        ]
                        [ div
                            [ class "fr-header__tools-links"
                            ]
                            [ ul [ class "fr-links-group" ] <|
                                case shared.identity of
                                    Nothing ->
                                        [ loginLink
                                        ]

                                    Just _ ->
                                        [ profileAccess
                                        , logoutLink
                                        ]
                            ]
                        ]
                    ]
                ]
            ]
        , mobileMenu shared menuItems
        , div
            [ class "fr-header__menu fr-modal"
            , Attr.id modalMenuId
            ]
            [ div
                [ Grid.container
                ]
                [ div
                    []
                    [ nav
                        [ class "fr-nav"
                        , Attr.id "header-navigation"
                        , navigation
                        , Aria.label "Menu principal"
                        ]
                        [ ul
                            [ class "fr-nav__list"
                            ]
                          <|
                            List.map viewMenu <|
                                menuItems
                        ]
                    ]
                ]
            ]
        ]


modalMenuId : String
modalMenuId =
    "modal-menu"


modalMenuMobileTitle : String
modalMenuMobileTitle =
    "fr-btn-menu-mobile"


mobileMenu : Shared -> List (MenuItem (Spa.Msg Msg msg)) -> Html (Spa.Msg Msg msg)
mobileMenu shared menuItems =
    div
        [ class "fr-header__menu fr-modal"
        , Attr.classList [ ( "fr-modal--opened", shared.headerMenuOpen ) ]
        , Attr.id modalMenuId
        , Attr.attribute "aria-labelledby" modalMenuMobileTitle
        ]
        [ div
            [ Grid.container
            ]
            [ button
                [ class "fr-link--close fr-link"
                , Attr.attribute "aria-controls" modalMenuId
                , Events.onClick <| Spa.mapSharedMsg <| Shared.ToggleHeaderMenu
                ]
                [ text "Fermer" ]
            , div
                [ class "fr-header__menu-links hidden"
                ]
                []
            , div
                [ class "fr-header__menu-links"
                ]
                [ ul
                    [ class "fr-links-group"
                    ]
                  <|
                    (\list ->
                        case shared.identity of
                            Just _ ->
                                logoutLink
                                    :: profileAccess
                                    :: list

                            Nothing ->
                                loginLink
                                    :: list
                    )
                    <|
                        List.map viewMenu <|
                            menuItems
                ]
            ]
        ]


profileAccess : Html msg
profileAccess =
    li
        []
        [ Typo.linkStandalone
            (Typo.linkLeftIcon DSFR.Icons.User.accountFill)
            (Route.toUrl <| Route.Profil)
            []
            [ text "Mes informations personnelles" ]
        ]


loginLink : Html (Spa.Msg Msg msg)
loginLink =
    li
        []
        [ Typo.linkStandalone
            (Typo.linkLeftIcon DSFR.Icons.User.userSettingLine)
            (Route.toUrl <| Route.Connexion Nothing)
            []
            [ text "Me connecter" ]
        ]


logoutLink : Html (Spa.Msg Msg msg)
logoutLink =
    li
        []
        [ Typo.linkStandalone
            (Typo.linkLeftIcon DSFR.Icons.System.logoutBoxRLine)
            (Route.toUrl <| Route.Deconnexion)
            [ Attr.id "logout" ]
            [ text "Me déconnecter" ]
        ]


type alias MenuItem msg =
    { label : Html msg, current : Bool, href : String }


viewMenu : MenuItem msg -> Html msg
viewMenu { label, current, href } =
    li
        [ class "fr-nav__item"
        ]
        [ a
            [ class "fr-nav__link"
            , Attr.href href
            , Attr.attribute "role" "tab"
            , Role.tab
            , Html.Attributes.Extra.attributeIf current <| class "blue-text"
            , Attr.rel ""
            , Html.Attributes.Extra.attributeIf current Aria.currentPage
            ]
            [ label ]
        ]
