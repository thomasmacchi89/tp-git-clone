module UI.Echange exposing (EchangeInput, Field(..), default, echangeInputToEchange, echangeToEchangeInput, form, isValid, update)

import Accessibility exposing (Html, div, text)
import Api.EntityId exposing (EntityId)
import DSFR.Checkbox
import DSFR.Input
import DSFR.Radio
import Data.Demande as Demande exposing (TypeDemande)
import Data.Echange as Echange exposing (Echange, EchangeId, TypeEchange)
import Date exposing (Date)
import Dict exposing (Dict)
import Html.Attributes exposing (class)


type Field
    = DateEchange
    | TitreEchange
    | CompteRendu


type alias EchangeInput =
    { id : EntityId EchangeId
    , typeEchange : TypeEchange
    , dateEchange : String
    , titreEchange : String
    , compteRendu : String
    , typesDemande : List TypeDemande
    }


default : String -> Date -> EchangeInput
default title today =
    { id = Api.EntityId.newId
    , typeEchange = Echange.Telephone
    , dateEchange = today |> Date.toIsoString
    , titreEchange = title
    , compteRendu = ""
    , typesDemande = []
    }


update : Field -> String -> EchangeInput -> EchangeInput
update field value echange =
    case field of
        DateEchange ->
            { echange | dateEchange = value }

        TitreEchange ->
            { echange | titreEchange = value }

        CompteRendu ->
            { echange | compteRendu = value }


form : Maybe String -> (TypeEchange -> msg) -> (TypeDemande -> Bool -> msg) -> (Field -> String -> msg) -> EchangeInput -> Html msg
form formErrors updateType updateDemande updateField echange =
    div [ class "flex flex-col gap-4" ]
        [ DSFR.Radio.group
            { id = "type-echange"
            , options = typeEchanges
            , current = Just echange.typeEchange
            , toLabel = echangeToDisplay >> text
            , toId = Echange.typeEchangeToString
            , msg = updateType
            , legend = text "Type d'échange"
            }
            |> DSFR.Radio.withError formErrors
            |> DSFR.Radio.inline
            |> DSFR.Radio.view
        , div [ class "fr-form-group sm:w-[180px]" ]
            [ DSFR.Input.new
                { value = echange.dateEchange
                , onInput = updateField DateEchange
                , label = text "Date de l'échange"
                , name = "nouvelle-fiche-particulier-date-echange"
                }
                |> DSFR.Input.date
                |> DSFR.Input.view
            ]
        , DSFR.Checkbox.group
            { id = "type-demande"
            , label = text "Qualification du besoin"
            , onChecked = updateDemande
            , values = Demande.liste
            , checked = echange.typesDemande
            , valueAsString = Demande.toString
            , toId = Demande.toString >> (++) "type-demande-option-"
            , toLabel = Demande.typeDemandeToDisplay
            }
            |> DSFR.Checkbox.inline
            |> DSFR.Checkbox.viewGroup
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = echange.titreEchange
                , onInput = updateField TitreEchange
                , label = text "Titre de l'échange"
                , name = "nouvelle-fiche-particulier-titre-echange"
                }
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = echange.compteRendu
                , onInput = updateField CompteRendu
                , label = text "Compte-rendu de l'échange"
                , name = "nouvelle-fiche-particulier-compte-rendu"
                }
                |> DSFR.Input.textArea (Just 8)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


isValid : EchangeInput -> Bool
isValid =
    echangeInputToEchange >> Result.map (\_ -> True) >> Result.withDefault False


echangeInputToEchange : EchangeInput -> Result (Dict String (List String)) Echange
echangeInputToEchange input =
    let
        validateDate =
            ( .dateEchange
            , "date"
            , \date ->
                date |> Date.fromIsoString |> Result.mapError (\_ -> "Date invalide")
            )
    in
    Ok (\id type_ date titre compteRendu themes -> Echange.createEchange <| { id = id, type_ = type_, titre = titre, compteRendu = compteRendu, date = date, themes = themes })
        |> keepErrors (Ok <| .id <| input)
        |> keepErrors (Ok <| .typeEchange <| input)
        |> keepErrors (validate validateDate input)
        |> keepErrors (Ok <| .titreEchange <| input)
        |> keepErrors (Ok <| .compteRendu <| input)
        |> keepErrors (Ok <| .typesDemande <| input)


echangeToEchangeInput : Echange -> EchangeInput
echangeToEchangeInput echange =
    { id = Echange.id echange
    , typeEchange = Echange.type_ echange
    , dateEchange = Echange.date echange |> Date.toIsoString
    , titreEchange = Echange.titre echange
    , compteRendu = Echange.compteRendu echange
    , typesDemande = Echange.themes echange
    }


validate : ( input -> field, String, field -> Result error value ) -> input -> Result (Dict String (List error)) value
validate ( accessor, name, validator ) object =
    case validator <| accessor object of
        Ok v ->
            Ok v

        Err err ->
            Err <| Dict.insert name [ err ] Dict.empty


keepErrors : Result (Dict String (List error)) value -> Result (Dict String (List error)) (value -> nextStep) -> Result (Dict String (List error)) nextStep
keepErrors value function =
    case ( value, function ) of
        ( Ok v, Ok f ) ->
            Ok (f v)

        ( Err errs, Ok _ ) ->
            Err errs

        ( Ok _, Err errs ) ->
            Err errs

        ( Err errV, Err errF ) ->
            Err <| Dict.merge (\k v d -> Dict.insert k v d) (\k v1 v2 d -> Dict.insert k (v1 ++ v2) d) (\k v d -> Dict.insert k v d) errV errF Dict.empty


typeEchanges : List TypeEchange
typeEchanges =
    [ Echange.Telephone
    , Echange.Email
    , Echange.Rencontre
    , Echange.Courrier
    ]


echangeToDisplay : TypeEchange -> String
echangeToDisplay echange =
    case echange of
        Echange.Telephone ->
            "Téléphone"

        Echange.Email ->
            "Email"

        Echange.Rencontre ->
            "Rencontre"

        Echange.Courrier ->
            "Courrier"
