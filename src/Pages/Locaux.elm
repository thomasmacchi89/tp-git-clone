module Pages.Locaux exposing (Model, Msg, page)

import Accessibility exposing (a, decorativeImg, div, formWithListeners, h1, h2, label, p, span, text)
import Api
import Browser.Dom
import DSFR.Button
import DSFR.Grid as Grid
import DSFR.Icons.System
import DSFR.Pagination
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Local as Local exposing (Local)
import Effect
import Html exposing (Html, hr)
import Html.Attributes as Attr exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Html.Lazy
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap)
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import QS
import RemoteData as RD
import Route
import Shared
import Spa.Page exposing (Page)
import Task
import UI.Layout
import UI.Local
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.onNewFlags (parseRawQuery >> DoSearch) <|
        Spa.Page.element <|
            { init = init
            , update = update
            , view = view
            , subscriptions = \_ -> Sub.none
            }


type alias Model =
    { locaux : RD.WebData (List Local)
    , filters : Filters
    , pagination : Pagination
    , firstFetch : Bool
    }


type alias Pagination =
    { page : Int
    , pages : Int
    , results : Int
    }


type alias Filters =
    { recherche : String
    , localStatut : Maybe Local.LocalStatut
    , localTypes : List Local.LocalType
    , localisations : List String
    }


defaultFilters : Filters
defaultFilters =
    { recherche = ""
    , localStatut = Nothing
    , localTypes = []
    , localisations = []
    }


defaultPagination : Pagination
defaultPagination =
    { page = 1, pages = 1, results = 0 }


type Msg
    = NoOp
    | FetchLocaux
    | UpdatedSearch String
    | ReceivedLocaux (RD.WebData ( List Local, Pagination ))
    | ClearAllFilters
    | ToggleLocalStatutFilter (Maybe Local.LocalStatut)
    | ToggleLocalTypeFilter Bool Local.LocalType
    | DoSearch ( Filters, Pagination )


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init rawQuery =
    let
        ( filters, pagination ) =
            parseRawQuery rawQuery
    in
    ( { locaux = RD.Loading
      , filters = filters
      , pagination = pagination
      , firstFetch = True
      }
    , Effect.fromCmd <| getLocaux ( filters, pagination )
    )
        |> Shared.pageChangeEffects


getLocaux : ( Filters, Pagination ) -> Cmd Msg
getLocaux ( filters, pagination ) =
    Http.get
        { url = Api.getLocaux <| serializeModel ( filters, pagination )
        , expect = Http.expectJson (RD.fromResult >> ReceivedLocaux) decodePayload
        }


decodePayload : Decode.Decoder ( List Local, Pagination )
decodePayload =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "results" Decode.int)
        |> andMap (Decode.field "data" decodeLocaux)


decodeLocaux : Decode.Decoder (List Local)
decodeLocaux =
    Decode.list Local.decodeLocal


rechercherLocalInputName : String
rechercherLocalInputName =
    "rechercher-local"


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        ReceivedLocaux response ->
            case response of
                RD.Success ( locaux, pagination ) ->
                    ( { model | locaux = RD.Success locaux, pagination = pagination }
                    , Task.attempt (\_ -> NoOp) (Browser.Dom.focus rechercherLocalInputName) |> Effect.fromCmd
                    )

                _ ->
                    ( { model | locaux = response |> RD.map Tuple.first }
                    , Task.attempt (\_ -> NoOp) (Browser.Dom.focus rechercherLocalInputName) |> Effect.fromCmd
                    )

        FetchLocaux ->
            ( { model
                | locaux = RD.Loading
                , firstFetch = False
              }
            , updateUrl model
            )

        DoSearch ( filters, pagination ) ->
            ( { model
                | pagination = pagination
                , filters = filters
                , locaux = RD.Loading
                , firstFetch = False
              }
            , Effect.fromCmd (getLocaux ( filters, pagination ))
            )

        UpdatedSearch recherche ->
            ( { model
                | filters =
                    model.filters
                        |> (\f -> { f | recherche = recherche })
              }
            , Effect.none
            )

        ClearAllFilters ->
            let
                newModel =
                    { model | filters = defaultFilters }
            in
            ( newModel
            , updateUrl newModel
            )

        ToggleLocalTypeFilter on localType ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | localTypes =
                                                if on then
                                                    if List.member localType filters.localTypes then
                                                        filters.localTypes

                                                    else
                                                        filters.localTypes |> List.reverse |> (::) localType |> List.reverse

                                                else
                                                    List.filter ((/=) localType) filters.localTypes
                                        }
                                   )
                    }
            in
            ( newModel
            , updateUrl newModel
            )

        ToggleLocalStatutFilter localStatutFilter ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters -> { filters | localStatut = localStatutFilter })
                    }
            in
            ( newModel
            , updateUrl newModel
            )


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.fromShared <|
        Shared.Navigate <|
            Route.Locaux <|
                Just <|
                    serializeModel ( model.filters, model.pagination )


serializeModel : ( Filters, Pagination ) -> String
serializeModel ( filters, pagination ) =
    QS.merge (paginationToQuery pagination) (filtersToQuery filters)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Locaux <|
            Just <|
                serializeModel ( filters, { pagination | page = newPage } )


parseRawQuery : Maybe String -> ( Filters, Pagination )
parseRawQuery rawQuery =
    case rawQuery of
        Nothing ->
            ( defaultFilters, defaultPagination )

        Just query ->
            ( parseFilters query, parsePagination query )


paginationToQuery : Pagination -> QS.Query
paginationToQuery pagination =
    (QS.setStr queryKeys.page <| String.fromInt <| pagination.page) <|
        (QS.setStr queryKeys.pages <| String.fromInt <| pagination.pages) <|
            QS.empty


queryKeys : { types : String, statut : String, recherche : String, page : String, pages : String }
queryKeys =
    { types = "types"
    , statut = "statut"
    , recherche = "recherche"
    , page = "page"
    , pages = "pages"
    }


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setType =
            case filters.localTypes of
                [] ->
                    identity

                types ->
                    case types of
                        [] ->
                            identity

                        localTypes ->
                            localTypes
                                |> List.map Local.localTypeToString
                                |> QS.setListStr queryKeys.types

        setStatut =
            case filters.localStatut of
                Nothing ->
                    identity

                Just type_ ->
                    type_
                        |> Local.localStatutToString
                        |> QS.setStr queryKeys.statut

        setRecherche =
            case filters.recherche of
                "" ->
                    identity

                recherche ->
                    recherche
                        |> QS.setStr queryKeys.recherche
    in
    QS.empty
        |> setType
        |> setStatut
        |> setRecherche


parseFilters : String -> Filters
parseFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    localType =
                        query
                            |> QS.getAsStringList queryKeys.types
                            |> List.map Local.stringToLocalType
                            |> List.filterMap identity

                    localStatut =
                        query
                            |> QS.getAsStringList queryKeys.statut
                            |> List.head
                            |> Maybe.andThen Local.stringToLocalStatut
                in
                { defaultFilters
                    | recherche = recherche
                    , localTypes = localType
                    , localStatut = localStatut
                }
           )


parsePagination : String -> Pagination
parsePagination rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    p =
                        query
                            |> QS.getAsStringList queryKeys.page
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1

                    pages =
                        query
                            |> QS.getAsStringList queryKeys.pages
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1
                in
                { defaultPagination
                    | page = p
                    , pages = pages
                }
           )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Mes locaux"
    , body = UI.Layout.lazyBody body model
    , route =
        Route.Locaux <|
            Just <|
                serializeModel ( model.filters, model.pagination )
    }


body : Model -> Html Msg
body model =
    case ( model.firstFetch, model.locaux ) of
        ( True, RD.Loading ) ->
            div [ class "p-2" ] <|
                List.singleton <|
                    div [ Grid.gridRow ] <|
                        [ div [ class "fr-card--white text-center p-20", Grid.col ]
                            [ text "Chargement en cours" ]
                        ]

        ( True, RD.Success [] ) ->
            div [ class "p-2" ] <|
                List.singleton <|
                    div [ class "fr-card--white text-center p-20" ]
                        [ div [ Grid.gridRow ]
                            [ div [ Grid.col3, class "flex justify-center" ]
                                [ decorativeImg [ Attr.src "/assets/deveco-carte-identite.svg" ] ]
                            , div [ Grid.col9, class "flex flex-col fr-card--white text-center justify-center" ]
                                [ p []
                                    [ text "Créez votre première fiche "
                                    , span [ Typo.textBold, class "blue-text" ] [ text "local" ]
                                    ]
                                , div [ class "flex justify-center" ]
                                    [ DSFR.Button.new { onClick = Nothing, label = "Créer un local" }
                                        |> DSFR.Button.linkButton (Route.toUrl <| Route.LocalNew)
                                        |> DSFR.Button.view
                                    ]
                                ]
                            ]
                        ]

        _ ->
            div []
                [ div [ Grid.gridRow, Grid.gridRowGutters, Grid.gridRowMiddle ] <|
                    [ h1 [ class "fr-h6 !my-0", Grid.colOffsetSm4, Grid.colSm4, Grid.col12 ]
                        [ text "Locaux du portefeuille"
                        , model.pagination
                            |> (.results >> String.fromInt >> (\t -> " (" ++ t ++ ")") >> text)
                        ]
                    , div [ Grid.colSm4, Grid.col12, class "text-right" ]
                        [ [ DSFR.Button.new { onClick = Nothing, label = "Créer un local" }
                                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                                |> DSFR.Button.linkButton (Route.toUrl Route.LocalNew)
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.iconsLeft
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.inline
                            |> DSFR.Button.viewGroup
                        ]
                    ]
                , div [ Grid.gridRow ] <|
                    [ div [ Grid.col12, Grid.colSm4 ]
                        [ Html.Lazy.lazy filterPanel model.filters ]
                    , div [ Grid.col12, Grid.colSm8, class "p-2" ]
                        [ div [ Grid.gridRow ] <|
                            List.singleton <|
                                viewLocalList model.pagination model.filters model.locaux
                        ]
                    ]
                ]


filterPanel : Filters -> Html Msg
filterPanel filters =
    div [ class "p-2" ]
        [ div [ class "p-4 fr-card--white" ]
            [ formWithListeners [ Events.onSubmit FetchLocaux ]
                [ DSFR.SearchBar.searchBar
                    { submitMsg = FetchLocaux
                    , buttonLabel = "Rechercher"
                    , inputMsg = UpdatedSearch
                    , inputLabel = "Rechercher un local"
                    , inputPlaceholder = Nothing
                    , inputId = rechercherLocalInputName
                    , inputValue = filters.recherche
                    , hints =
                        "Recherche par nom"
                            |> text
                            |> List.singleton
                    , fullLabel =
                        Just <|
                            label
                                [ class "fr-label"
                                , Typo.textBold
                                , Typo.textLg
                                , class "!mb-0"
                                , Attr.for rechercherLocalInputName
                                ]
                                [ text "Rechercher dans mes locaux" ]
                    }
                ]
            , text "\u{00A0}"
            , hr [ class "fr-hr" ] []
            , viewFilters filters
            ]
        ]


filterStatutToString : Maybe Local.LocalStatut -> String
filterStatutToString =
    Maybe.map Local.localStatutToString
        >> Maybe.withDefault "Tous"


filterStatutToDisplay : Maybe Local.LocalStatut -> String
filterStatutToDisplay =
    Maybe.map Local.localStatutToDisplay
        >> Maybe.withDefault "Tous"


viewFilters : Filters -> Html Msg
viewFilters ({ localStatut, localTypes } as filters) =
    div [ class "flex flex-col" ]
        [ div [ class "flex flex-row items-center justify-between mb-4" ]
            [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtrer" ]
            , DSFR.Button.new { label = "Tout effacer", onClick = Just ClearAllFilters }
                |> DSFR.Button.withDisabled ({ filters | recherche = "" } == { defaultFilters | recherche = "" })
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.view
            ]
        , DSFR.Radio.group
            { id = "filtres-local-statut"
            , options = [ Nothing, Just Local.Occupe, Just Local.Vacant ]
            , current = Just localStatut
            , toLabel = filterStatutToDisplay >> text
            , toId = filterStatutToString
            , msg = ToggleLocalStatutFilter
            , legend = div [ Typo.textBold ] [ text "Statut" ]
            }
            |> DSFR.Radio.inline
            |> DSFR.Radio.view
        , div [ class "flex flex-col gap-4" ]
            [ label [ Typo.textBold ] [ text "Type de local" ]
            , [ Local.Commerce
              , Local.Bureaux
              , Local.AteliersArtisanaux
              , Local.BatimentsIndustriels
              , Local.Entrepot
              ]
                |> List.map (viewTag localTypes)
                |> DSFR.Tag.medium
            ]
        ]


viewTag : List Local.LocalType -> Local.LocalType -> DSFR.Tag.TagConfig Msg Local.LocalType
viewTag selected localType =
    DSFR.Tag.selectable (ToggleLocalTypeFilter <| not <| List.member localType selected)
        (List.member localType selected)
        { data = localType
        , toString = Local.localTypeToDisplay
        }


viewLocalList : Pagination -> Filters -> RD.WebData (List Local) -> Html Msg
viewLocalList pagination filters locaux =
    case locaux of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ Grid.col12 ]
                [ div [ class "flex flex-row justify-between w-full pt-2 h-[56px]" ]
                    []
                , div [ Grid.gridRow ] <|
                    List.repeat 3 <|
                        UI.Local.cardPlaceholder
                ]

        RD.Failure _ ->
            div [ class "text-center", Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucun local ne correspond à cette recherche. Essayez "
                    , DSFR.Button.new
                        { label = "sans filtre"
                        , onClick = Just <| ClearAllFilters
                        }
                        |> DSFR.Button.withAttrs [ class "!p-0" ]
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    , text "."
                    ]
                ]

        RD.Success payload ->
            div [ Grid.col12 ]
                [ if pagination.pages > 1 then
                    div [ class "flex flex-row justify-between w-full pt-2" ]
                        [ div [ Grid.col, class "items-center flex flex-row" ] <|
                            [ DSFR.Pagination.view pagination.page pagination.pages <|
                                toHref ( filters, pagination )
                            ]
                        ]

                  else
                    nothing
                , div [ Grid.gridRow ] <|
                    List.map localCard <|
                        List.sortWith (Lib.Date.sortOlderPosixLast .dateModification) <|
                            payload
                , if pagination.pages > 1 then
                    div [ class "!mt-4" ]
                        [ DSFR.Pagination.view pagination.page pagination.pages <|
                            toHref ( filters, pagination )
                        ]

                  else
                    nothing
                ]


localCard : Local -> Html Msg
localCard local =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2" ]
            [ a
                [ class "fr-card--white flex flex-col h-[240px] gap-4 p-4 custom-hover overflow-y-auto"
                , Attr.style "background-image" "none"
                , Attr.href (Route.toUrl <| Route.Local <| .id <| local)
                ]
                [ div [ class "min-h-[2rem]" ]
                    [ div [ class "flex flex-row justify-between" ]
                        [ UI.Local.localTypeTags <| .types <| local
                        , UI.Local.localVacantBadge <| .statut <| local
                        ]
                    , div [ class "font-bold" ] <| List.singleton <| text <| .titre <| local
                    , div [] <|
                        List.singleton <|
                            text <|
                                withEmptyAs "-" <|
                                    .adresse <|
                                        local
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ] <|
                    [ Lib.UI.infoLine Nothing "Zone géographique\u{00A0}: " <|
                        withEmptyAs "-" <|
                            String.join "\u{00A0}— " <|
                                .localisations <|
                                    local
                    ]
                , div [ Typo.textSm, class "!mb-0 min-h-[2rem]" ] <|
                    [ Lib.UI.infoLine Nothing
                        ("Élément"
                            ++ (if List.length [] > 1 then
                                    "s"

                                else
                                    ""
                               )
                            ++ " de suivi\u{00A0}: "
                        )
                        "-"
                    ]
                ]
            ]
        ]
