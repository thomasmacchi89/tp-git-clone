module Pages.Etablissement.Siret_ exposing (page)

import Accessibility exposing (Html, div, formWithListeners, h1, h2, h3, h4, hr, label, span, sup, text)
import Api
import Api.EntityId exposing (entityIdToString)
import Browser.Dom as Dom
import DSFR.Accordion
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Design
import DSFR.Icons.Device
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Input
import DSFR.Modal
import DSFR.Radio
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Cloture exposing (Cloture)
import Data.Demande exposing (Demande)
import Data.Echange exposing (Echange)
import Data.Entite exposing (Contact, Entite)
import Data.Entreprise exposing (Entreprise)
import Data.Fiche exposing (Fiche)
import Data.Rappel exposing (Rappel)
import Date exposing (Date)
import Dict
import Effect
import Html
import Html.Attributes exposing (class, classList, id)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Html.Keyed as Keyed
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (capitalizeName, displayPhone, withEmptyAs)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import List.Extra exposing (isPermutationOf)
import MultiSelectRemote exposing (SelectConfig)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Task
import Time exposing (Posix, Zone)
import UI.Cloture
import UI.Echange
import UI.Entite
import UI.Layout
import UI.Rappel
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page String Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element
        { view = view shared
        , init = init shared
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { siret : String
    , entreprise : WebData EntrepriseAugmented
    , entrepriseInfo : Maybe EntrepriseInfo
    , saveEnterpriseInfoRequest : WebData EntrepriseAugmented
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , contactAction : ContactAction
    , saveContactRequest : WebData EntrepriseAugmented
    , suiviAction : SuiviAction
    , saveSuiviRequest : WebData EntrepriseAugmented
    , rappelMode : RappelMode
    , today : Date
    }


type alias EntrepriseAugmented =
    { entreprise : Entreprise
    , entite : Maybe Entite
    , fiche : Maybe Fiche
    }


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


type Msg
    = ReceivedEntreprise (WebData EntrepriseAugmented)
    | EditEntrepriseInfo
    | CancelEditEntrepriseInfo
    | RequestedSaveEntrepriseInfo
    | ReceivedSaveEntrepriseInfo (WebData EntrepriseAugmented)
    | UpdatedAutre String
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | SetContactAction ContactAction
    | ConfirmAction
    | CancelAction
    | UpdatedContact ContactField String
    | ReceivedSaveContact (WebData EntrepriseAugmented)
    | SetSuiviAction SuiviAction
    | UpdatedEchangeForm UI.Echange.Field String
    | SelectedTypeEchange Data.Echange.TypeEchange
    | ToggledTypeDemande Data.Demande.TypeDemande Bool
    | UpdatedRappelForm UI.Rappel.Field String
    | UpdatedClotureDemande Demande
    | UpdatedClotureForm String
    | ConfirmSuiviAction
    | CancelSuiviAction
    | ReceivedSaveSuivi (WebData EntrepriseAugmented)
    | SetRappelMode RappelMode
    | NoOp
    | SharedMsg Shared.Msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model |> .selectActivites |> MultiSelectRemote.subscriptions
        , model |> .selectLocalisations |> MultiSelectRemote.subscriptions
        , model |> .selectMots |> MultiSelectRemote.subscriptions
        ]


type SuiviAction
    = NoSuivi
    | NewSuivi SuiviType
    | EditSuivi SuiviType
    | DeleteSuivi SuiviType


type SuiviType
    = SuiviEchange UI.Echange.EchangeInput
    | SuiviRappel UI.Rappel.RappelInput
    | SuiviCloture UI.Cloture.ClotureInput


type ContactAction
    = None
    | New Contact
    | Delete Contact
    | Edit Contact


type alias Activite =
    String


type alias Localisation =
    String


type alias MotCle =
    String


type alias EntrepriseInfo =
    { activites : List Activite
    , localisations : List Localisation
    , autre : String
    , mots : List MotCle
    }


emptyContact : Contact
emptyContact =
    { id = Api.EntityId.newId
    , fonction = ""
    , prenom = ""
    , nom = ""
    , email = ""
    , telephone = ""
    }


type ContactField
    = Fonction
    | Prenom
    | Nom
    | Email
    | Telephone


type RappelMode
    = RappelsActifs
    | RappelsClotures


type alias Qualifications =
    { activites : List String
    , localisations : List String
    , mots : List String
    , autre : String
    }


init : Shared.Shared -> String -> ( Model, Effect.Effect Shared.Msg Msg )
init shared siret =
    { siret = siret
    , entreprise = RD.Loading
    , entrepriseInfo = Nothing
    , saveEnterpriseInfoRequest = RD.NotAsked
    , selectActivites =
        MultiSelectRemote.init selectActivitesId
            { selectionMsg = SelectedActivites
            , internalMsg = UpdatedSelectActivites
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , selectLocalisations =
        MultiSelectRemote.init selectLocalisationsId
            { selectionMsg = SelectedLocalisations
            , internalMsg = UpdatedSelectLocalisations
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , selectMots =
        MultiSelectRemote.init selectMotsId
            { selectionMsg = SelectedMots
            , internalMsg = UpdatedSelectMots
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , contactAction = None
    , saveContactRequest = RD.NotAsked
    , suiviAction = NoSuivi
    , saveSuiviRequest = RD.NotAsked
    , rappelMode = RappelsActifs
    , today = Date.fromPosix shared.timezone shared.now
    }
        |> Effect.withCmd (fetchEntreprise siret)
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ReceivedEntreprise resp ->
            { model | entreprise = resp }
                |> Effect.withNone

        NoOp ->
            ( model, Effect.none )

        EditEntrepriseInfo ->
            case model.entreprise of
                RD.Success { entite } ->
                    ( { model
                        | entrepriseInfo =
                            Just
                                { activites = Maybe.withDefault [] <| Maybe.map Data.Entite.activitesReelles entite
                                , localisations = Maybe.withDefault [] <| Maybe.map Data.Entite.localisations entite
                                , autre = Maybe.withDefault "" <| Maybe.map Data.Entite.activiteAutre entite
                                , mots = Maybe.withDefault [] <| Maybe.map Data.Entite.motsCles entite
                                }
                        , saveEnterpriseInfoRequest = RD.NotAsked
                      }
                    , Effect.none
                    )

                _ ->
                    ( model, Effect.none )

        CancelEditEntrepriseInfo ->
            ( { model | entrepriseInfo = Nothing }, Effect.none )

        RequestedSaveEntrepriseInfo ->
            case model.entreprise of
                RD.Success _ ->
                    case model.entrepriseInfo of
                        Just entrepriseData ->
                            ( { model | saveEnterpriseInfoRequest = RD.Loading }, updateFiche model.siret entrepriseData )

                        _ ->
                            ( model, Effect.none )

                _ ->
                    ( model, Effect.none )

        ReceivedSaveEntrepriseInfo response ->
            case response of
                RD.Success _ ->
                    ( { model | entreprise = response, saveEnterpriseInfoRequest = response, entrepriseInfo = Nothing }, Effect.none )

                _ ->
                    ( { model | saveEnterpriseInfoRequest = response }, Effect.none )

        UpdatedAutre autre ->
            ( { model
                | entrepriseInfo = model.entrepriseInfo |> Maybe.map (\ei -> { ei | autre = autre })
                , saveEnterpriseInfoRequest = RD.NotAsked
              }
            , Effect.none
            )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites
            in
            ( { model
                | selectActivites = updatedSelect
                , entrepriseInfo = model.entrepriseInfo |> Maybe.map (\ei -> { ei | activites = activitesUniques })
                , saveEnterpriseInfoRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            if model.saveEnterpriseInfoRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | entrepriseInfo =
                        model.entrepriseInfo
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | activites =
                                            ei.activites
                                                |> List.filter ((/=) activite)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedLocalisations ( localisations, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                localisationsUniques =
                    case localisations of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                localisations
            in
            ( { model
                | selectLocalisations = updatedSelect
                , entrepriseInfo =
                    model.entrepriseInfo
                        |> Maybe.map (\ei -> { ei | localisations = localisationsUniques })
                , saveEnterpriseInfoRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            if model.saveEnterpriseInfoRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | entrepriseInfo =
                        model.entrepriseInfo
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | localisations =
                                            ei.localisations
                                                |> List.filter ((/=) localisation)
                                    }
                                )
                  }
                , Effect.none
                )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots
            in
            ( { model
                | selectMots = updatedSelect
                , entrepriseInfo =
                    model.entrepriseInfo
                        |> Maybe.map (\ei -> { ei | mots = motsUniques })
                , saveEnterpriseInfoRequest = RD.NotAsked
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            if model.saveEnterpriseInfoRequest == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | entrepriseInfo =
                        model.entrepriseInfo
                            |> Maybe.map
                                (\ei ->
                                    { ei
                                        | mots =
                                            ei.mots
                                                |> List.filter ((/=) mot)
                                    }
                                )
                  }
                , Effect.none
                )

        SetContactAction action ->
            let
                cmd =
                    case action of
                        Edit _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        New _ ->
                            Task.attempt (\_ -> NoOp) (Dom.focus <| "nouveau-contact-fonction")

                        _ ->
                            Cmd.none
            in
            ( { model | contactAction = action }, Effect.fromCmd cmd )

        CancelAction ->
            ( { model | saveContactRequest = RD.NotAsked, contactAction = None }, Effect.none )

        UpdatedContact field value ->
            case model.saveContactRequest of
                RD.Loading ->
                    ( model, Effect.none )

                _ ->
                    Effect.withNone <|
                        (\m -> { m | saveContactRequest = RD.NotAsked }) <|
                            case model.contactAction of
                                None ->
                                    model

                                Delete _ ->
                                    model

                                New contact ->
                                    { model | contactAction = New <| updateContact field value contact }

                                Edit contact ->
                                    { model | contactAction = Edit <| updateContact field value contact }

        ConfirmAction ->
            case model.contactAction of
                None ->
                    ( model, Effect.none )

                New contact ->
                    ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| createContact model.siret contact )

                Delete contact ->
                    ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| deleteContact contact )

                Edit contact ->
                    ( { model | saveContactRequest = RD.Loading }, Effect.fromCmd <| editContact contact )

        ReceivedSaveContact response ->
            let
                ( contactAction, augmentedEntreprise ) =
                    case response of
                        RD.Success _ ->
                            ( None, response )

                        _ ->
                            ( model.contactAction, model.entreprise )
            in
            ( { model
                | entreprise = augmentedEntreprise
                , contactAction = contactAction
                , saveContactRequest = response
              }
            , Effect.none
            )

        SetSuiviAction suiviAction ->
            { model | suiviAction = suiviAction } |> Effect.withNone

        UpdatedEchangeForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model | suiviAction = NewSuivi <| SuiviEchange <| UI.Echange.update field value <| echange }
                        |> Effect.withNone

                EditSuivi (SuiviEchange echange) ->
                    { model | suiviAction = EditSuivi <| SuiviEchange <| UI.Echange.update field value <| echange }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        SelectedTypeEchange typeEchange ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model | suiviAction = NewSuivi <| SuiviEchange <| (\ech -> { ech | typeEchange = typeEchange }) <| echange }
                        |> Effect.withNone

                EditSuivi (SuiviEchange echange) ->
                    { model | suiviAction = EditSuivi <| SuiviEchange <| (\ech -> { ech | typeEchange = typeEchange }) <| echange }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        ToggledTypeDemande typesDemande on ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model
                        | suiviAction =
                            NewSuivi <|
                                SuiviEchange <|
                                    (\ech ->
                                        { ech
                                            | typesDemande =
                                                ech.typesDemande
                                                    |> (\ts ->
                                                            if on then
                                                                if List.member typesDemande ts then
                                                                    ts

                                                                else
                                                                    ts |> List.reverse |> (::) typesDemande |> List.reverse

                                                            else
                                                                List.filter ((/=) typesDemande) ts
                                                       )
                                        }
                                    )
                                    <|
                                        echange
                    }
                        |> Effect.withNone

                EditSuivi (SuiviEchange echange) ->
                    { model
                        | suiviAction =
                            EditSuivi <|
                                SuiviEchange <|
                                    (\ech ->
                                        { ech
                                            | typesDemande =
                                                ech.typesDemande
                                                    |> (\ts ->
                                                            if on then
                                                                if List.member typesDemande ts then
                                                                    ts

                                                                else
                                                                    ts |> List.reverse |> (::) typesDemande |> List.reverse

                                                            else
                                                                List.filter ((/=) typesDemande) ts
                                                       )
                                        }
                                    )
                                    <|
                                        echange
                    }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedRappelForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviRappel rappel) ->
                    { model | suiviAction = NewSuivi <| SuiviRappel <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone

                EditSuivi (SuiviRappel rappel) ->
                    { model | suiviAction = EditSuivi <| SuiviRappel <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedClotureDemande demande ->
            case model.suiviAction of
                NewSuivi (SuiviCloture cloture) ->
                    { model
                        | suiviAction =
                            NewSuivi <|
                                SuiviCloture <|
                                    { cloture
                                        | demande = demande
                                    }
                    }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedClotureForm value ->
            case model.suiviAction of
                NewSuivi (SuiviCloture cloture) ->
                    { model | suiviAction = NewSuivi <| SuiviCloture <| UI.Cloture.update value <| cloture }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        ConfirmSuiviAction ->
            case model.saveSuiviRequest of
                RD.Loading ->
                    model |> Effect.withNone

                _ ->
                    case model.suiviAction of
                        NoSuivi ->
                            model |> Effect.withNone

                        NewSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createEchange model.siret echange)

                        NewSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createRappel model.siret rappel)

                        NewSuivi (SuiviCloture cloture) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createCloture model.siret cloture)

                        EditSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateEchange echange)

                        EditSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateRappel rappel)

                        EditSuivi (SuiviCloture _) ->
                            model |> Effect.withNone

                        DeleteSuivi (SuiviEchange _) ->
                            model |> Effect.withNone

                        DeleteSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (clotureRappel rappel)

                        DeleteSuivi (SuiviCloture _) ->
                            model |> Effect.withNone

        CancelSuiviAction ->
            { model | saveSuiviRequest = RD.NotAsked, suiviAction = NoSuivi }
                |> Effect.withNone

        ReceivedSaveSuivi response ->
            case model.saveSuiviRequest of
                RD.Loading ->
                    case model.suiviAction of
                        NoSuivi ->
                            model
                                |> Effect.withNone

                        _ ->
                            case response of
                                RD.Success _ ->
                                    { model | entreprise = response, saveSuiviRequest = response, suiviAction = NoSuivi }
                                        |> Effect.withNone

                                _ ->
                                    { model | saveSuiviRequest = response }
                                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        SetRappelMode rappelMode ->
            { model | rappelMode = rappelMode } |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg


view : Shared.Shared -> Model -> View Msg
view { now, timezone } model =
    let
        title =
            case model.entreprise of
                RD.Success { entreprise } ->
                    "Fiche de " ++ Data.Entreprise.displayNomEnseigne entreprise

                _ ->
                    "Fiche de " ++ model.siret
    in
    { title = UI.Layout.pageTitle <| title
    , body = body timezone now model
    , route = Route.Etablissement <| model.siret
    }


body : Zone -> Posix -> Model -> List (Html Msg)
body timezone now model =
    [ div [ class "flex flex-col p-2" ]
        [ div [ class "flex flex-row justify-between" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Just <| SharedMsg <| Shared.goBack
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            ]
        , case model.entreprise of
            RD.Success { fiche } ->
                case fiche of
                    Nothing ->
                        nothing

                    Just { dateModification, auteurModification, sireneUpdate } ->
                        let
                            auteur =
                                if auteurModification == "" then
                                    ""

                                else
                                    " par " ++ auteurModification
                        in
                        div [ Typo.textSm, Typo.textSpectralRegular, class "flex flex-col !mb-0 p-2 text-right" ]
                            [ div [] [ text <| "Dernière modification de la fiche le " ++ Lib.Date.formatShort timezone dateModification ++ auteur ]
                            , div [] [ text <| "Mise à jour de la base SIRENE\u{00A0}: " ++ Maybe.withDefault "date inconnue" sireneUpdate ]
                            ]

            _ ->
                nothing
        , case model.entreprise of
            RD.Loading ->
                div [ class "p-6 sm:p-8 fr-card--white" ]
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , "Chargement en cours..."
                        |> text
                    ]

            RD.Success { entreprise, entite, fiche } ->
                let
                    contacts =
                        entite
                            |> Maybe.map .contacts
                            |> Maybe.withDefault []

                    suivis =
                        fiche
                            |> Maybe.map (\f -> { echanges = f.echanges, rappels = f.rappels, clotures = f.clotures })
                            |> Maybe.withDefault { echanges = [], rappels = [], clotures = [] }

                    demandes =
                        fiche
                            |> Maybe.map .demandes
                            |> Maybe.withDefault []

                    qualifications =
                        entite
                            |> Maybe.map
                                (\e ->
                                    { activites = Data.Entite.activitesReelles e
                                    , localisations = Data.Entite.localisations e
                                    , mots = Data.Entite.motsCles e
                                    , autre = Data.Entite.activiteAutre e
                                    }
                                )
                            |> Maybe.withDefault
                                { activites = []
                                , localisations = []
                                , mots = []
                                , autre = ""
                                }
                in
                div [ class "flex flex-col gap-4 p-2" ]
                    [ viewDemandesModal demandes model
                    , div [ class "p-4 sm:p-8 fr-card--white" ]
                        [ Lazy.lazy5 viewEntreprise model demandes entreprise.exogene qualifications entreprise
                        ]
                    , div [ class "p-4 sm:p-8 fr-card--white" ]
                        [ Lazy.lazy2 viewContacts model contacts
                        ]
                    , div [ class "p-4 sm:p-8 fr-card--white" ]
                        [ Lazy.lazy4 viewSuivis timezone now model suivis ]
                    ]

            _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]
        ]
    ]


viewDemandesModal : List Demande -> Model -> Html Msg
viewDemandesModal demandes model =
    case model.suiviAction of
        NoSuivi ->
            nothing

        NewSuivi suiviType ->
            viewSuiviForm True model.today model.saveSuiviRequest demandes suiviType

        EditSuivi suiviType ->
            viewSuiviForm False model.today model.saveSuiviRequest demandes suiviType

        DeleteSuivi suiviType ->
            viewDeleteSuiviConfirmation model.saveSuiviRequest suiviType


viewEntreprise : Model -> List Demande -> Bool -> Qualifications -> Entreprise -> Html Msg
viewEntreprise model demandes exogene qualifications ({ adresse, etatAdministratif, siret } as entreprise) =
    div []
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col, class "flex flex-row justify-between items-start" ]
                [ div [ class "flex flex-col" ]
                    [ h1 [ Typo.textBold, Typo.fr_h3, class "!mb-0" ] [ DSFR.Icons.iconLG UI.Entite.iconeEtablissement, text "\u{00A0}", text <| Data.Entreprise.displayNomEnseigne entreprise ]
                    , div [ Typo.textBold, Typo.textSm, class "!mb-0" ] [ text <| adresse ]
                    ]
                , div [ class "flex flex-row gap-4 items-center" ]
                    [ etatAdministratif
                        |> Just
                        |> UI.Entite.badgeInactif
                    , UI.Entite.badgeExogene <|
                        exogene
                    ]
                ]
            ]
        , div [ class "pb-8" ] []
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col5 ]
                [ Lazy.lazy viewEntrepriseResume entreprise
                ]
            , div
                [ DSFR.Grid.col4
                , classList
                    [ ( "fr-card--grey", model.entrepriseInfo == Nothing )
                    ]
                ]
                [ viewEntrepriseData model qualifications
                ]
            , div [ DSFR.Grid.col3, class "flex flex-col gap-4 justify-between" ]
                [ viewFicheDemandes demandes
                , viewLinks siret
                ]
            ]
        ]


viewSuiviForm : Bool -> Date -> WebData EntrepriseAugmented -> List Demande -> SuiviType -> Html Msg
viewSuiviForm new today saveSuiviRequest demandes suiviType =
    DSFR.Modal.view
        { id = "suivi"
        , label = "suivi"
        , openMsg = NoOp
        , closeMsg = Just CancelSuiviAction
        , title =
            text <|
                (if new then
                    "Ajouter"

                 else
                    "Modifier"
                )
                    ++ " un élément de suivi"
        , opened = True
        }
        (viewSuiviBody saveSuiviRequest new today demandes suiviType)
        Nothing
        |> Tuple.first


viewDeleteSuiviConfirmation : WebData EntrepriseAugmented -> SuiviType -> Html Msg
viewDeleteSuiviConfirmation saveSuiviRequest suiviType =
    DSFR.Modal.view
        { id = "delete"
        , label = "delete"
        , openMsg = NoOp
        , closeMsg = Just CancelSuiviAction
        , title =
            text <|
                case suiviType of
                    SuiviEchange _ ->
                        "Supprimer un échange"

                    SuiviRappel rappel ->
                        if rappel.dateCloture /= Nothing then
                            "Clôturer un rappel"

                        else
                            "Réouvrir un rappel"

                    SuiviCloture _ ->
                        ""
        , opened = True
        }
        (viewDeleteSuiviBody saveSuiviRequest suiviType)
        Nothing
        |> Tuple.first


viewSuiviBody : WebData EntrepriseAugmented -> Bool -> Date -> List Demande -> SuiviType -> Html Msg
viewSuiviBody saveSuiviRequest new today demandes suiviType =
    let
        openDemandes =
            demandes
                |> List.filter (Data.Demande.cloture >> not)
    in
    div []
        [ if new then
            div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ]
                    [ DSFR.Radio.group
                        { id = "suivi-type-radio"
                        , options = [ "Ajouter un échange", "Ajouter un rappel", "Clôturer une demande" ]
                        , current =
                            case suiviType of
                                SuiviEchange _ ->
                                    Just "Ajouter un échange"

                                SuiviRappel _ ->
                                    Just "Ajouter un rappel"

                                SuiviCloture _ ->
                                    Just "Clôturer une demande"
                        , toLabel = text
                        , toId = String.replace " " "-"
                        , msg =
                            \s ->
                                case s of
                                    "Ajouter un échange" ->
                                        SetSuiviAction <| NewSuivi <| SuiviEchange <| UI.Echange.default "" today

                                    "Ajouter un rappel" ->
                                        SetSuiviAction <| NewSuivi <| SuiviRappel <| UI.Rappel.default "" today

                                    "Clôturer une demande" ->
                                        case demandes of
                                            [] ->
                                                SetSuiviAction NoSuivi

                                            demande :: _ ->
                                                SetSuiviAction <| NewSuivi <| SuiviCloture <| UI.Cloture.default demande

                                    _ ->
                                        SetSuiviAction NoSuivi
                        , legend = nothing
                        }
                        |> DSFR.Radio.withDisabledOption
                            (\opt ->
                                case opt of
                                    "Clôturer une demande" ->
                                        List.length demandes == 0

                                    _ ->
                                        False
                            )
                        |> DSFR.Radio.inline
                        |> DSFR.Radio.view
                    ]
                ]

          else
            nothing
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col, class "flex flex-col gap-4" ] <|
                case suiviType of
                    SuiviEchange echange ->
                        [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue" ] [ text "Qualification de l'échange" ] ]
                        , UI.Echange.form (validateEchange echange) SelectedTypeEchange ToggledTypeDemande UpdatedEchangeForm echange
                        ]

                    SuiviRappel rappel ->
                        [ div [ class "flex font-bold" ]
                            [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue" ]
                                [ text <|
                                    (if new then
                                        "Création"

                                     else
                                        "Modification"
                                    )
                                        ++ " d'un rappel"
                                ]
                            ]
                        , UI.Rappel.form UpdatedRappelForm rappel
                        ]

                    SuiviCloture cloture ->
                        if List.length openDemandes == 0 then
                            [ div [ class "italic" ] [ text "Aucune demande en cours" ] ]

                        else
                            [ div [ class "flex font-bold" ]
                                [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue" ]
                                    [ text "Demande"
                                    , text <|
                                        if List.length openDemandes > 1 then
                                            "s"

                                        else
                                            ""
                                    , text " en cours"
                                    ]
                                ]
                            , UI.Cloture.form UpdatedClotureDemande UpdatedClotureForm openDemandes cloture
                            ]
            ]
        , div [ class "py-4" ] [ viewSuiviFooter saveSuiviRequest suiviType ]
        ]


validateEchange : UI.Echange.EchangeInput -> Maybe String
validateEchange echange =
    case UI.Echange.echangeInputToEchange echange of
        Err errs ->
            errs
                |> Dict.get "type"
                |> Maybe.map (String.join ", ")

        _ ->
            Nothing


viewSuiviFooter : WebData EntrepriseAugmented -> SuiviType -> Html Msg
viewSuiviFooter saveSuiviRequest suiviType =
    let
        disabled =
            saveSuiviRequest
                == RD.Loading
                || (case suiviType of
                        SuiviEchange echange ->
                            not <| UI.Echange.isValid echange

                        SuiviRappel rappel ->
                            not <| UI.Rappel.isValid rappel

                        SuiviCloture _ ->
                            False
                   )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewDeleteSuiviBody : WebData EntrepriseAugmented -> SuiviType -> Html Msg
viewDeleteSuiviBody saveSuiviRequest suiviType =
    div [ DSFR.Grid.gridRow ]
        [ div [ DSFR.Grid.col, class "flex flex-col gap-4" ] <|
            case suiviType of
                SuiviRappel rappel ->
                    let
                        action =
                            if rappel.dateCloture /= Nothing then
                                "clôturer"

                            else
                                "réouvrir"
                    in
                    [ text <| "Êtes-vous sûr(e) de vouloir " ++ action ++ " ce rappel\u{00A0}?" ]

                _ ->
                    []
        , viewDeleteSuiviFooter saveSuiviRequest
        ]


viewDeleteSuiviFooter : WebData EntrepriseAugmented -> Html Msg
viewDeleteSuiviFooter saveSuiviRequest =
    let
        disabled =
            saveSuiviRequest == RD.Loading
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Confirmer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


columnWrapper : Html msg -> Html msg
columnWrapper =
    List.singleton
        >> div [ class "!px-[1rem]", Html.Attributes.style "overflow-wrap" "break-word" ]


type EchangeAndCloture
    = Ech Echange
    | Clot Cloture


toEchangesAndClotures : List Echange -> List Cloture -> List EchangeAndCloture
toEchangesAndClotures echanges clotures =
    List.map Ech echanges ++ List.map Clot clotures


viewRappels : Zone -> Posix -> RappelMode -> List Rappel -> Html Msg
viewRappels timezone now rappelMode rappels =
    let
        shownRappels =
            rappels
                |> List.filter
                    (Data.Rappel.dateCloture
                        >> (case rappelMode of
                                RappelsActifs ->
                                    (==) Nothing

                                RappelsClotures ->
                                    (/=) Nothing
                           )
                    )
    in
    div [ class "flex flex-col gap-2" ]
        (div [ class "flex flex-col" ]
            [ div [ class "flex flex-row justify-between items-center" ]
                [ h4 [ Typo.fr_h6, class "!mb-1" ]
                    [ text "Rappels"
                    , text <|
                        case rappelMode of
                            RappelsActifs ->
                                " actifs"

                            RappelsClotures ->
                                " clôturés"
                    ]
                , case rappelMode of
                    RappelsActifs ->
                        DSFR.Button.new { label = "voir les rappels clôturés", onClick = Just <| SetRappelMode RappelsClotures }
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ class "!text-black underline" ]
                            |> DSFR.Button.view

                    RappelsClotures ->
                        DSFR.Button.new { label = "voir les rappels actifs", onClick = Just <| SetRappelMode RappelsActifs }
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ class "!text-black underline" ]
                            |> DSFR.Button.view
                ]
            , hr [ class "!pb-1" ] []
            ]
            :: (if List.length shownRappels == 0 then
                    span [ class "text-center italic" ]
                        [ text "Aucun rappel "
                        , text <|
                            case rappelMode of
                                RappelsActifs ->
                                    "actif"

                                RappelsClotures ->
                                    "clôturé"
                        ]

                else
                    div [ class "border-transparent" ]
                        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                            [ div [ DSFR.Grid.col1 ] [ nothing ]
                            , div [ DSFR.Grid.col2, class "font-bold" ]
                                [ text <|
                                    case rappelMode of
                                        RappelsClotures ->
                                            "Date de clôture"

                                        RappelsActifs ->
                                            "Date d'échéance"
                                ]
                            , div [ DSFR.Grid.col7, class "font-bold" ] [ text "Titre" ]
                            , div [ DSFR.Grid.col2, class "font-bold text-right !pr-[1.5rem]" ] [ text "Actions" ]
                            ]
                        ]
               )
            :: (List.map (viewRappel timezone now rappelMode) <|
                    List.sortWith sortRappelsOlderFirst <|
                        shownRappels
               )
        )


sortRappelsOlderFirst : Rappel -> Rappel -> Order
sortRappelsOlderFirst r1 r2 =
    case ( Data.Rappel.dateCloture r1, Data.Rappel.dateCloture r2 ) of
        ( Nothing, Nothing ) ->
            let
                comparison =
                    Lib.Date.sortOlderDateFirst Data.Rappel.date r1 r2
            in
            case comparison of
                EQ ->
                    Api.EntityId.compare (Data.Rappel.id r1) (Data.Rappel.id r2)

                _ ->
                    comparison

        ( Just d1, Just d2 ) ->
            let
                comparison =
                    Date.compare d1 d2
            in
            case comparison of
                EQ ->
                    Api.EntityId.compare (Data.Rappel.id r1) (Data.Rappel.id r2)

                _ ->
                    comparison

        _ ->
            EQ


viewRappel : Zone -> Posix -> RappelMode -> Rappel -> Html Msg
viewRappel timezone now rappelMode rappel =
    let
        isLate =
            rappel
                |> Data.Rappel.date
                |> Lib.Date.firstDateIsAfterSecondDate (Date.fromPosix timezone now)

        ( icon, color ) =
            if isLate && (Nothing == Data.Rappel.dateCloture rappel) then
                ( div [ class "fr-text-default--error text-center pt-[0.5em]" ] [ DSFR.Icons.custom "ri-checkbox-blank-circle-fill" |> DSFR.Icons.icon ], class "fr-text-default--error" )

            else
                ( nothing, empty )

        buttons =
            [ [ DSFR.Button.new { onClick = Just <| SetSuiviAction <| EditSuivi <| SuiviRappel <| UI.Rappel.rappelToRappelInput <| rappel, label = "" }
                    |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1", Html.Attributes.title "Modifier le rappel" ]
              , DSFR.Button.new
                    { onClick =
                        Just <|
                            SetSuiviAction <|
                                DeleteSuivi <|
                                    SuiviRappel <|
                                        (\r ->
                                            { r
                                                | dateCloture =
                                                    if r.dateCloture == Nothing then
                                                        Date.fromPosix timezone now
                                                            |> Date.toIsoString
                                                            |> Just

                                                    else
                                                        Nothing
                                            }
                                        )
                                        <|
                                            UI.Rappel.rappelToRappelInput <|
                                                rappel
                    , label = ""
                    }
                    |> DSFR.Button.onlyIcon
                        (if Nothing /= Data.Rappel.dateCloture rappel then
                            DSFR.Icons.System.arrowGoBackLine

                         else
                            DSFR.Icons.System.closeLine
                        )
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withAttrs
                        [ class "!p-0 !m-0 !ml-1"
                        , Html.Attributes.title <|
                            (if Nothing /= Data.Rappel.dateCloture rappel then
                                "Réouvrir"

                             else
                                "Clôturer"
                            )
                                ++ " le rappel"
                        ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup
            ]
    in
    div [ class "!py-4 border-2 dark-grey-border" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "items-center" ]
            [ div [ DSFR.Grid.col1 ] [ icon ]
            , div [ color, DSFR.Grid.col2 ]
                [ case rappelMode of
                    RappelsActifs ->
                        text <|
                            Lib.Date.formatDateShort <|
                                Data.Rappel.date rappel

                    RappelsClotures ->
                        text <|
                            Maybe.withDefault "" <|
                                Maybe.map Lib.Date.formatDateShort <|
                                    Data.Rappel.dateCloture rappel
                ]
            , div [ DSFR.Grid.col7 ]
                [ text <| Data.Rappel.titre rappel
                ]
            , div [ DSFR.Grid.col2 ] <| List.singleton <| div [ class "p-2" ] <| buttons
            ]
        ]


viewEchangesAndClotures : Zone -> List EchangeAndCloture -> Html Msg
viewEchangesAndClotures timezone echangesAndClotures =
    div [ class "flex flex-col gap-4" ]
        (div [ class "flex flex-col" ]
            [ div [ class "flex flex-row justify-between" ]
                [ h4 [ Typo.fr_h6, class "!mb-1" ] [ text "Échanges" ]
                ]
            , hr [ class "!pb-1" ] []
            ]
            :: (if List.length echangesAndClotures == 0 then
                    span [ class "text-center italic" ] [ text "Aucun échange pour l'instant" ]

                else
                    nothing
               )
            :: (List.map
                    (\eac ->
                        div [ class "!py-2 border-2 dark-grey-border" ] <|
                            List.singleton <|
                                Keyed.node "div" [] <|
                                    case eac of
                                        Ech echange ->
                                            [ ( "echange-" ++ (entityIdToString <| Data.Echange.id <| echange)
                                              , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "items-first-baseline" ] <|
                                                    viewEchange timezone echange
                                              )
                                            ]

                                        Clot cloture ->
                                            [ ( "cloture-" ++ (entityIdToString <| Data.Cloture.id <| cloture)
                                              , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                                                    viewCloture cloture
                                              )
                                            ]
                    )
                <|
                    List.sortWith
                        (\eac1 eac2 ->
                            let
                                comparison =
                                    Lib.Date.sortOlderDateLast
                                        (\eac ->
                                            case eac of
                                                Ech echange ->
                                                    Data.Echange.date echange

                                                Clot cloture ->
                                                    Data.Cloture.date cloture
                                        )
                                        eac1
                                        eac2
                            in
                            case comparison of
                                EQ ->
                                    case ( eac1, eac2 ) of
                                        ( Ech e1, Ech e2 ) ->
                                            Api.EntityId.compare (Data.Echange.id e2) (Data.Echange.id e1)

                                        ( Clot c1, Clot c2 ) ->
                                            Lib.Date.sortOlderDateLast Data.Cloture.date c1 c2

                                        ( Ech _, Clot _ ) ->
                                            GT

                                        ( Clot _, Ech _ ) ->
                                            LT

                                _ ->
                                    comparison
                        )
                    <|
                        echangesAndClotures
               )
        )


viewCloture : Cloture -> List (Html Msg)
viewCloture cloture =
    let
        label =
            (++) "Demande clôturée\u{00A0}: " <|
                Data.Demande.typeDemandeToDisplay <|
                    Data.Cloture.demande cloture
    in
    [ div [ DSFR.Grid.col1 ]
        [ div [ class "grey-text text-center" ]
            [ DSFR.Icons.iconMD <| DSFR.Icons.System.deleteLine
            ]
        ]
    , div [ DSFR.Grid.col2 ] [ text <| Lib.Date.formatDateShort <| Data.Cloture.date cloture ]
    , div [ DSFR.Grid.col5 ]
        [ div [ class "flex flex-col gap-2" ]
            [ span [ class "font-bold" ] [ text <| label ]
            , div [ class "whitespace-pre-wrap" ] <| List.singleton <| text <| Data.Cloture.motif cloture
            ]
        ]
    , div [ DSFR.Grid.col4 ]
        [ DSFR.Tag.unclickable { data = label, toString = identity }
            |> List.singleton
            |> DSFR.Tag.medium
        ]
    ]


viewEchange : Zone -> Echange -> List (Html Msg)
viewEchange timezone echange =
    [ div [ DSFR.Grid.col1, class "grey-text text-center" ]
        [ DSFR.Icons.iconMD <|
            case Data.Echange.type_ echange of
                Data.Echange.Telephone ->
                    DSFR.Icons.Device.phoneLine

                Data.Echange.Email ->
                    DSFR.Icons.custom "ri-at-line"

                Data.Echange.Rencontre ->
                    DSFR.Icons.User.userLine

                Data.Echange.Courrier ->
                    DSFR.Icons.Business.mailLine
        ]
    , div [ DSFR.Grid.col2 ]
        [ div [ class "flex flex-col" ]
            [ div [] [ text <| Lib.Date.formatDateShort <| Data.Echange.date echange ]
            , case Data.Echange.modification echange of
                Nothing ->
                    nothing

                Just ( auteur, date ) ->
                    div
                        [ Html.Attributes.title <|
                            "Dernière modification de l'échange le "
                                ++ Lib.Date.formatShort timezone date
                                ++ " par "
                                ++ auteur
                        , Typo.textSm
                        , Typo.textSpectralRegular
                        ]
                        [ text "Modifié "
                        , DSFR.Icons.iconSM DSFR.Icons.System.informationLine
                        ]
            ]
        ]
    , div [ DSFR.Grid.col5 ]
        [ div [ class "flex flex-col gap-2" ]
            [ let
                viewAsAccordion () =
                    DSFR.Accordion.raw
                        { id = "echange" ++ (entityIdToString <| Data.Echange.id <| echange)
                        , title =
                            [ div [ class "whitespace-pre-wrap" ]
                                [ span [ class "font-bold" ]
                                    [ text <| withEmptyAs "-" <| Data.Echange.titre echange ]
                                ]
                            ]
                        , content = [ div [ class "whitespace-pre-wrap" ] [ text <| Data.Echange.compteRendu echange ] ]
                        , borderless = True
                        }
              in
              case Data.Echange.compteRendu echange of
                "" ->
                    span [ class "font-bold" ]
                        [ text <| withEmptyAs "-" <| Data.Echange.titre echange ]

                _ ->
                    viewAsAccordion ()
            ]
        ]
    , div [ DSFR.Grid.col3 ]
        [ Data.Echange.themes echange
            |> List.map (\theme -> DSFR.Tag.unclickable { data = Data.Demande.typeDemandeToDisplay theme, toString = identity })
            |> DSFR.Tag.medium
        ]
    , div [ DSFR.Grid.col1 ]
        [ DSFR.Button.new { onClick = Just <| SetSuiviAction <| EditSuivi <| SuiviEchange <| UI.Echange.echangeToEchangeInput <| echange, label = "" }
            |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
            |> DSFR.Button.tertiaryNoOutline
            |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1", Html.Attributes.title "Modifier l'échange" ]
            |> DSFR.Button.view
        ]
    ]


viewContactForm : WebData EntrepriseAugmented -> Contact -> Html Msg
viewContactForm request contact =
    let
        contactIsEmpty =
            contact.fonction == "" && contact.prenom == "" && contact.nom == "" && contact.email == "" && contact.telephone == ""
    in
    formWithListeners [ Events.onSubmit <| ConfirmAction, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col2 ]
            [ DSFR.Input.new { value = contact.fonction, onInput = UpdatedContact Fonction, label = nothing, name = "nouveau-contact-fonction" }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col ]
            [ DSFR.Input.new { value = contact.prenom, onInput = UpdatedContact Prenom, label = nothing, name = "nouveau-contact-prenom" }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col ]
            [ DSFR.Input.new { value = contact.nom, onInput = UpdatedContact Nom, label = nothing, name = "nouveau-contact-nom" }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col ]
            [ DSFR.Input.new { value = contact.email, onInput = UpdatedContact Email, label = nothing, name = "nouveau-contact-email" }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col2 ]
            [ DSFR.Input.new { value = contact.telephone, onInput = UpdatedContact Telephone, label = nothing, name = "nouveau-contact-telephone" }
                |> DSFR.Input.view
            ]
        , div [ DSFR.Grid.col1, class "flex flex-col !pt-4" ]
            [ div []
                [ [ DSFR.Button.new { onClick = Nothing, label = "" }
                        |> DSFR.Button.onlyIcon (DSFR.Icons.custom "")
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withDisabled True
                        |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1 !pr-8" ]
                  , DSFR.Button.new { onClick = Nothing, label = "" }
                        |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withDisabled True
                        |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1" ]
                  ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            , div []
                [ [ DSFR.Button.new { onClick = Nothing, label = "" }
                        |> DSFR.Button.onlyIcon DSFR.Icons.System.checkboxCircleFill
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withDisabled contactIsEmpty
                        |> DSFR.Button.submit
                        |> DSFR.Button.withAttrs [ id <| "annuler-modifier-contact-" ++ entityIdToString contact.id, class "!p-0 !m-0 !ml-1", Html.Attributes.title "Confirmer" ]
                  , DSFR.Button.new { onClick = Just <| CancelAction, label = "" }
                        |> DSFR.Button.onlyIcon DSFR.Icons.System.closeCircleLine
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.withAttrs [ id <| "confirmer-modifier-contact-" ++ entityIdToString contact.id, class "!p-0 !m-0 !ml-1", Html.Attributes.title "Annuler" ]
                  ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]
            ]
        , div [ class "flex flex-row justify-end w-full" ]
            [ case request of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = "Une erreur s'est produite" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        ]


viewContacts : Model -> List Contact -> Html Msg
viewContacts model contacts =
    div [ class "flex flex-col gap-4" ]
        (div [ class "flex flex-row justify-between" ]
            [ h3 [ Typo.fr_h4 ] [ text "Contacts", text " (", text <| String.fromInt <| List.length contacts, text ")" ]
            , DSFR.Button.new { label = "Ajouter un contact", onClick = Just <| SetContactAction <| New emptyContact }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (model.contactAction /= None)
                |> DSFR.Button.view
            ]
            :: div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col2 ] [ columnWrapper <| label [ class "font-bold", Html.Attributes.for "nouveau-contact-fonction" ] [ text "Fonction" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold", Html.Attributes.for "nouveau-contact-prenom" ] [ text "Prénom" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold", Html.Attributes.for "nouveau-contact-nom" ] [ text "Nom" ] ]
                , div [ DSFR.Grid.col ] [ columnWrapper <| label [ class "font-bold", Html.Attributes.for "nouveau-contact-email" ] [ text "Email" ] ]
                , div [ DSFR.Grid.col2 ] [ columnWrapper <| label [ class "font-bold", Html.Attributes.for "nouveau-contact-telephone" ] [ text "Téléphone" ] ]
                , div [ DSFR.Grid.col1 ] [ nothing ]
                ]
            :: hr [ class "!pb-1" ] []
            :: (case model.contactAction of
                    New c ->
                        viewContactForm model.saveContactRequest c

                    _ ->
                        nothing
               )
            :: (case model.contactAction of
                    New _ ->
                        hr [ class "!pb-1" ] []

                    _ ->
                        nothing
               )
            :: (if List.length contacts == 0 then
                    span [ class "text-center italic" ] [ text "Aucun contact pour l'instant" ]

                else
                    nothing
               )
            :: (List.intersperse (hr [ class "!pb-1" ] []) <|
                    List.map (viewContact model.saveContactRequest model.contactAction) <|
                        contacts
               )
        )


viewContact : WebData EntrepriseAugmented -> ContactAction -> Contact -> Html Msg
viewContact request contactAction contact =
    case contactAction of
        None ->
            let
                buttons =
                    [ [ DSFR.Button.new { onClick = Just <| SetContactAction <| Edit contact, label = "" }
                            |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ id <| "modifier-contact-" ++ entityIdToString contact.id, class "!p-0 !m-0 !ml-1" ]
                      , DSFR.Button.new { onClick = Just <| SetContactAction <| Delete contact, label = "" }
                            |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteFill
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ id <| "supprimer-contact-" ++ entityIdToString contact.id, class "!p-0 !m-0 !ml-1" ]
                      ]
                        |> DSFR.Button.group
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRight
                        |> DSFR.Button.viewGroup
                    ]
            in
            viewStaticContact buttons contact

        New _ ->
            viewStaticContact [] contact

        Edit c ->
            if c.id == contact.id then
                viewContactForm request c

            else
                viewStaticContact [] contact

        Delete c ->
            let
                buttons =
                    [ div []
                        [ [ DSFR.Button.new { onClick = Nothing, label = "" }
                                |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteFill
                                |> DSFR.Button.tertiaryNoOutline
                                |> DSFR.Button.withDisabled True
                                |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1" ]
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    , div []
                        [ [ DSFR.Button.new { onClick = Just <| ConfirmAction, label = "" }
                                |> DSFR.Button.onlyIcon DSFR.Icons.System.checkboxCircleFill
                                |> DSFR.Button.tertiaryNoOutline
                                |> DSFR.Button.withAttrs [ id <| "confirmer-supprimer-contact-" ++ entityIdToString contact.id, class "!p-0 !m-0 !ml-1", Html.Attributes.title "Confirmer la suppression" ]
                          , DSFR.Button.new { onClick = Just <| CancelAction, label = "" }
                                |> DSFR.Button.onlyIcon DSFR.Icons.System.closeCircleLine
                                |> DSFR.Button.tertiaryNoOutline
                                |> DSFR.Button.withAttrs [ id <| "annuler-supprimer-contact-" ++ entityIdToString contact.id, class "!p-0 !m-0 !ml-1", Html.Attributes.title "Annuler la suppression" ]
                          ]
                            |> DSFR.Button.group
                            |> DSFR.Button.inline
                            |> DSFR.Button.alignedRightInverted
                            |> DSFR.Button.viewGroup
                        ]
                    ]
            in
            if c.id == contact.id then
                viewStaticContact buttons contact

            else
                viewStaticContact [] contact


viewStaticContact : List (Html Msg) -> Contact -> Html Msg
viewStaticContact buttons contact =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ columnWrapper <| text <| capitalizeName <| contact.fonction ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ columnWrapper <| text <| capitalizeName <| contact.prenom ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ columnWrapper <| text <| capitalizeName <| contact.nom ]
        , div [ DSFR.Grid.col, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ columnWrapper <| text contact.email ]
        , div [ DSFR.Grid.col2, class "lg:!py-[1.25rem] !py-[1rem] !mt-[0.5rem]" ] [ columnWrapper <| text <| displayPhone contact.telephone ]
        , div [ DSFR.Grid.col1, class "flex flex-col !mt-[0.5rem]" ] buttons
        ]


viewSuivis : Zone -> Posix -> Model -> { echanges : List Echange, rappels : List Rappel, clotures : List Cloture } -> Html Msg
viewSuivis timezone now model { echanges, rappels, clotures } =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-row justify-between" ]
            [ h3 [ Typo.fr_h4 ] [ text "Suivi de la relation" ]
            , DSFR.Button.new
                { label = "Ajouter un élément de suivi"
                , onClick = Just <| SetSuiviAction <| NewSuivi <| SuiviEchange <| UI.Echange.default "" model.today
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (model.suiviAction /= NoSuivi)
                |> DSFR.Button.view
            ]
        , viewRappels timezone now model.rappelMode rappels
        , viewEchangesAndClotures timezone <| toEchangesAndClotures echanges clotures
        , div [] [ text "\u{00A0}" ] -- layout hack
        , hr [ class "!pb-1" ] []
        ]


viewFicheDemandes : List Demande -> Html msg
viewFicheDemandes demandes =
    let
        openDemandes =
            demandes
                |> List.filter (Data.Demande.cloture >> not)
    in
    div [ class "p-2" ]
        [ div [ Typo.textBold ]
            [ text "Demande"
            , text <|
                if List.length openDemandes > 1 then
                    "s"

                else
                    ""
            , text " en cours"
            ]
        , hr [ class "!pb-4" ] []
        , case openDemandes of
            [] ->
                span [ class "italic" ] [ text "Aucune demande en cours." ]

            ds ->
                ds
                    |> List.map
                        (Data.Demande.label
                            >> (\t -> { data = t, toString = identity })
                            >> DSFR.Tag.unclickable
                        )
                    |> DSFR.Tag.medium
        ]


viewEntrepriseResume : Entreprise -> Html msg
viewEntrepriseResume ({ exercices, effectifs } as entreprise) =
    div [ class "p-2" ]
        [ Lazy.lazy viewSummary entreprise
        , Lazy.lazy2 viewFinancialData effectifs exercices
        ]


viewSummary : Entreprise -> Html msg
viewSummary { siren, siret, activitePrincipaleUniteLegale, categorieActivitePrincipaleUniteLegale, dateCreationEtablissement, formeJuridique, zonagesPrioritaires } =
    div [ class "pb-4" ]
        [ div [ Typo.textBold ] [ text "Résumé" ]
        , hr [ class "!pb-4" ] []
        , div []
            [ Lib.UI.infoLine Nothing "SIREN\u{00A0}: " <| siren
            , Lib.UI.infoLine Nothing "SIRET\u{00A0}: " <| siret
            , Lib.UI.infoLine Nothing "Catégorie\u{00A0}: " <| Maybe.withDefault "-" <| categorieActivitePrincipaleUniteLegale
            , Lib.UI.infoLine Nothing "Activité NAF\u{00A0}: " <| Maybe.withDefault "-" <| activitePrincipaleUniteLegale
            , Lib.UI.infoLine Nothing "Forme juridique\u{00A0}: " <| Maybe.withDefault "-" <| formeJuridique
            , Lib.UI.infoLine Nothing "Zonage Prioritaire\u{00A0}: " <|
                case zonagesPrioritaires of
                    [] ->
                        "-"

                    _ ->
                        String.join "\u{00A0}— " <|
                            zonagesPrioritaires
            , Lib.UI.infoLine Nothing "Date de création\u{00A0}: " <| Maybe.withDefault "-" <| Maybe.map Lib.Date.formatDateShort <| dateCreationEtablissement
            ]
        ]


viewFinancialData : Maybe ( String, Date ) -> List ( String, Date ) -> Html msg
viewFinancialData effectifs exercices =
    div [ class "pb-4" ]
        [ div [ Typo.textBold ] [ text "Effectifs et données financières du groupe" ]
        , hr [ class "!pb-4" ] []
        , div [] <|
            (case effectifs of
                Nothing ->
                    Lib.UI.infoLine Nothing "Effectifs\u{00A0}: " <| "Indisponible"

                Just ( eff, date ) ->
                    Lib.UI.infoLine Nothing ("Effectifs " ++ Lib.Date.formatDateMonthNameYear date ++ "\u{00A0}: ") <| eff
            )
                :: (case exercices of
                        [] ->
                            [ Lib.UI.infoLine Nothing "CA\u{00A0}: " <| "Indisponible" ]

                        _ ->
                            exercices
                                |> List.sortWith (Lib.Date.sortOlderDateLast Tuple.second)
                                |> List.map viewExercice
                   )
        ]


viewLinks : String -> Html msg
viewLinks siret =
    div [ class "p-2" ]
        [ div [ Typo.textBold ] [ text "Liens" ]
        , hr [ class "!pb-4" ] []
        , div []
            [ div []
                [ span [ Typo.textXs ] [ text "Annuaire des entreprises\u{00A0}: " ]
                , Typo.externalLink ("https://annuaire-entreprises.data.gouv.fr/etablissement/" ++ siret)
                    [ Typo.textXs ]
                    [ text "https://annuaire-entreprises.data.gouv.fr" ]
                ]
            ]
        , div []
            [ span [ Typo.textXs ] [ text "Bodacc\u{00A0}: " ]
            , Typo.externalLink "https://www.bodacc.fr/"
                [ Typo.textXs ]
                [ text "https://www.bodacc.fr/" ]
            ]
        ]


viewExercice : ( String, Date ) -> Html msg
viewExercice ( ca, date ) =
    div
        []
        [ span [] [ text <| ("CA " ++ String.fromInt (Date.year date) ++ "\u{00A0}: ") ]
        , span [ Typo.textBold ]
            [ Html.node "currency-display"
                [ Html.Attributes.attribute "culture-code" "fr-FR"
                , Html.Attributes.attribute "currency" "EUR"
                , Html.Attributes.attribute "amount" ca
                ]
                []
            ]
        ]


viewEntrepriseData : Model -> Qualifications -> Html Msg
viewEntrepriseData { entrepriseInfo, selectActivites, selectLocalisations, selectMots, saveEnterpriseInfoRequest } { activites, localisations, mots, autre } =
    let
        classAttrs =
            class "flex flex-col px-4 h-full gap-4"
    in
    case entrepriseInfo of
        Nothing ->
            div [ classAttrs ]
                [ div []
                    [ div [ Typo.textBold, class "flex flex-row justify-between items-center" ]
                        [ span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                        , DSFR.Button.new { label = "Modifier", onClick = Just EditEntrepriseInfo }
                            |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ class "!p-0" ]
                            |> DSFR.Button.view
                        ]
                    , case activites of
                        [] ->
                            text "-"

                        _ ->
                            activites
                                |> String.join "\u{00A0}— "
                                |> text
                    ]
                , div []
                    [ div [ Typo.textBold ] [ span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
                    , case localisations of
                        [] ->
                            text "-"

                        _ ->
                            localisations
                                |> String.join "\u{00A0}— "
                                |> text
                    ]
                , div []
                    [ div [ Typo.textBold ] [ span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
                    , case mots of
                        [] ->
                            text "-"

                        _ ->
                            mots
                                |> String.join "\u{00A0}— "
                                |> text
                    ]
                , div []
                    [ div [ Typo.textBold ] [ text "Commentaires" ]
                    , div [ class "whitespace-pre-wrap" ]
                        [ text <|
                            Lib.UI.textWithDefault "-" <|
                                autre
                        ]
                    ]
                ]

        Just infos ->
            let
                noDifference =
                    infos.autre == autre && isPermutationOf infos.activites activites && isPermutationOf infos.localisations localisations && isPermutationOf infos.mots mots
            in
            formWithListeners [ Events.onSubmit <| RequestedSaveEntrepriseInfo, classAttrs, class "border-[0.1rem] border-france-blue py-[0.4rem]" ]
                [ div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectActivites
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.activites
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                                , noOptionsMsg = "Aucune activité n'a été trouvée"
                                , newOption = Just identity
                                }
                        , infos.activites
                            |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectLocalisations
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.localisations
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                , newOption = Just identity
                                }
                        , infos.localisations
                            |> List.map (\localisation -> DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , div []
                    [ div [ class "flex flex-col gap-4" ]
                        [ selectMots
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = infos.mots
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                , newOption = Just identity
                                }
                        , infos.mots
                            |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , DSFR.Input.new { value = infos.autre, label = span [ Typo.textBold ] [ text "Commentaires" ], onInput = UpdatedAutre, name = "autre" }
                    |> DSFR.Input.textArea (Just 4)
                    |> DSFR.Input.view
                , case saveEnterpriseInfoRequest of
                    RD.NotAsked ->
                        nothing

                    RD.Loading ->
                        nothing

                    RD.Failure _ ->
                        DSFR.Alert.small { title = Nothing, description = "Une erreur s'est produite" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.error

                    RD.Success _ ->
                        DSFR.Alert.small { title = Nothing, description = "Modifications enregistrées\u{00A0}!" }
                            |> DSFR.Alert.alert Nothing DSFR.Alert.success
                , [ DSFR.Button.new { onClick = Nothing, label = "Valider" }
                        |> DSFR.Button.withDisabled (noDifference || saveEnterpriseInfoRequest == RD.Loading)
                        |> DSFR.Button.submit
                  , DSFR.Button.new { onClick = Just CancelEditEntrepriseInfo, label = "Annuler" }
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withDisabled (saveEnterpriseInfoRequest == RD.Loading)
                  ]
                    |> DSFR.Button.group
                    |> DSFR.Button.inline
                    |> DSFR.Button.alignedRightInverted
                    |> DSFR.Button.viewGroup
                ]


fetchEntreprise : String -> Cmd Msg
fetchEntreprise siret =
    Http.get
        { url = Api.getEntreprise siret
        , expect =
            Http.expectJson
                (RD.fromResult
                    >> ReceivedEntreprise
                )
                decodeEntrepriseAugmented
        }


decodeEntrepriseAugmented : Decode.Decoder EntrepriseAugmented
decodeEntrepriseAugmented =
    Decode.succeed EntrepriseAugmented
        |> andMap (Decode.field "entreprise" Data.Entreprise.decodeEntreprise)
        |> andMap (optionalNullableField "entite" Data.Entite.decodeEntite)
        |> andMap (optionalNullableField "fiche" Data.Fiche.decodeFiche)


createEchange : String -> UI.Echange.EchangeInput -> Cmd Msg
createEchange siret echangeInput =
    case UI.Echange.echangeInputToEchange echangeInput of
        Err _ ->
            Cmd.none

        Ok echange ->
            Http.post
                { url = Api.createEchange (Just siret) Nothing
                , body =
                    echange
                        |> Data.Echange.encodeEchange "entreprise"
                        |> Http.jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) decodeEntrepriseAugmented
                }


createRappel : String -> UI.Rappel.RappelInput -> Cmd Msg
createRappel siret rappelInput =
    case UI.Rappel.rappelInputToRappel rappelInput of
        Err _ ->
            Cmd.none

        Ok rappel ->
            Http.post
                { url = Api.createRappel (Just siret) Nothing
                , body = rappel |> Data.Rappel.encodeRappel "entreprise" |> Http.jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) decodeEntrepriseAugmented
                }


createCloture : String -> UI.Cloture.ClotureInput -> Cmd Msg
createCloture siret cloture =
    case UI.Cloture.clotureInputToCloture cloture of
        Err _ ->
            Cmd.none

        Ok clot ->
            Http.post
                { url = Api.createCloture (Just siret) Nothing
                , body = clot |> Data.Demande.encodeCloture "entreprise" |> Http.jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) decodeEntrepriseAugmented
                }


updateEchange : UI.Echange.EchangeInput -> Cmd Msg
updateEchange echangeInput =
    case UI.Echange.echangeInputToEchange echangeInput of
        Err _ ->
            Cmd.none

        Ok echange ->
            let
                jsonBody =
                    echange
                        |> Data.Echange.encodeEchange "entreprise"
                        |> Http.jsonBody
            in
            Http.post
                { url = Api.updateEchange <| Data.Echange.id echange
                , body = jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) decodeEntrepriseAugmented
                }


updateRappel : UI.Rappel.RappelInput -> Cmd Msg
updateRappel rappelInput =
    case UI.Rappel.rappelInputToRappel rappelInput of
        Err _ ->
            Cmd.none

        Ok rappel ->
            let
                jsonBody =
                    Data.Rappel.encodeRappel "entreprise" rappel
                        |> Http.jsonBody
            in
            Http.post
                { url = Api.updateRappel <| Data.Rappel.id rappel
                , body = jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) decodeEntrepriseAugmented
                }


clotureRappel : UI.Rappel.RappelInput -> Cmd Msg
clotureRappel { id, dateCloture } =
    let
        rappel =
            [ ( "dateCloture", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| dateCloture )
            ]
                |> Encode.object

        jsonBody =
            [ ( "rappel", rappel )
            , ( "from", Encode.string "entreprise" )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Http.post
        { url = Api.updateRappel id
        , body = jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) decodeEntrepriseAugmented
        }


createContact : String -> Contact -> Cmd Msg
createContact siret contact =
    Http.post
        { url = Api.createContact (Just siret) Nothing
        , body = contact |> Data.Fiche.encodeContact |> Http.jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedSaveContact) decodeEntrepriseAugmented
        }


editContact : Contact -> Cmd Msg
editContact contact =
    Http.post
        { url = Api.editContact contact.id
        , body =
            [ ( "contact", contact |> Data.Fiche.encodeContact ) ]
                |> Encode.object
                |> Http.jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedSaveContact) decodeEntrepriseAugmented
        }


deleteContact : Contact -> Cmd Msg
deleteContact { id } =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = Api.getContact id
        , body = Http.emptyBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedSaveContact) decodeEntrepriseAugmented
        , timeout = Nothing
        , tracker = Nothing
        }


updateContact : ContactField -> String -> Contact -> Contact
updateContact field value contact =
    case field of
        Fonction ->
            { contact | fonction = value }

        Prenom ->
            { contact | prenom = value }

        Nom ->
            { contact | nom = value }

        Email ->
            { contact | email = value }

        Telephone ->
            { contact | telephone = value }


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = Decode.list <| Decode.field "activite" Decode.string
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = Decode.list <| Decode.field "motCle" Decode.string
    }


updateFiche : String -> EntrepriseInfo -> Effect.Effect Shared.Msg Msg
updateFiche siret { activites, localisations, mots, autre } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "mots", Encode.list Encode.string mots )
            , ( "autre", Encode.string autre )
            ]
                |> (Encode.object >> Http.jsonBody)
    in
    Http.post
        { url = Api.updateEntreprise siret
        , body = jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedSaveEntrepriseInfo) decodeEntrepriseAugmented
        }
        |> Effect.fromCmd
