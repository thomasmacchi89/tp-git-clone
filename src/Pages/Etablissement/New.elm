module Pages.Etablissement.New exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, h1, h2, span, text)
import Api
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.SearchBar
import DSFR.Typography as Typo exposing (link)
import Data.Entreprise exposing (Entreprise)
import Data.Fiche as Fiche exposing (Fiche)
import Dict exposing (Dict)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import RemoteData as RD
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Entite
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element <|
        { init = init shared
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }


type alias Model =
    { recherche : String
    , resultEntreprises : RD.WebData ResultEntreprise
    , entreprise : Maybe Entreprise
    , validationFiche : RD.RemoteData FormErrors ()
    , enregistrementFiche : RD.WebData Fiche
    }


init : Shared.Shared -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ _ =
    ( { recherche = ""
      , resultEntreprises = RD.NotAsked
      , entreprise = Nothing
      , validationFiche = RD.NotAsked
      , enregistrementFiche = RD.NotAsked
      }
    , Effect.none
    )
        |> Shared.pageChangeEffects


type Msg
    = UpdatedRecherche String
    | RequestedRecherche
    | ReceivedRecherche (RD.WebData ResultEntreprise)
    | RequestedCreationFiche
    | ReceivedCreationFiche (RD.WebData Fiche)


type ResultEntreprise
    = New Entreprise
    | Existing String


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        UpdatedRecherche recherche ->
            ( { model | recherche = recherche }, Effect.none )

        RequestedRecherche ->
            ( { model | resultEntreprises = RD.Loading, entreprise = Nothing }, searchEntreprise model.recherche )

        ReceivedRecherche resultEntreprises ->
            let
                entreprise =
                    resultEntreprises
                        |> RD.toMaybe
                        |> Maybe.andThen
                            (\res ->
                                case res of
                                    New e ->
                                        Just e

                                    Existing _ ->
                                        Nothing
                            )
            in
            ( { model | resultEntreprises = resultEntreprises, entreprise = entreprise }, Effect.none )

        ReceivedCreationFiche ((RD.Success fiche) as enregistrementFiche) ->
            ( { model | enregistrementFiche = enregistrementFiche }
            , Effect.fromShared <|
                Shared.Navigate <|
                    Route.Etablissement <|
                        Maybe.withDefault "" <|
                            -- TODO
                            Maybe.map .siret
                            <|
                                Fiche.getEntreprise <|
                                    fiche
            )

        ReceivedCreationFiche enregistrementFiche ->
            ( { model | enregistrementFiche = enregistrementFiche }, Effect.none )

        RequestedCreationFiche ->
            let
                parsedFiche =
                    validateFiche model

                ( enregistrementFiche, effect ) =
                    case parsedFiche of
                        RD.Success () ->
                            ( RD.Loading, createFiche model )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationFiche = parsedFiche, enregistrementFiche = enregistrementFiche }, effect )


type alias FormErrors =
    { global : Maybe String
    , fields : Dict String (List String)
    }


validateFiche : Model -> RD.RemoteData FormErrors ()
validateFiche { entreprise } =
    case entreprise of
        Nothing ->
            RD.Failure { global = Just "Aucun établissement sélectionné", fields = Dict.empty }

        Just _ ->
            RD.Success ()


createFiche : Model -> Effect.Effect Shared.Msg Msg
createFiche { entreprise } =
    let
        encodedFiche =
            entreprise
                |> Maybe.map
                    (Http.jsonBody
                        << Encode.object
                        << (\{ siret } ->
                                [ ( "ficheType", Encode.string "PM" )
                                , ( "entreprise", Encode.object [ ( "siret", Encode.string siret ) ] )
                                ]
                           )
                    )
    in
    case encodedFiche of
        Nothing ->
            Effect.none

        Just jsonBody ->
            Http.post
                { url = Api.createFiche
                , body = jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedCreationFiche) Fiche.decodeFiche
                }
                |> Effect.fromCmd


searchEntreprise : String -> Effect.Effect Shared.Msg Msg
searchEntreprise recherche =
    Effect.fromCmd <|
        Http.get
            { url = Api.searchInseeSiret recherche
            , expect =
                Http.expectJson (RD.fromResult >> ReceivedRecherche) <|
                    decodeResultEntreprises
            }


decodeResultEntreprises : Decoder ResultEntreprise
decodeResultEntreprises =
    Decode.field "existing" Decode.bool
        |> Decode.andThen
            (\existing ->
                Decode.field "entreprise" Data.Entreprise.decodeEntreprise
                    |> Decode.map
                        (if existing then
                            .siret >> Existing

                         else
                            New
                        )
            )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Ajout d'un Établissement exogène"
    , body = UI.Layout.lazyBody body model
    , route = Route.EtablissementNew
    }


ficheTypeForm : Model -> Html Msg
ficheTypeForm model =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Rechercher l'établissement" ] ]
        , div
            [ class "flex flex-col gap-8" ]
            [ div
                [ class "flex flex-col", DSFR.Grid.colSm7, DSFR.Grid.col12 ]
                [ DSFR.SearchBar.searchBar
                    { submitMsg = RequestedRecherche
                    , buttonLabel = "Rechercher"
                    , inputMsg = UpdatedRecherche
                    , inputLabel = ""
                    , inputPlaceholder = Nothing
                    , inputId = "etablissement-exogene-siret-search"
                    , inputValue = model.recherche
                    , hints =
                        "Un SIRET (14 chiffres)"
                            |> text
                            |> List.singleton
                    , fullLabel = Nothing
                    }
                ]
            , div []
                [ text "Vous ne connaissez pas le SIRET de l'établissement, recherchez-le\u{00A0}: "
                , Typo.externalLink "https://annuaire-entreprises.data.gouv.fr/" [] [ text "https://annuaire-entreprises.data.gouv.fr/" ]
                ]
            , div [ DSFR.Grid.col7 ]
                [ viewEntrepriseList model
                ]
            ]
        ]


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [] [ text "Ajouter un Établissement exogène" ]
        , formWithListeners [ Events.onSubmit <| RequestedCreationFiche, class "flex flex-col gap-8" ]
            [ ficheTypeForm model
            , footer model
            ]
        ]


viewEntrepriseList : Model -> Html Msg
viewEntrepriseList { resultEntreprises, entreprise } =
    case resultEntreprises of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            text "Recherche en cours..."

        RD.Failure _ ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , div []
                    [ text "Une erreur s'est produite"
                    ]
                ]

        RD.Success (Existing siret) ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , span [ class "font-bold" ]
                    [ text "Cet établissement existe déjà sur le territoire."
                    , link
                        (Route.toUrl <|
                            Route.Etablissement <|
                                siret
                        )
                        []
                        [ text "Voir l'établissement." ]
                    ]
                ]

        RD.Success _ ->
            div [ class "flex flex-col gap-2" ]
                [ div []
                    [ text "Résultat\u{00A0}:"
                    ]
                , entreprise
                    |> Maybe.map viewEntrepriseCard
                    |> Maybe.withDefault (text "Aucun résultat")
                ]


viewEntrepriseCard : Entreprise -> Html msg
viewEntrepriseCard { nom, siren, siret, adresse, etatAdministratif } =
    div [ class "flex flex-col" ]
        [ div [ class "font-bold flex flex-row gap-4 items-baseline" ]
            [ text nom
            , etatAdministratif
                |> Just
                |> UI.Entite.badgeInactif
            ]
        , div []
            [ text "SIREN\u{00A0}: "
            , span [ class "font-bold" ] [ text siren ]
            ]
        , div []
            [ text "SIRET\u{00A0}: "
            , span [ class "font-bold" ] [ text siret ]
            ]
        , div []
            [ text "Adresse\u{00A0}: "
            , span [ class "font-bold" ] [ text adresse ]
            ]
        ]


footer : Model -> Html Msg
footer model =
    let
        label =
            case model.enregistrementFiche of
                RD.NotAsked ->
                    "Ajouter l'établissement"

                RD.Loading ->
                    "Ajout en cours..."

                RD.Failure _ ->
                    "Ajouter l'établissement"

                RD.Success _ ->
                    "Ajouter l'établissement"

        isButtonDisabled =
            model.enregistrementFiche == RD.Loading || model.entreprise == Nothing
    in
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationFiche of
                RD.Failure { global } ->
                    case global of
                        Nothing ->
                            nothing

                        Just error ->
                            DSFR.Alert.small { title = Nothing, description = error }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementFiche of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = label }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled isButtonDisabled
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Etablissements Nothing)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
