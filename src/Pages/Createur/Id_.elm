module Pages.Createur.Id_ exposing (Model, Msg(..), page)

import Accessibility exposing (Html, div, h1, h2, h3, h4, hr, span, sup, text)
import Api
import Api.EntityId exposing (EntityId, entityIdToString)
import DSFR.Accordion
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Business
import DSFR.Icons.Design
import DSFR.Icons.Device
import DSFR.Icons.System
import DSFR.Icons.User
import DSFR.Modal
import DSFR.Radio
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Cloture exposing (Cloture)
import Data.Demande
import Data.Echange exposing (Echange)
import Data.Entite exposing (EntiteType(..))
import Data.Fiche exposing (Fiche, FicheId)
import Data.Particulier exposing (Particulier)
import Data.Rappel exposing (Rappel)
import Date exposing (Date)
import Dict
import Effect
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Extra exposing (nothing, viewIf)
import Html.Keyed as Keyed
import Html.Lazy as Lazy
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (displayPhone, withEmptyAs)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import Time exposing (Posix, Zone)
import UI.Cloture
import UI.Echange
import UI.Entite
import UI.Layout
import UI.Rappel
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (EntityId FicheId) Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element
        { view = view shared
        , init = init shared
        , update = update
        , subscriptions = \_ -> Sub.none
        }


type alias Model =
    { fiche : WebData Fiche
    , ficheId : EntityId FicheId
    , suiviAction : SuiviAction
    , saveSuiviRequest : WebData Fiche
    , rappelMode : RappelMode
    , today : Date
    , deleteFiche : Maybe (WebData ())
    }


type SuiviAction
    = NoSuivi
    | NewSuivi SuiviType
    | EditSuivi SuiviType
    | DeleteSuivi SuiviType


type SuiviType
    = SuiviEchange UI.Echange.EchangeInput
    | SuiviRappel UI.Rappel.RappelInput
    | SuiviCloture UI.Cloture.ClotureInput


type RappelMode
    = RappelsActifs
    | RappelsClotures


init : Shared.Shared -> EntityId FicheId -> ( Model, Effect.Effect Shared.Msg Msg )
init shared id =
    ( { fiche = RD.Loading
      , ficheId = id
      , suiviAction = NoSuivi
      , saveSuiviRequest = RD.NotAsked
      , rappelMode = RappelsActifs
      , today = Date.fromPosix shared.timezone shared.now
      , deleteFiche = Nothing
      }
    , Effect.fromCmd <| fetchFiche id
    )
        |> Shared.pageChangeEffects


fetchFiche : EntityId FicheId -> Cmd Msg
fetchFiche id =
    Http.get
        { url = Api.getFiche id
        , expect = Http.expectJson (RD.fromResult >> ReceivedFiche) Data.Fiche.decodeFiche
        }


type Msg
    = ReceivedFiche (WebData Fiche)
    | SetSuiviAction SuiviAction
    | UpdatedEchangeForm UI.Echange.Field String
    | SelectedTypeEchange Data.Echange.TypeEchange
    | ToggledTypeDemande Data.Demande.TypeDemande Bool
    | UpdatedRappelForm UI.Rappel.Field String
    | UpdatedClotureDemande Data.Demande.Demande
    | UpdatedClotureForm String
    | ConfirmSuiviAction
    | CancelSuiviAction
    | ReceivedSaveSuivi (WebData Fiche)
    | SetRappelMode RappelMode
    | ClickedDelete
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData ())
    | NoOp
    | SharedMsg Shared.Msg


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        ReceivedFiche response ->
            ( { model | fiche = response }, Effect.none )

        SetSuiviAction suiviAction ->
            { model | suiviAction = suiviAction } |> Effect.withNone

        UpdatedEchangeForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model | suiviAction = NewSuivi <| SuiviEchange <| UI.Echange.update field value <| echange }
                        |> Effect.withNone

                EditSuivi (SuiviEchange echange) ->
                    { model | suiviAction = EditSuivi <| SuiviEchange <| UI.Echange.update field value <| echange }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        SelectedTypeEchange typeEchange ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model | suiviAction = NewSuivi <| SuiviEchange <| (\ech -> { ech | typeEchange = typeEchange }) <| echange }
                        |> Effect.withNone

                EditSuivi (SuiviEchange echange) ->
                    { model | suiviAction = EditSuivi <| SuiviEchange <| (\ech -> { ech | typeEchange = typeEchange }) <| echange }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        ToggledTypeDemande typesDemande on ->
            case model.suiviAction of
                NewSuivi (SuiviEchange echange) ->
                    { model
                        | suiviAction =
                            NewSuivi <|
                                SuiviEchange <|
                                    (\ech ->
                                        { ech
                                            | typesDemande =
                                                ech.typesDemande
                                                    |> (\ts ->
                                                            if on then
                                                                if List.member typesDemande ts then
                                                                    ts

                                                                else
                                                                    ts |> List.reverse |> (::) typesDemande |> List.reverse

                                                            else
                                                                List.filter ((/=) typesDemande) ts
                                                       )
                                        }
                                    )
                                    <|
                                        echange
                    }
                        |> Effect.withNone

                EditSuivi (SuiviEchange echange) ->
                    { model
                        | suiviAction =
                            EditSuivi <|
                                SuiviEchange <|
                                    (\ech ->
                                        { ech
                                            | typesDemande =
                                                ech.typesDemande
                                                    |> (\ts ->
                                                            if on then
                                                                if List.member typesDemande ts then
                                                                    ts

                                                                else
                                                                    ts |> List.reverse |> (::) typesDemande |> List.reverse

                                                            else
                                                                List.filter ((/=) typesDemande) ts
                                                       )
                                        }
                                    )
                                    <|
                                        echange
                    }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedRappelForm field value ->
            case model.suiviAction of
                NewSuivi (SuiviRappel rappel) ->
                    { model | suiviAction = NewSuivi <| SuiviRappel <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone

                EditSuivi (SuiviRappel rappel) ->
                    { model | suiviAction = EditSuivi <| SuiviRappel <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedClotureDemande demande ->
            case model.suiviAction of
                NewSuivi (SuiviCloture cloture) ->
                    { model
                        | suiviAction =
                            NewSuivi <|
                                SuiviCloture <|
                                    { cloture
                                        | demande = demande
                                    }
                    }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        UpdatedClotureForm value ->
            case model.suiviAction of
                NewSuivi (SuiviCloture cloture) ->
                    { model | suiviAction = NewSuivi <| SuiviCloture <| UI.Cloture.update value <| cloture }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        ConfirmSuiviAction ->
            case model.saveSuiviRequest of
                RD.Loading ->
                    model |> Effect.withNone

                _ ->
                    case model.suiviAction of
                        NoSuivi ->
                            model |> Effect.withNone

                        NewSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createEchange model.ficheId echange)

                        NewSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createRappel model.ficheId rappel)

                        NewSuivi (SuiviCloture cloture) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (createCloture model.ficheId cloture)

                        EditSuivi (SuiviEchange echange) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateEchange echange)

                        EditSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (updateRappel rappel)

                        EditSuivi (SuiviCloture _) ->
                            model |> Effect.withNone

                        DeleteSuivi (SuiviEchange _) ->
                            model |> Effect.withNone

                        DeleteSuivi (SuiviRappel rappel) ->
                            { model | saveSuiviRequest = RD.Loading }
                                |> Effect.withCmd (clotureRappel rappel)

                        DeleteSuivi (SuiviCloture _) ->
                            model |> Effect.withNone

        CancelSuiviAction ->
            { model | saveSuiviRequest = RD.NotAsked, suiviAction = NoSuivi }
                |> Effect.withNone

        ReceivedSaveSuivi response ->
            case model.saveSuiviRequest of
                RD.Loading ->
                    case model.suiviAction of
                        NoSuivi ->
                            model
                                |> Effect.withNone

                        _ ->
                            case response of
                                RD.Success _ ->
                                    { model | saveSuiviRequest = response, fiche = response, suiviAction = NoSuivi }
                                        |> Effect.withNone

                                _ ->
                                    { model | saveSuiviRequest = response }
                                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        SetRappelMode rappelMode ->
            { model | rappelMode = rappelMode } |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ClickedDelete ->
            { model | deleteFiche = Just RD.NotAsked }
                |> Effect.withNone

        CanceledDelete ->
            { model | deleteFiche = Nothing }
                |> Effect.withNone

        ConfirmedDelete ->
            case model.fiche of
                RD.Success fiche ->
                    { model | deleteFiche = Just RD.Loading }
                        |> Effect.withCmd (deleteFiche fiche)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedDelete response ->
            { model | deleteFiche = Just response }
                |> Effect.withNone


deleteFiche : Fiche -> Cmd Msg
deleteFiche fiche =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = Api.getFiche <| Data.Fiche.id fiche
        , body = Http.emptyBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedDelete) (Decode.succeed ())
        , timeout = Nothing
        , tracker = Nothing
        }


createEchange : EntityId FicheId -> UI.Echange.EchangeInput -> Cmd Msg
createEchange ficheId echangeInput =
    case UI.Echange.echangeInputToEchange echangeInput of
        Err _ ->
            Cmd.none

        Ok echange ->
            Http.post
                { url = Api.createEchange Nothing (Just ficheId)
                , body =
                    echange
                        |> Data.Echange.encodeEchange "createur"
                        |> Http.jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) Data.Fiche.decodeFiche
                }


createRappel : EntityId FicheId -> UI.Rappel.RappelInput -> Cmd Msg
createRappel ficheId rappelInput =
    case UI.Rappel.rappelInputToRappel rappelInput of
        Err _ ->
            Cmd.none

        Ok rappel ->
            Http.post
                { url = Api.createRappel Nothing (Just ficheId)
                , body = rappel |> Data.Rappel.encodeRappel "createur" |> Http.jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) Data.Fiche.decodeFiche
                }


createCloture : EntityId FicheId -> UI.Cloture.ClotureInput -> Cmd Msg
createCloture ficheId cloture =
    case UI.Cloture.clotureInputToCloture cloture of
        Err _ ->
            Cmd.none

        Ok clot ->
            Http.post
                { url = Api.createCloture Nothing (Just ficheId)
                , body = clot |> Data.Demande.encodeCloture "createur" |> Http.jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) Data.Fiche.decodeFiche
                }


updateEchange : UI.Echange.EchangeInput -> Cmd Msg
updateEchange echangeInput =
    case UI.Echange.echangeInputToEchange echangeInput of
        Err _ ->
            Cmd.none

        Ok echange ->
            let
                jsonBody =
                    echange
                        |> Data.Echange.encodeEchange "createur"
                        |> Http.jsonBody
            in
            Http.post
                { url = Api.updateEchange <| Data.Echange.id echange
                , body = jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) Data.Fiche.decodeFiche
                }


updateRappel : UI.Rappel.RappelInput -> Cmd Msg
updateRappel rappelInput =
    case UI.Rappel.rappelInputToRappel rappelInput of
        Err _ ->
            Cmd.none

        Ok rappel ->
            let
                jsonBody =
                    Data.Rappel.encodeRappel "createur" rappel
                        |> Http.jsonBody
            in
            Http.post
                { url = Api.updateRappel <| Data.Rappel.id rappel
                , body = jsonBody
                , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) Data.Fiche.decodeFiche
                }


clotureRappel : UI.Rappel.RappelInput -> Cmd Msg
clotureRappel { id, dateCloture } =
    let
        rappel =
            [ ( "dateCloture", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| dateCloture )
            ]
                |> Encode.object

        jsonBody =
            [ ( "rappel", rappel )
            , ( "from", Encode.string "createur" )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Http.post
        { url = Api.updateRappel id
        , body = jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedSaveSuivi) Data.Fiche.decodeFiche
        }


view : Shared.Shared -> Model -> View Msg
view { now, timezone } model =
    let
        title =
            case model.fiche of
                RD.Success fiche ->
                    "Fiche de " ++ Data.Fiche.nom fiche

                _ ->
                    "Fiche"
    in
    { title = UI.Layout.pageTitle <| title
    , body = body timezone now model
    , route = Route.Createur <| model.ficheId
    }


body : Zone -> Posix -> Model -> List (Html Msg)
body timezone now model =
    [ div [ class "flex flex-col p-2" ]
        [ div [ class "flex flex-row justify-between" ]
            [ DSFR.Button.new
                { label = "Retour"
                , onClick = Just <| SharedMsg <| Shared.goBack
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.arrowLeftLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withAttrs [ class "text-underline" ]
                |> DSFR.Button.view
            , case model.fiche of
                RD.Success { auteurModification, dateModification } ->
                    let
                        auteur =
                            if auteurModification == "" then
                                ""

                            else
                                " par " ++ auteurModification
                    in
                    div [ Typo.textSm, Typo.textSpectralRegular, class "flex flex-col !mb-0 p-2 text-right" ]
                        [ div [] [ text <| "Dernière modification de la fiche le " ++ Lib.Date.formatShort timezone dateModification ++ auteur ]
                        ]

                _ ->
                    nothing
            ]
        , case model.fiche of
            RD.Loading ->
                div [ class "p-6 sm:p-8 fr-card--white" ]
                    [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                    , "Chargement en cours..."
                        |> text
                    ]

            RD.Success ({ entite, demandes } as fiche) ->
                let
                    { type_ } =
                        entite
                in
                div [ class "flex flex-col gap-4 p-2" ]
                    [ viewDeleteModal fiche model.deleteFiche
                    , viewDemandesModal demandes model
                    , div [ class "p-4 sm:p-8 fr-card--white" ]
                        [ case type_ of
                            EntiteE _ ->
                                nothing

                            EntiteP particulier ->
                                Lazy.lazy3 viewParticulier model.ficheId fiche particulier
                        ]
                    , div [ class "p-4 sm:p-8 fr-card--white" ]
                        [ Lazy.lazy4 viewSuivis timezone now model fiche ]
                    ]

            _ ->
                div [ class "p-4 sm:p-8 fr-card--white" ]
                    [ "Une erreur s'est produite, veuillez recharger la page."
                        |> text
                    ]
        ]
    ]


viewDemandesModal : List Data.Demande.Demande -> Model -> Html Msg
viewDemandesModal demandes model =
    case model.suiviAction of
        NoSuivi ->
            nothing

        NewSuivi suiviType ->
            viewSuiviForm True model.today model.saveSuiviRequest demandes suiviType

        EditSuivi suiviType ->
            viewSuiviForm False model.today model.saveSuiviRequest demandes suiviType

        DeleteSuivi suiviType ->
            viewDeleteSuiviConfirmation model.saveSuiviRequest suiviType


viewDeleteModal : Fiche -> Maybe (WebData ()) -> Html Msg
viewDeleteModal fiche deleteRequest =
    case deleteRequest of
        Nothing ->
            nothing

        Just request ->
            let
                modalBody =
                    case request of
                        RD.NotAsked ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Êtes-vous sûr(e) de vouloir supprimer la fiche\u{00A0}?"
                                    , description = Just "Cette opération est définitive et irréversible."
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.warning
                                , [ DSFR.Button.new { label = "Je veux vraiment supprimer la fiche", onClick = Just ConfirmedDelete }
                                        |> DSFR.Button.leftIcon DSFR.Icons.System.deleteFill
                                  , DSFR.Button.new { label = "Annuler", onClick = Just CanceledDelete }
                                        |> DSFR.Button.tertiaryNoOutline
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.iconsLeft
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Loading ->
                            DSFR.Alert.medium
                                { title = "Suppression en cours..."
                                , description = Nothing
                                }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                        RD.Failure _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "Une erreur s'est produite, veuillez fermer la fenêtre et réessayer."
                                    , description = Nothing
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                                , [ DSFR.Button.new { label = "OK", onClick = Just CanceledDelete }
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]

                        RD.Success () ->
                            div [ class "flex flex-col gap-4" ]
                                [ DSFR.Alert.medium
                                    { title = "La fiche a été supprimée avec succès\u{00A0}!"
                                    , description = Just "Cliquez sur OK pour retourner à la liste des fiches."
                                    }
                                    |> DSFR.Alert.alert Nothing DSFR.Alert.success
                                , [ DSFR.Button.new { label = "OK", onClick = Nothing }
                                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Createurs Nothing)
                                  ]
                                    |> DSFR.Button.group
                                    |> DSFR.Button.inline
                                    |> DSFR.Button.alignedRightInverted
                                    |> DSFR.Button.viewGroup
                                ]
            in
            DSFR.Modal.view
                { id = "suppression"
                , label = "suppression"
                , openMsg = NoOp
                , closeMsg = Nothing
                , title = text <| "Supprimer la fiche " ++ Data.Fiche.nom fiche ++ "\u{00A0}?"
                , opened = True
                }
                modalBody
                Nothing
                |> Tuple.first


viewSuiviForm : Bool -> Date -> WebData Fiche -> List Data.Demande.Demande -> SuiviType -> Html Msg
viewSuiviForm new today saveSuiviRequest demandes suiviType =
    DSFR.Modal.view
        { id = "suivi"
        , label = "suivi"
        , openMsg = NoOp
        , closeMsg = Just CancelSuiviAction
        , title =
            text <|
                (if new then
                    "Ajouter"

                 else
                    "Modifier"
                )
                    ++ " un élément de suivi"
        , opened = True
        }
        (viewSuiviBody saveSuiviRequest new today demandes suiviType)
        Nothing
        |> Tuple.first


viewDeleteSuiviConfirmation : WebData Fiche -> SuiviType -> Html Msg
viewDeleteSuiviConfirmation saveSuiviRequest suiviType =
    DSFR.Modal.view
        { id = "delete"
        , label = "delete"
        , openMsg = NoOp
        , closeMsg = Just CancelSuiviAction
        , title =
            text <|
                case suiviType of
                    SuiviEchange _ ->
                        "Supprimer un échange"

                    SuiviRappel rappel ->
                        if rappel.dateCloture /= Nothing then
                            "Clôturer un rappel"

                        else
                            "Réouvrir un rappel"

                    SuiviCloture _ ->
                        ""
        , opened = True
        }
        (viewDeleteSuiviBody saveSuiviRequest suiviType)
        Nothing
        |> Tuple.first


viewSuiviBody : WebData Fiche -> Bool -> Date -> List Data.Demande.Demande -> SuiviType -> Html Msg
viewSuiviBody saveSuiviRequest new today demandes suiviType =
    let
        openDemandes =
            demandes
                |> List.filter (Data.Demande.cloture >> not)
    in
    div []
        [ if new then
            div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ]
                    [ DSFR.Radio.group
                        { id = "suivi-type-radio"
                        , options = [ "Ajouter un échange", "Ajouter un rappel", "Clôturer une demande" ]
                        , current =
                            case suiviType of
                                SuiviEchange _ ->
                                    Just "Ajouter un échange"

                                SuiviRappel _ ->
                                    Just "Ajouter un rappel"

                                SuiviCloture _ ->
                                    Just "Clôturer une demande"
                        , toLabel = text
                        , toId = String.replace " " "-"
                        , msg =
                            \s ->
                                case s of
                                    "Ajouter un échange" ->
                                        SetSuiviAction <| NewSuivi <| SuiviEchange <| UI.Echange.default "" today

                                    "Ajouter un rappel" ->
                                        SetSuiviAction <| NewSuivi <| SuiviRappel <| UI.Rappel.default "" today

                                    "Clôturer une demande" ->
                                        case demandes of
                                            [] ->
                                                SetSuiviAction NoSuivi

                                            demande :: _ ->
                                                SetSuiviAction <| NewSuivi <| SuiviCloture <| UI.Cloture.default demande

                                    _ ->
                                        SetSuiviAction NoSuivi
                        , legend = nothing
                        }
                        |> DSFR.Radio.withDisabledOption
                            (\opt ->
                                case opt of
                                    "Clôturer une demande" ->
                                        List.length demandes == 0

                                    _ ->
                                        False
                            )
                        |> DSFR.Radio.inline
                        |> DSFR.Radio.view
                    ]
                ]

          else
            nothing
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col, class "flex flex-col gap-4" ] <|
                case suiviType of
                    SuiviEchange echange ->
                        [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue" ] [ text "Qualification de l'échange" ] ]
                        , UI.Echange.form (validateEchange echange) SelectedTypeEchange ToggledTypeDemande UpdatedEchangeForm echange
                        ]

                    SuiviRappel rappel ->
                        [ div [ class "flex font-bold" ]
                            [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue" ]
                                [ text <|
                                    (if new then
                                        "Création"

                                     else
                                        "Modification"
                                    )
                                        ++ " d'un rappel"
                                ]
                            ]
                        , UI.Rappel.form UpdatedRappelForm rappel
                        ]

                    SuiviCloture cloture ->
                        if List.length openDemandes == 0 then
                            [ div [ class "italic" ] [ text "Aucune demande en cours" ] ]

                        else
                            [ div [ class "flex font-bold" ]
                                [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue" ]
                                    [ text "Demande"
                                    , text <|
                                        if List.length openDemandes > 1 then
                                            "s"

                                        else
                                            ""
                                    , text " en cours"
                                    ]
                                ]
                            , UI.Cloture.form UpdatedClotureDemande UpdatedClotureForm openDemandes cloture
                            ]
            ]
        , div [ class "py-4" ] [ viewSuiviFooter saveSuiviRequest suiviType ]
        ]


validateEchange : UI.Echange.EchangeInput -> Maybe String
validateEchange echange =
    case UI.Echange.echangeInputToEchange echange of
        Err errs ->
            errs
                |> Dict.get "type"
                |> Maybe.map (String.join ", ")

        _ ->
            Nothing


viewSuiviFooter : WebData Fiche -> SuiviType -> Html Msg
viewSuiviFooter saveSuiviRequest suiviType =
    let
        disabled =
            saveSuiviRequest
                == RD.Loading
                || (case suiviType of
                        SuiviEchange echange ->
                            not <| UI.Echange.isValid echange

                        SuiviRappel rappel ->
                            not <| UI.Rappel.isValid rappel

                        SuiviCloture _ ->
                            False
                   )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewDeleteSuiviBody : WebData Fiche -> SuiviType -> Html Msg
viewDeleteSuiviBody saveSuiviRequest suiviType =
    div [ DSFR.Grid.gridRow ]
        [ div [ DSFR.Grid.col, class "flex flex-col gap-4" ] <|
            case suiviType of
                SuiviRappel rappel ->
                    let
                        action =
                            if rappel.dateCloture /= Nothing then
                                "clôturer"

                            else
                                "réouvrir"
                    in
                    [ text <| "Êtes-vous sûr(e) de vouloir " ++ action ++ " ce rappel\u{00A0}?" ]

                _ ->
                    []
        , viewDeleteSuiviFooter saveSuiviRequest
        ]


viewDeleteSuiviFooter : WebData Fiche -> Html Msg
viewDeleteSuiviFooter saveSuiviRequest =
    let
        disabled =
            saveSuiviRequest == RD.Loading
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmSuiviAction, label = "Confirmer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelSuiviAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewSuivis : Zone -> Posix -> Model -> Fiche -> Html Msg
viewSuivis timezone now model { echanges, rappels, clotures } =
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-row justify-between" ]
            [ h3 [ Typo.fr_h4 ] [ text "Suivi de la relation" ]
            , DSFR.Button.new
                { label = "Ajouter un élément de suivi"
                , onClick = Just <| SetSuiviAction <| NewSuivi <| SuiviEchange <| UI.Echange.default "" model.today
                }
                |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled (model.suiviAction /= NoSuivi)
                |> DSFR.Button.view
            ]
        , viewRappels timezone now model.rappelMode rappels
        , viewEchangesAndClotures timezone <| toEchangesAndClotures echanges clotures
        , div [] [ text "\u{00A0}" ] -- layout hack
        , hr [ class "!pb-1" ] []
        ]


type EchangeAndCloture
    = Ech Echange
    | Clot Cloture


toEchangesAndClotures : List Echange -> List Cloture -> List EchangeAndCloture
toEchangesAndClotures echanges clotures =
    List.map Ech echanges ++ List.map Clot clotures


viewRappels : Zone -> Posix -> RappelMode -> List Rappel -> Html Msg
viewRappels timezone now rappelMode rappels =
    let
        shownRappels =
            rappels
                |> List.filter
                    (Data.Rappel.dateCloture
                        >> (case rappelMode of
                                RappelsActifs ->
                                    (==) Nothing

                                RappelsClotures ->
                                    (/=) Nothing
                           )
                    )
    in
    div [ class "flex flex-col gap-2" ]
        (div [ class "flex flex-col" ]
            [ div [ class "flex flex-row justify-between items-center" ]
                [ h4 [ Typo.fr_h6, class "!mb-1" ]
                    [ text "Rappels"
                    , text <|
                        case rappelMode of
                            RappelsActifs ->
                                " actifs"

                            RappelsClotures ->
                                " clôturés"
                    ]
                , case rappelMode of
                    RappelsActifs ->
                        DSFR.Button.new { label = "voir les rappels clôturés", onClick = Just <| SetRappelMode RappelsClotures }
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ class "!text-black underline" ]
                            |> DSFR.Button.view

                    RappelsClotures ->
                        DSFR.Button.new { label = "voir les rappels actifs", onClick = Just <| SetRappelMode RappelsActifs }
                            |> DSFR.Button.tertiaryNoOutline
                            |> DSFR.Button.withAttrs [ class "!text-black underline" ]
                            |> DSFR.Button.view
                ]
            , hr [ class "!pb-1" ] []
            ]
            :: (if List.length shownRappels == 0 then
                    span [ class "text-center italic" ]
                        [ text "Aucun rappel "
                        , text <|
                            case rappelMode of
                                RappelsActifs ->
                                    "actif"

                                RappelsClotures ->
                                    "clôturé"
                        ]

                else
                    div [ class "border-transparent" ]
                        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                            [ div [ DSFR.Grid.col1 ] [ nothing ]
                            , div [ DSFR.Grid.col2, class "font-bold" ]
                                [ text <|
                                    case rappelMode of
                                        RappelsClotures ->
                                            "Date de clôture"

                                        RappelsActifs ->
                                            "Date d'échéance"
                                ]
                            , div [ DSFR.Grid.col7, class "font-bold" ] [ text "Titre" ]
                            , div [ DSFR.Grid.col2, class "font-bold text-right !pr-[1.5rem]" ] [ text "Actions" ]
                            ]
                        ]
               )
            :: (List.map (viewRappel timezone now rappelMode) <|
                    List.sortWith sortRappelsOlderFirst <|
                        shownRappels
               )
        )


sortRappelsOlderFirst : Rappel -> Rappel -> Order
sortRappelsOlderFirst r1 r2 =
    case ( Data.Rappel.dateCloture r1, Data.Rappel.dateCloture r2 ) of
        ( Nothing, Nothing ) ->
            let
                comparison =
                    Lib.Date.sortOlderDateFirst Data.Rappel.date r1 r2
            in
            case comparison of
                EQ ->
                    Api.EntityId.compare (Data.Rappel.id r1) (Data.Rappel.id r2)

                _ ->
                    comparison

        ( Just d1, Just d2 ) ->
            let
                comparison =
                    Date.compare d1 d2
            in
            case comparison of
                EQ ->
                    Api.EntityId.compare (Data.Rappel.id r1) (Data.Rappel.id r2)

                _ ->
                    comparison

        _ ->
            EQ


viewRappel : Zone -> Posix -> RappelMode -> Rappel -> Html Msg
viewRappel timezone now rappelMode rappel =
    let
        isLate =
            rappel
                |> Data.Rappel.date
                |> Lib.Date.firstDateIsAfterSecondDate (Date.fromPosix timezone now)

        ( icon, color ) =
            if isLate && (Nothing == Data.Rappel.dateCloture rappel) then
                ( div [ class "fr-text-default--error text-center pt-[0.5em]" ] [ DSFR.Icons.custom "ri-checkbox-blank-circle-fill" |> DSFR.Icons.icon ], class "fr-text-default--error" )

            else
                ( nothing, empty )

        buttons =
            [ [ DSFR.Button.new { onClick = Just <| SetSuiviAction <| EditSuivi <| SuiviRappel <| UI.Rappel.rappelToRappelInput <| rappel, label = "" }
                    |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1", Html.Attributes.title "Modifier le rappel" ]
              , DSFR.Button.new
                    { onClick =
                        Just <|
                            SetSuiviAction <|
                                DeleteSuivi <|
                                    SuiviRappel <|
                                        (\r ->
                                            { r
                                                | dateCloture =
                                                    if r.dateCloture == Nothing then
                                                        Date.fromPosix timezone now
                                                            |> Date.toIsoString
                                                            |> Just

                                                    else
                                                        Nothing
                                            }
                                        )
                                        <|
                                            UI.Rappel.rappelToRappelInput <|
                                                rappel
                    , label = ""
                    }
                    |> DSFR.Button.onlyIcon
                        (if Nothing /= Data.Rappel.dateCloture rappel then
                            DSFR.Icons.System.arrowGoBackLine

                         else
                            DSFR.Icons.System.closeLine
                        )
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withAttrs
                        [ class "!p-0 !m-0 !ml-1"
                        , Html.Attributes.title <|
                            (if Nothing /= Data.Rappel.dateCloture rappel then
                                "Réouvrir"

                             else
                                "Clôturer"
                            )
                                ++ " le rappel"
                        ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup
            ]
    in
    div [ class "!py-4 border-2 dark-grey-border" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "items-center" ]
            [ div [ DSFR.Grid.col1 ] [ icon ]
            , div [ color, DSFR.Grid.col2 ]
                [ case rappelMode of
                    RappelsActifs ->
                        text <|
                            Lib.Date.formatDateShort <|
                                Data.Rappel.date rappel

                    RappelsClotures ->
                        text <|
                            Maybe.withDefault "" <|
                                Maybe.map Lib.Date.formatDateShort <|
                                    Data.Rappel.dateCloture rappel
                ]
            , div [ DSFR.Grid.col7 ]
                [ text <| Data.Rappel.titre rappel
                ]
            , div [ DSFR.Grid.col2 ] <| List.singleton <| div [ class "p-2" ] <| buttons
            ]
        ]


viewEchangesAndClotures : Zone -> List EchangeAndCloture -> Html Msg
viewEchangesAndClotures timezone echangesAndClotures =
    div [ class "flex flex-col gap-4" ]
        (div [ class "flex flex-col" ]
            [ div [ class "flex flex-row justify-between" ]
                [ h4 [ Typo.fr_h6, class "!mb-1" ] [ text "Échanges" ]
                ]
            , hr [ class "!pb-1" ] []
            ]
            :: (if List.length echangesAndClotures == 0 then
                    span [ class "text-center italic" ] [ text "Aucun échange pour l'instant" ]

                else
                    nothing
               )
            :: (List.map
                    (\eac ->
                        div [ class "!py-2 border-2 dark-grey-border" ] <|
                            List.singleton <|
                                Keyed.node "div" [] <|
                                    case eac of
                                        Ech echange ->
                                            [ ( "echange-" ++ (entityIdToString <| Data.Echange.id <| echange)
                                              , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                                                    viewEchange timezone echange
                                              )
                                            ]

                                        Clot cloture ->
                                            [ ( "cloture-" ++ (entityIdToString <| Data.Cloture.id <| cloture)
                                              , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
                                                    viewCloture cloture
                                              )
                                            ]
                    )
                <|
                    List.sortWith
                        (\eac1 eac2 ->
                            let
                                comparison =
                                    Lib.Date.sortOlderDateLast
                                        (\eac ->
                                            case eac of
                                                Ech echange ->
                                                    Data.Echange.date echange

                                                Clot cloture ->
                                                    Data.Cloture.date cloture
                                        )
                                        eac1
                                        eac2
                            in
                            case comparison of
                                EQ ->
                                    case ( eac1, eac2 ) of
                                        ( Ech e1, Ech e2 ) ->
                                            Api.EntityId.compare (Data.Echange.id e2) (Data.Echange.id e1)

                                        ( Clot c1, Clot c2 ) ->
                                            Lib.Date.sortOlderDateLast Data.Cloture.date c1 c2

                                        ( Ech _, Clot _ ) ->
                                            GT

                                        ( Clot _, Ech _ ) ->
                                            LT

                                _ ->
                                    comparison
                        )
                    <|
                        echangesAndClotures
               )
        )


viewCloture : Cloture -> List (Html Msg)
viewCloture cloture =
    let
        label =
            (++) "Demande clôturée\u{00A0}: " <|
                Data.Demande.typeDemandeToDisplay <|
                    Data.Cloture.demande cloture
    in
    [ div [ DSFR.Grid.col1 ]
        [ div [ class "grey-text text-center" ]
            [ DSFR.Icons.iconMD <| DSFR.Icons.System.deleteLine
            ]
        ]
    , div [ DSFR.Grid.col2 ] [ text <| Lib.Date.formatDateShort <| Data.Cloture.date cloture ]
    , div [ DSFR.Grid.col5 ]
        [ div [ class "flex flex-col gap-2" ]
            [ span [ class "font-bold" ] [ text <| label ]
            , div [ class "whitespace-pre-wrap" ] <| List.singleton <| text <| Data.Cloture.motif cloture
            ]
        ]
    , div [ DSFR.Grid.col4 ]
        [ DSFR.Tag.unclickable { data = label, toString = identity }
            |> List.singleton
            |> DSFR.Tag.medium
        ]
    ]


viewEchange : Zone -> Echange -> List (Html Msg)
viewEchange timezone echange =
    [ div [ DSFR.Grid.col1, class "grey-text text-center" ]
        [ DSFR.Icons.iconMD <|
            case Data.Echange.type_ echange of
                Data.Echange.Telephone ->
                    DSFR.Icons.Device.phoneLine

                Data.Echange.Email ->
                    DSFR.Icons.custom "ri-at-line"

                Data.Echange.Rencontre ->
                    DSFR.Icons.User.userLine

                Data.Echange.Courrier ->
                    DSFR.Icons.Business.mailLine
        ]
    , div [ DSFR.Grid.col2 ]
        [ div [ class "flex flex-col" ]
            [ div [] [ text <| Lib.Date.formatDateShort <| Data.Echange.date echange ]
            , case Data.Echange.modification echange of
                Nothing ->
                    nothing

                Just ( auteur, date ) ->
                    div
                        [ Html.Attributes.title <|
                            "Dernière modification de l'échange le "
                                ++ Lib.Date.formatShort timezone date
                                ++ " par "
                                ++ auteur
                        , Typo.textSm
                        , Typo.textSpectralRegular
                        ]
                        [ text "Modifié "
                        , DSFR.Icons.iconSM DSFR.Icons.System.informationLine
                        ]
            ]
        ]
    , div [ DSFR.Grid.col5 ]
        [ div [ class "flex flex-col gap-2" ]
            [ let
                viewAsAccordion () =
                    DSFR.Accordion.raw
                        { id = "echange" ++ (entityIdToString <| Data.Echange.id <| echange)
                        , title =
                            [ div [ class "whitespace-pre-wrap" ]
                                [ span [ class "font-bold" ]
                                    [ text <| withEmptyAs "-" <| Data.Echange.titre echange ]
                                ]
                            ]
                        , content = [ div [ class "whitespace-pre-wrap" ] [ text <| Data.Echange.compteRendu echange ] ]
                        , borderless = True
                        }
              in
              case Data.Echange.compteRendu echange of
                "" ->
                    span [ class "font-bold" ]
                        [ text <| withEmptyAs "-" <| Data.Echange.titre echange ]

                _ ->
                    viewAsAccordion ()
            ]
        ]
    , div [ DSFR.Grid.col3 ]
        [ Data.Echange.themes echange
            |> List.map (\theme -> DSFR.Tag.unclickable { data = Data.Demande.typeDemandeToDisplay theme, toString = identity })
            |> DSFR.Tag.medium
        ]
    , div [ DSFR.Grid.col1 ]
        [ DSFR.Button.new { onClick = Just <| SetSuiviAction <| EditSuivi <| SuiviEchange <| UI.Echange.echangeToEchangeInput <| echange, label = "" }
            |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
            |> DSFR.Button.tertiaryNoOutline
            |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1", Html.Attributes.title "Modifier l'échange" ]
            |> DSFR.Button.view
        ]
    ]


deleteButton : Html Msg
deleteButton =
    DSFR.Button.new { label = "", onClick = Just <| ClickedDelete }
        |> DSFR.Button.onlyIcon DSFR.Icons.System.deleteFill
        |> DSFR.Button.tertiaryNoOutline
        |> DSFR.Button.view


viewParticulier : EntityId FicheId -> Fiche -> Particulier -> Html Msg
viewParticulier ficheId fiche ({ nom, prenom } as particulier) =
    div []
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col, class "flex flex-row justify-between" ]
                [ div [ class "flex flex-col" ]
                    [ h1 [ Typo.textBold, Typo.fr_h3, class "!mb-0" ] [ UI.Entite.icon fiche.entite.type_, text "\u{00A0}", text prenom, text "\u{00A0}", text <| String.toUpper nom ]
                    ]
                , div [ class "flex flex-row items-center" ]
                    [ DSFR.Button.new { label = "Modifier", onClick = Nothing }
                        |> DSFR.Button.linkButton (Route.toUrl <| Route.CreateurEdit ficheId)
                        |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                        |> DSFR.Button.tertiaryNoOutline
                        |> DSFR.Button.view
                    , deleteButton
                    ]
                ]
            ]
        , div [ class "pb-8" ] []
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col9, class "fr-card--grey" ]
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col7 ]
                        [ viewParticulierFutureEnseigne <| Data.Fiche.futureEnseigne <| fiche
                        , viewParticulierContact particulier
                        ]
                    , div [ DSFR.Grid.col5, class "flex flex-col gap-4" ]
                        [ viewQualifications fiche
                        , viewParticulierProjet particulier
                        ]
                    ]
                ]
            , div [ DSFR.Grid.col3 ]
                [ viewFicheDemandes fiche.demandes
                ]
            ]
        ]


viewParticulierFutureEnseigne : Maybe String -> Html msg
viewParticulierFutureEnseigne futureEnseigne =
    div [ class "p-2" ]
        [ div [ Typo.textBold ] [ text "Nom d'enseigne pressenti" ]
        , div [] [ text <| withEmptyAs "-" <| Maybe.withDefault "" <| futureEnseigne ]
        ]


viewParticulierContact : { a | adresse : String, email : String, telephone : String } -> Html msg
viewParticulierContact particulier =
    div [ class "p-2" ]
        [ div [ Typo.textBold ] [ text "Contact" ]
        , viewIf (List.all ((==) "") <| List.map String.trim <| [ particulier.adresse, particulier.email, particulier.telephone ]) <| text "-"
        , div [] [ text particulier.adresse ]
        , div []
            [ div [ class "py-4" ]
                [ div [] [ text particulier.email ]
                , div [] [ text <| displayPhone particulier.telephone ]
                ]
            ]
        ]


viewQualifications : Fiche -> Html msg
viewQualifications { entite } =
    div [ class "flex flex-col px-4 h-full gap-4" ]
        [ div []
            [ div [ Typo.textBold, class "flex flex-row justify-between items-center" ]
                [ span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                ]
            , case entite.activitesReelles of
                [] ->
                    text "-"

                _ ->
                    entite.activitesReelles
                        |> String.join "\u{00A0}— "
                        |> text
            ]
        , div []
            [ div [ Typo.textBold ] [ span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
            , case entite.localisations of
                [] ->
                    text "-"

                _ ->
                    entite.localisations
                        |> String.join "\u{00A0}— "
                        |> text
            ]
        , div []
            [ div [ Typo.textBold ] [ span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ] ]
            , case entite.motsCles of
                [] ->
                    text "-"

                _ ->
                    entite.motsCles
                        |> String.join "\u{00A0}— "
                        |> text
            ]
        , div []
            [ div [ Typo.textBold ] [ text "Commentaires" ]
            , div [ class "whitespace-pre-wrap" ]
                [ text <|
                    Lib.UI.textWithDefault "-" <|
                        entite.activiteAutre
                ]
            ]
        ]


viewParticulierProjet : Particulier -> Html msg
viewParticulierProjet particulier =
    div [ class "flex flex-col px-4 h-full" ]
        [ div [ Typo.textBold ] [ text "Description du projet" ]
        , div [ class "whitespace-pre-wrap" ]
            [ text <|
                withEmptyAs "-" <|
                    particulier.description
            ]
        ]


viewFicheDemandes : List Data.Demande.Demande -> Html msg
viewFicheDemandes demandes =
    let
        openDemandes =
            demandes
                |> List.filter (Data.Demande.cloture >> not)
    in
    div [ class "p-2" ]
        [ div [ Typo.textBold ]
            [ text "Demande"
            , text <|
                if List.length openDemandes > 1 then
                    "s"

                else
                    ""
            , text " en cours"
            ]
        , hr [ class "!pb-4" ] []
        , case openDemandes of
            [] ->
                span [ class "italic" ] [ text "Aucune demande en cours." ]

            ds ->
                ds
                    |> List.map
                        (Data.Demande.label
                            >> (\t -> { data = t, toString = identity })
                            >> DSFR.Tag.unclickable
                        )
                    |> DSFR.Tag.medium
        ]
