module Pages.Createur.New exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, h1, h2, span, sup, text)
import Api
import DSFR.Alert
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Demande as Demande
import Data.Echange as Echange
import Data.Fiche as Fiche exposing (Fiche)
import Date exposing (Date)
import Dict exposing (Dict)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import MultiSelectRemote
import RemoteData as RD
import Route
import Shared
import SingleSelectRemote exposing (SelectConfig)
import Spa.Page exposing (Page)
import UI.Echange
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page shared _ =
    Spa.Page.element <|
        { init = init shared
        , update = update
        , view = view
        , subscriptions =
            \model ->
                [ model.selectAdresse |> SingleSelectRemote.subscriptions
                , model.selectActivites |> MultiSelectRemote.subscriptions
                , model.selectLocalisations |> MultiSelectRemote.subscriptions
                , model.selectMots |> MultiSelectRemote.subscriptions
                ]
                    |> Sub.batch
        }


type alias Model =
    { particulier : DataParticulier
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    , selectedAdresse : Maybe ApiAdresse
    , validationFiche : RD.RemoteData FormErrors ()
    , enregistrementFiche : RD.WebData Fiche
    }


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-localisations"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


init : Shared.Shared -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init shared _ =
    let
        today =
            Date.fromPosix shared.timezone shared.now
    in
    ( { particulier = defaultDataParticulier today
      , selectActivites =
            MultiSelectRemote.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectMots =
            MultiSelectRemote.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , validationFiche = RD.NotAsked
      , enregistrementFiche = RD.NotAsked
      , selectAdresse =
            SingleSelectRemote.init "champ-selection-adresse"
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectedAdresse = Nothing
      }
    , Effect.none
    )
        |> Shared.pageChangeEffects


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActiviteParticulier
    , optionDecoder = Decode.list <| Decode.field "activite" Decode.string
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = Decode.list <| Decode.field "motCle" Decode.string
    }


apiAdresseToOption : ApiAdresse -> String
apiAdresseToOption =
    .label


defaultDataParticulier : Date -> DataParticulier
defaultDataParticulier today =
    { nom = ""
    , prenom = ""
    , email = ""
    , telephone = ""
    , adresse = ""
    , ville = ""
    , codePostal = ""
    , geolocation = ""
    , activites = []
    , localisations = []
    , mots = []
    , autre = ""
    , description = ""
    , echange = UI.Echange.default "Premier échange" today
    , futureEnseigne = ""
    }


type alias DataParticulier =
    { nom : String
    , prenom : String
    , email : String
    , telephone : String
    , adresse : String
    , ville : String
    , codePostal : String
    , geolocation : String
    , activites : List String
    , localisations : List String
    , mots : List String
    , autre : String
    , description : String
    , echange : UI.Echange.EchangeInput
    , futureEnseigne : String
    }


type Msg
    = UpdatedParticulierForm ParticulierField String
    | UpdatedAutre String
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | SelectedTypeEchange Echange.TypeEchange
    | ToggledTypeDemande Demande.TypeDemande Bool
    | RequestedCreationFiche
    | ReceivedCreationFiche (RD.WebData Fiche)
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)


type ParticulierField
    = ParticulierNom
    | ParticulierPrenom
    | ParticulierEmail
    | ParticulierTelephone
    | ParticulierActivite
    | ParticulierEchange UI.Echange.Field
    | ParticulierFutureEnseigne


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        UpdatedParticulierForm field value ->
            ( { model | particulier = updateParticulierForm field value model.particulier }, Effect.none )

        ReceivedCreationFiche ((RD.Success { id }) as enregistrementFiche) ->
            ( { model | enregistrementFiche = enregistrementFiche }
            , Effect.fromShared <|
                Shared.Navigate <|
                    Route.Createur id
            )

        ReceivedCreationFiche enregistrementFiche ->
            ( { model | enregistrementFiche = enregistrementFiche }, Effect.none )

        RequestedCreationFiche ->
            let
                parsedFiche =
                    validateFiche model

                ( enregistrementFiche, effect ) =
                    case parsedFiche of
                        RD.Success () ->
                            ( RD.Loading, createFiche model )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationFiche = parsedFiche, enregistrementFiche = enregistrementFiche }, effect )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse

                particulier =
                    model.particulier
                        |> (\p ->
                                { p
                                    | ville = apiAdresse.city
                                    , codePostal = apiAdresse.postcode
                                    , adresse = apiAdresse.label
                                    , geolocation = "(" ++ String.fromFloat apiAdresse.x ++ "," ++ String.fromFloat apiAdresse.y ++ ")"
                                }
                           )
            in
            ( { model | selectedAdresse = Just apiAdresse, selectAdresse = updatedSelect, particulier = particulier }, Effect.fromCmd selectCmd )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse
            in
            ( { model | selectAdresse = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedTypeEchange type_ ->
            ( { model | particulier = model.particulier |> (\p -> { p | echange = p.echange |> (\ech -> { ech | typeEchange = type_ }) }) }, Effect.none )

        ToggledTypeDemande type_ on ->
            ( { model
                | particulier =
                    model.particulier
                        |> (\p ->
                                { p
                                    | echange =
                                        p.echange
                                            |> (\ech ->
                                                    { ech
                                                        | typesDemande =
                                                            ech.typesDemande
                                                                |> (\ts ->
                                                                        if on then
                                                                            if List.member type_ ts then
                                                                                ts

                                                                            else
                                                                                ts |> List.reverse |> (::) type_ |> List.reverse

                                                                        else
                                                                            List.filter ((/=) type_) ts
                                                                   )
                                                    }
                                               )
                                }
                           )
              }
            , Effect.none
            )

        UpdatedAutre autre ->
            ( { model
                | particulier =
                    model.particulier |> (\p -> { p | autre = autre })
              }
            , Effect.none
            )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites
            in
            ( { model
                | selectActivites = updatedSelect
                , particulier =
                    model.particulier
                        |> (\part -> { part | activites = activitesUniques })
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | particulier =
                        model.particulier
                            |> (\part ->
                                    { part
                                        | activites =
                                            part.activites
                                                |> List.filter ((/=) activite)
                                    }
                               )
                  }
                , Effect.none
                )

        SelectedLocalisations ( localisations, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                    localisationsUniques =
                        case localisations of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    localisations
                in
                ( { model
                    | selectLocalisations = updatedSelect
                    , particulier =
                        model.particulier
                            |> (\p -> { p | localisations = localisationsUniques })
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            ( { model
                | particulier =
                    model.particulier
                        |> (\p -> { p | localisations = p.localisations |> List.filter ((/=) localisation) })
              }
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                    motsUniques =
                        case mots of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    mots
                in
                ( { model
                    | selectMots = updatedSelect
                    , particulier =
                        model.particulier
                            |> (\p -> { p | mots = motsUniques })
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            ( { model
                | particulier =
                    model.particulier
                        |> (\p ->
                                { p
                                    | mots =
                                        p.mots
                                            |> List.filter ((/=) mot)
                                }
                           )
              }
            , Effect.none
            )


selectConfig : SelectConfig ApiAdresse
selectConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


type alias FormErrors =
    { global : Maybe String, fields : Dict String (List String) }


formErrorsFor : String -> FormErrors -> Maybe (List String)
formErrorsFor key { fields } =
    fields
        |> Dict.get key


validateFiche : Model -> RD.RemoteData FormErrors ()
validateFiche { particulier } =
    if String.trim particulier.nom == "" || String.trim particulier.prenom == "" then
        let
            errors =
                [ ( "nom", .nom, "Ce champ est obligatoire" )
                , ( "prenom", .prenom, "Ce champ est obligatoire" )
                ]
                    |> List.foldl
                        (\( key, accessor, err ) dict ->
                            if accessor particulier == "" then
                                Dict.update key
                                    (\existingList ->
                                        case existingList of
                                            Just list ->
                                                Just <| list ++ [ err ]

                                            Nothing ->
                                                Just [ err ]
                                    )
                                    dict

                            else
                                dict
                        )
                        Dict.empty
        in
        RD.Failure
            { global =
                if Dict.isEmpty errors then
                    Nothing

                else
                    Just "Veuillez corriger les erreurs."
            , fields = errors
            }

    else
        RD.Success ()


updateParticulierForm : ParticulierField -> String -> DataParticulier -> DataParticulier
updateParticulierForm field value particulier =
    case field of
        ParticulierNom ->
            { particulier | nom = value }

        ParticulierPrenom ->
            { particulier | prenom = value }

        ParticulierEmail ->
            { particulier | email = value }

        ParticulierTelephone ->
            { particulier | telephone = value }

        ParticulierActivite ->
            { particulier | description = value }

        ParticulierEchange echangeField ->
            { particulier | echange = particulier.echange |> UI.Echange.update echangeField value }

        ParticulierFutureEnseigne ->
            { particulier | futureEnseigne = value }


createFiche : Model -> Effect.Effect Shared.Msg Msg
createFiche { particulier } =
    let
        jsonBody =
            particulier
                |> (\{ nom, prenom, email, telephone, adresse, ville, codePostal, geolocation, activites, localisations, mots, autre, description, echange, futureEnseigne } ->
                        let
                            { typeEchange, dateEchange, titreEchange, compteRendu, typesDemande } =
                                echange
                        in
                        [ ( "ficheType", Encode.string "PP" )
                        , ( "particulier"
                          , Encode.object
                                [ ( "nom", Encode.string nom )
                                , ( "prenom", Encode.string prenom )
                                , ( "email", Encode.string email )
                                , ( "telephone", Encode.string telephone )
                                , ( "adresse", Encode.string adresse )
                                , ( "ville", Encode.string ville )
                                , ( "codePostal", Encode.string codePostal )
                                , ( "geolocation", Encode.string geolocation )
                                , ( "description", Encode.string description )
                                , ( "futureEnseigne", Encode.string futureEnseigne )
                                ]
                          )
                        , ( "echange"
                          , Encode.object
                                [ ( "type"
                                  , Echange.encodeTypeEchange <|
                                        typeEchange
                                  )
                                , ( "date", Encode.string dateEchange )
                                , ( "titre", Encode.string titreEchange )
                                , ( "compteRendu", Encode.string compteRendu )
                                , ( "themes", Encode.list Demande.encodeTypeDemande typesDemande )
                                ]
                          )
                        , ( "activites", Encode.list Encode.string activites )
                        , ( "localisations", Encode.list Encode.string localisations )
                        , ( "mots", Encode.list Encode.string mots )
                        , ( "autre", Encode.string autre )
                        ]
                   )
                |> (Encode.object >> Http.jsonBody)
    in
    Http.post
        { url = Api.createFiche
        , body = jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedCreationFiche) Fiche.decodeFiche
        }
        |> Effect.fromCmd


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Nouvelle fiche"
    , body = UI.Layout.lazyBody body model
    , route = Route.CreateurNew
    }


ficheTypeForm : Model -> Html Msg
ficheTypeForm model =
    let
        formErrors =
            case model.validationFiche of
                RD.Failure errors ->
                    Just errors

                _ ->
                    Nothing
    in
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-4" ]
            [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Identité et contact du Créateur d'entreprise" ] ]
            , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                [ div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                    [ DSFR.Input.new
                        { value = model.particulier.nom
                        , onInput = UpdatedParticulierForm ParticulierNom
                        , label = text "Nom *"
                        , name = "nouvelle-fiche-particulier-nom"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "nom") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    , DSFR.Input.new
                        { value = model.particulier.prenom
                        , onInput = UpdatedParticulierForm ParticulierPrenom
                        , label = text "Prénom *"
                        , name = "nouvelle-fiche-particulier-prenom"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "prenom") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    ]
                , div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                    [ DSFR.Input.new
                        { value = model.particulier.futureEnseigne
                        , onInput = UpdatedParticulierForm ParticulierFutureEnseigne
                        , label = text "Nom d'enseigne pressenti"
                        , name = "nouvelle-fiche-particulier-futureEnseigne"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "futureEnseigne") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    ]
                , span []
                    [ model.selectAdresse
                        |> SingleSelectRemote.viewCustom
                            { isDisabled = False
                            , selected = model.selectedAdresse
                            , optionLabelFn = apiAdresseToOption
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = text "Adresse"
                            , searchPrompt = "Rechercher une adresse"
                            , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                            , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                            , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                            , noOptionsMsg = "Aucune adresse n'a été trouvée"
                            , error = Nothing
                            }
                    ]
                , div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                    [ DSFR.Input.new
                        { value = model.particulier.email
                        , onInput = UpdatedParticulierForm ParticulierEmail
                        , label = text "Email"
                        , name = "nouvelle-fiche-particulier-email"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "email") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    , DSFR.Input.new
                        { value = model.particulier.telephone
                        , onInput = UpdatedParticulierForm ParticulierTelephone
                        , label = text "Téléphone"
                        , name = "nouvelle-fiche-particulier-telephone"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "telephone") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    ]
                ]
            ]
        , div [ class "flex flex-col gap-4" ]
            [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Activité du Créateur d'entreprise" ] ]
            , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                [ model.selectActivites
                    |> MultiSelectRemote.viewCustom
                        { isDisabled = False
                        , selected = model.particulier.activites
                        , optionLabelFn = identity
                        , optionDescriptionFn = \_ -> ""
                        , optionsContainerMaxHeight = 300
                        , selectTitle = span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                        , viewSelectedOptionFn = text
                        , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                        , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                        , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                        , noOptionsMsg = "Aucune activité n'a été trouvée"
                        , newOption = Just identity
                        }
                , model.particulier.activites
                    |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                    |> DSFR.Tag.medium
                ]
            , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                [ model.selectLocalisations
                    |> MultiSelectRemote.viewCustom
                        { isDisabled = False
                        , selected = model.particulier.localisations
                        , optionLabelFn = identity
                        , optionDescriptionFn = \_ -> ""
                        , optionsContainerMaxHeight = 300
                        , selectTitle = span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                        , viewSelectedOptionFn = text
                        , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                        , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                        , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                        , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                        , newOption = Just identity
                        }
                , model.particulier.localisations
                    |> List.map (\localisation -> DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity })
                    |> DSFR.Tag.medium
                ]
            , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                [ model.selectMots
                    |> MultiSelectRemote.viewCustom
                        { isDisabled = False
                        , selected = model.particulier.mots
                        , optionLabelFn = identity
                        , optionDescriptionFn = \_ -> ""
                        , optionsContainerMaxHeight = 300
                        , selectTitle = span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                        , viewSelectedOptionFn = text
                        , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                        , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                        , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                        , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                        , newOption = Just identity
                        }
                , model.particulier.mots
                    |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                    |> DSFR.Tag.medium
                ]
            , DSFR.Input.new { value = model.particulier.autre, label = span [] [ text "Commentaires" ], onInput = UpdatedAutre, name = "autre" }
                |> DSFR.Input.textArea (Just 4)
                |> DSFR.Input.view
                |> List.singleton
                |> div [ class "sm:max-w-[60%]" ]
            , div [ class "flex flex-col sm:max-w-[60%]" ]
                [ DSFR.Input.new
                    { value = model.particulier.description
                    , onInput = UpdatedParticulierForm ParticulierActivite
                    , label = text "Description du projet (activité, produit)"
                    , name = "nouvelle-fiche-particulier-activite"
                    }
                    |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "activite") |> Maybe.map (List.map text))
                    |> DSFR.Input.textArea (Just 8)
                    |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                    |> DSFR.Input.view
                ]
            ]
        , div [ class "flex flex-col gap-4" ]
            [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Qualification de l'échange" ] ]
            , div [ class "sm:max-w-[60%]" ]
                [ UI.Echange.form (formErrors |> Maybe.andThen (formErrorsFor "typeEchange") |> Maybe.map (String.join ", ")) SelectedTypeEchange ToggledTypeDemande (ParticulierEchange >> UpdatedParticulierForm) model.particulier.echange
                ]
            ]
        ]


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [] [ text "Créer une fiche Créateur d'entreprise" ]
        , formWithListeners [ Events.onSubmit <| RequestedCreationFiche, class "flex flex-col gap-8" ]
            [ ficheTypeForm model
            , footer model
            ]
        ]


footer : Model -> Html Msg
footer model =
    let
        label =
            case model.enregistrementFiche of
                RD.NotAsked ->
                    "Créer la fiche"

                RD.Loading ->
                    "Création..."

                RD.Failure _ ->
                    "Créer la fiche"

                RD.Success _ ->
                    "Créer la fiche"

        isButtonDisabled =
            model.enregistrementFiche == RD.Loading
    in
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationFiche of
                RD.Failure { global } ->
                    case global of
                        Nothing ->
                            nothing

                        Just error ->
                            DSFR.Alert.small { title = Nothing, description = error }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementFiche of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = label }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled isButtonDisabled
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Createurs Nothing)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
