module Pages.Auth.Jwt.Uuid_ exposing (Model, Msg, page)

import Accessibility as Html
import Api
import Effect
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Process
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page, onNewFlags)
import Task
import UI.Layout
import UI.NewFeatures
import User exposing (User)
import View exposing (View)


page : Shared.Shared -> Page ( String, Maybe String ) Shared.Msg (View Msg) Model Msg
page shared =
    Spa.Page.element
        { init = init
        , update = update
        , view = view shared
        , subscriptions = subscriptions
        }
        |> onNewFlags (\_ -> NoOp)



-- INIT


type alias Model =
    { authenticationRequest : WebData AuthenticatedUser
    , uuid : String
    , redirectUrl : Maybe String
    }


init : ( String, Maybe String ) -> ( Model, Effect.Effect Shared.Msg Msg )
init ( uuid, redirectUrl ) =
    ( { authenticationRequest = RD.Loading, uuid = uuid, redirectUrl = redirectUrl }
    , Effect.fromCmd <|
        Cmd.batch <|
            [ authenticate uuid redirectUrl
            , timeout
            ]
    )
        |> Shared.pageChangeEffects


timeout : Cmd Msg
timeout =
    Process.sleep 5000
        |> Task.perform
            (\_ ->
                RD.Failure Http.Timeout
                    |> ReceivedAuthentication
            )


authenticate : String -> Maybe String -> Cmd Msg
authenticate uuid redirectUrl =
    Http.post
        { url = Api.jwt ++ "/" ++ uuid
        , body =
            redirectUrl
                |> Maybe.map (\url -> [ ( "redirectUrl", Encode.string url ) ] |> Encode.object |> Http.jsonBody)
                |> Maybe.withDefault Http.emptyBody
        , expect =
            Http.expectJson (RD.fromResult >> ReceivedAuthentication) <|
                decodeAuthenticatedUser
        }


decodeAuthenticatedUser : Decoder AuthenticatedUser
decodeAuthenticatedUser =
    Decode.succeed AuthenticatedUser
        |> andMap (Decode.field "token" Decode.string)
        |> andMap User.decodeUser
        |> andMap (optionalNullableField "redirectUrl" Decode.string)
        |> andMap (Decode.field "newFeatures" <| UI.NewFeatures.decodeCategory)



-- UPDATE


type Msg
    = ReceivedAuthentication (WebData AuthenticatedUser)
    | NoOp


type alias AuthenticatedUser =
    { token : String
    , user : User
    , redirectUrl : Maybe String
    , category : Maybe UI.NewFeatures.Category
    }


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        ReceivedAuthentication (RD.Success { user, redirectUrl, category }) ->
            ( model
            , Effect.fromShared <|
                Shared.LoggedIn user
                    (Just <|
                        (redirectUrl
                            |> Maybe.withDefault
                                (Route.toUrl <|
                                    Route.defaultPageForRole <|
                                        User.role user
                                )
                        )
                    )
                    category
            )

        ReceivedAuthentication response ->
            ( { model | authenticationRequest = response }, Effect.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Shared.Shared -> Model -> View Msg
view _ model =
    { title = UI.Layout.pageTitle <| "Validation de l'authentification"
    , body = body model
    , route = Route.AuthJwtUuid model.uuid model.redirectUrl
    }


body : Model -> List (Html.Html Msg)
body { authenticationRequest } =
    [ case authenticationRequest of
        RD.Failure _ ->
            Html.text "Une erreur s'est produite, veuillez réessayer."

        _ ->
            Html.text "Validation du jeton en cours..."
    ]
