module Pages.Admin.Stats exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, h3, span, text)
import Api
import Api.Auth
import DSFR.Grid
import DSFR.Radio
import DSFR.Tag
import DSFR.Typography as Typo
import Effect
import Html.Attributes exposing (class)
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { territoires : WebData (List Zone)
    , stats : WebData Stats
    , territoire : Maybe Zone
    , periode : Periode
    }


type alias Stats =
    { territoires :
        { total : Maybe Int
        , nouveaux : Maybe Int
        , actifs : Maybe Int
        }
    , devecos :
        { total : Maybe Int
        , nouveaux : Maybe Int
        , actifs : Maybe Int
        }
    , zone :
        { devecos :
            { total : Maybe Int
            , nouveaux : Maybe Int
            , actifs : Maybe Int
            }
        , fiches :
            { total : Maybe Int
            , nouvelles : Maybe Int
            }
        , echanges :
            { total : Maybe Int
            , nouveaux : Maybe Int
            }
        , demandes :
            { total : Maybe Int
            , nouvelles : Maybe Int
            , fermees : Maybe Int
            }
        }
    }


type Periode
    = AllTime
    | LastMonth
    | LastWeek


type alias Zone =
    { id : ZoneId
    , nom : String
    , type_ : TypeTerritoire
    }


type ZoneId
    = ZoneId String


zoneId : Zone -> String
zoneId zone =
    let
        (ZoneId id) =
            zone.id
    in
    id


type TypeTerritoire
    = Commune
    | EPCI


typeTerritoireToString : TypeTerritoire -> String
typeTerritoireToString t =
    case t of
        Commune ->
            "commune"

        EPCI ->
            "epci"


type Msg
    = ReceivedStats (WebData Stats)
    | ReceivedTerritoires (WebData (List Zone))
    | ChangedPeriode Periode
    | ChangedTerritoire (Maybe Zone)
    | GetStats ( Maybe Zone, Periode )


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { identity } _ =
    Spa.Page.onNewFlags (parseRawQuery >> GetStats) <|
        Spa.Page.element
            { view = view
            , init = init identity
            , update = update
            , subscriptions = \_ -> Sub.none
            }


init : Maybe User.User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init identity query =
    let
        ( territoire, periode ) =
            parseRawQuery query
    in
    ( { territoires = RD.Loading
      , stats = RD.Loading
      , territoire = territoire
      , periode = periode
      }
    , Effect.batch
        [ getTerritoires
        , getStats territoire periode
        , case identity of
            Nothing ->
                Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

            _ ->
                Effect.none
        ]
    )
        |> Shared.pageChangeEffects


parseRawQuery : Maybe String -> ( Maybe Zone, Periode )
parseRawQuery rawQuery =
    case rawQuery of
        Nothing ->
            ( Nothing, AllTime )

        Just query ->
            ( parseTerritoire query, parsePeriode query )


parseTerritoire : String -> Maybe Zone
parseTerritoire rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    territoire =
                        query
                            |> QS.getAsStringList "territoire"
                            |> List.head

                    type_ =
                        query
                            |> QS.getAsStringList "type"
                            |> List.head
                            |> Maybe.andThen
                                (\s ->
                                    case s of
                                        "commune" ->
                                            Just Commune

                                        "epci" ->
                                            Just EPCI

                                        _ ->
                                            Nothing
                                )
                in
                Maybe.map2
                    (\t_ t -> Zone (ZoneId t) "le territoire" t_)
                    type_
                    territoire
           )


parsePeriode : String -> Periode
parsePeriode rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    periode =
                        query
                            |> QS.getAsStringList "periode"
                            |> List.head
                            |> Maybe.withDefault ""
                in
                case periode of
                    "semaine" ->
                        LastWeek

                    "mois" ->
                        LastMonth

                    "tout" ->
                        AllTime

                    _ ->
                        AllTime
           )


getTerritoires : Effect.Effect sharedMsg Msg
getTerritoires =
    Effect.fromCmd <|
        Http.get
            { url = Api.getAdminStatsTerritoires
            , expect = Http.expectJson (RD.fromResult >> ReceivedTerritoires) <| Decode.list <| decodeZone
            }


decodeZone : Decoder Zone
decodeZone =
    Decode.succeed Zone
        |> andMap (Decode.map ZoneId <| Decode.field "id" <| Decode.string)
        |> andMap (Decode.field "name" Decode.string)
        |> andMap (Decode.field "type" decodeTypeTerritoire)


decodeTypeTerritoire : Decoder TypeTerritoire
decodeTypeTerritoire =
    Decode.string
        |> Decode.andThen
            (\s ->
                case s of
                    "epci" ->
                        Decode.succeed EPCI

                    "commune" ->
                        Decode.succeed Commune

                    _ ->
                        Decode.fail <| "Unknown territoire type: " ++ s
            )


getStats : Maybe Zone -> Periode -> Effect.Effect sharedMsg Msg
getStats territoire periode =
    let
        serializedQuery =
            serializeQuery territoire periode
    in
    Effect.fromCmd <|
        Http.get
            { url = Api.getAdminStatsData <| serializedQuery
            , expect = Http.expectJson (RD.fromResult >> ReceivedStats) <| decodeStats
            }


serializeQuery : Maybe Zone -> Periode -> String
serializeQuery zone periode =
    QS.serialize (QS.config |> QS.addQuestionMark False) <|
        (Maybe.withDefault identity <| Maybe.map (.type_ >> typeTerritoireToString >> QS.setStr "type") <| zone) <|
            (Maybe.withDefault identity <| Maybe.map (zoneId >> QS.setStr "territoire") <| zone) <|
                (QS.setStr "periode" <| periodeToString <| periode) <|
                    QS.empty


periodeToString : Periode -> String
periodeToString periode =
    case periode of
        AllTime ->
            "tout"

        LastMonth ->
            "mois"

        LastWeek ->
            "semaine"


periodeToDisplay : Periode -> String
periodeToDisplay periode =
    case periode of
        AllTime ->
            "Depuis le début"

        LastMonth ->
            "Les 30 derniers jours"

        LastWeek ->
            "Les 7 derniers jours"


decodeStats : Decoder Stats
decodeStats =
    Decode.succeed Stats
        |> andMap (Decode.field "territoires" decodeTerritoiresStats)
        |> andMap (Decode.field "devecos" decodeDevecoStats)
        |> andMap (Decode.field "zone" decodeZoneStats)


decodeTerritoiresStats : Decoder { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }
decodeTerritoiresStats =
    Decode.succeed (\total nouveaux actifs -> { total = total, nouveaux = nouveaux, actifs = actifs })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)
        |> andMap (optionalNullableField "actifs" Decode.int)


decodeDevecoStats : Decoder { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }
decodeDevecoStats =
    Decode.succeed (\total nouveaux actifs -> { total = total, nouveaux = nouveaux, actifs = actifs })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)
        |> andMap (optionalNullableField "actifs" Decode.int)


decodeZoneStats : Decoder { devecos : { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }, fiches : { total : Maybe Int, nouvelles : Maybe Int }, echanges : { total : Maybe Int, nouveaux : Maybe Int }, demandes : { total : Maybe Int, nouvelles : Maybe Int, fermees : Maybe Int } }
decodeZoneStats =
    Decode.succeed
        (\devecos fiches echanges demandes ->
            { devecos = devecos
            , fiches = fiches
            , echanges = echanges
            , demandes = demandes
            }
        )
        |> andMap (Decode.field "devecos" decodeDevecos)
        |> andMap (Decode.field "fiches" decodeFiches)
        |> andMap (Decode.field "echanges" decodeEchanges)
        |> andMap (Decode.field "demandes" decodeDemandes)


decodeDevecos : Decoder { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }
decodeDevecos =
    Decode.succeed (\total nouveaux actifs -> { total = total, nouveaux = nouveaux, actifs = actifs })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)
        |> andMap (optionalNullableField "actifs" Decode.int)


decodeFiches : Decoder { total : Maybe Int, nouvelles : Maybe Int }
decodeFiches =
    Decode.succeed (\total nouvelles -> { total = total, nouvelles = nouvelles })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouvelles" Decode.int)


decodeEchanges : Decoder { total : Maybe Int, nouveaux : Maybe Int }
decodeEchanges =
    Decode.succeed (\total nouveaux -> { total = total, nouveaux = nouveaux })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)


decodeDemandes : Decoder { total : Maybe Int, nouvelles : Maybe Int, fermees : Maybe Int }
decodeDemandes =
    Decode.succeed (\total nouvelles fermees -> { total = total, nouvelles = nouvelles, fermees = fermees })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouvelles" Decode.int)
        |> andMap (optionalNullableField "fermees" Decode.int)


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        GetStats ( territoire, periode ) ->
            { model | stats = RD.Loading }
                |> Effect.with (getStats territoire periode)

        ReceivedTerritoires response ->
            let
                getInTerritoires : String -> Maybe Zone
                getInTerritoires id =
                    case response of
                        RD.Success ts ->
                            ts
                                |> List.filter (\t -> zoneId t == id)
                                |> List.head

                        _ ->
                            Nothing

                newTerritoire : Maybe Zone
                newTerritoire =
                    model.territoire
                        |> Maybe.map (zoneId >> getInTerritoires)
                        |> Maybe.withDefault model.territoire
            in
            { model | territoires = response, territoire = newTerritoire }
                |> Effect.withNone

        ReceivedStats response ->
            { model | stats = response }
                |> Effect.withNone

        ChangedPeriode periode ->
            let
                newModel =
                    { model | periode = periode, stats = RD.Loading }
            in
            newModel
                |> Effect.with (updateUrl newModel)

        ChangedTerritoire territoire ->
            let
                newModel =
                    { model | territoire = territoire, stats = RD.Loading }
            in
            newModel
                |> Effect.with (updateUrl newModel)


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.fromShared <|
        Shared.Navigate <|
            routeWithQuery model.territoire model.periode


routeWithQuery : Maybe Zone -> Periode -> Route.Route
routeWithQuery territoire periode =
    Route.AdminStats <|
        Just <|
            serializeQuery territoire periode


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            "Statistiques d'utilisation - Deveco - version beta"
    , body = UI.Layout.lazyBody body model
    , route = Route.AdminStats Nothing
    }


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Statistiques d'utilisation de Deveco" ]
            ]
        , div [ DSFR.Grid.col4 ]
            [ Html.Lazy.lazy3 viewFilters model.periode model.territoire model.territoires
            ]
        , div [ DSFR.Grid.col8 ]
            [ Html.Lazy.lazy2 viewStats model.territoire model.stats
            ]
        ]


viewFilters : Periode -> Maybe Zone -> WebData (List Zone) -> Html Msg
viewFilters periode territoire territoires =
    div [ class "flex flex-col gap-8 fr-card--white h-full p-2 sm:p-4" ]
        [ DSFR.Radio.group
            { id = "select-periode"
            , options = [ AllTime, LastMonth, LastWeek ]
            , current = Just periode
            , toLabel = periodeToDisplay >> text
            , toId = periodeToString
            , msg = ChangedPeriode
            , legend = text "Période"
            }
            |> DSFR.Radio.withExtraAttrs [ class "!mb-0 !grow-0" ]
            |> DSFR.Radio.withLegendAttrs [ Typo.textBold ]
            |> DSFR.Radio.view
        , div []
            [ div [ Typo.textBold ] [ text "Territoires" ]
            , let
                ( epcis, communes ) =
                    territoires
                        |> RD.withDefault []
                        |> List.partition (\terr -> terr.type_ == EPCI)
                        |> Tuple.mapBoth (List.map Just) (List.map Just)
              in
              div [ class "mt-4" ]
                [ DSFR.Tag.small <| [ viewTag territoire Nothing ]
                , div [ Typo.textXs, Typo.textBold, class "!mb-2" ] [ text "EPCIs" ]
                , case epcis of
                    [] ->
                        text "-"

                    _ ->
                        DSFR.Tag.small <|
                            List.map (viewTag territoire) <|
                                epcis
                , div [ Typo.textXs, Typo.textBold, class "!mb-2" ] [ text "Communes" ]
                , case communes of
                    [] ->
                        text "-"

                    _ ->
                        DSFR.Tag.small <|
                            List.map (viewTag territoire) <|
                                communes
                ]
            ]
        ]


viewTag : Maybe Zone -> Maybe Zone -> DSFR.Tag.TagConfig Msg (Maybe Zone)
viewTag selected territoire =
    DSFR.Tag.selectable ChangedTerritoire
        ((selected |> Maybe.map zoneId) == (territoire |> Maybe.map zoneId))
        { data = territoire
        , toString = Maybe.map .nom >> Maybe.withDefault "France entière"
        }


viewStats : Maybe Zone -> WebData Stats -> Html msg
viewStats territoire remoteStats =
    let
        statLine =
            displayStats remoteStats
    in
    div [ class "flex flex-col gap-4" ]
        [ div [ DSFR.Grid.gridRow, class "fr-card--white p-2 sm:p-4" ]
            [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ]
                    [ text "Stats globales" ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Territoires" ]
                        , statLine "Total" (.territoires >> .total >> Maybe.map String.fromInt)
                        , statLine "Actifs" (.territoires >> .actifs >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" (.territoires >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    , div [ DSFR.Grid.col ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Devecos" ]
                        , statLine "Total" (.devecos >> .total >> Maybe.map String.fromInt)
                        , statLine "Actifs" (.devecos >> .actifs >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" (.devecos >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow, class "fr-card--white p-2 sm:p-4" ]
            [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ]
                    [ text "Stats pour "
                    , territoire
                        |> Maybe.map .nom
                        |> Maybe.withDefault "la France entière"
                        |> text
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Devecos" ]
                        , statLine "Total" (.zone >> .devecos >> .total >> Maybe.map String.fromInt)
                        , statLine "Actifs" (.zone >> .devecos >> .actifs >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" (.zone >> .devecos >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Fiches" ]
                        , statLine "Total" (.zone >> .fiches >> .total >> Maybe.map String.fromInt)
                        , statLine "Nouvelles" (.zone >> .fiches >> .nouvelles >> Maybe.map String.fromInt)
                        ]
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Échanges" ]
                        , statLine "Total" (.zone >> .echanges >> .total >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" (.zone >> .echanges >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Demandes" ]
                        , statLine "Total" (.zone >> .demandes >> .total >> Maybe.map String.fromInt)
                        , statLine "Nouvelles" (.zone >> .demandes >> .nouvelles >> Maybe.map String.fromInt)
                        , statLine "Fermées" (.zone >> .demandes >> .fermees >> Maybe.map String.fromInt)
                        ]
                    ]
                ]
            ]
        ]


viewMaybeFigure : Maybe String -> Html msg
viewMaybeFigure =
    Maybe.withDefault "-"
        >> text
        >> List.singleton
        >> span [ Typo.textBold ]


displayStats : WebData Stats -> String -> (Stats -> Maybe String) -> Html msg
displayStats stats lab toMaybeString =
    div []
        [ text <| lab ++ "\u{00A0}: "
        , case stats of
            RD.Loading ->
                greyPlaceholder 1

            RD.Success s ->
                s
                    |> toMaybeString
                    |> viewMaybeFigure

            _ ->
                text "-"
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "pulse inline-block h-[22px]" ]
