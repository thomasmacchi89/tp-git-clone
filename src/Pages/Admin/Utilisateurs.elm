module Pages.Admin.Utilisateurs exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, label, span, text)
import Api
import Api.EntityId
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons exposing (iconMD)
import DSFR.Icons.Design exposing (editFill)
import DSFR.Icons.System exposing (addLine, arrowDownLine, arrowUpLine, downloadLine)
import DSFR.Input
import DSFR.Modal
import DSFR.SearchBar
import DSFR.Table
import Data.Role
import Data.Territoire exposing (Territoire)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events exposing (onClick)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import List.Extra
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelect
import SingleSelectRemote exposing (SelectConfig)
import Spa.Page exposing (Page)
import Time exposing (Posix)
import TimeZone
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { recherche : String
    , users : WebData (List UserWithAdminInfo)
    , userInput : Maybe UserInput
    , saveUserRequest : WebData (List UserWithAdminInfo)
    , passwordInput : Maybe PasswordInput
    , savePasswordRequest : WebData ()
    , selectTerritoire : SingleSelect.SmartSelect Msg (Maybe Territoire)
    , selectedTerritoire : Maybe Territoire
    , sorting : Sorting
    }


type Msg
    = NoOp
    | ReceivedUsers (WebData (List UserWithAdminInfo))
    | UpdatedRecherche String
    | SelectedTerritoire ( Maybe Territoire, SingleSelect.Msg (Maybe Territoire) )
    | UpdatedSelectTerritoire (SingleSelect.Msg (Maybe Territoire))
    | ClickedAddUser
    | CanceledAddUser
    | UpdatedUserInput UserField String
    | ClickedEditPassword (Api.EntityId.EntityId User.UserId)
    | CanceledEditPassword
    | UpdatedPasswordInput PasswordField String
    | SelectedNewTerritoire ( NewTerritoire, SingleSelectRemote.Msg NewTerritoire )
    | UpdatedSelectNewTerritoire (SingleSelectRemote.Msg NewTerritoire)
    | RequestedSaveUser
    | ReceivedSaveUser (WebData (List UserWithAdminInfo))
    | RequestedSavePassword
    | ReceivedSavePassword (WebData ())
    | SetSorting Header


type UserField
    = UserEmail
    | UserNom
    | UserPrenom


type PasswordField
    = Password
    | Repeat


type alias NewTerritoire =
    { id : String
    , type_ : TypeTerritoire
    , name : String
    }


type TypeTerritoire
    = Commune
    | EPCI
    | Metropole


type alias UserWithAdminInfo =
    { user : User
    , lastLogin : Maybe Posix
    , lastAuth : Maybe Posix
    , lastRequest : Maybe Posix
    , accessUrl : Maybe String
    }


type alias Sorting =
    ( Header, Direction )


type Direction
    = Ascending
    | Descending


typeTerritoireToString : TypeTerritoire -> String
typeTerritoireToString t =
    case t of
        Commune ->
            "commune"

        EPCI ->
            "epci"

        Metropole ->
            "metropole"


type alias UserInput =
    { email : String
    , nom : String
    , prenom : String
    , nomTerritoire : String
    , selectNewTerritoire : SingleSelectRemote.SmartSelect Msg NewTerritoire
    , selectedNewTerritoire : Maybe NewTerritoire
    }


defaultUserInput : UserInput
defaultUserInput =
    { email = ""
    , nom = ""
    , prenom = ""
    , nomTerritoire = ""
    , selectNewTerritoire =
        SingleSelectRemote.init "champ-selection-territoire"
            { selectionMsg = SelectedNewTerritoire
            , internalMsg = UpdatedSelectNewTerritoire
            , characterSearchThreshold = selectCharacterThreshold
            , debounceDuration = selectDebounceDuration
            }
    , selectedNewTerritoire = Nothing
    }


type alias PasswordInput =
    { password : String
    , repeat : String
    , id : Api.EntityId.EntityId User.UserId
    }


defaultPasswordInput : Api.EntityId.EntityId User.UserId -> PasswordInput
defaultPasswordInput id =
    { password = ""
    , repeat = ""
    , id = id
    }


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectConfig : SelectConfig NewTerritoire
selectConfig =
    { headers = []
    , url = Api.newTerritoireSearch
    , optionDecoder = Decode.list <| decodeNewTerritoire
    }


decodeNewTerritoire : Decode.Decoder NewTerritoire
decodeNewTerritoire =
    Decode.succeed NewTerritoire
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "territoryType" decodeTypeTerritoire)
        |> andMap (Decode.field "name" Decode.string)


decodeTypeTerritoire : Decode.Decoder TypeTerritoire
decodeTypeTerritoire =
    Decode.string
        |> Decode.andThen
            (\s ->
                case s of
                    "epci" ->
                        Decode.succeed EPCI

                    "metropole" ->
                        Decode.succeed Metropole

                    "commune" ->
                        Decode.succeed Commune

                    _ ->
                        Decode.fail <| "Unknown territoire type: " ++ s
            )


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = .selectTerritoire >> SingleSelect.subscriptions
        }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { recherche = ""
      , users = RD.NotAsked
      , userInput = Nothing
      , saveUserRequest = RD.NotAsked
      , passwordInput = Nothing
      , savePasswordRequest = RD.NotAsked
      , selectTerritoire =
            SingleSelect.init "territoire-select"
                { selectionMsg = SelectedTerritoire
                , internalMsg = UpdatedSelectTerritoire
                }
      , selectedTerritoire = Nothing
      , sorting = ( HEmail, Ascending )
      }
    , getUsers
    )
        |> Shared.pageChangeEffects


getUsers : Effect.Effect Shared.Msg Msg
getUsers =
    Http.get
        { url = Api.getUtilisateurs
        , expect =
            Http.expectJson (RD.fromResult >> ReceivedUsers) <|
                decodeAdminUtilisateurs
        }
        |> Effect.fromCmd


decodeAdminUtilisateurs : Decode.Decoder (List UserWithAdminInfo)
decodeAdminUtilisateurs =
    Decode.list
        (Decode.succeed UserWithAdminInfo
            |> andMap User.decodeUser
            |> andMap (optionalNullableField "lastLogin" <| Lib.Date.decodeDateWithTimeFromISOString)
            |> andMap (optionalNullableField "lastAuth" <| Lib.Date.decodeDateWithTimeFromISOString)
            |> andMap (optionalNullableField "accessKeyDate" <| Lib.Date.decodeDateWithTimeFromISOString)
            |> andMap (optionalNullableField "accessUrl" Decode.string)
        )


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        ReceivedUsers response ->
            { model | users = response }
                |> Effect.withNone

        UpdatedRecherche recherche ->
            { model | recherche = recherche }
                |> Effect.withNone

        SelectedTerritoire ( territoire, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectTerritoire
            in
            ( { model | selectedTerritoire = territoire, selectTerritoire = updatedSelect }, Effect.fromCmd selectCmd )

        UpdatedSelectTerritoire sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectTerritoire
            in
            ( { model | selectTerritoire = updatedSelect }, Effect.fromCmd selectCmd )

        UpdatedUserInput field value ->
            { model | userInput = model.userInput |> Maybe.map (updateUserInput field value) }
                |> Effect.withNone

        ClickedAddUser ->
            { model | userInput = Just defaultUserInput, saveUserRequest = RD.NotAsked }
                |> Effect.withNone

        CanceledAddUser ->
            { model | userInput = Nothing }
                |> Effect.withNone

        UpdatedPasswordInput field value ->
            { model | passwordInput = model.passwordInput |> Maybe.map (updatePasswordInput field value) }
                |> Effect.withNone

        ClickedEditPassword id ->
            { model | passwordInput = Just <| defaultPasswordInput id }
                |> Effect.withNone

        CanceledEditPassword ->
            { model | passwordInput = Nothing }
                |> Effect.withNone

        RequestedSaveUser ->
            case ( model.saveUserRequest, model.userInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just userInput ) ->
                    { model | saveUserRequest = RD.Loading }
                        |> Effect.withCmd (saveUser userInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSaveUser response ->
            case model.saveUserRequest of
                RD.Loading ->
                    case response of
                        RD.Success _ ->
                            { model
                                | saveUserRequest = response
                                , userInput = Nothing
                                , users = response
                            }
                                |> Effect.withNone

                        _ ->
                            { model | saveUserRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        RequestedSavePassword ->
            case ( model.savePasswordRequest, model.passwordInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just passwordInput ) ->
                    { model | savePasswordRequest = RD.Loading }
                        |> Effect.withCmd (savePassword passwordInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSavePassword response ->
            case model.savePasswordRequest of
                RD.Loading ->
                    case response of
                        RD.Success _ ->
                            { model
                                | savePasswordRequest = response
                                , passwordInput = Nothing
                            }
                                |> Effect.withNone

                        _ ->
                            { model | savePasswordRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        SelectedNewTerritoire ( newTerritoire, sMsg ) ->
            case model.userInput of
                Nothing ->
                    model
                        |> Effect.withNone

                Just input ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelectRemote.update sMsg selectConfig input.selectNewTerritoire
                    in
                    ( { model | userInput = Just { input | selectNewTerritoire = updatedSelect, selectedNewTerritoire = Just newTerritoire } }, Effect.fromCmd selectCmd )

        UpdatedSelectNewTerritoire sMsg ->
            case model.userInput of
                Nothing ->
                    model
                        |> Effect.withNone

                Just input ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelectRemote.update sMsg selectConfig input.selectNewTerritoire
                    in
                    ( { model | userInput = Just { input | selectNewTerritoire = updatedSelect } }, Effect.fromCmd selectCmd )

        SetSorting header ->
            let
                sorting =
                    case model.sorting of
                        ( h, Ascending ) ->
                            if h == header then
                                ( h, Descending )

                            else
                                ( header, Ascending )

                        ( h, Descending ) ->
                            if h == header then
                                ( h, Ascending )

                            else
                                ( header, Ascending )
            in
            ( { model | sorting = sorting }, Effect.none )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Utilisateurs - Superadmin"
    , body = [ body model ]
    , route = Route.AdminUtilisateurs
    }


rechercherUtilisateursInputName : String
rechercherUtilisateursInputName =
    "recherches-utilisateurs"


body : Model -> Html Msg
body model =
    let
        territoires =
            model.users
                |> RD.withDefault []
                |> List.map (.user >> User.role)
                |> List.filterMap
                    (\r ->
                        case r of
                            Data.Role.Superadmin ->
                                Nothing

                            Data.Role.Deveco territoire ->
                                Just territoire
                    )
                |> List.Extra.unique
                |> List.sortBy Data.Territoire.nom
                |> List.map Just
                |> (::) Nothing
    in
    div [ DSFR.Grid.gridRow ]
        [ case model.userInput of
            Just input ->
                addUserModal model.saveUserRequest input

            Nothing ->
                nothing
        , case model.passwordInput of
            Just input ->
                addPasswordModal model.savePasswordRequest input

            Nothing ->
                nothing
        , div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
            [ div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col, class "flex flex-row gap-4 justify-end" ]
                    [ DSFR.Button.new { label = "Exporter les utilisateurs", onClick = Nothing }
                        |> DSFR.Button.linkButton Api.getUtilisateursCsv
                        |> DSFR.Button.leftIcon downloadLine
                        |> DSFR.Button.withAttrs [ Html.Attributes.target "_self" ]
                        |> DSFR.Button.secondary
                        |> DSFR.Button.view
                    , DSFR.Button.new { label = "Ajouter un utilisateur", onClick = Just ClickedAddUser }
                        |> DSFR.Button.leftIcon addLine
                        |> DSFR.Button.view
                    ]
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
                [ div [ DSFR.Grid.col6 ] <|
                    [ label
                        [ class "fr-label"
                        , Html.Attributes.for rechercherUtilisateursInputName
                        ]
                        [ text "Rechercher un utilisateur" ]
                    , DSFR.SearchBar.searchBar
                        { submitMsg = NoOp
                        , buttonLabel = "Rechercher"
                        , inputMsg = UpdatedRecherche
                        , inputLabel = "Rechercher un utilisateur"
                        , inputPlaceholder = Just "Recherche par email, nom, prénom"
                        , inputId = rechercherUtilisateursInputName
                        , inputValue = model.recherche
                        , hints = []
                        , fullLabel = Nothing
                        }
                    ]
                , div [ DSFR.Grid.col6 ] <|
                    List.singleton <|
                        SingleSelect.viewCustom
                            { selected = Just model.selectedTerritoire
                            , options = territoires
                            , optionLabelFn = Maybe.map Data.Territoire.nom >> Maybe.withDefault "Tous"
                            , isDisabled = False
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = text "Territoire"
                            , searchPrompt = "Rechercher un territoire"
                            , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                            , noOptionsMsg = "Aucune territoire n'a été trouvé"
                            , searchFn =
                                \searchText allOptions ->
                                    List.filter
                                        (Maybe.map
                                            (String.contains (String.toLower searchText) << String.toLower << Data.Territoire.nom)
                                            >> Maybe.withDefault False
                                        )
                                        allOptions
                            }
                            model.selectTerritoire
                ]
            , div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ] <|
                    List.singleton <|
                        viewUsersTable model.sorting model.recherche model.selectedTerritoire model.users
                ]
            ]
        ]


viewUsersTable : Sorting -> String -> Maybe Territoire -> WebData (List UserWithAdminInfo) -> Html Msg
viewUsersTable sorting recherche territoire data =
    case data of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Chargement en cours"
                    ]
                ]

        RD.Failure _ ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucun utilisateur pour l'instant."
                    ]
                ]

        RD.Success users ->
            case users |> List.filter (.user >> filterUsers recherche territoire) of
                [] ->
                    div [ class "text-center", DSFR.Grid.col ]
                        [ div [ class "fr-card--white p-20" ]
                            [ text "Aucune utilisateur correspondant."
                            ]
                        ]

                us ->
                    div [ DSFR.Grid.col12 ]
                        [ DSFR.Table.table
                            { id = "tableau-utilisateurs"
                            , caption = text "Utilisateurs"
                            , headers = headers
                            , rows = us |> sortWithSorting sorting
                            , toHeader =
                                \header ->
                                    let
                                        sortOnClick =
                                            if isSortable header then
                                                [ onClick <| SetSorting <| header, class "cursor-pointer" ]

                                            else
                                                []

                                        sortIcon =
                                            if isSortable header then
                                                sortingIcon sorting header

                                            else
                                                nothing
                                    in
                                    Html.span sortOnClick
                                        [ headerToString header |> text
                                        , sortIcon
                                        ]
                            , toRowId = .user >> User.email
                            , toCell = toCell
                            }
                            |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                            |> DSFR.Table.noBorders
                            |> DSFR.Table.captionHidden
                            |> DSFR.Table.fixed
                            |> DSFR.Table.view
                        ]


sortingIcon : ( Header, Direction ) -> Header -> Html msg
sortingIcon ( sortingHeader, direction ) header =
    if header == sortingHeader then
        case direction of
            Ascending ->
                iconMD arrowDownLine

            Descending ->
                iconMD arrowUpLine

    else
        nothing


sortWithSorting : ( Header, Direction ) -> List UserWithAdminInfo -> List UserWithAdminInfo
sortWithSorting ( header, direction ) =
    let
        sortWith =
            case header of
                HEmail ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HPrenom ->
                    .user
                        >> User.prenom
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HNom ->
                    .user
                        >> User.nom
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireNom ->
                    .user
                        >> (\user ->
                                case User.role user of
                                    Data.Role.Superadmin ->
                                        "ZZZZZZZZZZZZZZZZZZZZZ"

                                    Data.Role.Deveco territoire ->
                                        withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ" <|
                                            Data.Territoire.nom territoire
                           )

                HTerritoireType ->
                    .user
                        >> (\user ->
                                case User.role user of
                                    Data.Role.Superadmin ->
                                        "ZZZZZZZZZZZZZZZZZZZZZ"

                                    Data.Role.Deveco territoire ->
                                        Data.Territoire.type_ territoire
                                            |> Data.Territoire.typeToDisplay
                           )

                HLogin ->
                    .lastLogin
                        >> Maybe.map Time.posixToMillis
                        >> Maybe.withDefault 0
                        >> String.fromInt

                HAuth ->
                    .lastAuth
                        >> Maybe.map Time.posixToMillis
                        >> Maybe.withDefault 0
                        >> String.fromInt

                HToken ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HPassword ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"
    in
    List.Extra.stableSortWith
        (\a b ->
            case direction of
                Ascending ->
                    compare (sortWith a) (sortWith b)

                Descending ->
                    compare (sortWith b) (sortWith a)
        )


filterUsers : String -> Maybe Territoire -> User -> Bool
filterUsers recherche territoire user =
    let
        lowerRecherche =
            String.toLower recherche

        matches =
            String.toLower
                >> String.contains lowerRecherche

        matchRecherche =
            (lowerRecherche == "")
                || (matches <| User.email user)
                || (matches <| User.nom user)
                || (matches <| User.prenom user)

        userTerritoire =
            case User.role user of
                Data.Role.Superadmin ->
                    Nothing

                Data.Role.Deveco terr ->
                    Just terr

        matchTerritoire =
            case ( territoire, userTerritoire ) of
                ( Nothing, _ ) ->
                    True

                ( Just _, Nothing ) ->
                    False

                ( Just t, Just ut ) ->
                    t == ut
    in
    matchTerritoire && matchRecherche


type Header
    = HEmail
    | HPrenom
    | HNom
    | HTerritoireNom
    | HTerritoireType
    | HLogin
    | HAuth
    | HToken
    | HPassword


headers : List Header
headers =
    [ HEmail
    , HPrenom
    , HNom
    , HTerritoireNom
    , HTerritoireType
    , HLogin
    , HAuth
    , HToken
    , HPassword
    ]


isSortable : Header -> Bool
isSortable header =
    not <|
        List.member header [ HToken, HPassword ]


headerToString : Header -> String
headerToString header =
    case header of
        HEmail ->
            "Email"

        HNom ->
            "Nom"

        HPrenom ->
            "Prénom"

        HTerritoireNom ->
            "Territoire"

        HTerritoireType ->
            "Type"

        HLogin ->
            "Dernier login"

        HAuth ->
            "Dernière action"

        HToken ->
            "Lien"

        HPassword ->
            ""


toCell : Header -> UserWithAdminInfo -> Html Msg
toCell header { user, accessUrl, lastRequest, lastLogin, lastAuth } =
    case header of
        HEmail ->
            span [ Html.Attributes.style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        withEmptyAs "-" <|
                            User.email user

        HPrenom ->
            text <|
                withEmptyAs "-" <|
                    User.prenom user

        HNom ->
            text <|
                withEmptyAs "-" <|
                    User.nom user

        HTerritoireNom ->
            case User.role user of
                Data.Role.Superadmin ->
                    text "-"

                Data.Role.Deveco territoire ->
                    text <|
                        withEmptyAs "-" <|
                            Data.Territoire.nom territoire

        HTerritoireType ->
            case User.role user of
                Data.Role.Superadmin ->
                    text "-"

                Data.Role.Deveco territoire ->
                    Data.Territoire.type_ territoire
                        |> Data.Territoire.typeToDisplay
                        |> text

        HLogin ->
            lastLogin
                |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ()))
                |> Maybe.withDefault "-"
                |> text

        HAuth ->
            lastAuth
                |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ()))
                |> Maybe.withDefault "-"
                |> text

        HToken ->
            accessUrl
                |> Maybe.withDefault "-"
                |> withEmptyAs "-"
                |> text
                |> List.singleton
                |> span [ class "break-all", lastRequest |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ())) |> Maybe.map Html.Attributes.title |> Maybe.withDefault empty ]

        HPassword ->
            DSFR.Button.new { label = "", onClick = Just <| ClickedEditPassword <| User.id <| user }
                |> DSFR.Button.withAttrs [ Html.Attributes.name <| "edit-password-" ++ (Api.EntityId.entityIdToString <| User.id user) ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.onlyIcon editFill
                |> DSFR.Button.view


addUserModal : WebData (List UserWithAdminInfo) -> UserInput -> Html Msg
addUserModal request input =
    DSFR.Modal.view
        { id = "utilisateur"
        , label = "utilisateur"
        , openMsg = NoOp
        , closeMsg = Just CanceledAddUser
        , title = text "Ajouter un utilisateur"
        , opened = True
        }
        (viewUserForm request input)
        Nothing
        |> Tuple.first


addPasswordModal : WebData () -> PasswordInput -> Html Msg
addPasswordModal request input =
    DSFR.Modal.view
        { id = "password"
        , label = "password"
        , openMsg = NoOp
        , closeMsg = Just CanceledEditPassword
        , title = text "Remplacer le mot de passe de l'utilisateur"
        , opened = True
        }
        (viewPasswordForm request input)
        Nothing
        |> Tuple.first


updateUserInput : UserField -> String -> UserInput -> UserInput
updateUserInput field value userInput =
    case field of
        UserEmail ->
            { userInput | email = value }

        UserNom ->
            { userInput | nom = value }

        UserPrenom ->
            { userInput | prenom = value }


updatePasswordInput : PasswordField -> String -> PasswordInput -> PasswordInput
updatePasswordInput field value passwordInput =
    case field of
        Password ->
            { passwordInput | password = value }

        Repeat ->
            { passwordInput | repeat = value }


viewUserForm : WebData (List UserWithAdminInfo) -> UserInput -> Html Msg
viewUserForm request input =
    formWithListeners [ Events.onSubmit <| RequestedSaveUser, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ DSFR.Input.new { value = input.prenom, label = text "Prénom", onInput = UpdatedUserInput UserPrenom, name = "prenom" }
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.nom, label = text "Nom", onInput = UpdatedUserInput UserNom, name = "nom" }
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.email, label = text "Courriel *", onInput = UpdatedUserInput UserEmail, name = "email" }
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , div [ DSFR.Grid.col6 ]
            [ input.selectNewTerritoire
                |> SingleSelectRemote.viewCustom
                    { isDisabled = False
                    , selected = input.selectedNewTerritoire
                    , optionLabelFn = .name
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = text "Territoire"
                    , searchPrompt = "Rechercher un territoire"
                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                    , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                    , noOptionsMsg = "Aucun territoire n'a été trouvé"
                    , error = Nothing
                    }
            ]
        , case request of
            RD.Failure _ ->
                DSFR.Alert.small { title = Just "Création échouée", description = "Assurez-vous qu'un utilisateur avec ce courriel n'existe pas déjà." }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            _ ->
                nothing
        , div [ DSFR.Grid.col12 ]
            [ footerUtilisateur request input
            ]
        ]


viewPasswordForm : WebData () -> PasswordInput -> Html Msg
viewPasswordForm request input =
    formWithListeners [ Events.onSubmit <| RequestedSavePassword, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ DSFR.Input.new
            { value = input.password
            , label = text "Mot de passe *"
            , onInput = UpdatedPasswordInput Password
            , name = "password"
            }
            |> DSFR.Input.password
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new
            { value = input.repeat
            , label = text "Confirmez le mot de passe *"
            , onInput = UpdatedPasswordInput Repeat
            , name = "repeat"
            }
            |> DSFR.Input.password
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , case request of
            RD.Failure _ ->
                DSFR.Alert.small { title = Just "Modification du mot de passe échouée", description = "Assurez-vous que les deux mots de passe sont identiques." }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            _ ->
                nothing
        , div [ DSFR.Grid.col12 ]
            [ footerPassword request input
            ]
        ]


footerUtilisateur : WebData (List UserWithAdminInfo) -> UserInput -> Html Msg
footerUtilisateur request userInput =
    let
        valid =
            userInput.email /= "" && userInput.selectedNewTerritoire /= Nothing

        ( isButtonDisabled, title ) =
            case ( request, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "submit-new-user" ]
            |> DSFR.Button.withDisabled isButtonDisabled
        , DSFR.Button.new { onClick = Just CanceledAddUser, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


footerPassword : WebData () -> PasswordInput -> Html Msg
footerPassword request passwordInput =
    let
        valid =
            passwordInput.password /= "" && passwordInput.repeat /= "" && passwordInput.password == passwordInput.repeat

        ( isButtonDisabled, title ) =
            case ( request, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "submit-password" ]
            |> DSFR.Button.withDisabled isButtonDisabled
        , DSFR.Button.new { onClick = Just CanceledAddUser, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


saveUser : UserInput -> Cmd Msg
saveUser { nom, prenom, email, selectedNewTerritoire } =
    case selectedNewTerritoire of
        Nothing ->
            Cmd.none

        Just { id, type_, name } ->
            let
                jsonBody =
                    [ ( "nom", Encode.string nom )
                    , ( "prenom", Encode.string prenom )
                    , ( "email", Encode.string email )
                    , ( "nameTerritoire", Encode.string <| name )
                    , ( "typeTerritoire", Encode.string <| typeTerritoireToString <| type_ )
                    , ( "idTerritoire", Encode.string id )
                    ]
                        |> Encode.object
                        |> Http.jsonBody
            in
            Http.post
                { url = Api.createUtilisateur
                , body = jsonBody
                , expect =
                    Http.expectJson (RD.fromResult >> ReceivedSaveUser) <|
                        decodeAdminUtilisateurs
                }


savePassword : PasswordInput -> Cmd Msg
savePassword { password, repeat, id } =
    let
        jsonBody =
            [ ( "password", Encode.string password )
            , ( "repeat", Encode.string repeat )
            , ( "id", Api.EntityId.encodeEntityId id )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Http.post
        { url = Api.updatePasswordAdmin
        , body = jsonBody
        , expect =
            Http.expectWhatever (RD.fromResult >> ReceivedSavePassword)
        }
