module Pages.Admin.Tags exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h2, p, span, text)
import Api
import Api.EntityId exposing (EntityId, decodeEntityId, entityIdToString)
import DSFR.Accordion
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System exposing (deleteFill)
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Territoire exposing (TerritoireId)
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (plural)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelect
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { territoires : WebData (List Zone)
    , selectTerritoire : SingleSelect.SmartSelect Msg Zone
    , selectedTerritoire : Maybe Zone
    , tags : WebData Tags
    , deleteRequest : Maybe DeleteRequest
    }


type Msg
    = NoOp
    | SelectedTerritoire ( Zone, SingleSelect.Msg Zone )
    | UpdatedSelectTerritoire (SingleSelect.Msg Zone)
    | ReceivedTerritoires (WebData (List Zone))
    | ReceivedCurrentTags (WebData Tags)
    | ClickedDelete (EntityId TerritoireId) TagType Tag
    | CanceledDelete
    | ConfirmedDelete
    | ReceivedDelete (WebData Tags)


type alias DeleteRequest =
    { request : WebData Tags
    , territoireId : EntityId TerritoireId
    , tagType : TagType
    , tag : Tag
    }


type alias Tags =
    { activites : List Tag
    , zones : List Tag
    , mots : List Tag
    }


type TagType
    = Activites
    | Zones
    | Mots


tagTypeToString : TagType -> String
tagTypeToString tagType =
    case tagType of
        Activites ->
            "activites_reelles"

        Zones ->
            "entreprise_localisations"

        Mots ->
            "mots_cles"


tagTypeToDisplay : TagType -> String
tagTypeToDisplay tagType =
    case tagType of
        Activites ->
            "Activités réelles"

        Zones ->
            "Zones géographiques"

        Mots ->
            "Mots-clés"


type alias Tag =
    { id : EntityId TagId
    , label : String
    , usages : Int
    }


type TagId
    = TagId


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = .selectTerritoire >> SingleSelect.subscriptions
        }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { territoires = RD.Loading
      , selectTerritoire =
            SingleSelect.init "territoire-select"
                { selectionMsg = SelectedTerritoire
                , internalMsg = UpdatedSelectTerritoire
                }
      , selectedTerritoire = Nothing
      , tags = RD.NotAsked
      , deleteRequest = Nothing
      }
    , getTerritoires
    )
        |> Shared.pageChangeEffects


getTerritoires : Effect.Effect sharedMsg Msg
getTerritoires =
    Effect.fromCmd <|
        Http.get
            { url = Api.getAdminStatsTerritoires
            , expect = Http.expectJson (RD.fromResult >> ReceivedTerritoires) <| Decode.list <| decodeZone
            }


decodeZone : Decoder Zone
decodeZone =
    Decode.succeed Zone
        |> andMap (Decode.field "id" Decode.string)
        |> andMap (Decode.field "name" Decode.string)
        |> andMap (Decode.field "territoryId" <| decodeEntityId)


getCurrentTags : Zone -> Effect.Effect sharedMsg Msg
getCurrentTags territory =
    Effect.fromCmd <|
        Http.get
            { url = Api.getAdminTags <| territory.territoryId
            , expect = Http.expectJson (RD.fromResult >> ReceivedCurrentTags) <| decodeAdminTags
            }


decodeAdminTags : Decoder Tags
decodeAdminTags =
    Decode.succeed Tags
        |> andMap (Decode.field "activites" <| Decode.list decodeTag)
        |> andMap (Decode.field "zones" <| Decode.list decodeTag)
        |> andMap (Decode.field "mots" <| Decode.list decodeTag)


decodeTag : Decoder Tag
decodeTag =
    Decode.succeed Tag
        |> andMap (Decode.field "id" <| decodeEntityId)
        |> andMap (Decode.field "label" <| Decode.string)
        |> andMap (Decode.field "usages" <| Decode.int)


deleteTag : EntityId TerritoireId -> TagType -> Tag -> Effect.Effect sharedMsg Msg
deleteTag territoryId tagType tag =
    Effect.fromCmd <|
        Http.request
            { method = "DELETE"
            , headers = []
            , url = Api.getAdminTags territoryId
            , body =
                [ ( "tagType", Encode.string <| tagTypeToString tagType )
                , ( "id", Encode.string <| entityIdToString tag.id )
                , ( "tagLabel", Encode.string tag.label )
                ]
                    |> Encode.object
                    |> Http.jsonBody
            , expect = Http.expectJson (RD.fromResult >> ReceivedDelete) <| decodeAdminTags
            , timeout = Nothing
            , tracker = Nothing
            }


type alias Zone =
    { id : String
    , nom : String
    , territoryId : EntityId TerritoireId
    }


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SelectedTerritoire ( territoire, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectTerritoire
            in
            ( { model
                | selectedTerritoire = Just territoire
                , selectTerritoire = updatedSelect
                , tags = RD.Loading
              }
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , getCurrentTags territoire
                ]
            )

        UpdatedSelectTerritoire sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectTerritoire
            in
            ( { model | selectTerritoire = updatedSelect }, Effect.fromCmd selectCmd )

        ReceivedTerritoires response ->
            { model | territoires = response }
                |> Effect.withNone

        ReceivedCurrentTags tags ->
            { model | tags = tags }
                |> Effect.withNone

        ClickedDelete territoryId tagType tag ->
            ( { model
                | deleteRequest =
                    Just <|
                        DeleteRequest RD.NotAsked territoryId tagType tag
              }
            , Effect.none
            )

        CanceledDelete ->
            ( { model | deleteRequest = Nothing }
            , Effect.none
            )

        ConfirmedDelete ->
            case model.deleteRequest of
                Nothing ->
                    model |> Effect.withNone

                Just dr ->
                    ( { model | deleteRequest = Just <| { dr | request = RD.Loading } }, deleteTag dr.territoireId dr.tagType dr.tag )

        ReceivedDelete response ->
            case response of
                RD.Success _ ->
                    { model
                        | deleteRequest = Nothing
                        , tags = response
                    }
                        |> Effect.withNone

                _ ->
                    { model
                        | deleteRequest =
                            model.deleteRequest
                                |> Maybe.map (\dr -> { dr | request = response })
                    }
                        |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Tags - Superadmin"
    , body = UI.Layout.lazyBody body model
    , route = Route.AdminTags
    }


body : Model -> Html Msg
body model =
    let
        territoires =
            model.territoires
                |> RD.withDefault []
    in
    div [ DSFR.Grid.gridRow ]
        [ div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
            [ viewDeleteModal model.deleteRequest
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
                [ div [ DSFR.Grid.col6 ] <|
                    List.singleton <|
                        div [ class "p-4" ] <|
                            List.singleton <|
                                SingleSelect.viewCustom
                                    { selected = model.selectedTerritoire
                                    , options = territoires
                                    , optionLabelFn = .nom
                                    , isDisabled = False
                                    , optionDescriptionFn = \_ -> ""
                                    , optionsContainerMaxHeight = 300
                                    , selectTitle = h2 [] [ text "Territoire" ]
                                    , searchPrompt = "Rechercher un territoire"
                                    , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                                    , noOptionsMsg = "Aucune territoire n'a été trouvé"
                                    , searchFn =
                                        \searchText allOptions ->
                                            List.filter
                                                (String.contains (String.toLower searchText) << String.toLower << .nom)
                                                allOptions
                                    }
                                    model.selectTerritoire
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ] <|
                case ( model.selectedTerritoire, model.tags ) of
                    ( Nothing, _ ) ->
                        [ div [ DSFR.Grid.col6 ] <|
                            List.singleton <|
                                div [ class "p-4" ]
                                    [ text "Veuillez sélectionner un territoire." ]
                        ]

                    ( Just { territoryId }, RD.Success { activites, zones, mots } ) ->
                        [ div [ DSFR.Grid.col6 ] <|
                            viewTags territoryId Activites <|
                                activites
                        , div [ DSFR.Grid.col6 ] <|
                            viewTags territoryId Zones <|
                                zones
                        , div [ DSFR.Grid.col6 ] <|
                            viewTags territoryId Mots <|
                                mots
                        ]

                    ( _, RD.Loading ) ->
                        [ text "Chargement en cours..." ]

                    ( _, RD.Failure _ ) ->
                        [ text "Erreur, veuillez contacter l'équipe de développement." ]

                    _ ->
                        [ text "Quelque chose aurait dû se passer..." ]
            ]
        ]


viewTags : EntityId TerritoireId -> TagType -> List Tag -> List (Html Msg)
viewTags territoryId tagType tags =
    List.singleton <|
        div [ class "p-2" ] <|
            [ h2 [] [ text <| tagTypeToDisplay tagType ]
            , case tags of
                [] ->
                    span [ class "italic" ] [ text "Aucun tag." ]

                _ ->
                    DSFR.Accordion.raw
                        { id = "accordion-" ++ tagTypeToString tagType
                        , title =
                            text "Voir "
                                :: (if List.length tags > 1 then
                                        [ text "les ", text <| String.fromInt <| List.length tags, text " tags" ]

                                    else
                                        [ text "le tag"
                                        ]
                                   )
                        , content =
                            List.singleton <|
                                div [ class "flex flex-col gap-2" ] <|
                                    List.map (viewTag territoryId tagType) <|
                                        List.sortBy .label <|
                                            tags
                        , borderless = True
                        }
            ]


viewTag : EntityId TerritoireId -> TagType -> Tag -> Html Msg
viewTag territoryId tagType ({ label, usages } as tag) =
    div [ class "flex flex-row gap-4 items-center" ]
        [ div []
            [ span [ Typo.textBold ] [ text label ]
            , text " "
            , text "(utilisé "
            , text <| String.fromInt <| usages
            , text " fois)"
            ]
        , DSFR.Button.new
            { label = "Supprimer"
            , onClick =
                Just <|
                    ClickedDelete territoryId tagType tag
            }
            |> DSFR.Button.onlyIcon deleteFill
            |> DSFR.Button.view
        ]


viewDeleteModal : Maybe DeleteRequest -> Html Msg
viewDeleteModal deleteRequest =
    case deleteRequest of
        Nothing ->
            nothing

        Just dr ->
            DSFR.Modal.view
                { id = "delete-tag-modal"
                , label = "delete-tag-modal"
                , openMsg = NoOp
                , closeMsg = Just CanceledDelete
                , title = div [] [ h2 [] [ text "Suppression de «\u{00A0}", text dr.tag.label, text "\u{00A0}»" ] ]
                , opened = True
                }
                (div [ class "flex flex-col gap-4" ]
                    [ if dr.tag.usages > 0 then
                        div []
                            [ p []
                                [ text "Cette qualification est utilisée par "
                                , text <| String.fromInt <| dr.tag.usages
                                , text " entreprise"
                                , text <| plural dr.tag.usages
                                , text " ou créateur"
                                , text <| plural dr.tag.usages
                                , text " d'entreprises."
                                ]
                            , p [] [ text "Confirmez-vous la suppression\u{00A0}?" ]
                            ]

                      else
                        text "Cette qualification n'est pas utilisée."
                    , DSFR.Button.group
                        [ DSFR.Button.new
                            { onClick = Just ConfirmedDelete
                            , label = "Confirmer la suppression"
                            }
                            |> DSFR.Button.withDisabled (dr.request == RD.Loading)
                        , DSFR.Button.new { onClick = Just CanceledDelete, label = "Annuler" }
                            |> DSFR.Button.secondary
                        ]
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                )
                Nothing
                |> Tuple.first
