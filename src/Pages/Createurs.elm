module Pages.Createurs exposing (Model, Msg, page)

import Accessibility exposing (a, decorativeImg, div, form, h1, h2, label, p, select, span, text)
import Api
import Api.EntityId exposing (EntityId)
import DSFR.Accordion
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Pagination
import DSFR.SearchBar
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiStreet, decodeApiStreet)
import Data.Demande exposing (TypeDemande)
import Data.Entite as Entite exposing (EntiteId)
import Data.Fiche as Fiche exposing (Fiche)
import Effect
import Html exposing (Html, hr, input, option, sup)
import Html.Attributes as Attr exposing (class, classList)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Lazy
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (formatNumberWithThousandSpacing, plural, withEmptyAs)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import MultiSelect
import MultiSelectRemote exposing (SelectConfig)
import QS
import RemoteData as RD
import Route
import Shared
import SingleSelectRemote
import Spa.Page exposing (Page)
import UI.Entite
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { entiteSelection } user =
    Spa.Page.onNewFlags (parseRawQuery >> DoSearch) <|
        Spa.Page.element <|
            { init = init entiteSelection user
            , update = update
            , view = view
            , subscriptions = subscriptions
            }


type alias Model =
    { fiches : RD.WebData (List Fiche)
    , fichesRequests : Int
    , filters : Filters
    , filtersSource : RD.WebData FiltersSource
    , pagination : Pagination
    , selectActivites : MultiSelect.SmartSelect Msg String
    , selectDemandes : MultiSelect.SmartSelect Msg TypeDemande
    , selectZones : MultiSelect.SmartSelect Msg String
    , selectMots : MultiSelect.SmartSelect Msg String
    , lastUrlFetched : String
    , selectedEntites : List (EntityId EntiteId)
    , batchRequest : Maybe BatchRequest
    , selectRue : SingleSelectRemote.SmartSelect Msg ApiStreet
    }


type alias BatchRequest =
    { request : RD.WebData ( List Fiche, Pagination, BatchResults )
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , activites : List String
    , localisations : List String
    , mots : List String
    }


type alias BatchResults =
    { total : Int
    , successes : Int
    , errors : List String
    }


type alias Filters =
    { recherche : String
    , activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    , rue : Maybe ApiStreet
    , orderBy : OrderBy
    , orderDirection : OrderDirection
    }


type OrderBy
    = Alphabetical
    | FicheModification
    | EtablissementCreation


type OrderDirection
    = Desc
    | Asc


defaultFilters : Filters
defaultFilters =
    { recherche = ""
    , activites = []
    , demandes = []
    , zones = []
    , mots = []
    , rue = Nothing
    , orderBy = FicheModification
    , orderDirection = Desc
    }


type alias FiltersSource =
    { activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    }


type Msg
    = NoOp
    | ReceivedFiltersSource (RD.WebData FiltersSource)
    | FetchFiches
    | DoSearch ( Filters, Pagination )
    | UpdatedSearch String
    | ReceivedFiches (RD.WebData ( List Fiche, Pagination ))
    | ClearAllFilters
    | SelectedActivites ( List String, MultiSelect.Msg String )
    | UpdatedSelectActivites (MultiSelect.Msg String)
    | UnselectActivite String
    | SelectedDemandes ( List TypeDemande, MultiSelect.Msg TypeDemande )
    | UpdatedSelectDemandes (MultiSelect.Msg TypeDemande)
    | UnselectDemande TypeDemande
    | SelectedZones ( List String, MultiSelect.Msg String )
    | UpdatedSelectZones (MultiSelect.Msg String)
    | UnselectZone String
    | SelectedMots ( List String, MultiSelect.Msg String )
    | UpdatedSelectMots (MultiSelect.Msg String)
    | UnselectMot String
    | ToggleSelection (List (EntityId EntiteId)) Bool
    | EmptySelection
    | ClickedQualifierFiches
    | CanceledQualifierFiches
    | ConfirmedQualifierFiches
    | ReceivedQualifierFiches (RD.WebData ( List Fiche, Pagination, BatchResults ))
    | SelectedBatchActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchActivites (MultiSelectRemote.Msg String)
    | UnselectBatchActivite String
    | SelectedBatchLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchLocalisations (MultiSelectRemote.Msg String)
    | UnselectBatchLocalisation String
    | SelectedBatchMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchMots (MultiSelectRemote.Msg String)
    | UnselectBatchMot String
    | SelectedRue ( ApiStreet, SingleSelectRemote.Msg ApiStreet )
    | UpdatedSelectRue (SingleSelectRemote.Msg ApiStreet)
    | ClearRue
    | ClickedOrderBy OrderBy OrderDirection
    | ClickedSearch


parseFilters : String -> Filters
parseFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    activites =
                        query
                            |> QS.getAsStringList queryKeys.activites

                    demandes =
                        query
                            |> QS.getAsStringList queryKeys.demandes
                            |> List.map Data.Demande.stringToTypeDemande
                            |> List.filterMap identity

                    zones =
                        query
                            |> QS.getAsStringList queryKeys.zones

                    orderBy =
                        query
                            |> QS.getAsStringList queryKeys.ordre
                            |> List.head
                            |> (\ob ->
                                    case ob of
                                        Just "AZ" ->
                                            Alphabetical

                                        Just "DC" ->
                                            EtablissementCreation

                                        Just "DM" ->
                                            FicheModification

                                        Just _ ->
                                            FicheModification

                                        Nothing ->
                                            FicheModification
                               )

                    orderDirection =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> (\ob ->
                                    case ob of
                                        Just "ASC" ->
                                            Asc

                                        Just "DESC" ->
                                            Desc

                                        Just _ ->
                                            Desc

                                        Nothing ->
                                            Desc
                               )

                    mots =
                        query
                            |> QS.getAsStringList queryKeys.mots

                    rue =
                        query
                            |> QS.getAsStringList queryKeys.rue
                            |> List.head

                    cp =
                        query
                            |> QS.getAsStringList queryKeys.cp
                            |> List.head
                in
                { defaultFilters
                    | recherche = recherche
                    , activites = activites
                    , demandes = demandes
                    , zones = zones
                    , mots = mots
                    , rue = Maybe.map2 (\r c -> ApiStreet (r ++ " - " ++ c) r c "") rue cp
                    , orderBy = orderBy
                    , orderDirection = orderDirection
                }
           )


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setActivites =
            case filters.activites of
                [] ->
                    identity

                activites ->
                    activites
                        |> QS.setListStr queryKeys.activites

        setDemandes =
            case filters.demandes of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map Data.Demande.demandeTypeToString
                        |> QS.setListStr queryKeys.demandes

        setZones =
            case filters.zones of
                [] ->
                    identity

                zones ->
                    zones
                        |> QS.setListStr queryKeys.zones

        setType =
            "particulier"
                |> QS.setStr queryKeys.type_

        setRecherche =
            case filters.recherche of
                "" ->
                    identity

                recherche ->
                    recherche
                        |> QS.setStr queryKeys.recherche

        setMots =
            case filters.mots of
                [] ->
                    identity

                mots ->
                    mots
                        |> QS.setListStr queryKeys.mots

        setRue =
            filters.rue
                |> Maybe.map .name
                |> Maybe.map (QS.setStr queryKeys.rue)
                |> Maybe.withDefault identity

        setCp =
            filters.rue
                |> Maybe.map .postcode
                |> Maybe.map (QS.setStr queryKeys.cp)
                |> Maybe.withDefault identity

        setOrder =
            case filters.orderBy of
                Alphabetical ->
                    QS.setStr queryKeys.ordre <|
                        "AZ"

                FicheModification ->
                    identity

                EtablissementCreation ->
                    QS.setStr queryKeys.ordre <|
                        "DC"

        setDirection =
            case filters.orderDirection of
                Asc ->
                    QS.setStr queryKeys.direction <|
                        "ASC"

                Desc ->
                    identity
    in
    QS.empty
        |> setActivites
        |> setDemandes
        |> setZones
        |> setType
        |> setRecherche
        |> setMots
        |> setRue
        |> setCp
        |> setOrder
        |> setDirection


parsePagination : String -> Pagination
parsePagination rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    p =
                        query
                            |> QS.getAsStringList "page"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1

                    pages =
                        query
                            |> QS.getAsStringList "pages"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1
                in
                { defaultPagination
                    | page = p
                    , pages = pages
                }
           )


defaultPagination : Pagination
defaultPagination =
    { page = 1, pages = 1, results = 0 }


paginationToQuery : Pagination -> QS.Query
paginationToQuery pagination =
    let
        setPage =
            if pagination.page > 1 then
                QS.setStr queryKeys.page <| String.fromInt <| pagination.page

            else
                identity
    in
    QS.empty
        |> setPage


queryKeys : { type_ : String, activites : String, demandes : String, zones : String, recherche : String, page : String, pages : String, mots : String, rue : String, cp : String, ordre : String, direction : String }
queryKeys =
    { type_ = "type"
    , activites = "activites"
    , demandes = "demandes"
    , zones = "zones"
    , recherche = "recherche"
    , page = "page"
    , pages = "pages"
    , mots = "mots"
    , rue = "rue"
    , cp = "cp"
    , ordre = "o"
    , direction = "d"
    }


serializeModel : ( Filters, Pagination ) -> String
serializeModel ( filters, pagination ) =
    QS.merge (paginationToQuery pagination) (filtersToQuery filters)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Createurs <|
            Just <|
                serializeModel ( filters, { pagination | page = newPage } )


parseRawQuery : Maybe String -> ( Filters, Pagination )
parseRawQuery rawQuery =
    case rawQuery of
        Nothing ->
            ( defaultFilters, defaultPagination )

        Just query ->
            ( parseFilters query, parsePagination query )


init : List (EntityId EntiteId) -> Shared.User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init ficheSelection _ rawQuery =
    let
        ( filters, pagination ) =
            parseRawQuery rawQuery

        apiStreet =
            filters.rue |> Maybe.map .label |> Maybe.withDefault ""

        ( selectRue, selectRueCmd ) =
            SingleSelectRemote.init selectRueId
                { selectionMsg = SelectedRue
                , internalMsg = UpdatedSelectRue
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
                |> SingleSelectRemote.setText apiStreet selectRueConfig
    in
    ( { fiches = RD.Loading
      , fichesRequests = 0
      , filters = filters
      , filtersSource = RD.Loading
      , pagination = pagination
      , selectActivites =
            MultiSelect.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                }
      , selectDemandes =
            MultiSelect.init selectDemandesId
                { selectionMsg = SelectedDemandes
                , internalMsg = UpdatedSelectDemandes
                }
      , selectZones =
            MultiSelect.init selectZonesId
                { selectionMsg = SelectedZones
                , internalMsg = UpdatedSelectZones
                }
      , selectMots =
            MultiSelect.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                }
      , lastUrlFetched = serializeModel ( filters, pagination )
      , selectedEntites = ficheSelection
      , batchRequest = Nothing
      , selectRue = selectRue
      }
    , Effect.batch
        [ Effect.fromCmd <| getFiches 0 ( filters, pagination )
        , Effect.fromCmd <| getFiltersSource
        , Effect.fromCmd selectRueCmd
        ]
    )
        |> Shared.pageChangeEffects


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.selectActivites |> MultiSelect.subscriptions
        , model.selectDemandes |> MultiSelect.subscriptions
        , model.selectZones |> MultiSelect.subscriptions
        , model.selectMots |> MultiSelect.subscriptions
        , model.batchRequest
            |> Maybe.map .selectActivites
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectLocalisations
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectMots
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.selectRue |> SingleSelectRemote.subscriptions
        ]


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectDemandesId : String
selectDemandesId =
    "champ-selection-demandes"


selectZonesId : String
selectZonesId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


getFichesTracker : String
getFichesTracker =
    "get-fiches-tracker"


getFiches : Int -> ( Filters, Pagination ) -> Cmd Msg
getFiches trackerCount ( filters, pagination ) =
    Http.request
        { method = "GET"
        , headers = []
        , url = Api.getFiches <| serializeModel ( filters, pagination )
        , body = Http.emptyBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedFiches) decodePayload
        , timeout = Nothing
        , tracker = Just <| getFichesTracker ++ "-" ++ String.fromInt trackerCount
        }


decodePayload : Decode.Decoder ( List Fiche, Pagination )
decodePayload =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "results" Decode.int)
        |> andMap (Decode.field "data" decodeFiches)


decodeFiches : Decode.Decoder (List Fiche)
decodeFiches =
    Decode.list Fiche.decodeFiche


getFiltersSource : Cmd Msg
getFiltersSource =
    Http.get
        { url = Api.getFichesFiltersSource
        , expect = Http.expectJson (RD.fromResult >> ReceivedFiltersSource) decodeFiltersSource
        }


decodeFiltersSource : Decode.Decoder FiltersSource
decodeFiltersSource =
    Decode.succeed FiltersSource
        |> andMap (Decode.field "activites" <| Decode.list Decode.string)
        |> andMap (Decode.field "demandes" <| Decode.list Data.Demande.decodeTypeDemande)
        |> andMap (Decode.field "zones" <| Decode.list Decode.string)
        |> andMap (Decode.field "mots" <| Decode.list Decode.string)


sendBatchRequest : Filters -> Pagination -> List (EntityId EntiteId) -> BatchRequest -> Cmd Msg
sendBatchRequest filters pagination selectedEntites { activites, localisations, mots } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "mots", Encode.list Encode.string mots )
            , ( "entiteIds", Encode.list Api.EntityId.encodeEntityId selectedEntites )
            ]
                |> (Encode.object >> Http.jsonBody)
    in
    Http.post
        { url = Api.updateFiches <| serializeModel ( filters, pagination )
        , body = jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedQualifierFiches) <| decodeBatchResponse
        }


decodeBatchResponse : Decode.Decoder ( List Fiche, Pagination, BatchResults )
decodeBatchResponse =
    Decode.succeed (\( f, p ) b -> ( f, p, b ))
        |> andMap decodePayload
        |> andMap (Decode.field "batchResults" <| decodeBatchResults)


decodeBatchResults : Decode.Decoder BatchResults
decodeBatchResults =
    Decode.succeed BatchResults
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "successes" Decode.int)
        |> andMap (Decode.field "errors" <| Decode.list Decode.string)


rechercherFicheInputName : String
rechercherFicheInputName =
    "rechercher-fiche"


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        ReceivedFiltersSource response ->
            { model | filtersSource = response }
                |> Effect.withNone

        ReceivedFiches fiches ->
            case fiches of
                RD.Success ( fs, pagination ) ->
                    ( { model | fiches = RD.Success fs, pagination = pagination }
                    , Effect.none
                    )

                _ ->
                    ( { model | fiches = fiches |> RD.map Tuple.first }
                    , Effect.none
                    )

        FetchFiches ->
            let
                nextUrl =
                    serializeModel ( model.filters, model.pagination )
            in
            if nextUrl /= model.lastUrlFetched then
                ( { model | fiches = RD.Loading }
                , updateUrl model
                )

            else
                ( model, Effect.none )

        DoSearch ( filters, pagination ) ->
            let
                nextUrl =
                    serializeModel ( filters, pagination )

                requestChanged =
                    nextUrl /= model.lastUrlFetched
            in
            if requestChanged then
                ( { model
                    | pagination = pagination
                    , filters = filters
                    , fiches = RD.Loading
                    , lastUrlFetched = nextUrl
                    , fichesRequests = model.fichesRequests + 1
                  }
                , Effect.batch
                    [ Effect.fromCmd (Http.cancel <| getFichesTracker ++ "-" ++ String.fromInt model.fichesRequests)
                    , Effect.fromCmd (getFiches (model.fichesRequests + 1) ( filters, pagination ))
                    ]
                )

            else
                ( model, Effect.none )

        ClearAllFilters ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = { defaultFilters | recherche = model.filters.recherche }, selectRue = updatedSelectRue }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSearch recherche ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | recherche = recherche }) }
            in
            ( newModel
            , Effect.none
            )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites

                newModel =
                    { model
                        | selectActivites = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | activites = activitesUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | activites =
                                                filters.activites
                                                    |> List.filter ((/=) activite)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDemandes ( demandes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes

                demandesUniques =
                    case demandes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                demandes

                newModel =
                    { model
                        | selectDemandes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | demandes = demandesUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectDemandes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes
            in
            ( { model
                | selectDemandes = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectDemande demande ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | demandes =
                                                filters.demandes
                                                    |> List.filter ((/=) demande)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZones ( zones, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones

                zonesUniques =
                    case zones of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                zones

                newModel =
                    { model
                        | selectZones = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zones = zonesUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectZones sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones
            in
            ( { model
                | selectZones = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZone zone ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zones =
                                                filters.zones
                                                    |> List.filter ((/=) zone)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots

                newModel =
                    { model
                        | selectMots = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | mots = motsUniques })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | mots =
                                                filters.mots
                                                    |> List.filter ((/=) mot)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleSelection fichesToToggle add ->
            let
                entiteInSelection : EntityId EntiteId -> Bool
                entiteInSelection selected =
                    List.any
                        (\ficheToToggle ->
                            ficheToToggle == selected
                        )
                        fichesToToggle

                action selected =
                    selected
                        |> List.filter (entiteInSelection >> not)
                        -- remove any fichesToToggle from the current list if present
                        |> (if add then
                                -- add all of them to the selection
                                (++) fichesToToggle

                            else
                                -- do nothing, they're already gone
                                identity
                           )

                selectedEntites =
                    model.selectedEntites
                        |> action
            in
            { model | selectedEntites = selectedEntites }
                |> Effect.withShared (Shared.BackUpEntitesSelection selectedEntites)

        EmptySelection ->
            { model | selectedEntites = [] }
                |> Effect.withShared (Shared.BackUpEntitesSelection [])

        ClickedQualifierFiches ->
            { model
                | batchRequest = initBatchRequest
            }
                |> Effect.withNone

        CanceledQualifierFiches ->
            { model | batchRequest = Nothing }
                |> Effect.withNone

        ConfirmedQualifierFiches ->
            case model.batchRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just br ->
                    { model
                        | batchRequest =
                            Just { br | request = RD.Loading }
                    }
                        |> Effect.withCmd (sendBatchRequest model.filters model.pagination model.selectedEntites br)

        ReceivedQualifierFiches response ->
            let
                ( fiches, pagination, cmd ) =
                    case response of
                        RD.Success ( f, p, _ ) ->
                            ( RD.Success f, p, getFiltersSource )

                        _ ->
                            ( model.fiches, model.pagination, Cmd.none )
            in
            { model
                | batchRequest =
                    model.batchRequest
                        |> Maybe.map (\br -> { br | request = response })
                , pagination = pagination
                , fiches = fiches
            }
                |> Effect.withCmd cmd

        SelectedBatchActivites ( activites, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites

                        activitesUniques =
                            case activites of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        activites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect, activites = activitesUniques }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchActivites sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchActivite activite ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | activites =
                                        br.activites
                                            |> List.filter ((/=) activite)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchLocalisations ( localisations, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations

                        localisationsUniques =
                            case localisations of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        localisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations = localisationsUniques
                                    , selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchLocalisations sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchLocalisation localisation ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations =
                                        br.localisations
                                            |> List.filter ((/=) localisation)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchMots ( mots, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots

                        motsUniques =
                            case mots of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        mots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots = motsUniques
                                    , selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchMots sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchMot mot ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots =
                                        br.mots
                                            |> List.filter ((/=) mot)
                                }
                      }
                    , Effect.none
                    )

        SelectedRue ( apiRue, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model
                        | selectRue = updatedSelect
                        , filters =
                            model.filters
                                |> (\f -> { f | rue = Just apiRue })
                    }
            in
            ( newModel
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectRue sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model | selectRue = updatedSelect }
            in
            ( newModel, Effect.fromCmd selectCmd )

        ClearRue ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = model.filters |> (\f -> { f | rue = Nothing }), selectRue = updatedSelectRue }
            in
            ( newModel, Effect.fromCmd selectCmd )

        ClickedOrderBy orderBy orderDirection ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | orderBy = orderBy, orderDirection = orderDirection }) }
            in
            ( newModel
            , updateUrl newModel
            )

        ClickedSearch ->
            ( model, updateUrl model )


initBatchRequest : Maybe BatchRequest
initBatchRequest =
    Just
        { request = RD.NotAsked
        , selectActivites = initSelectActivites
        , selectLocalisations = initSelectLocalisations
        , selectMots = initSelectMots
        , activites = []
        , localisations = []
        , mots = []
        }


initSelectActivites : MultiSelectRemote.SmartSelect Msg String
initSelectActivites =
    MultiSelectRemote.init "select-activites"
        { selectionMsg = SelectedBatchActivites
        , internalMsg = UpdatedSelectBatchActivites
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectLocalisations : MultiSelectRemote.SmartSelect Msg String
initSelectLocalisations =
    MultiSelectRemote.init "select-zones"
        { selectionMsg = SelectedBatchLocalisations
        , internalMsg = UpdatedSelectBatchLocalisations
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectMots : MultiSelectRemote.SmartSelect Msg String
initSelectMots =
    MultiSelectRemote.init "select-mots"
        { selectionMsg = SelectedBatchMots
        , internalMsg = UpdatedSelectBatchMots
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = Decode.list <| Decode.field "activite" Decode.string
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = Decode.list <| Decode.field "motCle" Decode.string
    }


selectRueId : String
selectRueId =
    "champ-selection-rue"


selectRueConfig : SingleSelectRemote.SelectConfig ApiStreet
selectRueConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiStreet
    }


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.batch
        [ Effect.fromShared <|
            Shared.Navigate <|
                Route.Createurs <|
                    Just <|
                        serializeModel ( model.filters, model.pagination )
        , Effect.fromCmd <| Lib.UI.scrollElementTo (\_ -> NoOp) Nothing ( 0, 0 )
        ]


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Créateurs d'entreprise"
    , body = UI.Layout.lazyBody body model
    , route =
        Route.Createurs <|
            Just <|
                serializeModel ( model.filters, model.pagination )
    }


body : Model -> Html Msg
body model =
    case ( showingUnfilteredResults model.lastUrlFetched, model.fiches ) of
        ( True, RD.Success [] ) ->
            div [ class "fr-card--white text-center p-20" ]
                [ div [ Grid.gridRow ]
                    [ div [ Grid.col3, class "flex justify-center" ]
                        [ decorativeImg [ Attr.src "/assets/deveco-carte-identite.svg" ] ]
                    , div [ Grid.col9, class "flex flex-col fr-card--white text-center justify-center" ]
                        [ p []
                            [ text "Créez votre première fiche "
                            , span [ Typo.textBold, class "blue-text" ] [ text "Créateur d'entreprise" ]
                            , text " (porteur de projet)"
                            ]
                        , div [ class "flex justify-center" ]
                            [ DSFR.Button.new { onClick = Nothing, label = "Créer une fiche" }
                                |> DSFR.Button.linkButton (Route.toUrl <| Route.CreateurNew)
                                |> DSFR.Button.view
                            ]
                        ]
                    ]
                ]

        _ ->
            let
                { activites, demandes, zones, mots } =
                    case model.filtersSource of
                        RD.Success filtersSource ->
                            filtersSource

                        _ ->
                            { activites = [], demandes = [], zones = [], mots = [] }
            in
            div []
                [ viewMaybe (viewModal model.pagination.results model.selectedEntites) model.batchRequest
                , div [ Grid.gridRow, Grid.gridRowGutters, Grid.gridRowMiddle ] <|
                    [ h1 [ class "fr-h6 !my-0", Grid.colOffsetSm4, Grid.colSm4, Grid.col12 ]
                        [ div []
                            [ text <|
                                if showingUnfilteredResults model.lastUrlFetched then
                                    "Créateurs d'entreprise"

                                else
                                    "Résultats de la recherche"
                            , model.pagination
                                |> (.results >> formatNumberWithThousandSpacing >> (\t -> " (" ++ t ++ ")") >> text)
                            ]
                        ]
                    , let
                        selectedLength =
                            model.selectedEntites |> List.length

                        results =
                            model.pagination.results
                      in
                      div [ Grid.colSm4, Grid.col12, class "flex flex-row items-baseline justify-end" ]
                        [ div []
                            [ DSFR.Button.dropdownSelector { label = "Actions", hint = Just "Actions du portefeuille", id = "portefeuille-actions" } <|
                                [ DSFR.Button.new { onClick = Nothing, label = "Créer une fiche" }
                                    |> DSFR.Button.linkButton (Route.toUrl <| Route.CreateurNew)
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.withAttrs [ class "!w-full" ]
                                    |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                                    |> DSFR.Button.view
                                , DSFR.Button.new
                                    { label =
                                        if selectedLength > 0 then
                                            "Qualifier la sélection"

                                        else if showingUnfilteredResults model.lastUrlFetched then
                                            "Qualifier les Créateurs ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"

                                        else
                                            "Qualifier les résultats ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"
                                    , onClick = Just <| ClickedQualifierFiches
                                    }
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                                    |> DSFR.Button.withAttrs [ class "!w-full" ]
                                    |> DSFR.Button.view
                                , let
                                    lab =
                                        if selectedLength > 0 then
                                            "Exporter la sélection ("
                                                ++ String.fromInt selectedLength
                                                ++ " fiche"
                                                ++ plural selectedLength
                                                ++ ")"

                                        else if showingUnfilteredResults model.lastUrlFetched then
                                            "Exporter les Créateurs ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"

                                        else
                                            "Exporter les résultats ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"
                                  in
                                  form
                                    [ Attr.action
                                        (Api.getExportFiches <|
                                            model.lastUrlFetched
                                        )
                                    , Attr.method "POST"
                                    , Attr.target "_blank"
                                    ]
                                    [ input
                                        [ Attr.type_ "hidden"
                                        , Attr.name "ids"
                                        , Attr.value <| String.join "," <| List.map Api.EntityId.entityIdToString <| model.selectedEntites
                                        ]
                                        []
                                    , DSFR.Button.new { onClick = Nothing, label = lab }
                                        |> DSFR.Button.submit
                                        |> DSFR.Button.tertiaryNoOutline
                                        |> DSFR.Button.withDisabled (results == 0)
                                        |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                                        |> DSFR.Button.withAttrs [ class "!w-full" ]
                                        |> DSFR.Button.view
                                    ]
                                ]
                            ]
                        ]
                    ]
                , div [ Grid.gridRow ] <|
                    [ div [ Grid.col12, Grid.colSm4 ]
                        [ filterPanel model.selectActivites model.selectDemandes model.selectZones model.selectMots model.selectRue activites demandes zones mots model.filters model.pagination model.lastUrlFetched ]
                    , div [ Grid.col12, Grid.colSm8 ]
                        [ div [ Grid.gridRow ] <|
                            List.singleton <|
                                Html.Lazy.lazy4 viewFicheList model.selectedEntites model.pagination model.filters model.fiches
                        ]
                    ]
                ]


viewModal : Int -> List (EntityId EntiteId) -> BatchRequest -> Html Msg
viewModal foundFiches selectedEntites { request, selectLocalisations, selectActivites, selectMots, activites, localisations, mots } =
    DSFR.Modal.view
        { id = "batch-tag"
        , label = "batch-tag"
        , openMsg = NoOp
        , closeMsg = Just <| CanceledQualifierFiches
        , title =
            text <|
                case selectedEntites of
                    [] ->
                        "Qualifier "
                            ++ String.fromInt foundFiches
                            ++ " fiche"
                            ++ (if foundFiches > 1 then
                                    "s"

                                else
                                    ""
                               )

                    _ ->
                        "Qualifier "
                            ++ (String.fromInt <| List.length <| selectedEntites)
                            ++ " fiche"
                            ++ (if List.length selectedEntites > 1 then
                                    "s"

                                else
                                    ""
                               )
        , opened = True
        }
        (let
            disabled =
                request == RD.Loading
         in
         div []
            [ div [ Grid.gridRow, Grid.gridRowGutters ]
                [ div [ Grid.col12 ]
                    [ case request of
                        RD.Success ( _, _, results ) ->
                            div [ class "flex flex-col gap-2" ]
                                [ viewIf (List.length activites > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Activité(s) ajoutée(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " activites
                                            ]
                                        ]
                                , viewIf (List.length localisations > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Localisation(s) ajoutée(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " localisations
                                            ]
                                        ]
                                , viewIf (List.length mots > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Mot(s)-clé(s) ajouté(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " mots
                                            ]
                                        ]
                                , div [ class "flex flex-col gap-4" ]
                                    [ div [ Typo.textBold ]
                                        [ text "Résultat de la mise à jour\u{00A0}:"
                                        ]
                                    , let
                                        { total, successes, errors } =
                                            results
                                      in
                                      div [] <|
                                        [ if total == successes then
                                            div [ class "fr-text-default--success" ] [ text "Les ", text <| String.fromInt successes, text " fiches ont été qualifiées avec succès\u{00A0}!" ]

                                          else if successes > 0 then
                                            div [ class "fr-text-default--success" ] [ text <| String.fromInt <| successes, text " fiches ont été qualifiées avec succès." ]

                                          else
                                            nothing
                                        , viewIf (List.length errors > 0) <|
                                            div [ class "fr-text-default--error" ]
                                                [ text "Les fiches suivantes n'ont pas été qualifiées correctement\u{00A0}:"
                                                ]
                                        , viewIf (List.length errors > 0) <|
                                            div [ Grid.gridRow ] <|
                                                List.map
                                                    (\ficheName ->
                                                        div [ Grid.col6, class "fr-text-default--error" ]
                                                            [ DSFR.Icons.icon DSFR.Icons.System.closeCircleFill
                                                            , text " "
                                                            , text ficheName
                                                            ]
                                                    )
                                                <|
                                                    errors
                                        ]
                                    ]
                                ]

                        _ ->
                            div []
                                [ div [ Grid.gridRow, Grid.gridRowGutters ]
                                    [ div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectActivites
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = activites
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Attr.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                                                    , noOptionsMsg = "Aucune activité n'a été trouvée"
                                                    , newOption = Just identity
                                                    }
                                            , activites
                                                |> List.map (\activite -> DSFR.Tag.deletable UnselectBatchActivite { data = activite, toString = identity })
                                                |> DSFR.Tag.medium
                                            ]
                                        ]
                                    , div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectLocalisations
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = localisations
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Attr.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                                                    , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                                    , newOption = Just identity
                                                    }
                                            , localisations
                                                |> List.map (\localisation -> DSFR.Tag.deletable UnselectBatchLocalisation { data = localisation, toString = identity })
                                                |> DSFR.Tag.medium
                                            ]
                                        ]
                                    ]
                                , div [ Grid.gridRow, Grid.gridRowGutters ]
                                    [ div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectMots
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = mots
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Attr.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                                                    , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                                    , newOption = Just identity
                                                    }
                                            , mots
                                                |> List.map (\mot -> DSFR.Tag.deletable UnselectBatchMot { data = mot, toString = identity })
                                                |> DSFR.Tag.medium
                                            ]
                                        ]
                                    ]
                                ]
                    ]
                ]
            , div [ Grid.gridRow, Grid.gridRowGutters ]
                [ div [ Grid.col12 ]
                    [ (case request of
                        RD.Success _ ->
                            [ DSFR.Button.new
                                { onClick = Just <| CanceledQualifierFiches
                                , label = "OK"
                                }
                                |> DSFR.Button.withDisabled disabled
                            ]

                        _ ->
                            [ DSFR.Button.new
                                { onClick = Just <| ConfirmedQualifierFiches
                                , label =
                                    if request == RD.Loading then
                                        "Qualification en cours..."

                                    else
                                        "Confirmer"
                                }
                                |> DSFR.Button.withDisabled disabled
                            , DSFR.Button.new { onClick = Just <| CanceledQualifierFiches, label = "Annuler" }
                                |> DSFR.Button.withDisabled disabled
                                |> DSFR.Button.secondary
                            ]
                      )
                        |> DSFR.Button.group
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]
            ]
        )
        Nothing
        |> Tuple.first


titleWithBadge : String -> Int -> List (Html msg)
titleWithBadge title selected =
    [ div [ class "flex flex-row gap-4" ]
        [ span [] [ text title ]
        , if selected > 0 then
            span [ class "circled" ]
                [ text <| String.fromInt <| selected
                ]

          else
            nothing
        ]
    ]


filterPanel : MultiSelect.SmartSelect Msg String -> MultiSelect.SmartSelect Msg TypeDemande -> MultiSelect.SmartSelect Msg String -> MultiSelect.SmartSelect Msg String -> SingleSelectRemote.SmartSelect Msg ApiStreet -> List String -> List TypeDemande -> List String -> List String -> Filters -> Pagination -> String -> Html Msg
filterPanel selectActivites selectDemandes selectZones selectMots selectRue optionsActivites optionsDemandes optionsZones optionsMots ({ activites, demandes, zones, mots, rue } as filters) pagination lastUrlFetched =
    div [ class "p-2" ]
        [ div [ class "p-4 fr-card--white" ]
            [ DSFR.SearchBar.searchBar
                { submitMsg = FetchFiches
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedSearch
                , inputLabel = "Rechercher dans les créateurs d'entreprise"
                , inputPlaceholder = Nothing
                , inputId = rechercherFicheInputName
                , inputValue = filters.recherche
                , hints =
                    "Recherche par nom, nom d'enseigne pressenti"
                        |> text
                        |> List.singleton
                , fullLabel =
                    Just <|
                        label
                            [ class "fr-label !mb-0"
                            , Typo.textBold
                            , Typo.textLg
                            , Attr.for rechercherFicheInputName
                            ]
                            [ text "Rechercher dans les créateurs d'entreprise" ]
                }
            , text "\u{00A0}"
            , hr [ class "fr-hr" ] []
            , div [ class "flex flex-col" ]
                [ div [ class "flex flex-row items-center justify-between mb-4" ]
                    [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtrer" ]
                    , Html.Lazy.lazy clearAllFiltersButton filters
                    ]
                , let
                    selected =
                        List.length activites + List.length zones + List.length mots
                  in
                  DSFR.Accordion.raw
                    { id = "filter-group-qualification"
                    , title = titleWithBadge "Champs qualifiés" selected
                    , content =
                        [ Html.Lazy.lazy3 activitesFilter selectActivites optionsActivites activites
                        , Html.Lazy.lazy3 zonesFilter selectZones optionsZones zones
                        , Html.Lazy.lazy3 motsFilter selectMots optionsMots mots
                        ]
                    , borderless = False
                    }
                , let
                    selected =
                        List.length demandes
                  in
                  DSFR.Accordion.raw
                    { id = "filter-group-demandes"
                    , title = titleWithBadge "Demandes" selected
                    , content =
                        [ Html.Lazy.lazy3 demandesFilter selectDemandes optionsDemandes demandes
                        ]
                    , borderless = False
                    }
                , let
                    selected =
                        rue |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0
                  in
                  DSFR.Accordion.raw
                    { id = "filter-group-localisation"
                    , title = titleWithBadge "Adresse" selected
                    , content =
                        [ Html.Lazy.lazy2 rueFilter selectRue rue
                        ]
                    , borderless = False
                    }
                , DSFR.Button.new { label = "Appliquer les filtres", onClick = Just ClickedSearch }
                    |> DSFR.Button.withDisabled (lastUrlFetched == serializeModel ( filters, pagination ))
                    |> DSFR.Button.view
                    |> List.singleton
                    |> div [ class "flex flex-row justify-center mt-4" ]
                ]
            ]
        ]


rueFilter : SingleSelectRemote.SmartSelect Msg ApiStreet -> Maybe ApiStreet -> Html Msg
rueFilter selectRue selectedRue =
    div [ class "flex flex-col gap-2" ]
        [ selectRue
            |> SingleSelectRemote.viewCustom
                { isDisabled = False
                , selected = selectedRue
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Adresse" ]
                , searchPrompt = "Rechercher une adresse"
                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune adresse n'a été trouvée"
                , error = Nothing
                }
        , selectedRue
            |> viewMaybe (\rue -> DSFR.Tag.deletable (\_ -> ClearRue) { data = rue.label, toString = identity } |> List.singleton |> DSFR.Tag.medium)
        ]


activitesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
activitesFilter selectActivites optionsActivites activites =
    div [ class "flex flex-col gap-4" ]
        [ selectActivites
            |> MultiSelect.viewCustom
                { isDisabled = optionsActivites |> List.length |> (==) 0
                , selected = activites
                , options = optionsActivites
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Activité réelle et filière"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune activité n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par activité"
                }
        , activites
            |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
            |> DSFR.Tag.medium
        ]


zonesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
zonesFilter selectZones optionsZones zones =
    div [ class "flex flex-col gap-4" ]
        [ selectZones
            |> MultiSelect.viewCustom
                { isDisabled = optionsZones |> List.length |> (==) 0
                , selected = zones
                , options = optionsZones
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Zone géographique"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par zone géographique"
                }
        , zones
            |> List.map (\zone -> DSFR.Tag.deletable UnselectZone { data = zone, toString = identity })
            |> DSFR.Tag.medium
        ]


motsFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
motsFilter selectMots optionsMots mots =
    div [ class "flex flex-col gap-4" ]
        [ selectMots
            |> MultiSelect.viewCustom
                { isDisabled = optionsMots |> List.length |> (==) 0
                , selected = mots
                , options = optionsMots
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Mots-clés"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par mot-clé"
                }
        , mots
            |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
            |> DSFR.Tag.medium
        ]


demandesFilter : MultiSelect.SmartSelect Msg TypeDemande -> List TypeDemande -> List TypeDemande -> Html Msg
demandesFilter selectDemandes optionsDemandes demandes =
    div [ class "flex flex-col gap-4" ]
        [ selectDemandes
            |> MultiSelect.viewCustom
                { isDisabled = optionsDemandes |> List.length |> (==) 0
                , selected = demandes
                , options = optionsDemandes
                , optionLabelFn = Data.Demande.typeDemandeToDisplay
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Demandes en cours"
                , viewSelectedOptionFn = Data.Demande.typeDemandeToDisplay >> text
                , noResultsForMsg = \searchText -> "Aucune autre demande n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune demande n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << Data.Demande.typeDemandeToDisplay)
                            allOptions
                , searchPrompt = "Filtrer par demande"
                }
        , demandes
            |> List.map (\demande -> DSFR.Tag.deletable UnselectDemande { data = demande, toString = Data.Demande.typeDemandeToDisplay })
            |> DSFR.Tag.medium
        ]


clearAllFiltersButton : Filters -> Html Msg
clearAllFiltersButton filters =
    DSFR.Button.new
        { label = "Tout effacer"
        , onClick = Just <| ClearAllFilters
        }
        |> DSFR.Button.withAttrs [ class "!p-0" ]
        |> DSFR.Button.withDisabled ({ filters | recherche = "" } == { defaultFilters | recherche = "" })
        |> DSFR.Button.tertiaryNoOutline
        |> DSFR.Button.view


type alias Pagination =
    { page : Int
    , pages : Int
    , results : Int
    }


viewFicheList : List (EntityId EntiteId) -> Pagination -> Filters -> RD.WebData (List Fiche) -> Html Msg
viewFicheList selectedEntites pagination filters fiches =
    case fiches of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ Grid.col12 ]
                [ []
                    |> viewSelection filters selectedEntites
                , div [ Grid.gridRow ] <|
                    List.repeat 3 <|
                        cardPlaceholder
                ]

        RD.Failure _ ->
            div [ class "text-center", Grid.col, class "p-2" ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ Grid.col12 ]
                [ []
                    |> viewSelection filters selectedEntites
                , div [ class "p-2" ]
                    [ div [ class "fr-card--white h-[250px] text-center p-20" ]
                        [ text "Aucune fiche ne correspond à cette recherche."
                        , text " "
                        , viewMaybe (\_ -> text "Moteur de recherche par adresse en cours de perfectionnement\u{00A0}: certaines adresses peuvent ne pas être trouvées.") filters.rue
                        ]
                    ]
                ]

        RD.Success fs ->
            div [ Grid.col12 ]
                [ fs
                    |> viewSelection filters selectedEntites
                , fs
                    |> List.map (ficheCard selectedEntites)
                    |> div [ Grid.gridRow ]
                , if pagination.pages > 1 then
                    div [ class "!mt-4" ]
                        [ DSFR.Pagination.view pagination.page pagination.pages <|
                            toHref ( filters, pagination )
                        ]

                  else
                    nothing
                ]


showingUnfilteredResults : String -> Bool
showingUnfilteredResults lastUrlFetched =
    lastUrlFetched
        |> String.split "&"
        |> List.all
            (\q ->
                List.member q [ "type=particulier", "d=ASC", "o=AZ" ]
            )


codeToOrderBy : String -> Msg
codeToOrderBy code =
    case code of
        "FMDESC" ->
            ClickedOrderBy FicheModification Desc

        "FMASC" ->
            ClickedOrderBy FicheModification Asc

        "AZASC" ->
            ClickedOrderBy Alphabetical Asc

        _ ->
            ClickedOrderBy FicheModification Desc


viewSelection : Filters -> List (EntityId EntiteId) -> List Fiche -> Html Msg
viewSelection { orderBy, orderDirection } selectedEntites fichesOnPage =
    let
        entiteIdsOfFichesOnPage =
            fichesOnPage
                |> List.map Fiche.entiteId

        inSelected id =
            selectedEntites
                |> List.member id
    in
    div [ class "flex flex-col p-2 gap-2" ]
        [ DSFR.Checkbox.single
            { value = "toggle-page"
            , checked =
                if List.length entiteIdsOfFichesOnPage == 0 then
                    Just False

                else if List.all inSelected entiteIdsOfFichesOnPage then
                    Just True

                else if List.all (inSelected >> not) entiteIdsOfFichesOnPage then
                    Just False

                else
                    Nothing
            , valueAsString = identity
            , id = "fiche-page-selection"
            , label = "Sélectionner les fiches de cette page"
            , onChecked = \_ bool -> ToggleSelection entiteIdsOfFichesOnPage bool
            }
            |> DSFR.Checkbox.singleWithDisabled (List.length entiteIdsOfFichesOnPage == 0)
            |> DSFR.Checkbox.viewSingle
            |> List.singleton
            |> div [ class "flex shrink" ]
        , case selectedEntites of
            [] ->
                nothing

            _ ->
                let
                    length =
                        List.length selectedEntites
                in
                div [ class "fr-notice--info flex flex-row justify-between items-center gap-2 p-2" ] <|
                    [ p [ class "!mb-0" ]
                        [ text "Il y a "
                        , span [ Typo.textBold, class "blue-text" ]
                            [ text <| String.fromInt <| length
                            , text " fiche"
                            , viewIf (length > 1) (text "s")
                            ]
                        , text " dans la sélection"
                        ]
                    , DSFR.Button.new
                        { label = "Vider la sélection"
                        , onClick = Just <| EmptySelection
                        }
                        |> DSFR.Button.withDisabled (List.length selectedEntites == 0)
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withAttrs [ class "!mb-0" ]
                        |> DSFR.Button.view
                    ]
        , div [ class "flex flex-row justify-end items-baseline gap-4" ]
            [ text "Trier par"
            , div
                [ class "fr-select-group"
                ]
                [ label
                    [ class "fr-label"
                    , Attr.for "select"
                    ]
                    []
                , select
                    [ class "fr-select"
                    , Attr.id "select"
                    , Attr.name "select"
                    , Events.onInput codeToOrderBy
                    ]
                    [ option
                        [ Attr.value "FMDESC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( FicheModification, Desc )
                        ]
                        [ text "Fiches modifiées récemment en premier" ]
                    , option
                        [ Attr.value "FMASC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( FicheModification, Asc )
                        ]
                        [ text "Fiches modifiées récemment en dernier" ]
                    , option
                        [ Attr.value "AZASC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( Alphabetical, Asc )
                        ]
                        [ text "Noms classés par ordre alphabétique" ]
                    ]
                ]
            ]
        ]


ficheCard : List (EntityId EntiteId) -> Fiche -> Html Msg
ficheCard selectedEntites fiche =
    let
        selected =
            List.member (Fiche.entiteId fiche) selectedEntites
    in
    div [ Grid.col12, Grid.colSm6, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 gap-2 min-h-[18rem] border-2"
                , Attr.style "background-image" "none"
                , classList [ ( "dark-blue-border", selected ), ( "border-transparent", not selected ) ]
                ]
                [ div [ class "flex flex-row justify-between relative p-2" ]
                    [ div [ class "flex flex-row items-center gap-2" ]
                        [ UI.Entite.icon fiche.entite.type_
                        , UI.Entite.badgeInactif <|
                            Fiche.statut fiche
                        , UI.Entite.badgeExogene <|
                            Fiche.exogene fiche
                        ]
                    , div [ class "absolute top-[12px] right-0" ]
                        [ DSFR.Checkbox.single
                            { value = fiche
                            , checked = Just <| selected
                            , valueAsString = Fiche.id >> Api.EntityId.entityIdToString
                            , id = "fiche-selection-" ++ (fiche |> Fiche.id |> Api.EntityId.entityIdToString)
                            , label = ""
                            , onChecked = Fiche.entiteId >> List.singleton >> ToggleSelection
                            }
                            |> DSFR.Checkbox.viewSingle
                        ]
                    ]
                , a
                    [ class "flex flex-col grow gap-2 custom-hover p-2"
                    , Attr.style "background-image" "none"
                    , Attr.href <|
                        Route.toUrl <|
                            Route.Createur <|
                                Fiche.id fiche
                    ]
                    [ div
                        [ Typo.textBold
                        , class "!mb-0"
                        , class "line-clamp-1"
                        ]
                      <|
                        List.singleton <|
                            text <|
                                Fiche.nom fiche
                    , viewIf (Entite.isEntreprise fiche.entite) <|
                        div
                            [ class "!mb-0"
                            , class "line-clamp-1"
                            , Attr.title <| Fiche.adresse fiche
                            ]
                        <|
                            List.singleton <|
                                text <|
                                    withEmptyAs "-" <|
                                        Fiche.adresse fiche
                    , Fiche.getParticulier fiche
                        |> viewMaybe
                            (.email
                                >> withEmptyAs "-"
                                >> Lib.UI.infoLine (Just 1) "Email\u{00A0}: "
                                >> List.singleton
                                >> div [ class "!mb-0" ]
                            )
                    , Fiche.getParticulier fiche
                        |> viewMaybe
                            (\_ ->
                                fiche
                                    |> Fiche.futureEnseigne
                                    |> Maybe.withDefault ""
                                    |> withEmptyAs "-"
                                    |> Lib.UI.infoLine (Just 1) "Nom d'enseigne pressenti\u{00A0}: "
                                    |> List.singleton
                                    |> div [ class "!mb-0" ]
                            )
                    , Fiche.getParticulier fiche
                        |> viewMaybe
                            (.telephone
                                >> withEmptyAs "-"
                                >> Lib.UI.infoLine (Just 1) "Téléphone\u{00A0}: "
                                >> List.singleton
                                >> div [ class "!mb-0" ]
                            )
                    , Fiche.getEntreprise fiche
                        |> viewMaybe
                            (.activitePrincipaleUniteLegale
                                >> Maybe.withDefault "-"
                                >> Lib.UI.infoLine (Just 2) "Activité NAF\u{00A0}: "
                                >> List.singleton
                                >> div [ class "!mb-0" ]
                            )
                    , let
                        activites =
                            Entite.activitesReelles <|
                                .entite <|
                                    fiche
                      in
                      div [ class "!mb-0" ] <|
                        [ Lib.UI.infoLine (Just 1)
                            ("Activité"
                                ++ (if List.length activites > 1 then
                                        "s"

                                    else
                                        ""
                                   )
                                ++ " réelle"
                                ++ (if List.length activites > 1 then
                                        "s"

                                    else
                                        ""
                                   )
                                ++ "\u{00A0}: "
                            )
                          <|
                            case activites of
                                [] ->
                                    "-"

                                act ->
                                    String.join "\u{00A0}— " <|
                                        act
                        ]
                    , let
                        demandes =
                            fiche.demandes |> List.filter (Data.Demande.cloture >> not)
                      in
                      div [ class "!mb-0" ] <|
                        [ Lib.UI.infoLine (Just 1)
                            ("Demande"
                                ++ (if List.length demandes > 1 then
                                        "s"

                                    else
                                        ""
                                   )
                                ++ " en cours\u{00A0}: "
                            )
                          <|
                            case demandes of
                                [] ->
                                    "-"

                                ds ->
                                    String.join "\u{00A0}— " <|
                                        List.map Data.Demande.label <|
                                            ds
                        ]
                    ]
                ]
            ]
        ]


cardPlaceholder : Html Msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2", Typo.textSm ]
            [ div
                [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> List.repeat length
        |> String.join ""
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]
