module Pages.Local.New exposing (Model, Msg, page)

import Accessibility exposing (Html, div, formWithListeners, h1, span, sup, text)
import Api
import Api.EntityId exposing (EntityId)
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Radio
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Local as Local
import Dict exposing (Dict)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Variables exposing (hintZoneGeographique)
import MultiSelectRemote
import RemoteData as RD
import Route
import Shared
import SingleSelectRemote exposing (SelectConfig)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Model =
    { local : DataLocal
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    , selectedAdresse : Maybe ApiAdresse
    , validationLocal : RD.RemoteData FormErrors ()
    , enregistrementLocal : RD.WebData (EntityId Local.LocalId)
    }


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-localisations"


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ =
    ( { local = defaultDataLocal
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectAdresse =
            SingleSelectRemote.init "champ-selection-adresse"
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectedAdresse = Nothing
      , validationLocal = RD.NotAsked
      , enregistrementLocal = RD.NotAsked
      }
    , Effect.none
    )
        |> Shared.pageChangeEffects


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model |> .selectAdresse |> SingleSelectRemote.subscriptions
        , model |> .selectLocalisations |> MultiSelectRemote.subscriptions
        ]


apiAdresseToOption : ApiAdresse -> String
apiAdresseToOption =
    .label


defaultDataLocal : DataLocal
defaultDataLocal =
    { titre = ""
    , adresse = ""
    , ville = ""
    , codePostal = ""
    , geolocation = ""
    , statut = Local.Occupe
    , types = []
    , surface = ""
    , loyer = ""
    , localisations = []
    , commentaire = ""
    }


type alias DataLocal =
    { titre : String
    , adresse : String
    , ville : String
    , codePostal : String
    , geolocation : String
    , statut : Local.LocalStatut
    , types : List Local.LocalType
    , surface : String
    , loyer : String
    , localisations : List String
    , commentaire : String
    }


type Msg
    = UpdatedLocalForm LocalField String
    | SetStatut Local.LocalStatut
    | ToggleType Bool Local.LocalType
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | RequestedCreationLocal
    | ReceivedCreationLocal (RD.WebData (EntityId Local.LocalId))
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)


type LocalField
    = LocalTitre
    | LocalSurface
    | LocalLoyer
    | LocalCommentaire


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        UpdatedLocalForm field value ->
            ( { model | local = updateLocalForm field value model.local }, Effect.none )

        SetStatut statut ->
            ( { model | local = model.local |> (\l -> { l | statut = statut }) }, Effect.none )

        ToggleType on type_ ->
            let
                newModel =
                    { model
                        | local =
                            model.local
                                |> (\local ->
                                        { local
                                            | types =
                                                if on then
                                                    if List.member type_ local.types then
                                                        local.types

                                                    else
                                                        local.types |> List.reverse |> (::) type_ |> List.reverse

                                                else
                                                    List.filter ((/=) type_) local.types
                                        }
                                   )
                    }
            in
            ( newModel, Effect.none )

        ReceivedCreationLocal enregistrementLocal ->
            let
                cmd =
                    case enregistrementLocal of
                        RD.Success id ->
                            Effect.fromShared <|
                                Shared.Navigate <|
                                    Route.Local id

                        _ ->
                            Effect.none
            in
            ( { model | enregistrementLocal = enregistrementLocal }
            , cmd
            )

        RequestedCreationLocal ->
            let
                parsedLocal =
                    validateLocal model

                ( enregistrementLocal, effect ) =
                    case parsedLocal of
                        RD.Success () ->
                            ( RD.Loading, createLocal model )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationLocal = parsedLocal, enregistrementLocal = enregistrementLocal }, effect )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse

                local =
                    model.local
                        |> (\p ->
                                { p
                                    | ville = apiAdresse.city
                                    , codePostal = apiAdresse.postcode
                                    , adresse = apiAdresse.label
                                    , geolocation = "(" ++ String.fromFloat apiAdresse.x ++ "," ++ String.fromFloat apiAdresse.y ++ ")"
                                }
                           )
            in
            ( { model | selectedAdresse = Just apiAdresse, selectAdresse = updatedSelect, local = local }, Effect.fromCmd selectCmd )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse
            in
            ( { model | selectAdresse = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedLocalisations ( localisations, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                localisationsUniques =
                    case localisations of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                localisations
            in
            ( { model
                | selectLocalisations = updatedSelect
                , local =
                    model.local
                        |> (\l ->
                                { l | localisations = localisationsUniques }
                           )
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            ( { model
                | local =
                    model.local
                        |> (\l ->
                                { l
                                    | localisations =
                                        l.localisations
                                            |> List.filter ((/=) localisation)
                                }
                           )
              }
            , Effect.none
            )


selectConfig : SelectConfig ApiAdresse
selectConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


type alias FormErrors =
    { global : Maybe String, fields : Dict String (List String) }


formErrorsFor : String -> FormErrors -> Maybe (List String)
formErrorsFor key { fields } =
    fields
        |> Dict.get key


validateLocal : Model -> RD.RemoteData FormErrors ()
validateLocal { local } =
    if String.trim local.titre == "" || String.trim local.adresse == "" then
        let
            errors =
                [ ( "titre", .titre, "Ce champ est obligatoire" )
                , ( "adresse", .adresse, "Ce champ est obligatoire" )
                ]
                    |> List.foldl
                        (\( key, accessor, err ) dict ->
                            if accessor local == "" then
                                Dict.update key
                                    (\existingList ->
                                        case existingList of
                                            Just list ->
                                                Just <| list ++ [ err ]

                                            Nothing ->
                                                Just [ err ]
                                    )
                                    dict

                            else
                                dict
                        )
                        Dict.empty
        in
        RD.Failure
            { global =
                if Dict.isEmpty errors then
                    Nothing

                else
                    Just "Veuillez corriger les erreurs."
            , fields = errors
            }

    else
        RD.Success ()


updateLocalForm : LocalField -> String -> DataLocal -> DataLocal
updateLocalForm field value local =
    case field of
        LocalTitre ->
            { local | titre = value }

        LocalSurface ->
            { local | surface = value }

        LocalLoyer ->
            { local | loyer = value }

        LocalCommentaire ->
            { local | commentaire = value }


createLocal : Model -> Effect.Effect Shared.Msg Msg
createLocal { local } =
    let
        encodedLocal =
            local
                |> (\{ titre, adresse, ville, codePostal, geolocation, statut, types, surface, loyer, localisations, commentaire } ->
                        Encode.object
                            [ ( "titre", Encode.string titre )
                            , ( "adresse", Encode.string adresse )
                            , ( "ville", Encode.string ville )
                            , ( "codePostal", Encode.string codePostal )
                            , ( "geolocation", Encode.string geolocation )
                            , ( "localStatut", Local.encodeLocalStatut statut )
                            , ( "localTypes", Encode.list Local.encodeLocalType types )
                            , ( "surface", Encode.string surface )
                            , ( "loyer", Encode.string loyer )
                            , ( "localisations", Encode.list Encode.string localisations )
                            , ( "commentaire", Encode.string commentaire )
                            ]
                   )
                |> Http.jsonBody
    in
    Http.post
        { url = Api.createLocal
        , body = encodedLocal
        , expect = Http.expectJson (RD.fromResult >> ReceivedCreationLocal) <| Decode.field "id" Api.EntityId.decodeEntityId
        }
        |> Effect.fromCmd


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Nouveau local"
    , body = UI.Layout.lazyBody body model
    , route = Route.LocalNew
    }


localTypeForm : Model -> Html Msg
localTypeForm model =
    let
        formErrors =
            case model.validationLocal of
                RD.Failure errors ->
                    Just errors

                _ ->
                    Nothing
    in
    div [ class "flex flex-col gap-8" ]
        [ div [ class "flex flex-col gap-4" ]
            [ div [ class "flex flex-col sm:max-w-[60%]" ]
                [ div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                    [ DSFR.Input.new
                        { value = model.local.titre
                        , onInput = UpdatedLocalForm LocalTitre
                        , label = text "Titre *"
                        , name = "nouveau-local-titre"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "titre") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    ]
                , span [ class "my-4" ]
                    [ model.selectAdresse
                        |> SingleSelectRemote.viewCustom
                            { isDisabled = False
                            , selected = model.selectedAdresse
                            , optionLabelFn = apiAdresseToOption
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = text "Adresse *"
                            , searchPrompt = "Rechercher une adresse"
                            , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                            , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                            , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                            , noOptionsMsg = "Aucune adresse n'a été trouvée"
                            , error = formErrors |> Maybe.andThen (formErrorsFor "adresse") |> Maybe.map (List.map text)
                            }
                    ]
                , DSFR.Radio.group
                    { id = "locaux-statut"
                    , options = [ Local.Occupe, Local.Vacant ]
                    , current = Just model.local.statut
                    , toLabel =
                        (\s ->
                            case s of
                                Local.Occupe ->
                                    "Local occupé"

                                Local.Vacant ->
                                    "Local vacant"
                        )
                            >> text
                    , toId =
                        \s ->
                            case s of
                                Local.Occupe ->
                                    "occupe"

                                Local.Vacant ->
                                    "vacant"
                    , msg = SetStatut
                    , legend = text "Statut du local"
                    }
                    |> DSFR.Radio.inline
                    |> DSFR.Radio.withExtraAttrs [ class "sm:max-w-[60%]" ]
                    |> DSFR.Radio.view
                , DSFR.Checkbox.group
                    { id = "locaux-types"
                    , label = span [] [ text "Type de local" ]
                    , onChecked =
                        \filter bool ->
                            ToggleType bool <| filter
                    , values =
                        [ Local.Commerce
                        , Local.Bureaux
                        , Local.AteliersArtisanaux
                        , Local.BatimentsIndustriels
                        , Local.Entrepot
                        ]
                    , checked = model.local.types
                    , valueAsString = Local.localTypeToString
                    , toId = Local.localTypeToString
                    , toLabel = Local.localTypeToDisplay
                    }
                    |> DSFR.Checkbox.inline
                    |> DSFR.Checkbox.viewGroup
                , div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                    [ DSFR.Input.new
                        { value = model.local.surface
                        , onInput = UpdatedLocalForm LocalSurface
                        , label = text "Surface total (m²)"
                        , name = "nouveau-local-surface"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "surface") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    , DSFR.Input.new
                        { value = model.local.loyer
                        , onInput = UpdatedLocalForm LocalLoyer
                        , label = text "Loyer annuel (€)"
                        , name = "nouveau-local-loyer"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "loyer") |> Maybe.map (List.map text))
                        |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                        |> DSFR.Input.view
                    ]
                ]
            ]
        , div []
            [ div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                [ model.selectLocalisations
                    |> MultiSelectRemote.viewCustom
                        { isDisabled = False
                        , selected = model.local.localisations
                        , optionLabelFn = identity
                        , optionDescriptionFn = \_ -> ""
                        , optionsContainerMaxHeight = 300
                        , selectTitle = span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                        , viewSelectedOptionFn = text
                        , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                        , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                        , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                        , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                        , newOption = Just identity
                        }
                , model.local.localisations
                    |> List.map (\localisation -> DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity })
                    |> DSFR.Tag.medium
                ]
            ]
        , div [ class "flex flex-col sm:max-w-[60%]" ]
            [ DSFR.Input.new
                { value = model.local.commentaire
                , onInput = UpdatedLocalForm LocalCommentaire
                , label = text "Commentaires (commerces et transports à proximité, restriction d'activité, etc.)"
                , name = "nouveau-local-activite"
                }
                |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "activite") |> Maybe.map (List.map text))
                |> DSFR.Input.textArea (Just 8)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [] [ text "Créer une fiche local d'activité" ]
        , formWithListeners [ Events.onSubmit <| RequestedCreationLocal, class "flex flex-col gap-8" ]
            [ localTypeForm model
            , footer model
            ]
        ]


footer : Model -> Html Msg
footer model =
    let
        isButtonDisabled =
            model.enregistrementLocal == RD.Loading
    in
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationLocal of
                RD.Failure { global } ->
                    case global of
                        Nothing ->
                            nothing

                        Just error ->
                            DSFR.Alert.small { title = Nothing, description = error }
                                |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementLocal of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = "Créer la fiche" }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled isButtonDisabled
            , DSFR.Button.new { onClick = Nothing, label = "Annuler" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Locaux Nothing)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
