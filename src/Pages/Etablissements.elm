module Pages.Etablissements exposing (Model, Msg, makeQueryForPortefeuilleMode, page)

import Accessibility exposing (a, div, form, h1, h2, label, option, select, span, sup, text)
import Api
import Api.EntityId exposing (EntityId)
import DSFR.Accordion
import DSFR.Button
import DSFR.Checkbox
import DSFR.Grid as Grid
import DSFR.Icons
import DSFR.Icons.Communication
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Pagination
import DSFR.Radio
import DSFR.SearchBar
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiStreet, decodeApiStreet)
import Data.Commune exposing (Commune)
import Data.Demande exposing (TypeDemande)
import Data.Effectifs exposing (Effectifs)
import Data.Entite exposing (Entite)
import Data.Entreprise exposing (Entreprise, EtatAdministratif(..), etatAdministratifToDisplay, etatAdministratifToString)
import Data.Fiche exposing (Fiche)
import Data.FormeJuridique
import Data.Naf exposing (CodeNaf)
import Data.Role
import Data.Territoire
import Effect
import Html exposing (Html, hr, input, p)
import Html.Attributes as Attr exposing (class, classList)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf, viewMaybe)
import Html.Lazy
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (formatNumberWithThousandSpacing, plural, withEmptyAs)
import Lib.Variables exposing (hintActivites, hintMotsCles, hintSuiviPortefeuille, hintSuiviTous, hintZoneGeographique)
import MultiSelect
import MultiSelectRemote
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote
import Spa.Page exposing (Page)
import UI.Entite
import UI.Layout
import User
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { etablissementSelection } user =
    Spa.Page.onNewFlags (parseRawQuery >> DoSearch) <|
        Spa.Page.element <|
            { init = init etablissementSelection user
            , update = update
            , view = view
            , subscriptions = subscriptions
            }


type alias Siret =
    String


type alias Model =
    { etablissements : WebData (List EtablissementWithExtraInfo)
    , etablissementsRequests : Int
    , filters : Filters
    , filtersSource : WebData FiltersSource
    , selectCodesNaf : MultiSelectRemote.SmartSelect Msg CodeNaf
    , selectCommunes : MultiSelect.SmartSelect Msg Commune
    , selectEffectifs : MultiSelect.SmartSelect Msg Effectifs
    , selectRue : SingleSelectRemote.SmartSelect Msg ApiStreet
    , selectActivites : MultiSelect.SmartSelect Msg String
    , selectDemandes : MultiSelect.SmartSelect Msg TypeDemande
    , selectZones : MultiSelect.SmartSelect Msg String
    , selectMots : MultiSelect.SmartSelect Msg String
    , pagination : Pagination
    , lastUrlFetched : String
    , isEpci : Bool
    , communes : WebData (List Commune)
    , selectedEtablissements : List Siret
    , batchRequest : Maybe BatchRequest
    }


type alias Filters =
    { recherche : String
    , etatType : Maybe EtatAdministratif
    , codesNaf : List CodeNaf
    , formesJuridiques : FormesJuridiques
    , communes : List Commune
    , effectifs : List Effectifs
    , rue : Maybe ApiStreet
    , suivi : Suivi
    , activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    , orderBy : OrderBy
    , orderDirection : OrderDirection
    , territorialite : Maybe Territorialite
    }


makeQueryForPortefeuilleMode : () -> String
makeQueryForPortefeuilleMode () =
    serializeModel ( { defaultFilters | suivi = Portefeuille }, defaultPagination )


type FormesJuridiques
    = Courantes
    | Toutes


type Suivi
    = Tout
    | Portefeuille


type OrderBy
    = Alphabetical
    | FicheModification
    | EtablissementCreation


type OrderDirection
    = Desc
    | Asc


type Territorialite
    = Endogene
    | Exogene


territorialiteToLabel : Maybe Territorialite -> String
territorialiteToLabel territorialite =
    case territorialite of
        Just Endogene ->
            "Sur le territoire"

        Just Exogene ->
            "Hors du territoire"

        Nothing ->
            "Tous"


territorialiteToString : Maybe Territorialite -> String
territorialiteToString territorialite =
    case territorialite of
        Just Endogene ->
            "endogene"

        Just Exogene ->
            "exogene"

        Nothing ->
            "tous"


stringToTerritorialite : String -> Maybe Territorialite
stringToTerritorialite s =
    case s of
        "exogene" ->
            Just Exogene

        "endogene" ->
            Just Endogene

        "tous" ->
            Nothing

        _ ->
            Nothing


defaultFilters : Filters
defaultFilters =
    { recherche = ""
    , etatType = Nothing
    , codesNaf = []
    , formesJuridiques = Courantes
    , communes = []
    , effectifs = []
    , rue = Nothing
    , suivi = Tout
    , activites = []
    , demandes = []
    , zones = []
    , mots = []
    , orderBy = EtablissementCreation
    , orderDirection = Desc
    , territorialite = Nothing
    }


type alias FiltersSource =
    { activites : List String
    , demandes : List TypeDemande
    , zones : List String
    , mots : List String
    }


type alias BatchRequest =
    { request : WebData ( List EtablissementWithExtraInfo, Pagination, BatchResults )
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , activites : List String
    , localisations : List String
    , mots : List String
    }


type alias BatchResults =
    { total : Int
    , successes : Int
    , errors : List String
    }


type Msg
    = NoOp
    | FetchEtablissements
    | ReceivedFiltersSource (WebData FiltersSource)
    | DoSearch ( Filters, Pagination )
    | UpdatedSearch String
    | ReceivedEntreprises (WebData ( List EtablissementWithExtraInfo, Pagination ))
    | ClearAllFilters
    | SetEtatTypeFilter (Maybe EtatAdministratif)
    | SetSuiviFilter Suivi
    | SetFormesJuridiquesFilter FormesJuridiques
    | SetTerritorialiteFilter (Maybe Territorialite)
    | SelectedCodesNaf ( List CodeNaf, MultiSelectRemote.Msg CodeNaf )
    | UpdatedSelectCodesNaf (MultiSelectRemote.Msg CodeNaf)
    | UnselectedCodeNaf CodeNaf
    | ReceivedCommunes (WebData (List Commune))
    | SelectedCommunes ( List Commune, MultiSelect.Msg Commune )
    | UpdatedSelectCommunes (MultiSelect.Msg Commune)
    | UnselectCommune Commune
    | SelectedEffectifs ( List Effectifs, MultiSelect.Msg Effectifs )
    | UpdatedSelectEffectifs (MultiSelect.Msg Effectifs)
    | UnselectedEffectifs Effectifs
    | SelectedRue ( ApiStreet, SingleSelectRemote.Msg ApiStreet )
    | UpdatedSelectRue (SingleSelectRemote.Msg ApiStreet)
    | ClearRue
    | SelectedActivites ( List String, MultiSelect.Msg String )
    | UpdatedSelectActivites (MultiSelect.Msg String)
    | UnselectActivite String
    | SelectedDemandes ( List TypeDemande, MultiSelect.Msg TypeDemande )
    | UpdatedSelectDemandes (MultiSelect.Msg TypeDemande)
    | UnselectDemande TypeDemande
    | SelectedZones ( List String, MultiSelect.Msg String )
    | UpdatedSelectZones (MultiSelect.Msg String)
    | UnselectZone String
    | SelectedMots ( List String, MultiSelect.Msg String )
    | UpdatedSelectMots (MultiSelect.Msg String)
    | UnselectMot String
    | ToggleSelection (List Siret) Bool
    | EmptySelection
    | ClickedQualifierFiches
    | CanceledQualifierFiches
    | ConfirmedQualifierFiches
    | ReceivedQualifierFiches (WebData ( List EtablissementWithExtraInfo, Pagination, BatchResults ))
    | SelectedBatchActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchActivites (MultiSelectRemote.Msg String)
    | UnselectBatchActivite String
    | SelectedBatchLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchLocalisations (MultiSelectRemote.Msg String)
    | UnselectBatchLocalisation String
    | SelectedBatchMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectBatchMots (MultiSelectRemote.Msg String)
    | UnselectBatchMot String
    | ClickedOrderBy OrderBy OrderDirection
    | ClickedSearch


type alias EtablissementWithExtraInfo =
    { entreprise : Entreprise
    , entite : Maybe Entite
    , fiche : Maybe Fiche
    , infos : FicheInfos
    }


type alias FicheInfos =
    { suivi : Bool
    , accompagne : Bool
    }


parseFilters : String -> Filters
parseFilters rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    recherche =
                        query
                            |> QS.getAsStringList queryKeys.recherche
                            |> List.head
                            |> Maybe.withDefault ""

                    etatType =
                        query
                            |> QS.getAsStringList queryKeys.etat
                            |> List.head
                            |> Maybe.andThen
                                (\etat ->
                                    if etat == "F" then
                                        Just Inactif

                                    else if etat == "A" then
                                        Just Actif

                                    else
                                        Nothing
                                )

                    codesNaf =
                        query
                            |> QS.getAsStringList queryKeys.codesNaf
                            |> List.map (\id -> { id = id, label = "" })

                    effectifs =
                        query
                            |> QS.getAsStringList queryKeys.effectifs
                            |> List.map (\id -> { id = id, label = "" })

                    formesJuridiques =
                        query
                            |> QS.getAsStringList queryKeys.formesJuridiques
                            |> List.head
                            |> Maybe.map
                                (\formes ->
                                    if formes == "toutes" then
                                        Toutes

                                    else
                                        Courantes
                                )
                            |> Maybe.withDefault Courantes

                    communes =
                        query
                            |> QS.getAsStringList queryKeys.communes
                            |> List.map (\id -> { id = id, label = "", departement = "" })

                    rue =
                        query
                            |> QS.getAsStringList queryKeys.rue
                            |> List.head

                    cp =
                        query
                            |> QS.getAsStringList queryKeys.cp
                            |> List.head

                    suivi =
                        query
                            |> QS.getAsStringList queryKeys.suivi
                            |> List.head
                            |> Maybe.map
                                (\s ->
                                    if s == "portefeuille" then
                                        Portefeuille

                                    else
                                        Tout
                                )
                            |> Maybe.withDefault Tout

                    activites =
                        query
                            |> QS.getAsStringList queryKeys.activites

                    demandes =
                        query
                            |> QS.getAsStringList queryKeys.demandes
                            |> List.map Data.Demande.stringToTypeDemande
                            |> List.filterMap identity

                    zones =
                        query
                            |> QS.getAsStringList queryKeys.zones

                    mots =
                        query
                            |> QS.getAsStringList queryKeys.mots

                    orderBy =
                        query
                            |> QS.getAsStringList queryKeys.ordre
                            |> List.head
                            |> (\ob ->
                                    case ob of
                                        -- Just "AZ" ->
                                        --     Alphabetical
                                        --
                                        Just "DC" ->
                                            EtablissementCreation

                                        Just "DM" ->
                                            FicheModification

                                        Just _ ->
                                            EtablissementCreation

                                        Nothing ->
                                            EtablissementCreation
                               )

                    orderDirection =
                        query
                            |> QS.getAsStringList queryKeys.direction
                            |> List.head
                            |> (\ob ->
                                    case ob of
                                        Just "ASC" ->
                                            Asc

                                        Just "DESC" ->
                                            Desc

                                        Just _ ->
                                            Desc

                                        Nothing ->
                                            Desc
                               )

                    territorialite =
                        query
                            |> QS.getAsStringList queryKeys.territorialite
                            |> List.head
                            |> Maybe.map stringToTerritorialite
                            |> Maybe.withDefault Nothing
                in
                { recherche = recherche
                , etatType = etatType
                , codesNaf = codesNaf
                , formesJuridiques = formesJuridiques
                , communes = communes
                , effectifs = effectifs
                , rue = Maybe.map2 (\r c -> ApiStreet (r ++ " - " ++ c) r c "") rue cp
                , suivi = suivi
                , activites = activites
                , zones = zones
                , mots = mots
                , demandes = demandes
                , orderBy = orderBy
                , orderDirection = orderDirection
                , territorialite = territorialite
                }
           )


filtersToQuery : Filters -> QS.Query
filtersToQuery filters =
    let
        setType =
            case filters.etatType of
                Just Actif ->
                    "A"
                        |> QS.setStr queryKeys.etat

                Just Inactif ->
                    "F"
                        |> QS.setStr queryKeys.etat

                _ ->
                    identity

        setFormesJuridiques =
            case filters.formesJuridiques of
                Toutes ->
                    "toutes"
                        |> QS.setStr queryKeys.formesJuridiques

                _ ->
                    identity

        setRecherche =
            case filters.recherche of
                "" ->
                    identity

                recherche ->
                    recherche
                        |> QS.setStr queryKeys.recherche

        setCodesNaf =
            filters.codesNaf
                |> List.map .id
                |> QS.setListStr queryKeys.codesNaf

        setEffectifs =
            filters.effectifs
                |> List.map .id
                |> QS.setListStr queryKeys.effectifs

        setCodeCommune =
            filters.communes
                |> List.map .id
                |> QS.setListStr queryKeys.communes

        setRue =
            filters.rue
                |> Maybe.map .name
                |> Maybe.map (QS.setStr queryKeys.rue)
                |> Maybe.withDefault identity

        setCp =
            filters.rue
                |> Maybe.map .postcode
                |> Maybe.map (QS.setStr queryKeys.cp)
                |> Maybe.withDefault identity

        setSuivi =
            case filters.suivi of
                Portefeuille ->
                    "portefeuille"
                        |> QS.setStr queryKeys.suivi

                _ ->
                    identity

        setActivites =
            case filters.activites of
                [] ->
                    identity

                activites ->
                    activites
                        |> QS.setListStr queryKeys.activites

        setDemandes =
            case filters.demandes of
                [] ->
                    identity

                demandes ->
                    demandes
                        |> List.map Data.Demande.demandeTypeToString
                        |> QS.setListStr queryKeys.demandes

        setZones =
            case filters.zones of
                [] ->
                    identity

                zones ->
                    zones
                        |> QS.setListStr queryKeys.zones

        setMots =
            case filters.mots of
                [] ->
                    identity

                mots ->
                    mots
                        |> QS.setListStr queryKeys.mots

        setOrder =
            case filters.orderBy of
                -- Alphabetical ->
                --     QS.setStr queryKeys.ordre <|
                --         "AZ"
                --
                FicheModification ->
                    QS.setStr queryKeys.ordre <|
                        "DM"

                EtablissementCreation ->
                    identity

                _ ->
                    identity

        setDirection =
            case filters.orderDirection of
                Asc ->
                    QS.setStr queryKeys.direction <|
                        "ASC"

                Desc ->
                    identity

        setTerritorialite =
            case filters.territorialite of
                Just Exogene ->
                    QS.setStr queryKeys.territorialite <|
                        "exogene"

                Just Endogene ->
                    QS.setStr queryKeys.territorialite <|
                        "endogene"

                Nothing ->
                    identity
    in
    QS.empty
        |> setType
        |> setFormesJuridiques
        |> setRecherche
        |> setCodesNaf
        |> setCodeCommune
        |> setEffectifs
        |> setRue
        |> setCp
        |> setSuivi
        |> setActivites
        |> setZones
        |> setMots
        |> setDemandes
        |> setOrder
        |> setDirection
        |> setTerritorialite


parsePagination : String -> Pagination
parsePagination rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    p =
                        query
                            |> QS.getAsStringList "page"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1

                    pages =
                        query
                            |> QS.getAsStringList "pages"
                            |> List.head
                            |> Maybe.andThen String.toInt
                            |> Maybe.withDefault 1
                in
                { defaultPagination
                    | page = p
                    , pages = pages
                }
           )


defaultPagination : Pagination
defaultPagination =
    { page = 1, pages = 1, results = 0 }


paginationToQuery : Pagination -> QS.Query
paginationToQuery pagination =
    let
        setPage =
            if pagination.page > 1 then
                QS.setStr queryKeys.page <|
                    String.fromInt <|
                        pagination.page

            else
                identity
    in
    QS.empty
        |> setPage


queryKeys : { etat : String, recherche : String, page : String, pages : String, codesNaf : String, formesJuridiques : String, communes : String, effectifs : String, rue : String, cp : String, suivi : String, activites : String, demandes : String, zones : String, mots : String, ordre : String, direction : String, territorialite : String }
queryKeys =
    { etat = "etat"
    , recherche = "recherche"
    , page = "page"
    , pages = "pages"
    , codesNaf = "codesNaf"
    , formesJuridiques = "formes"
    , communes = "communes"
    , effectifs = "effectifs"
    , rue = "rue"
    , cp = "cp"
    , suivi = "suivi"
    , activites = "activites"
    , zones = "zones"
    , mots = "mots"
    , demandes = "demandes"
    , ordre = "o"
    , direction = "d"
    , territorialite = "territorialite"
    }


serializeModel : ( Filters, Pagination ) -> String
serializeModel ( filters, pagination ) =
    QS.merge (paginationToQuery pagination) (filtersToQuery filters)
        |> QS.serialize (QS.config |> QS.addQuestionMark False)


toHref : ( Filters, Pagination ) -> Int -> String
toHref ( filters, pagination ) newPage =
    Route.toUrl <|
        Route.Etablissements <|
            Just <|
                serializeModel ( filters, { pagination | page = newPage } )


parseRawQuery : Maybe String -> ( Filters, Pagination )
parseRawQuery rawQuery =
    case rawQuery of
        Nothing ->
            ( defaultFilters, defaultPagination )

        Just query ->
            ( parseFilters query, parsePagination query )


isEpci : Shared.User -> Bool
isEpci =
    User.role >> Data.Role.territoire >> Maybe.map Data.Territoire.isEpci >> Maybe.withDefault False


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.selectCodesNaf |> MultiSelectRemote.subscriptions
        , model.selectCommunes |> MultiSelect.subscriptions
        , model.selectEffectifs |> MultiSelect.subscriptions
        , model.selectRue |> SingleSelectRemote.subscriptions
        , model.selectActivites |> MultiSelect.subscriptions
        , model.selectDemandes |> MultiSelect.subscriptions
        , model.selectZones |> MultiSelect.subscriptions
        , model.selectMots |> MultiSelect.subscriptions
        , model.batchRequest
            |> Maybe.map .selectActivites
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectLocalisations
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.batchRequest
            |> Maybe.map .selectMots
            |> Maybe.map MultiSelectRemote.subscriptions
            |> Maybe.withDefault Sub.none
        , model.selectRue |> SingleSelectRemote.subscriptions
        ]


init : List Siret -> Shared.User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init etablissementSelection user rawQuery =
    let
        ( filters, pagination ) =
            parseRawQuery rawQuery

        effectifsIds =
            filters.effectifs
                |> List.map .id

        newEffectifs =
            Data.Effectifs.tranchesEffectifs
                |> List.filter (\{ id } -> List.member id effectifsIds)

        apiStreet =
            filters.rue |> Maybe.map .label |> Maybe.withDefault ""

        ( selectRue, selectRueCmd ) =
            SingleSelectRemote.init selectRueId
                { selectionMsg = SelectedRue
                , internalMsg = UpdatedSelectRue
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
                |> SingleSelectRemote.setText apiStreet selectRueConfig
    in
    ( { etablissements = RD.Loading
      , etablissementsRequests = 0
      , filters = { filters | effectifs = newEffectifs }
      , filtersSource = RD.Loading
      , selectCodesNaf =
            MultiSelectRemote.init selectCodesNafId
                { selectionMsg = SelectedCodesNaf
                , internalMsg = UpdatedSelectCodesNaf
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectCommunes =
            MultiSelect.init "select-commune"
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                }
      , selectEffectifs =
            MultiSelect.init "select-effectifs"
                { selectionMsg = SelectedEffectifs
                , internalMsg = UpdatedSelectEffectifs
                }
      , selectRue = selectRue
      , selectActivites =
            MultiSelect.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                }
      , selectDemandes =
            MultiSelect.init selectDemandesId
                { selectionMsg = SelectedDemandes
                , internalMsg = UpdatedSelectDemandes
                }
      , selectZones =
            MultiSelect.init selectZonesId
                { selectionMsg = SelectedZones
                , internalMsg = UpdatedSelectZones
                }
      , selectMots =
            MultiSelect.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                }
      , pagination = pagination
      , lastUrlFetched = serializeModel ( filters, pagination )
      , isEpci = isEpci user
      , communes = RD.Loading
      , batchRequest = Nothing
      , selectedEtablissements = etablissementSelection
      }
    , Effect.batch <|
        [ Effect.fromCmd <| getEntreprises 0 ( filters, pagination )
        , Effect.fromCmd <| getFiltersSource
        , Effect.fromCmd <|
            getCommunesForTerritoire <|
                Maybe.map Data.Territoire.id <|
                    Data.Role.territoire <|
                        User.role <|
                            user
        , Effect.fromCmd selectRueCmd
        ]
    )
        |> Shared.pageChangeEffects


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectCodesNafId : String
selectCodesNafId =
    "champ-selection-codes-naf"


selectCodesNafConfig : MultiSelectRemote.SelectConfig CodeNaf
selectCodesNafConfig =
    { headers = []
    , url = Api.rechercheCodeNaf
    , optionDecoder = Decode.list <| Data.Naf.decodeCodeNaf
    }


selectRueId : String
selectRueId =
    "champ-selection-rue"


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectDemandesId : String
selectDemandesId =
    "champ-selection-demandes"


selectZonesId : String
selectZonesId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectRueConfig : SingleSelectRemote.SelectConfig ApiStreet
selectRueConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiStreet
    }


initSelectActivites : MultiSelectRemote.SmartSelect Msg String
initSelectActivites =
    MultiSelectRemote.init "select-activites"
        { selectionMsg = SelectedBatchActivites
        , internalMsg = UpdatedSelectBatchActivites
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectLocalisations : MultiSelectRemote.SmartSelect Msg String
initSelectLocalisations =
    MultiSelectRemote.init "select-zones"
        { selectionMsg = SelectedBatchLocalisations
        , internalMsg = UpdatedSelectBatchLocalisations
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


initSelectMots : MultiSelectRemote.SmartSelect Msg String
initSelectMots =
    MultiSelectRemote.init "select-mots"
        { selectionMsg = SelectedBatchMots
        , internalMsg = UpdatedSelectBatchMots
        , characterSearchThreshold = selectCharacterThreshold
        , debounceDuration = selectDebounceDuration
        }


selectActivitesConfig : MultiSelectRemote.SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActivite
    , optionDecoder = Decode.list <| Decode.field "activite" Decode.string
    }


selectLocalisationsConfig : MultiSelectRemote.SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


selectMotsConfig : MultiSelectRemote.SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = Decode.list <| Decode.field "motCle" Decode.string
    }


getEtablissementsTracker : String
getEtablissementsTracker =
    "get-etablissements-tracker"


getEntreprises : Int -> ( Filters, Pagination ) -> Cmd Msg
getEntreprises trackerCount ( filters, pagination ) =
    Http.request
        { method = "GET"
        , headers = []
        , url = Api.searchEntreprises <| serializeModel ( filters, pagination )
        , body = Http.emptyBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedEntreprises) decodePayload
        , timeout = Nothing
        , tracker = Just <| getEtablissementsTracker ++ "-" ++ String.fromInt trackerCount
        }


getCommunesForTerritoire : Maybe (EntityId Data.Territoire.TerritoireId) -> Cmd Msg
getCommunesForTerritoire territoryId =
    case territoryId of
        Just id ->
            Http.get
                { url = Api.getCommunesForTerritoire id
                , expect = Http.expectJson (RD.fromResult >> ReceivedCommunes) <| Decode.list <| Data.Commune.decodeCommune
                }

        Nothing ->
            Cmd.none


sendBatchRequest : Filters -> Pagination -> List Siret -> BatchRequest -> Cmd Msg
sendBatchRequest filters pagination selectedEtablissements { activites, localisations, mots } =
    let
        jsonBody =
            [ ( "activites", Encode.list Encode.string activites )
            , ( "localisations", Encode.list Encode.string localisations )
            , ( "mots", Encode.list Encode.string mots )
            , ( "sirets", Encode.list Encode.string selectedEtablissements )
            ]
                |> (Encode.object >> Http.jsonBody)
    in
    Http.post
        { url = Api.updateQualificationsEntreprises <| serializeModel ( filters, pagination )
        , body = jsonBody
        , expect = Http.expectJson (RD.fromResult >> ReceivedQualifierFiches) <| decodeBatchResponse
        }


decodeBatchResponse : Decode.Decoder ( List EtablissementWithExtraInfo, Pagination, BatchResults )
decodeBatchResponse =
    Decode.succeed (\( f, p ) b -> ( f, p, b ))
        |> andMap decodePayload
        |> andMap (Decode.field "batchResults" <| decodeBatchResults)


decodeBatchResults : Decode.Decoder BatchResults
decodeBatchResults =
    Decode.succeed BatchResults
        |> andMap (Decode.field "total" Decode.int)
        |> andMap (Decode.field "successes" Decode.int)
        |> andMap (Decode.field "errors" <| Decode.list Decode.string)


decodePayload : Decode.Decoder ( List EtablissementWithExtraInfo, Pagination )
decodePayload =
    Decode.succeed (\p pages results data -> ( data, Pagination p pages results ))
        |> andMap (Decode.field "page" Decode.int)
        |> andMap (Decode.field "pages" Decode.int)
        |> andMap (Decode.field "results" Decode.int)
        |> andMap (Decode.field "data" decodeEntreprisesWithFiche)


decodeEntreprisesWithFiche : Decode.Decoder (List EtablissementWithExtraInfo)
decodeEntreprisesWithFiche =
    Decode.succeed EtablissementWithExtraInfo
        |> andMap (Decode.field "entreprise" Data.Entreprise.decodeEntreprise)
        |> andMap (optionalNullableField "entite" Data.Entite.decodeEntite)
        |> andMap (optionalNullableField "fiche" Data.Fiche.decodeFiche)
        |> andMap (Decode.field "infos" decodeFicheInfos)
        |> Decode.list


decodeFicheInfos : Decode.Decoder FicheInfos
decodeFicheInfos =
    Decode.succeed FicheInfos
        |> andMap (Decode.field "suivi" Decode.bool)
        |> andMap (Decode.field "accompagne" Decode.bool)


getFiltersSource : Cmd Msg
getFiltersSource =
    Http.get
        { url = Api.getFichesFiltersSource
        , expect = Http.expectJson (RD.fromResult >> ReceivedFiltersSource) decodeFiltersSource
        }


decodeFiltersSource : Decode.Decoder FiltersSource
decodeFiltersSource =
    Decode.succeed FiltersSource
        |> andMap (Decode.field "activites" <| Decode.list Decode.string)
        |> andMap (Decode.field "demandes" <| Decode.list Data.Demande.decodeTypeDemande)
        |> andMap (Decode.field "zones" <| Decode.list Decode.string)
        |> andMap (Decode.field "mots" <| Decode.list Decode.string)


rechercherEntrepriseInputName : String
rechercherEntrepriseInputName =
    "rechercher-etablissement"


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )

        ReceivedEntreprises entreprises ->
            case entreprises of
                RD.Success ( fs, pagination ) ->
                    ( { model | etablissements = RD.Success fs, pagination = pagination }
                    , Effect.none
                    )

                _ ->
                    ( { model | etablissements = entreprises |> RD.map Tuple.first }
                    , Effect.none
                    )

        ReceivedFiltersSource response ->
            { model | filtersSource = response }
                |> Effect.withNone

        FetchEtablissements ->
            let
                nextUrl =
                    serializeModel ( model.filters, model.pagination )
            in
            if nextUrl /= model.lastUrlFetched then
                ( { model | etablissements = RD.Loading }
                , updateUrl model
                )

            else
                ( model, Effect.none )

        DoSearch ( filters, pagination ) ->
            let
                nextUrl =
                    serializeModel ( filters, pagination )

                requestChanged =
                    nextUrl /= model.lastUrlFetched

                communesSources =
                    model.communes
                        |> RD.toMaybe

                communes =
                    filters.communes

                newCommunes =
                    communes
                        |> List.map
                            (\comm ->
                                communesSources
                                    |> Maybe.map (List.filter (.id >> (==) comm.id))
                                    |> Maybe.andThen List.head
                                    |> Maybe.withDefault comm
                            )

                effectifsIds =
                    filters.effectifs
                        |> List.map .id

                newEffectifs =
                    Data.Effectifs.tranchesEffectifs
                        |> List.filter (\{ id } -> List.member id effectifsIds)

                currentApiStreet =
                    model.filters.rue

                apiStreet =
                    case currentApiStreet of
                        Nothing ->
                            filters.rue

                        Just current ->
                            filters.rue
                                |> Maybe.andThen
                                    (\rue ->
                                        if rue.name == current.name && rue.postcode == current.postcode then
                                            Just current

                                        else
                                            filters.rue
                                    )

                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.setText (apiStreet |> Maybe.map .label |> Maybe.withDefault "") selectRueConfig model.selectRue
            in
            if requestChanged then
                ( { model
                    | pagination = pagination
                    , filters = { filters | communes = newCommunes, effectifs = newEffectifs, rue = apiStreet }
                    , etablissements = RD.Loading
                    , etablissementsRequests = model.etablissementsRequests + 1
                    , lastUrlFetched = nextUrl
                    , selectRue = updatedSelect
                  }
                , Effect.batch
                    [ Effect.fromCmd (getEntreprises (model.etablissementsRequests + 1) ( filters, pagination ))
                    , Effect.fromCmd selectCmd
                    , Effect.fromCmd (Http.cancel <| getEtablissementsTracker ++ "-" ++ String.fromInt model.etablissementsRequests)
                    ]
                )

            else
                ( model, Effect.none )

        ClearAllFilters ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = { defaultFilters | recherche = model.filters.recherche }, selectRue = updatedSelectRue }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSearch recherche ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | recherche = recherche }) }
            in
            ( newModel
            , Effect.none
            )

        SetEtatTypeFilter filter ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | etatType = filter }) }
            in
            ( newModel
            , Effect.none
            )

        SetSuiviFilter suivi ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | suivi = suivi }) }
            in
            ( newModel
            , Effect.none
            )

        SetFormesJuridiquesFilter filter ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | formesJuridiques = filter }) }
            in
            ( newModel
            , Effect.none
            )

        SetTerritorialiteFilter territorialite ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | territorialite = territorialite }) }
            in
            ( newModel
            , Effect.none
            )

        SelectedCodesNaf ( codesNaf, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCodesNafConfig model.selectCodesNaf

                codesNafUniques =
                    case codesNaf of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                codesNaf

                newModel =
                    { model
                        | selectCodesNaf = updatedSelect
                        , filters =
                            model.filters
                                |> (\l ->
                                        { l | codesNaf = codesNafUniques }
                                   )
                    }
            in
            ( newModel
            , Effect.batch [ Effect.none, Effect.fromCmd selectCmd ]
            )

        UpdatedSelectCodesNaf sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectCodesNafConfig model.selectCodesNaf
            in
            ( { model
                | selectCodesNaf = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedCodeNaf codeNaf ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\f ->
                                        { f
                                            | codesNaf =
                                                f.codesNaf
                                                    |> List.filter ((/=) codeNaf)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ReceivedCommunes resp ->
            let
                communesSources =
                    resp
                        |> RD.withDefault []

                communes =
                    model.filters.communes

                newCommunes =
                    communes
                        |> List.map
                            (\comm ->
                                communesSources
                                    |> List.filter (.id >> (==) comm.id)
                                    |> List.head
                                    |> Maybe.withDefault comm
                            )
            in
            { model
                | communes = resp
                , filters =
                    model.filters |> (\f -> { f | communes = newCommunes })
            }
                |> Effect.withNone

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | communes = communesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | communes =
                                                filters.communes
                                                    |> List.filter (\c -> c.id /= commune.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedEffectifs ( effectifs, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEffectifs

                effectifsUniques =
                    case effectifs of
                        [] ->
                            []

                        e :: rest ->
                            if List.member e rest then
                                rest

                            else
                                effectifs

                newModel =
                    { model
                        | selectEffectifs = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | effectifs = effectifsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectEffectifs sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectEffectifs
            in
            ( { model
                | selectEffectifs = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectedEffectifs effectifs ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | effectifs =
                                                filters.effectifs
                                                    |> List.filter ((/=) effectifs)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedRue ( apiRue, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model
                        | selectRue = updatedSelect
                        , filters =
                            model.filters
                                |> (\f -> { f | rue = Just apiRue })
                    }
            in
            ( newModel
            , Effect.batch [ Effect.fromCmd selectCmd, Effect.none ]
            )

        UpdatedSelectRue sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectRueConfig model.selectRue

                newModel =
                    { model | selectRue = updatedSelect }
            in
            ( newModel, Effect.batch [ Effect.fromCmd selectCmd, Effect.none ] )

        ClearRue ->
            let
                ( updatedSelectRue, selectCmd ) =
                    SingleSelectRemote.setText "" selectRueConfig model.selectRue

                newModel =
                    { model | filters = model.filters |> (\f -> { f | rue = Nothing }), selectRue = updatedSelectRue }
            in
            ( newModel, Effect.batch [ Effect.fromCmd selectCmd, Effect.none ] )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites

                newModel =
                    { model
                        | selectActivites = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | activites = activitesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | activites =
                                                filters.activites
                                                    |> List.filter ((/=) activite)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedDemandes ( demandes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes

                demandesUniques =
                    case demandes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                demandes

                newModel =
                    { model
                        | selectDemandes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | demandes = demandesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectDemandes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectDemandes
            in
            ( { model
                | selectDemandes = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectDemande demande ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | demandes =
                                                filters.demandes
                                                    |> List.filter ((/=) demande)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedZones ( zones, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones

                zonesUniques =
                    case zones of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                zones

                newModel =
                    { model
                        | selectZones = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | zones = zonesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectZones sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectZones
            in
            ( { model
                | selectZones = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectZone zone ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | zones =
                                                filters.zones
                                                    |> List.filter ((/=) zone)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots

                motsUniques =
                    case mots of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                mots

                newModel =
                    { model
                        | selectMots = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | mots = motsUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.none
                ]
            )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | mots =
                                                filters.mots
                                                    |> List.filter ((/=) mot)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.none
            )

        ToggleSelection etablissementsToToggle add ->
            let
                etablissementInSelection : Siret -> Bool
                etablissementInSelection selected =
                    List.any
                        (\etablissementToToggle ->
                            etablissementToToggle == selected
                        )
                        etablissementsToToggle

                action selected =
                    selected
                        |> List.filter (etablissementInSelection >> not)
                        -- remove any fichesToToggle from the current list if present
                        |> (if add then
                                -- add all of them to the selection
                                (++) etablissementsToToggle

                            else
                                -- do nothing, they're already gone
                                identity
                           )

                selectedFiches =
                    model.selectedEtablissements
                        |> action
            in
            { model | selectedEtablissements = selectedFiches }
                |> Effect.withShared (Shared.BackUpEtablissementsSelection selectedFiches)

        EmptySelection ->
            { model | selectedEtablissements = [] }
                |> Effect.withShared (Shared.BackUpEtablissementsSelection [])

        ClickedQualifierFiches ->
            { model
                | batchRequest = initBatchRequest
            }
                |> Effect.withNone

        CanceledQualifierFiches ->
            { model | batchRequest = Nothing }
                |> Effect.withNone

        ConfirmedQualifierFiches ->
            case model.batchRequest of
                Nothing ->
                    model
                        |> Effect.withNone

                Just br ->
                    { model
                        | batchRequest =
                            Just { br | request = RD.Loading }
                    }
                        |> Effect.withCmd (sendBatchRequest model.filters model.pagination model.selectedEtablissements br)

        ReceivedQualifierFiches response ->
            let
                ( entreprises, pagination, cmd ) =
                    case response of
                        RD.Success ( f, p, _ ) ->
                            ( RD.Success f, p, getFiltersSource )

                        _ ->
                            ( model.etablissements, model.pagination, Cmd.none )
            in
            { model
                | batchRequest =
                    model.batchRequest
                        |> Maybe.map (\br -> { br | request = response })
                , pagination = pagination
                , etablissements = entreprises
            }
                |> Effect.withCmd cmd

        SelectedBatchActivites ( activites, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites

                        activitesUniques =
                            case activites of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        activites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect, activites = activitesUniques }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchActivites sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectActivitesConfig br.selectActivites
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br | selectActivites = updatedSelect }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchActivite activite ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | activites =
                                        br.activites
                                            |> List.filter ((/=) activite)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchLocalisations ( localisations, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations

                        localisationsUniques =
                            case localisations of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        localisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations = localisationsUniques
                                    , selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchLocalisations sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectLocalisationsConfig br.selectLocalisations
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectLocalisations = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchLocalisation localisation ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | localisations =
                                        br.localisations
                                            |> List.filter ((/=) localisation)
                                }
                      }
                    , Effect.none
                    )

        SelectedBatchMots ( mots, sMsg ) ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots

                        motsUniques =
                            case mots of
                                [] ->
                                    []

                                a :: rest ->
                                    if List.member a rest then
                                        rest

                                    else
                                        mots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots = motsUniques
                                    , selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UpdatedSelectBatchMots sMsg ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    let
                        ( updatedSelect, selectCmd ) =
                            MultiSelectRemote.update sMsg selectMotsConfig br.selectMots
                    in
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | selectMots = updatedSelect
                                }
                      }
                    , Effect.fromCmd selectCmd
                    )

        UnselectBatchMot mot ->
            case model.batchRequest of
                Nothing ->
                    ( model, Effect.none )

                Just br ->
                    ( { model
                        | batchRequest =
                            Just
                                { br
                                    | mots =
                                        br.mots
                                            |> List.filter ((/=) mot)
                                }
                      }
                    , Effect.none
                    )

        ClickedOrderBy orderBy orderDirection ->
            let
                newModel =
                    { model | filters = model.filters |> (\f -> { f | orderBy = orderBy, orderDirection = orderDirection }) }
            in
            ( newModel
            , updateUrl newModel
            )

        ClickedSearch ->
            ( model, updateUrl model )


initBatchRequest : Maybe BatchRequest
initBatchRequest =
    Just
        { request = RD.NotAsked
        , selectActivites = initSelectActivites
        , selectLocalisations = initSelectLocalisations
        , selectMots = initSelectMots
        , activites = []
        , localisations = []
        , mots = []
        }


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.batch
        [ Effect.fromShared <|
            Shared.Navigate <|
                Route.Etablissements <|
                    Just <|
                        serializeModel ( model.filters, model.pagination )
        , Effect.fromCmd <| Lib.UI.scrollElementTo (\_ -> NoOp) Nothing ( 0, 0 )
        ]


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Établissements"
    , body = UI.Layout.lazyBody body model
    , route =
        Route.Etablissements <|
            Just <|
                serializeModel ( model.filters, model.pagination )
    }


body : Model -> Html Msg
body model =
    case ( showingUnfilteredResults model.lastUrlFetched, model.etablissements ) of
        ( True, RD.Success [] ) ->
            div [ class "fr-card--white text-center p-20" ]
                [ div [ Grid.gridRow ]
                    [ text "Aucun établissement trouvé. C'est sans doute une erreur de notre côté, veuillez nous contacter\u{00A0}!"
                    ]
                ]

        _ ->
            div []
                [ viewMaybe (viewModal model.pagination.results model.selectedEtablissements) model.batchRequest
                , div [ Grid.gridRow, Grid.gridRowGutters, Grid.gridRowMiddle ] <|
                    [ h1 [ class "fr-h6 !my-0", Grid.colOffsetSm4, Grid.colSm4, Grid.col12 ]
                        [ div []
                            [ text <|
                                if showingUnfilteredResults model.lastUrlFetched then
                                    "Établissements du territoire"

                                else
                                    "Résultats de la recherche"
                            , model.pagination
                                |> (.results >> formatNumberWithThousandSpacing >> (\t -> " (" ++ t ++ ")") >> text)
                            ]
                        ]
                    , let
                        selectedLength =
                            model.selectedEtablissements |> List.length

                        results =
                            model.pagination.results
                      in
                      div [ Grid.colSm4, Grid.col12, class "flex flex-row items-baseline justify-end" ]
                        [ div []
                            [ DSFR.Button.dropdownSelector { label = "Actions", hint = Just "Actions", id = "etablissements-actions" } <|
                                [ DSFR.Button.new { onClick = Nothing, label = "Ajouter un Établissement exogène" }
                                    |> DSFR.Button.linkButton (Route.toUrl <| Route.EtablissementNew)
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.withAttrs [ class "!w-full" ]
                                    |> DSFR.Button.leftIcon DSFR.Icons.System.addLine
                                    |> DSFR.Button.view
                                , DSFR.Button.new
                                    { label =
                                        if selectedLength > 0 then
                                            "Qualifier la sélection"

                                        else if showingUnfilteredResults model.lastUrlFetched then
                                            "Qualifier les établissements ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"

                                        else
                                            "Qualifier les résultats ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"
                                    , onClick = Just <| ClickedQualifierFiches
                                    }
                                    |> DSFR.Button.tertiaryNoOutline
                                    |> DSFR.Button.leftIcon DSFR.Icons.Design.editLine
                                    |> DSFR.Button.withAttrs [ class "!w-full" ]
                                    |> DSFR.Button.view
                                , let
                                    lab =
                                        if selectedLength > 0 then
                                            "Exporter la sélection ("
                                                ++ String.fromInt selectedLength
                                                ++ " fiche"
                                                ++ plural selectedLength
                                                ++ ")"

                                        else if showingUnfilteredResults model.lastUrlFetched then
                                            "Exporter Mes établissements ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"

                                        else
                                            "Exporter les résultats ("
                                                ++ String.fromInt results
                                                ++ " fiche"
                                                ++ plural results
                                                ++ ")"
                                  in
                                  form
                                    [ Attr.action
                                        (Api.getExportEntreprises <|
                                            model.lastUrlFetched
                                        )
                                    , Attr.method "POST"
                                    , Attr.target "_blank"
                                    ]
                                    [ input
                                        [ Attr.type_ "hidden"
                                        , Attr.name "ids"
                                        , Attr.value <| String.join "," <| model.selectedEtablissements
                                        ]
                                        []
                                    , DSFR.Button.new { onClick = Nothing, label = lab }
                                        |> DSFR.Button.submit
                                        |> DSFR.Button.tertiaryNoOutline
                                        |> DSFR.Button.withDisabled (results == 0)
                                        |> DSFR.Button.leftIcon DSFR.Icons.System.downloadLine
                                        |> DSFR.Button.withAttrs [ class "!w-full" ]
                                        |> DSFR.Button.view
                                    ]
                                ]
                            ]
                        ]
                    ]
                , div [ Grid.gridRow ] <|
                    [ div [ Grid.col12, Grid.colSm4 ]
                        [ filterPanel model ]
                    , div [ Grid.col12, Grid.colSm8 ]
                        [ div [ Grid.gridRow ] <|
                            List.singleton <|
                                viewEntrepriseList model.selectedEtablissements model.pagination model.filters model.etablissements
                        ]
                    ]
                ]


showingUnfilteredResults : String -> Bool
showingUnfilteredResults lastUrlFetched =
    List.member lastUrlFetched [ "", "d=ASC", "o=AZ", "d=ASC&o=AZ", "o=DC", "d=ASC&o=DC" ]


codeToOrderBy : String -> Msg
codeToOrderBy code =
    case code of
        "FMDESC" ->
            ClickedOrderBy FicheModification Desc

        "FMASC" ->
            ClickedOrderBy FicheModification Asc

        "ECDESC" ->
            ClickedOrderBy EtablissementCreation Desc

        "ECASC" ->
            ClickedOrderBy EtablissementCreation Asc

        "AZASC" ->
            ClickedOrderBy Alphabetical Asc

        "AZDESC" ->
            ClickedOrderBy Alphabetical Desc

        _ ->
            ClickedOrderBy FicheModification Desc


viewSelection : Filters -> List Siret -> List EtablissementWithExtraInfo -> Html Msg
viewSelection { orderBy, orderDirection } selectedEtablissements etablissementsOnPage =
    let
        etablissementsOnPageIds =
            etablissementsOnPage
                |> List.map (.entreprise >> .siret)

        inSelected id =
            selectedEtablissements
                |> List.member id
    in
    div [ class "flex flex-col p-2 gap-2" ]
        [ DSFR.Checkbox.single
            { value = "toggle-page"
            , checked =
                if List.length etablissementsOnPageIds == 0 then
                    Just False

                else if List.all inSelected etablissementsOnPageIds then
                    Just True

                else if List.all (inSelected >> not) etablissementsOnPageIds then
                    Just False

                else
                    Nothing
            , valueAsString = identity
            , id = "etablissements-page-selection"
            , label = "Sélectionner les établissements de cette page"
            , onChecked = \_ bool -> ToggleSelection etablissementsOnPageIds bool
            }
            |> DSFR.Checkbox.singleWithDisabled (List.length etablissementsOnPageIds == 0)
            |> DSFR.Checkbox.viewSingle
            |> List.singleton
            |> div [ class "flex shrink" ]
        , case selectedEtablissements of
            [] ->
                nothing

            _ ->
                let
                    length =
                        List.length selectedEtablissements
                in
                div [ class "fr-notice--info flex flex-row justify-between items-center gap-2 p-2" ] <|
                    [ p [ class "!mb-0" ]
                        [ text "Il y a "
                        , span [ Typo.textBold, class "blue-text" ]
                            [ text <| String.fromInt <| length
                            , text " fiche"
                            , viewIf (length > 1) (text "s")
                            ]
                        , text " dans la sélection"
                        ]
                    , DSFR.Button.new
                        { label = "Vider la sélection"
                        , onClick = Just <| EmptySelection
                        }
                        |> DSFR.Button.withDisabled (List.length selectedEtablissements == 0)
                        |> DSFR.Button.secondary
                        |> DSFR.Button.withAttrs [ class "!mb-0" ]
                        |> DSFR.Button.view
                    ]
        , div [ class "flex flex-row justify-end items-baseline gap-4" ]
            [ text "Trier par"
            , div
                [ class "fr-select-group"
                ]
                [ label
                    [ class "fr-label"
                    , Attr.for "select"
                    ]
                    []
                , select
                    [ class "fr-select"
                    , Attr.id "select"
                    , Attr.name "select"
                    , Events.onInput codeToOrderBy
                    ]
                    [ option
                        [ Attr.value "ECDESC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( EtablissementCreation, Desc )
                        ]
                        [ text "Établissements crées récemment en premier" ]
                    , option
                        [ Attr.value "ECASC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( EtablissementCreation, Asc )
                        ]
                        [ text "Établissements crées récemment en dernier" ]

                    -- , option
                    --     [ Attr.value "AZASC"
                    --     , Attr.selected <| ( orderBy, orderDirection ) == ( Alphabetical, Asc )
                    --     ]
                    --     [ text "Noms classés par ordre alphabétique" ]
                    -- , option
                    --     [ Attr.value "AZDESC"
                    --     , Attr.selected <| ( orderBy, orderDirection ) == ( Alphabetical, Desc )
                    --     ]
                    --     [ text "Noms classés par ordre alphabétique inversé" ]
                    , option
                        [ Attr.value "FMDESC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( FicheModification, Desc )
                        ]
                        [ text "Fiches modifiées récemment en premier" ]
                    , option
                        [ Attr.value "FMASC"
                        , Attr.selected <| ( orderBy, orderDirection ) == ( FicheModification, Asc )
                        ]
                        [ text "Fiches modifiées récemment en dernier" ]
                    ]
                ]
            ]
        ]


filterPanel : Model -> Html Msg
filterPanel model =
    div [ class "p-2" ]
        [ div [ class "p-4 fr-card--white" ]
            [ DSFR.SearchBar.searchBar
                { submitMsg = FetchEtablissements
                , buttonLabel = "Rechercher"
                , inputMsg = UpdatedSearch
                , inputLabel = "Rechercher un établissement"
                , inputPlaceholder = Nothing
                , inputId = rechercherEntrepriseInputName
                , inputValue = model.filters.recherche
                , hints =
                    "Recherche par nom, SIRET, SIREN, nom de contact"
                        |> text
                        |> List.singleton
                , fullLabel =
                    Just <|
                        label
                            [ class "fr-label !mb-0"
                            , Typo.textBold
                            , Typo.textLg
                            , Attr.for rechercherEntrepriseInputName
                            ]
                            [ text "Rechercher dans mes établissements" ]
                }
            , text "\u{00A0}"
            , hr [ class "fr-hr" ] []
            , viewFilters model
            , DSFR.Button.new { label = "Appliquer les filtres", onClick = Just ClickedSearch }
                |> DSFR.Button.withDisabled (model.lastUrlFetched == serializeModel ( model.filters, model.pagination ))
                |> DSFR.Button.view
                |> List.singleton
                |> div [ class "flex flex-row justify-center mt-4" ]
            ]
        ]


titleWithBadge : String -> Int -> List (Html msg)
titleWithBadge title selected =
    [ div [ class "flex flex-row gap-4" ]
        [ span [] [ text title ]
        , if selected > 0 then
            span [ class "circled" ]
                [ text <| String.fromInt <| selected
                ]

          else
            nothing
        ]
    ]


viewFilters : Model -> Html Msg
viewFilters { filtersSource, communes, selectCommunes, selectCodesNaf, selectEffectifs, selectRue, selectActivites, selectZones, selectMots, selectDemandes, filters } =
    let
        { activites, zones, mots, demandes } =
            case filtersSource of
                RD.Success sources ->
                    sources

                _ ->
                    { activites = [], zones = [], mots = [], demandes = [] }

        moreThanOneCommune =
            communes
                |> RD.withDefault []
                |> List.length
                |> (\length -> length > 2)
    in
    div [ class "flex flex-col" ]
        [ div [ class "flex flex-row items-center justify-between mb-4" ]
            [ h2 [ Typo.fr_h6, class "!mb-0" ] [ text "Filtrer" ]
            , clearAllFiltersButton filters
            ]
        , Html.Lazy.lazy suiviFilter filters.suivi
        , let
            selected =
                List.length filters.activites + List.length filters.zones + List.length filters.mots
          in
          DSFR.Accordion.raw
            { id = "filter-group-qualification"
            , title = titleWithBadge "Champs qualifiés" selected
            , content =
                [ Html.Lazy.lazy3 activitesFilter selectActivites activites filters.activites
                , Html.Lazy.lazy3 zonesFilter selectZones zones filters.zones
                , Html.Lazy.lazy3 motsFilter selectMots mots filters.mots
                ]
            , borderless = False
            }
        , let
            selected =
                List.length filters.demandes
          in
          DSFR.Accordion.raw
            { id = "filter-group-demandes"
            , title = titleWithBadge "Demandes" selected
            , content =
                [ Html.Lazy.lazy3 demandesFilter selectDemandes demandes filters.demandes
                ]
            , borderless = False
            }
        , let
            selected =
                List.length filters.codesNaf + List.length filters.effectifs
          in
          DSFR.Accordion.raw
            { id = "filter-group-activite-et-effectifs"
            , title = titleWithBadge "Activité et effectifs" selected
            , content =
                [ Html.Lazy.lazy2 codesNafFilter filters.codesNaf selectCodesNaf
                , Html.Lazy.lazy2 effectifsFilter filters.effectifs selectEffectifs
                ]
            , borderless = False
            }
        , let
            selected =
                List.length filters.communes
                    + (filters.rue |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
                    + (case filters.territorialite of
                        Just Endogene ->
                            1

                        Just Exogene ->
                            1

                        _ ->
                            0
                      )
          in
          DSFR.Accordion.raw
            { id = "filter-group-localisation"
            , title = titleWithBadge "Localisation" selected
            , content =
                [ Html.Lazy.lazy territorialiteFilter filters.territorialite
                , viewIf moreThanOneCommune <|
                    Html.Lazy.lazy3 communeFilter (communes |> RD.withDefault []) selectCommunes filters.communes
                , Html.Lazy.lazy2 rueFilter selectRue filters.rue
                ]
            , borderless = False
            }
        , let
            selected =
                (if filters.formesJuridiques == Toutes then
                    1

                 else
                    0
                )
                    + (filters.etatType |> Maybe.map (\_ -> 1) |> Maybe.withDefault 0)
          in
          DSFR.Accordion.raw
            { id = "filter-group-statuts"
            , title = titleWithBadge "Statuts" selected
            , content =
                [ Html.Lazy.lazy formesFilter filters.formesJuridiques
                , Html.Lazy.lazy etatFilter filters.etatType
                ]
            , borderless = False
            }
        ]


codesNafFilter : List CodeNaf -> MultiSelectRemote.SmartSelect Msg CodeNaf -> Html Msg
codesNafFilter codesNaf selectCodesNaf =
    div [ class "flex flex-col gap-4" ]
        [ selectCodesNaf
            |> MultiSelectRemote.viewCustom
                { isDisabled = False
                , selected = codesNaf
                , optionLabelFn = \code -> code.id ++ " - " ++ code.label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Codes NAF" ]
                , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.label)
                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg = \searchText -> "Aucun autre code NAF n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucun code NAF n'a été trouvé"
                , newOption = Nothing
                }
        , codesNaf
            |> List.map
                (\codeNaf ->
                    { data = codeNaf, toString = .id }
                        |> DSFR.Tag.deletable UnselectedCodeNaf
                        |> DSFR.Tag.withAttrs
                            [ Attr.title <|
                                codeNaf.id
                                    ++ (if codeNaf.label == "" then
                                            ""

                                        else
                                            " - " ++ codeNaf.label
                                       )
                            ]
                )
            |> DSFR.Tag.medium
        ]


effectifsFilter : List Effectifs -> MultiSelect.SmartSelect Msg Effectifs -> Html Msg
effectifsFilter effectifs selectEffectifs =
    div [ class "flex flex-col gap-4" ]
        [ selectEffectifs
            |> MultiSelect.viewCustom
                { isDisabled = False
                , selected = effectifs
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Effectifs" ]
                , viewSelectedOptionFn = text << (\code -> code.id ++ " - " ++ code.label)
                , noResultsForMsg = \searchText -> "Aucune autre tranche d'effectifs n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune tranche d'effectifs n'a été trouvée"
                , options = Data.Effectifs.tranchesEffectifs
                , searchFn =
                    \searchText allOptions ->
                        List.filter
                            (String.contains (String.toLower searchText) << String.toLower << .label)
                            allOptions
                , searchPrompt = ""
                }
        , effectifs
            |> List.map
                (\effectif ->
                    { data = effectif, toString = .label }
                        |> DSFR.Tag.deletable UnselectedEffectifs
                )
            |> DSFR.Tag.medium
        ]


suiviToLabel : Suivi -> Html msg
suiviToLabel suivi =
    case suivi of
        Tout ->
            span [ Typo.textBold ]
                [ text "Tous"
                , sup [ Attr.title hintSuiviTous ]
                    [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                    ]
                ]

        Portefeuille ->
            span [ Typo.textBold ]
                [ text "Portefeuille"
                , sup [ Attr.title hintSuiviPortefeuille ]
                    [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ]
                    ]
                ]


suiviToId : Suivi -> String
suiviToId suivi =
    case suivi of
        Tout ->
            "suivi-toutes"

        Portefeuille ->
            "suivi-portefeuille"


suiviFilter : Suivi -> Html Msg
suiviFilter suivi =
    DSFR.Radio.group
        { id = "filtres-suivi"
        , options =
            [ Tout
            , Portefeuille
            ]
        , current = Just suivi
        , toLabel = suiviToLabel
        , toId = suiviToId
        , msg = SetSuiviFilter
        , legend = div [ Typo.textBold ] [ text "Type d'établissements" ]
        }
        |> DSFR.Radio.view


etatFilter : Maybe EtatAdministratif -> Html Msg
etatFilter etatType =
    DSFR.Radio.group
        { id = "filtres-etatType"
        , options =
            [ Nothing
            , Just Data.Entreprise.Actif
            , Just Data.Entreprise.Inactif
            ]
        , current = Just etatType
        , toLabel = Maybe.map etatAdministratifToDisplay >> Maybe.withDefault "Tous" >> text
        , toId = Maybe.map etatAdministratifToString >> Maybe.withDefault "tous-etats"
        , msg = SetEtatTypeFilter
        , legend = div [ Typo.textBold ] [ text "État administratif" ]
        }
        |> DSFR.Radio.view


formesFilter : FormesJuridiques -> Html Msg
formesFilter formesToutes =
    let
        formesJuridiquesHint =
            "Les formes juridiques les plus courantes excluent\u{00A0}:\n"
                ++ (Data.FormeJuridique.formesJuridiquesExclues |> List.map Data.FormeJuridique.formeJuridiqueToLabel |> List.sort |> String.join "\n")
    in
    DSFR.Radio.group
        { id = "filtres-formesToutes"
        , options =
            [ Courantes
            , Toutes
            ]
        , current = Just formesToutes
        , toLabel =
            \formes ->
                text <|
                    case formes of
                        Courantes ->
                            "Les plus courantes"

                        Toutes ->
                            "Toutes"
        , toId =
            \formes ->
                case formes of
                    Courantes ->
                        "courantes"

                    Toutes ->
                        "toutes"
        , msg = SetFormesJuridiquesFilter
        , legend =
            div [ Typo.textBold ]
                [ text "Formes juridiques"
                , sup [ Attr.title formesJuridiquesHint ]
                    [ span [ Typo.textXs ]
                        [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine
                        ]
                    ]
                ]
        }
        |> DSFR.Radio.view


communeFilter : List Commune -> MultiSelect.SmartSelect Msg Commune -> List Commune -> Html Msg
communeFilter optionsCommunes selectCommunes communes =
    let
        communeToLabel c =
            case ( c.label, c.departement ) of
                ( "", "" ) ->
                    c.id

                _ ->
                    c.label ++ " (" ++ c.departement ++ ")"
    in
    div [ class "flex flex-col gap-4" ]
        [ selectCommunes
            |> MultiSelect.viewCustom
                { isDisabled = optionsCommunes |> List.length |> (==) 0
                , selected = communes
                , options = optionsCommunes
                , optionLabelFn = communeToLabel
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Communes"
                , viewSelectedOptionFn = .label >> text
                , noResultsForMsg = \searchText -> "Aucune autre commune n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune commune n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << .label) <|
                            allOptions
                , searchPrompt = "Filtrer par commune"
                }
        , communes
            |> List.map (\commune -> DSFR.Tag.deletable UnselectCommune { data = commune, toString = communeToLabel })
            |> DSFR.Tag.medium
        ]


rueFilter : SingleSelectRemote.SmartSelect Msg ApiStreet -> Maybe ApiStreet -> Html Msg
rueFilter selectRue selectedRue =
    div [ class "flex flex-col gap-2" ]
        [ selectRue
            |> SingleSelectRemote.viewCustom
                { isDisabled = False
                , selected = selectedRue
                , optionLabelFn = .label
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = span [ Typo.textBold ] [ text "Adresse" ]
                , searchPrompt = "Rechercher une adresse"
                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune adresse n'a été trouvée"
                , error = Nothing
                }
        , selectedRue
            |> viewMaybe (\rue -> DSFR.Tag.deletable (\_ -> ClearRue) { data = rue.label, toString = identity } |> List.singleton |> DSFR.Tag.medium)
        ]


activitesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
activitesFilter selectActivites optionsActivites activites =
    div [ class "flex flex-col gap-4" ]
        [ selectActivites
            |> MultiSelect.viewCustom
                { isDisabled = optionsActivites |> List.length |> (==) 0
                , selected = activites
                , options = optionsActivites
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Activité réelle et filière"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune activité n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par activité"
                }
        , activites
            |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
            |> DSFR.Tag.medium
        ]


zonesFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
zonesFilter selectZones optionsZones zones =
    div [ class "flex flex-col gap-4" ]
        [ selectZones
            |> MultiSelect.viewCustom
                { isDisabled = optionsZones |> List.length |> (==) 0
                , selected = zones
                , options = optionsZones
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Zone géographique"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par zone géographique"
                }
        , zones
            |> List.map (\zone -> DSFR.Tag.deletable UnselectZone { data = zone, toString = identity })
            |> DSFR.Tag.medium
        ]


motsFilter : MultiSelect.SmartSelect Msg String -> List String -> List String -> Html Msg
motsFilter selectMots optionsMots mots =
    div [ class "flex flex-col gap-4" ]
        [ selectMots
            |> MultiSelect.viewCustom
                { isDisabled = optionsMots |> List.length |> (==) 0
                , selected = mots
                , options = optionsMots
                , optionLabelFn = identity
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Mots-clés"
                , viewSelectedOptionFn = text
                , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower)
                            allOptions
                , searchPrompt = "Filtrer par mot-clé"
                }
        , mots
            |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
            |> DSFR.Tag.medium
        ]


demandesFilter : MultiSelect.SmartSelect Msg TypeDemande -> List TypeDemande -> List TypeDemande -> Html Msg
demandesFilter selectDemandes optionsDemandes demandes =
    div [ class "flex flex-col gap-4" ]
        [ selectDemandes
            |> MultiSelect.viewCustom
                { isDisabled = optionsDemandes |> List.length |> (==) 0
                , selected = demandes
                , options = optionsDemandes
                , optionLabelFn = Data.Demande.typeDemandeToDisplay
                , optionDescriptionFn = \_ -> ""
                , optionsContainerMaxHeight = 300
                , selectTitle = text "Demandes en cours"
                , viewSelectedOptionFn = Data.Demande.typeDemandeToDisplay >> text
                , noResultsForMsg = \searchText -> "Aucune autre demande n'a été trouvée pour " ++ searchText
                , noOptionsMsg = "Aucune demande n'a été trouvée"
                , searchFn =
                    \searchText allOptions ->
                        let
                            lowerSearch =
                                String.toLower searchText
                        in
                        List.filter (String.contains lowerSearch << String.toLower << Data.Demande.typeDemandeToDisplay)
                            allOptions
                , searchPrompt = "Filtrer par demande"
                }
        , demandes
            |> List.map (\demande -> DSFR.Tag.deletable UnselectDemande { data = demande, toString = Data.Demande.typeDemandeToDisplay })
            |> DSFR.Tag.medium
        ]


territorialiteFilter : Maybe Territorialite -> Html Msg
territorialiteFilter territorialite =
    DSFR.Radio.group
        { id = "filtres-territorialite"
        , options =
            [ Nothing
            , Just Endogene
            , Just Exogene
            ]
        , current = Just territorialite
        , toLabel = territorialiteToLabel >> text
        , toId = territorialiteToString
        , msg = SetTerritorialiteFilter
        , legend =
            div [ Typo.textBold ]
                [ text "Appartenance au territoire"
                ]
        }
        |> DSFR.Radio.view


clearAllFiltersButton : Filters -> Html Msg
clearAllFiltersButton filters =
    DSFR.Button.new
        { label = "Tout effacer"
        , onClick = Just <| ClearAllFilters
        }
        |> DSFR.Button.withAttrs [ class "!p-0" ]
        |> DSFR.Button.withDisabled ({ filters | recherche = "" } == { defaultFilters | recherche = "" })
        |> DSFR.Button.tertiaryNoOutline
        |> DSFR.Button.view


type alias Pagination =
    { page : Int
    , pages : Int
    , results : Int
    }


viewEntrepriseList : List Siret -> Pagination -> Filters -> WebData (List EtablissementWithExtraInfo) -> Html Msg
viewEntrepriseList selectedEtablissements pagination filters entreprises =
    case entreprises of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ Grid.col12 ]
                [ []
                    |> viewSelection filters selectedEtablissements
                , div [ Grid.gridRow ] <|
                    List.repeat 3 <|
                        cardPlaceholder
                ]

        RD.Failure _ ->
            div [ class "text-center", Grid.col, class "p-2" ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ Grid.col12 ]
                [ div [ class "p-2" ]
                    [ div [ class "fr-card--white h-[250px] text-center p-20" ]
                        [ text "Aucun établissement ne correspond à cette recherche."
                        , text " "
                        , viewMaybe (\_ -> text "Moteur de recherche par adresse en cours de perfectionnement\u{00A0}: certaines adresses peuvent ne pas être trouvées.") filters.rue
                        ]
                    ]
                ]

        RD.Success fs ->
            div [ Grid.col12 ]
                [ fs
                    |> viewSelection filters selectedEtablissements
                , fs
                    |> List.map (entrepriseCard selectedEtablissements)
                    |> div [ Grid.gridRow ]
                , if pagination.pages > 1 then
                    div [ class "!mt-4" ]
                        [ DSFR.Pagination.view pagination.page pagination.pages <|
                            toHref ( filters, pagination )
                        ]

                  else
                    nothing
                ]


entrepriseCard : List Siret -> EtablissementWithExtraInfo -> Html Msg
entrepriseCard selectedEtablissements { entreprise, infos } =
    let
        selected =
            List.member entreprise.siret selectedEtablissements

        linkToPage =
            Route.Etablissement entreprise.siret
    in
    div [ Grid.col12, Grid.colSm6, class "p-2" ]
        [ div [ Typo.textSm, class "!mb-0" ]
            [ div
                [ class "flex flex-col overflow-y-auto fr-card--white p-2 gap-2 min-h-[18rem] border-2"
                , Attr.style "background-image" "none"
                , classList [ ( "dark-blue-border", selected ), ( "border-transparent", not selected ) ]
                ]
                [ div [ class "flex flex-row justify-between relative p-2" ]
                    [ div [ class "flex flex-row items-center gap-2" ]
                        [ DSFR.Icons.iconLG UI.Entite.iconeEtablissement
                        , viewIf infos.suivi <|
                            span [ Attr.title "Établissement suivi" ] <|
                                List.singleton <|
                                    DSFR.Icons.iconMD DSFR.Icons.System.eyeLine
                        , viewIf infos.accompagne <|
                            span [ Attr.title "Établissement accompagné" ] <|
                                List.singleton <|
                                    DSFR.Icons.iconMD DSFR.Icons.Communication.discussFill
                        , UI.Entite.badgeInactif <|
                            Just <|
                                .etatAdministratif <|
                                    entreprise
                        , UI.Entite.badgeExogene <|
                            .exogene <|
                                entreprise
                        ]
                    , div [ class "absolute top-[12px] right-0" ]
                        [ DSFR.Checkbox.single
                            { value = entreprise
                            , checked = Just <| selected
                            , valueAsString = .siret
                            , id = "etablisement-selection-" ++ entreprise.siret
                            , label = ""
                            , onChecked = .siret >> List.singleton >> ToggleSelection
                            }
                            |> DSFR.Checkbox.viewSingle
                        ]
                    ]
                , a
                    [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                    , Attr.style "background-image" "none"
                    , Attr.href <|
                        Route.toUrl <|
                            linkToPage
                    ]
                    [ div []
                        [ div
                            [ Typo.textBold
                            , class "!mb-0"
                            , class "line-clamp-1"
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    Data.Entreprise.displayNomEnseigne <|
                                        entreprise
                        , div
                            [ class "!mb-0"
                            , class "line-clamp-1"
                            , Attr.title <|
                                .adresse <|
                                    entreprise
                            ]
                          <|
                            List.singleton <|
                                text <|
                                    withEmptyAs "-" <|
                                        .adresse <|
                                            entreprise
                        ]
                    , entreprise
                        |> .siret
                        |> Lib.UI.infoLine (Just 1) "SIRET\u{00A0}: "
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , entreprise
                        |> .activitePrincipaleUniteLegale
                        |> Maybe.withDefault "-"
                        |> Lib.UI.infoLine (Just 2) "Activité NAF\u{00A0}: "
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    , entreprise
                        |> .dateCreationEtablissement
                        |> Maybe.map Lib.Date.formatDateShort
                        |> Maybe.withDefault "-"
                        |> Lib.UI.infoLine (Just 1) "Date de création\u{00A0}: "
                        |> List.singleton
                        |> div [ class "!mb-0" ]
                    ]
                ]
            ]
        ]


viewModal : Int -> List Siret -> BatchRequest -> Html Msg
viewModal foundFiches selectedFiches { request, selectLocalisations, selectActivites, selectMots, activites, localisations, mots } =
    DSFR.Modal.view
        { id = "batch-tag"
        , label = "batch-tag"
        , openMsg = NoOp
        , closeMsg = Just <| CanceledQualifierFiches
        , title =
            text <|
                case selectedFiches of
                    [] ->
                        "Qualifier "
                            ++ String.fromInt foundFiches
                            ++ " fiche"
                            ++ (if foundFiches > 1 then
                                    "s"

                                else
                                    ""
                               )

                    _ ->
                        "Qualifier "
                            ++ (String.fromInt <| List.length <| selectedFiches)
                            ++ " fiche"
                            ++ (if List.length selectedFiches > 1 then
                                    "s"

                                else
                                    ""
                               )
        , opened = True
        }
        (let
            disabled =
                request == RD.Loading
         in
         div []
            [ div [ Grid.gridRow, Grid.gridRowGutters ]
                [ div [ Grid.col12 ]
                    [ case request of
                        RD.Success ( _, _, results ) ->
                            div [ class "flex flex-col gap-2" ]
                                [ viewIf (List.length activites > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Activité(s) ajoutée(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " activites
                                            ]
                                        ]
                                , viewIf (List.length localisations > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Localisation(s) ajoutée(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " localisations
                                            ]
                                        ]
                                , viewIf (List.length mots > 0) <|
                                    div []
                                        [ div [ Typo.textBold ]
                                            [ text "Mot(s)-clé(s) ajouté(s)\u{00A0}:"
                                            ]
                                        , div []
                                            [ text <| String.join ", " mots
                                            ]
                                        ]
                                , div [ class "flex flex-col gap-4" ]
                                    [ div [ Typo.textBold ]
                                        [ text "Résultat de la mise à jour\u{00A0}:"
                                        ]
                                    , let
                                        { total, successes, errors } =
                                            results
                                      in
                                      div [] <|
                                        [ if total == successes then
                                            div [ class "fr-text-default--success" ] [ text "Les ", text <| String.fromInt successes, text " fiches ont été qualifiées avec succès\u{00A0}!" ]

                                          else if successes > 0 then
                                            div [ class "fr-text-default--success" ] [ text <| String.fromInt <| successes, text " fiches ont été qualifiées avec succès." ]

                                          else
                                            nothing
                                        , viewIf (List.length errors > 0) <|
                                            div [ class "fr-text-default--error" ]
                                                [ text "Les fiches suivantes n'ont pas été qualifiées correctement\u{00A0}:"
                                                ]
                                        , viewIf (List.length errors > 0) <|
                                            div [ Grid.gridRow ] <|
                                                List.map
                                                    (\ficheName ->
                                                        div [ Grid.col6, class "fr-text-default--error" ]
                                                            [ DSFR.Icons.icon DSFR.Icons.System.closeCircleFill
                                                            , text " "
                                                            , text ficheName
                                                            ]
                                                    )
                                                <|
                                                    errors
                                        ]
                                    ]
                                ]

                        RD.Failure err ->
                            text <|
                                case err of
                                    Http.BadBody cause ->
                                        cause

                                    _ ->
                                        ""

                        _ ->
                            div []
                                [ div [ Grid.gridRow, Grid.gridRowGutters ]
                                    [ div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectActivites
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = activites
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Activités réelles et filières", sup [ Attr.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                                                    , noOptionsMsg = "Aucune activité n'a été trouvée"
                                                    , newOption = Just identity
                                                    }
                                            , activites
                                                |> List.map (\activite -> DSFR.Tag.deletable UnselectBatchActivite { data = activite, toString = identity })
                                                |> DSFR.Tag.medium
                                            ]
                                        ]
                                    , div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectLocalisations
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = localisations
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Zone géographique", sup [ Attr.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                                                    , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                                    , newOption = Just identity
                                                    }
                                            , localisations
                                                |> List.map (\localisation -> DSFR.Tag.deletable UnselectBatchLocalisation { data = localisation, toString = identity })
                                                |> DSFR.Tag.medium
                                            ]
                                        ]
                                    ]
                                , div [ Grid.gridRow, Grid.gridRowGutters ]
                                    [ div [ Grid.col6 ]
                                        [ div [ class "flex flex-col gap-4" ]
                                            [ selectMots
                                                |> MultiSelectRemote.viewCustom
                                                    { isDisabled = False
                                                    , selected = mots
                                                    , optionLabelFn = identity
                                                    , optionDescriptionFn = \_ -> ""
                                                    , optionsContainerMaxHeight = 300
                                                    , selectTitle = span [ Typo.textBold ] [ text "Mots-clés", sup [ Attr.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                                    , viewSelectedOptionFn = text
                                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                                    , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                                                    , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                                    , newOption = Just identity
                                                    }
                                            , mots
                                                |> List.map (\mot -> DSFR.Tag.deletable UnselectBatchMot { data = mot, toString = identity })
                                                |> DSFR.Tag.medium
                                            ]
                                        ]
                                    ]
                                ]
                    ]
                ]
            , div [ Grid.gridRow, Grid.gridRowGutters ]
                [ div [ Grid.col12 ]
                    [ (case request of
                        RD.Success _ ->
                            [ DSFR.Button.new
                                { onClick = Just <| CanceledQualifierFiches
                                , label = "OK"
                                }
                                |> DSFR.Button.withDisabled disabled
                            ]

                        _ ->
                            [ DSFR.Button.new
                                { onClick = Just <| ConfirmedQualifierFiches
                                , label =
                                    if request == RD.Loading then
                                        "Qualification en cours..."

                                    else
                                        "Confirmer"
                                }
                                |> DSFR.Button.withDisabled disabled
                            , DSFR.Button.new { onClick = Just <| CanceledQualifierFiches, label = "Annuler" }
                                |> DSFR.Button.withDisabled disabled
                                |> DSFR.Button.secondary
                            ]
                      )
                        |> DSFR.Button.group
                        |> DSFR.Button.inline
                        |> DSFR.Button.alignedRightInverted
                        |> DSFR.Button.viewGroup
                    ]
                ]
            ]
        )
        Nothing
        |> Tuple.first


cardPlaceholder : Html Msg
cardPlaceholder =
    div [ Grid.col12, Grid.colSm6 ]
        [ div [ class "p-2", Typo.textSm ]
            [ div
                [ class "fr-card--white flex flex-col p-4 custom-hover overflow-y-auto gap-2"
                , Attr.style "background-image" "none"
                ]
                [ div [ class "flex flex-row justify-between" ]
                    [ greyBadge 30
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 14
                    , greyPlaceholder 20
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                , div [ class "!mb-0" ]
                    [ greyPlaceholder 15
                    , greyPlaceholder 8
                    ]
                ]
            ]
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "grey-background m-1" ]
        |> List.singleton
        |> div [ class "pulse-black" ]


greyBadge : Int -> Html msg
greyBadge length =
    "\u{00A0}"
        |> List.repeat length
        |> String.join ""
        |> (\t -> { data = t, toString = identity })
        |> DSFR.Tag.unclickable
        |> List.singleton
        |> DSFR.Tag.medium
        |> List.singleton
        |> div [ class "pulse-black" ]
