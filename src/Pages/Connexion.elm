module Pages.Connexion exposing (Model, Msg, page)

import Accessibility exposing (Html, decorativeImg, div, h1, hr, p, text)
import Api
import Api.Auth
import Browser.Dom
import DSFR.Alert
import DSFR.Button
import DSFR.Grid
import DSFR.Icons.System exposing (arrowDownSFill, arrowUpSFill)
import DSFR.Input
import DSFR.Typography as Typo exposing (externalLink)
import Effect
import Html as Root
import Html.Attributes as Attr exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing, viewIf)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.UI exposing (withEmptyAs)
import Lib.Variables exposing (contactEmail)
import RemoteData as RD
import Route
import Shared
import Spa.Page exposing (Page)
import Task
import UI.Layout
import Url.Builder
import View exposing (View)


type alias Model =
    { email : String
    , password : String
    , passwordMode : Bool
    , loginRequest : RD.RemoteData AuthType ( AuthType, String )
    , redirectUrl : Maybe String
    }


type Msg
    = ChangedUsername String
    | ChangedPassword String
    | RequestedAuth AuthType
    | ReceivedAuth (RD.RemoteData AuthType ( AuthType, String ))
    | ToggledPasswordMode
    | NoOp


type AuthType
    = MagicLink
    | Password


page : Shared.Shared -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init redirect =
    ( { email = ""
      , password = ""
      , passwordMode = False
      , loginRequest = RD.NotAsked
      , redirectUrl = redirect
      }
    , Effect.fromSharedCmd <| Api.Auth.checkAuth True redirect
    )
        |> Shared.pageChangeEffects


decodeAuth : Decode.Decoder ( AuthType, String )
decodeAuth =
    Decode.oneOf
        [ Decode.map (\link -> ( MagicLink, link )) decodeMagicLink
        , Decode.map (\_ -> ( Password, "" )) decodePasswordLogin
        ]


decodeMagicLink : Decode.Decoder String
decodeMagicLink =
    Decode.field "accessUrl" <|
        Decode.string


decodePasswordLogin : Decode.Decoder String
decodePasswordLogin =
    Decode.map (\_ -> "") <|
        Decode.field "password" <|
            Decode.bool


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ChangedUsername username ->
            ( { model
                | email = username
                , loginRequest = RD.NotAsked
              }
            , Effect.none
            )

        ToggledPasswordMode ->
            { model
                | passwordMode = not model.passwordMode
                , loginRequest = RD.NotAsked
            }
                |> Effect.withNone

        ChangedPassword password ->
            ( { model
                | password = password
                , loginRequest = RD.NotAsked
              }
            , Effect.none
            )

        RequestedAuth authType ->
            case authType of
                MagicLink ->
                    if model.email == "" then
                        { model | loginRequest = RD.NotAsked }
                            |> Effect.withNone

                    else
                        ( { model | loginRequest = RD.Loading }
                        , Effect.fromCmd <|
                            Http.post
                                { url = Api.login
                                , body =
                                    Http.jsonBody <|
                                        Encode.object
                                            [ ( "email", Encode.string <| String.trim <| model.email )
                                            , ( "redirectUrl", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| model.redirectUrl )
                                            ]
                                , expect =
                                    Http.expectJson (RD.fromResult >> RD.mapError (\_ -> authType) >> ReceivedAuth) <|
                                        decodeAuth
                                }
                        )

                Password ->
                    if model.email == "" || model.password == "" then
                        { model | loginRequest = RD.NotAsked }
                            |> Effect.withNone

                    else
                        ( { model | loginRequest = RD.Loading }
                        , Effect.fromCmd <|
                            Http.post
                                { url = Api.login
                                , body =
                                    Http.jsonBody <|
                                        Encode.object
                                            [ ( "email", Encode.string <| String.trim <| model.email )
                                            , ( "password", Encode.string <| String.trim <| model.password )
                                            , ( "redirectUrl", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| model.redirectUrl )
                                            ]
                                , expect =
                                    Http.expectWhatever (RD.fromResult >> RD.map (\_ -> ( Password, "" )) >> RD.mapError (\_ -> authType) >> ReceivedAuth)
                                }
                        )

        ReceivedAuth result ->
            let
                effect =
                    case result of
                        RD.Success ( MagicLink, _ ) ->
                            Task.attempt (\_ -> NoOp) (Browser.Dom.focus "accessUrl")
                                |> Effect.fromCmd

                        RD.Success ( Password, _ ) ->
                            Effect.fromShared (Shared.Navigate <| Route.Dashboard)

                        _ ->
                            Effect.none
            in
            ( { model | loginRequest = result }
            , effect
            )

        NoOp ->
            ( model, Effect.none )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Connexion"
    , body = UI.Layout.lazyBody body model
    , route = Route.Connexion Nothing
    }


body : Model -> Html Msg
body { email, password, passwordMode, loginRequest } =
    let
        disabled =
            loginRequest == RD.Loading
    in
    div [ class "fr-card--white pt-8" ]
        [ div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col2, DSFR.Grid.colOffset2, class "border-r-2 border-france-blue flex items-start justify-end pr-2" ]
                [ decorativeImg [ Attr.src "/assets/deveco-cadenas.svg" ] ]
            , div [ DSFR.Grid.col6, class "pl-8 flex" ]
                [ div [ class "self-center" ]
                    [ h1 [ Typo.fr_h4 ] [ text "Se connecter à Dévéco" ]
                    , case loginRequest of
                        RD.Success ( MagicLink, magicLink ) ->
                            div [ class "flex flex-col gap-4" ]
                                [ text "Un courriel vous a été envoyé\u{00A0}!"
                                , if magicLink == "" then
                                    nothing

                                  else
                                    div [ class "flex" ]
                                        [ Typo.linkStandalone Typo.linkNoIcon
                                            magicLink
                                            [ Attr.id "accessUrl"
                                            , class "focus:outline-none focus:ring focus:ring-france-blue"
                                            ]
                                            [ text "Se connecter" ]
                                        ]
                                ]

                        RD.Success _ ->
                            div [ class "flex flex-col gap-4" ]
                                [ text "Si vous n'êtes pas redirigé(e), "
                                , Typo.link (Route.toUrl <| Route.Dashboard) [] [ text "cliquez pour vous rendre sur votre page d'accueil." ]
                                ]

                        _ ->
                            div [ class "flex flex-col w-full" ]
                                [ p [] [ text "Veuillez saisir votre adresse de courriel et votre mot de passe." ]
                                , Root.form [ Events.onSubmit <| RequestedAuth MagicLink ]
                                    [ { value = email
                                      , onInput = ChangedUsername
                                      , label = text "Courriel *"
                                      , name = "email"
                                      }
                                        |> DSFR.Input.new
                                        |> DSFR.Input.email
                                        |> DSFR.Input.withHint [ text "ex\u{00A0}: nour@anct.gouv.fr" ]
                                        |> DSFR.Input.withDisabled disabled
                                        |> DSFR.Input.withError Nothing
                                        |> DSFR.Input.view
                                    , DSFR.Button.new { label = "Recevoir un lien de connexion", onClick = Nothing }
                                        |> DSFR.Button.withDisabled (disabled || email == "" || passwordMode)
                                        |> DSFR.Button.submit
                                        |> DSFR.Button.view
                                    ]
                                , viewLoginRequestErrorFor MagicLink loginRequest
                                , div [ class "mt-4" ]
                                    [ text "Vous ne recevez pas nos courriels\u{00A0}? Connectez-vous par "
                                    , DSFR.Button.new { label = "mot de passe", onClick = Just ToggledPasswordMode }
                                        |> DSFR.Button.rightIcon
                                            (if passwordMode then
                                                arrowUpSFill

                                             else
                                                arrowDownSFill
                                            )
                                        |> DSFR.Button.withAttrs [ class "!p-0" ]
                                        |> DSFR.Button.tertiaryNoOutline
                                        |> DSFR.Button.view
                                    ]
                                , viewIf passwordMode <|
                                    hr [] []
                                , viewIf passwordMode <|
                                    Root.form [ Events.onSubmit <| RequestedAuth Password ]
                                        [ { value = password
                                          , onInput = ChangedPassword
                                          , label = text "Mot de passe *"
                                          , name = "password"
                                          }
                                            |> DSFR.Input.new
                                            |> DSFR.Input.password
                                            |> DSFR.Input.withDisabled disabled
                                            |> DSFR.Input.withError Nothing
                                            |> DSFR.Input.view
                                        , DSFR.Button.new { onClick = Nothing, label = "Se connecter" }
                                            |> DSFR.Button.withDisabled (disabled || email == "" || password == "")
                                            |> DSFR.Button.withAttrs [ Attr.name "submit-connexion" ]
                                            |> DSFR.Button.submit
                                            |> DSFR.Button.view
                                        ]
                                , viewIf passwordMode <|
                                    viewLoginRequestErrorFor Password loginRequest
                                , viewIf passwordMode <|
                                    div [ class "mt-4" ]
                                        [ text "Vous n'avez pas encore de mot de passe\u{00A0}?"
                                        , text " "
                                        , let
                                            emails =
                                                [ contactEmail ]

                                            subject =
                                                Url.Builder.string "subject" <|
                                                    "Demande de création de mot de passe"

                                            bod =
                                                Url.Builder.string "body" <|
                                                    "Bonjour l'équipe Dévéco,"
                                                        ++ "\nje souhaiterais obtenir un mot de passe pour mon compte "
                                                        ++ (withEmptyAs "MON ADRESSE DE COURRIEL" <| email)
                                                        ++ "."
                                                        ++ "\nMerci\u{00A0}!"

                                            mailto =
                                                "mailto:"
                                                    ++ String.join "," emails
                                                    ++ Url.Builder.toQuery [ subject, bod ]
                                          in
                                          externalLink
                                            mailto
                                            []
                                            [ text "Contactez-nous" ]
                                        ]
                                ]
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ text "\u{00A0}"
                ]
            ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col12 ]
                [ hr [] []
                ]
            ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col2, DSFR.Grid.colOffset2, class "p-4 sm:p-8" ]
                [ text "\u{00A0}" ]
            , div [ DSFR.Grid.col6, class "pl-4 sm:pl-8" ]
                [ p [ Typo.textXs, class "flex flex-row items-center justify-between" ]
                    [ text "Vous n'êtes pas encore inscrit(e)\u{00A0}?"
                    , let
                        emails =
                            [ contactEmail ]

                        subject =
                            Url.Builder.string "subject" <|
                                "Demande de création d'un compte"

                        mailto =
                            "mailto:"
                                ++ String.join "," emails
                                ++ Url.Builder.toQuery [ subject ]
                      in
                      DSFR.Button.new { label = "Contactez-nous", onClick = Nothing }
                        |> DSFR.Button.linkButton mailto
                        |> DSFR.Button.secondary
                        |> DSFR.Button.view
                    ]
                ]
            ]
        ]


viewLoginRequestErrorFor : AuthType -> RD.RemoteData AuthType a -> Html msg
viewLoginRequestErrorFor authType loginRequest =
    case ( authType, loginRequest ) of
        ( Password, RD.Failure Password ) ->
            DSFR.Alert.small { title = Just "Connexion échouée", description = "Nous n'avons pas pu vous identifier. Veuillez vérifier que votre courriel et votre mot de passe sont corrects et nous contacter si le problème persiste." }
                |> DSFR.Alert.alert Nothing DSFR.Alert.error
                |> List.singleton
                |> div [ class "!my-8" ]

        ( MagicLink, RD.Failure MagicLink ) ->
            DSFR.Alert.small { title = Just "Connexion échouée", description = "Veuillez vérifier votre courriel et nous contacter si le problème persiste." }
                |> DSFR.Alert.alert Nothing DSFR.Alert.error
                |> List.singleton
                |> div [ class "!my-8" ]

        _ ->
            nothing
