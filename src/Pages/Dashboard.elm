module Pages.Dashboard exposing (Model, Msg, page)

import Accessibility exposing (Html, decorativeImg, div, h2, h3, hr, p, span, text)
import Api
import Api.Auth
import DSFR.Accordion
import DSFR.Button
import DSFR.Card
import DSFR.Grid
import DSFR.Icons exposing (icon)
import DSFR.Icons.System exposing (arrowDownLine)
import DSFR.Typography as Typo exposing (externalLink, link)
import Data.Role
import Data.Territoire
import Effect
import Html.Attributes exposing (class)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Lib.UI exposing (formatNumberWithThousandSpacing, plural)
import Lib.Variables exposing (contactEmail)
import Pages.Etablissements exposing (makeQueryForPortefeuilleMode)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import Url.Builder as Builder
import User
import View exposing (View)


type alias Model =
    { collaborateurs : WebData (List User)
    , stats : WebData Stats
    }


type alias Stats =
    { fiches : FichesStats
    , locaux : LocauxStats
    }


type alias FichesStats =
    { etablissements : Int
    , suivis : Int
    , personnes : Int
    }


type alias LocauxStats =
    { occupes : Int
    , vacants : Int
    }


type Msg
    = ReceivedCollaborateurs (WebData (List User))
    | ReceivedStats (WebData Stats)


page : Shared.Shared -> Page () Shared.Msg (View Msg) Model Msg
page { identity } =
    Spa.Page.element
        { view = view identity
        , init = init identity
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : Maybe User -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init identity () =
    ( { collaborateurs = RD.Loading
      , stats = RD.Loading
      }
    , case identity of
        Nothing ->
            Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

        Just user ->
            case User.role user of
                Data.Role.Deveco _ ->
                    Effect.batch
                        [ Effect.fromCmd getStats
                        , Effect.fromCmd getCollaborators
                        ]

                role ->
                    Effect.fromShared <|
                        Shared.ReplaceUrl <|
                            Route.defaultPageForRole role
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ReceivedCollaborateurs response ->
            { model | collaborateurs = response }
                |> Effect.withNone

        ReceivedStats response ->
            { model | stats = response }
                |> Effect.withNone


view : Maybe User -> Model -> View Msg
view identity model =
    { title =
        UI.Layout.pageTitle <|
            Maybe.withDefault "Outil pour développeur économique - Deveco - version beta" <|
                Maybe.map (\_ -> "Tableau de bord") <|
                    identity
    , body = [ body identity model ]
    , route = Route.Dashboard
    }


body : Maybe User -> Model -> Html Msg
body identity model =
    identity
        |> Maybe.map (viewUserDashboard model)
        |> Maybe.withDefault viewLanding


viewLanding : Html msg
viewLanding =
    div [ class "flex flex-col fr-card--white" ]
        [ div [ DSFR.Grid.gridRow, class "p-4 sm:p-8" ]
            [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col3, class "" ]
                [ decorativeImg [ Html.Attributes.src "/assets/deveco-accueil.svg" ] ]
            , div [ DSFR.Grid.col7, class "flex" ]
                [ div [ class "self-center" ]
                    [ h2 [ Typo.fr_h4 ] [ text "Bienvenue sur la version beta de Deveco\u{00A0}!" ]
                    , p [] [ text "Ce service facilite l'accès et la gestion des données des entreprises pour les développeurs économiques au sein des collectivités locales." ]
                    , DSFR.Button.new { label = "Accéder à mon compte", onClick = Nothing }
                        |> DSFR.Button.linkButton (Route.toUrl <| Route.Connexion Nothing)
                        |> DSFR.Button.primary
                        |> DSFR.Button.view
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow, class "fr-background-contrast--blue-ecume p-8 sm:p-16" ]
            [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col6 ]
                [ text "Gagnez en efficacité pour mieux accompagner le développement économique de votre territoire." ]
            , div [ DSFR.Grid.colOffset1, DSFR.Grid.col3 ]
                [ let
                    emails =
                        [ contactEmail ]

                    subject =
                        Builder.string "subject" <|
                            "Demande d'information sur Dévéco"

                    mailto =
                        "mailto:"
                            ++ String.join "," emails
                            ++ Builder.toQuery [ subject ]
                  in
                  DSFR.Button.new { label = "Contactez-nous", onClick = Nothing }
                    |> DSFR.Button.linkButton mailto
                    |> DSFR.Button.view
                ]
            ]
        , div [ class "p-4 sm:p-8" ]
            [ div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow, class "flex items-end justify-end" ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-information.svg" ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-connaissance.svg" ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ decorativeImg [ Html.Attributes.src "/assets/deveco-pilotage.svg" ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Partage d’infos entre agents d’une même équipe" ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Connaissance du tissu économique du territoire" ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [ Typo.textBold ] [ text "Pilotage de l’activité" ] ]
                            ]
                        ]
                    ]
                ]
            , div [ DSFR.Grid.gridRow, class "p-2 sm:p-4" ]
                [ div [ DSFR.Grid.colOffset1, DSFR.Grid.col10, class "fr-card--white" ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Centralisez le suivi de la relation aux établissements et créateurs d'entreprises du territoire. Gérez les locaux d’activité vacants." ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Explorez le tissu économique local à partir des données fiables et actualisées des institutions publiques." ] ]
                            ]
                        , div [ DSFR.Grid.col4, class "px-8" ]
                            [ div [] [ div [] [ text "Suivez et analysez l’activité des services de développement économique." ] ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewUserDashboard : Model -> User -> Html Msg
viewUserDashboard model user =
    let
        territoire =
            case User.role user of
                Data.Role.Superadmin ->
                    ""

                Data.Role.Deveco t ->
                    Data.Territoire.nom t
    in
    div [ class "p-2" ] <|
        List.singleton <|
            div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col12, DSFR.Grid.colSm4 ]
                    [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                        [ div [ DSFR.Grid.col12 ] [ viewRoleAndTerritory user ]
                        , div [ DSFR.Grid.col12 ] [ viewCollaborators territoire model.collaborateurs ]
                        ]
                    ]
                , div [ DSFR.Grid.col12, DSFR.Grid.colSm8 ]
                    [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                        [ div [ DSFR.Grid.col12 ]
                            [ viewBetaWelcome
                            ]
                        , div [ DSFR.Grid.col12 ]
                            [ viewStats model.stats
                            ]
                        , div [ DSFR.Grid.col12 ]
                            [ viewHelpBlock
                            ]
                        ]
                    ]
                ]


viewBetaWelcome : Html msg
viewBetaWelcome =
    DSFR.Card.card "Merci d'être parmi les premiers utilisateurs de Deveco\u{00A0}!" DSFR.Card.vertical
        |> DSFR.Card.withDescription
            (Just <|
                div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col2, class "flex p-4" ]
                        [ decorativeImg [ class "text-center", Html.Attributes.src "/assets/deveco-environnement.svg" ] ]
                    , div [ DSFR.Grid.col10, class "p-4" ]
                        [ p []
                            [ span [ Typo.textBold ] [ text "Cet outil est le vôtre" ]
                            , text ", la première version dite «\u{00A0}bêta\u{00A0}» déjà stabilisée sera enrichie et améliorée par vos retours."
                            ]
                        , p [ class "!mb-0" ]
                            [ text "Le passage d'un outil en construction à une version pérennisée n'a aucune incidence sur "
                            , span [ Typo.textBold ] [ text "vos données qui sont bien conservées" ]
                            , text "."
                            ]
                        ]
                    ]
            )
        |> DSFR.Card.view


viewRoleAndTerritory : User -> Html msg
viewRoleAndTerritory user =
    let
        greetings =
            "Territoire de référence"
    in
    div [ DSFR.Grid.col12 ]
        [ DSFR.Card.card greetings DSFR.Card.vertical
            |> DSFR.Card.withDescription
                (Just <|
                    div [] <|
                        case User.role user of
                            Data.Role.Deveco territoire ->
                                [ span [ Typo.textBold ] [ text <| Data.Territoire.nom territoire ]
                                ]

                            Data.Role.Superadmin ->
                                [ text "Vous êtes Superadmin"
                                ]
                )
            |> DSFR.Card.view
        ]


viewCollaborators : String -> WebData (List User) -> Html msg
viewCollaborators territoire collaborateurs =
    div [ DSFR.Grid.col12 ]
        [ DSFR.Card.card "Mes collaborateurs" DSFR.Card.vertical
            |> DSFR.Card.withDescription
                (Just <|
                    div [] <|
                        [ p []
                            [ span [ Typo.textBold ] [ text "Seuls les collaborateurs listés ci-dessous" ]
                            , text " peuvent accéder au contenu de l'outil Deveco de "
                            , text territoire
                            , text "."
                            ]
                        , case collaborateurs of
                            RD.NotAsked ->
                                nothing

                            RD.Loading ->
                                text "Chargement en cours..."

                            RD.Failure _ ->
                                text "Une erreur s'est produite"

                            RD.Success [] ->
                                div [ DSFR.Grid.col8 ]
                                    [ span [ class "italic" ]
                                        [ text "Aucun collaborateur sur ce territoire" ]
                                    ]

                            RD.Success collabs ->
                                DSFR.Accordion.raw
                                    { id = "collaborateurs"
                                    , title = [ text <| String.fromInt <| List.length collabs, text " ", text "collaborateur", text <| plural <| List.length collabs ]
                                    , content =
                                        List.intersperse (hr [ class "mt-4" ] []) <|
                                            List.map viewCollaborator <|
                                                collabs
                                    , borderless = True
                                    }
                        , let
                            emails =
                                [ contactEmail ]

                            subject =
                                Builder.string "subject" <|
                                    "Demande d'ajout d'un collaborateur"

                            bod =
                                Builder.string "body" <|
                                    "Territoire\u{00A0}: "
                                        ++ territoire
                                        ++ "\nEmail du collaborateur\u{00A0}: \nNom du collaborateur\u{00A0}: \nPrénom du collaborateur\u{00A0}: \nFonction du collaborateur\u{00A0}: \n"

                            mailto =
                                "mailto:"
                                    ++ String.join "," emails
                                    ++ Builder.toQuery [ subject, bod ]
                          in
                          p [ Typo.textSpectralRegular, Typo.textSm, class "!mt-8" ]
                            [ externalLink mailto [] [ text "Demander l'ajout d'un collaborateur" ] ]
                        ]
                )
            |> DSFR.Card.view
        ]


viewCollaborator : User -> Html msg
viewCollaborator collaborator =
    div []
        [ div [] <|
            [ span [ Typo.textBold ] <| List.singleton <| text <| User.email collaborator
            ]
        , div [] <|
            List.singleton <|
                case ( User.prenom collaborator, User.nom collaborator ) of
                    ( "", "" ) ->
                        span [ class "italic" ]
                            [ text <|
                                "Pas de nom"
                            ]

                    ( "", nom ) ->
                        text <|
                            nom

                    ( prenom, "" ) ->
                        text <|
                            prenom

                    ( prenom, nom ) ->
                        text <|
                            prenom
                                ++ " "
                                ++ nom
        ]


viewStats : WebData Stats -> Html Msg
viewStats stats =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ] <|
        case stats of
            RD.NotAsked ->
                []

            RD.Loading ->
                [ div [ DSFR.Grid.col ]
                    [ div [ DSFR.Grid.gridRow ]
                        [ div [ DSFR.Grid.col, class "fr-card--white p-20 flex justify-center items-center" ]
                            [ text "Chargement en cours..." ]
                        ]
                    ]
                ]

            RD.Failure _ ->
                [ text "Une erreur s'est produite" ]

            RD.Success { fiches, locaux } ->
                case ( fiches.etablissements, fiches.personnes ) of
                    ( 0, 0 ) ->
                        [ viewWelcomeBlock
                        ]

                    _ ->
                        [ div [ DSFR.Grid.col12 ]
                            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ div [ DSFR.Grid.col12 ]
                                    [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Établissements" ]
                                    ]
                                ]
                            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ div [ DSFR.Grid.col6 ]
                                    [ viewEtablissementsStats fiches
                                    ]
                                , div [ DSFR.Grid.col6 ]
                                    [ viewSuivisStats fiches
                                    ]
                                ]
                            ]
                        , div [ DSFR.Grid.col12 ]
                            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ div [ DSFR.Grid.col6 ]
                                    [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Créateurs" ]
                                    ]
                                , div [ DSFR.Grid.col6 ]
                                    [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Locaux" ]
                                    ]
                                ]
                            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                                [ div [ DSFR.Grid.col6 ]
                                    [ viewCreateursStats fiches
                                    ]
                                , div [ DSFR.Grid.col6 ]
                                    [ viewLocauxStats locaux
                                    ]
                                ]
                            ]
                        ]


viewWelcomeBlock : Html msg
viewWelcomeBlock =
    div [ DSFR.Grid.col12 ]
        [ div [ DSFR.Grid.gridRow, class "fr-card--white p-8" ]
            [ div [ DSFR.Grid.col9, class "flex flex-col p-8" ]
                [ h2 [ Typo.fr_h4 ] [ text "Bienvenue sur Déveco\u{00A0}!" ]
                , p [] [ text "Nous vous invitons à créer vos premières fiches pour suivre vos relations avec les établissements et les créateurs d'entreprise de votre territoire." ]
                , p [] [ text "Pour cela, ", link (Route.toUrl <| Route.Etablissements Nothing) [ Typo.textBold ] [ text "allez dans l'onglet Établissements" ], text "ou ", link (Route.toUrl <| Route.Etablissements Nothing) [ Typo.textBold ] [ text "dans l'onglet Créateurs d'entreprise" ], text " et créez votre première fiche\u{00A0}!" ]
                ]
            , div [ DSFR.Grid.col3, class "flex justify-center p-8" ]
                [ decorativeImg [ Html.Attributes.src "/assets/deveco-carte-identite.svg" ]
                ]
            ]
        ]


viewHelpBlock : Html msg
viewHelpBlock =
    div [ DSFR.Grid.col12 ]
        [ div [ DSFR.Grid.gridRow, class "fr-card--white p-8" ]
            [ div [ DSFR.Grid.col12 ]
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col6, class "flex flex-col px-4 my-4" ]
                        [ div [ class "flex flex-row justify-between items-center" ]
                            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Aide" ]
                            , decorativeImg [ Html.Attributes.src "/assets/deveco-cooperation.svg" ]
                            ]
                        , p [] [ text "Pour vous aider dans vos premières utilisations de l'outil Dévéco\u{00A0}:" ]
                        , p []
                            [ externalLink
                                (Builder.crossOrigin "https://deveco.notion.site" [ "Bienvenue-sur-Deveco-64aa970576434b9dbc9299b4f2583005" ] [])
                                []
                                [ text "Mes premiers pas sur Dévéco" ]
                            ]
                        ]
                    , div [ DSFR.Grid.col6, class "flex flex-col px-4 my-4 border-l-2" ]
                        [ div [ class "flex flex-row justify-between items-center" ]
                            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Vos avis" ]
                            , decorativeImg [ Html.Attributes.src "/assets/deveco-information-small.svg" ]
                            ]
                        , p [] [ text "Envoyez-nous vos idées et suggestions d'amélioration\u{00A0}:" ]
                        , p []
                            [ externalLink
                                ("mailto:" ++ contactEmail)
                                []
                                [ text contactEmail ]
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div
                        [ class "flex w-full justify-center"
                        , DSFR.Grid.col12
                        ]
                        [ link "#footer"
                            [ Typo.textXs
                            , Typo.textLight
                            , class "text-center"
                            , Html.Attributes.target "_self"
                            ]
                            [ icon arrowDownLine
                            , text "Retrouvez les liens à tout moment, en bas de votre écran"
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewEtablissementsStats : FichesStats -> Html msg
viewEtablissementsStats { etablissements } =
    div []
        [ DSFR.Card.card "" DSFR.Card.vertical
            |> DSFR.Card.linkFull (Route.toUrl <| Route.Etablissements Nothing)
            |> DSFR.Card.withArrow False
            |> DSFR.Card.withNoTitle
            |> DSFR.Card.withDescription
                (Just <|
                    div [ class "text-center" ] <|
                        [ div [ class "!m-0", Typo.textBold, Typo.fr_h1 ] <| List.singleton <| text <| formatNumberWithThousandSpacing etablissements
                        , div [] [ text "établissement", text <| plural etablissements, text " ", text "sur le territoire" ]
                        ]
                )
            |> DSFR.Card.view
        ]


viewSuivisStats : FichesStats -> Html msg
viewSuivisStats { suivis } =
    div []
        [ DSFR.Card.card "" DSFR.Card.vertical
            |> DSFR.Card.linkFull (Route.toUrl <| Route.Etablissements <| Just <| makeQueryForPortefeuilleMode ())
            |> DSFR.Card.withArrow False
            |> DSFR.Card.withNoTitle
            |> DSFR.Card.withDescription
                (Just <|
                    div [ class "text-center" ] <|
                        [ div [ class "!m-0", Typo.textBold, Typo.fr_h1 ] <| List.singleton <| text <| formatNumberWithThousandSpacing suivis
                        , div [] [ text "établissement", text <| plural suivis, text " dans le portefeuille" ]
                        ]
                )
            |> DSFR.Card.view
        ]


viewCreateursStats : FichesStats -> Html msg
viewCreateursStats { personnes } =
    div []
        [ DSFR.Card.card "" DSFR.Card.vertical
            |> DSFR.Card.linkFull (Route.toUrl <| Route.Createurs Nothing)
            |> DSFR.Card.withArrow False
            |> DSFR.Card.withNoTitle
            |> DSFR.Card.withDescription
                (Just <|
                    div [ class "text-center" ] <|
                        [ div [ class "!m-0", Typo.textBold, Typo.fr_h1 ] <| List.singleton <| text <| formatNumberWithThousandSpacing personnes
                        , div [] [ text "créateur", text <| plural personnes, text " d'entreprise" ]
                        ]
                )
            |> DSFR.Card.view
        ]


viewLocauxStats : LocauxStats -> Html msg
viewLocauxStats { occupes, vacants } =
    div []
        [ DSFR.Card.card "" DSFR.Card.vertical
            |> DSFR.Card.linkFull (Route.toUrl <| Route.Locaux Nothing)
            |> DSFR.Card.withArrow False
            |> DSFR.Card.withNoTitle
            |> DSFR.Card.withDescription
                (Just <|
                    div [ class "text-center" ] <|
                        [ div [ class "!m-0", Typo.textBold, Typo.fr_h1 ] <| List.singleton <| text <| formatNumberWithThousandSpacing <| occupes + vacants
                        , div [] [ text "locaux dont ", span [ Typo.textBold ] <| List.singleton <| text <| String.fromInt vacants, text " vacant", text <| plural vacants ]
                        ]
                )
            |> DSFR.Card.view
        ]


getCollaborators : Cmd Msg
getCollaborators =
    Http.get
        { url = Api.getCollaborateurs
        , expect =
            Http.expectJson (RD.fromResult >> ReceivedCollaborateurs) <|
                Decode.list <|
                    User.decodeUser
        }


getStats : Cmd Msg
getStats =
    Http.get
        { url = Api.getStats
        , expect =
            Http.expectJson (RD.fromResult >> ReceivedStats) <|
                decodeStats
        }


decodeStats : Decoder Stats
decodeStats =
    Decode.succeed Stats
        |> andMap (Decode.field "fiches" decodeFiches)
        |> andMap (Decode.field "locaux" decodeLocaux)


decodeFiches : Decoder FichesStats
decodeFiches =
    Decode.succeed FichesStats
        |> andMap (Decode.field "etablissements" Decode.int)
        |> andMap (Decode.field "suivis" Decode.int)
        |> andMap (Decode.field "personnes" Decode.int)


decodeLocaux : Decoder LocauxStats
decodeLocaux =
    Decode.succeed LocauxStats
        |> andMap (Decode.field "occupes" Decode.int)
        |> andMap (Decode.field "vacants" Decode.int)
