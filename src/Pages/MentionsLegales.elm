module Pages.MentionsLegales exposing (page)

import Accessibility exposing (Html, a, address, br, div, h1, h2, p, text)
import Api.Auth
import DSFR.Grid
import Effect
import Html.Attributes exposing (class)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Page () Shared.Msg (View ()) () ()
page { identity } =
    Spa.Page.element
        { init =
            \_ ->
                ( ()
                , case identity of
                    Nothing ->
                        Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

                    _ ->
                        Effect.none
                )
                    |> Shared.pageChangeEffects
        , update = \_ _ -> ( (), Effect.none )
        , view = \_ -> view identity
        , subscriptions = \_ -> Sub.none
        }


view : Maybe Shared.User -> View ()
view identity =
    { title =
        UI.Layout.pageTitle <|
            "Mentions Légales - Deveco - version beta"
    , body = UI.Layout.lazyBody body identity
    , route = Route.Stats Nothing
    }


body : Maybe Shared.User -> Html ()
body _ =
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "fr-card--white !p-8" ]
                [ h1 []
                    [ text "Mentions légales" ]
                , h2 []
                    [ text "Éditeur" ]
                , p []
                    [ text "Ce site est édité au sein de l’incubateur de l’Agence Nationale de la Cohésion des Territoires :" ]
                , p []
                    [ address []
                        [ text "Agence Nationale de la Cohésion des Territoires"
                        , br []
                        , text "20, avenue de Ségur"
                        , br []
                        , text "TSA 10717"
                        , br []
                        , text "75 334 Paris Cedex 07"
                        , br []
                        , a
                            [ Html.Attributes.href "tel:0185586000"
                            ]
                            [ text "Téléphone : 01 85 58 60 00" ]
                        ]
                    ]
                , h2 []
                    [ text "Directeur de la publication" ]
                , p []
                    [ text "Monsieur Yves Le Breton, Directeur général." ]
                , h2 []
                    [ text "Hébergement du site" ]
                , p []
                    [ text "Ce site est hébergé par :" ]
                , p []
                    [ address []
                        [ text "Scaleway SAS"
                        , br []
                        , text "8 rue de la ville l’évêque"
                        , br []
                        , text "75008 Paris"
                        ]
                    ]
                , h2 []
                    [ text "Accessibilité" ]
                , p []
                    [ text "La conformité aux normes d’accessibilité numérique est un objectif ultérieur mais nous tâchons de rendre ce site accessible à toutes et à tous." ]
                , h2 []
                    [ text "Signaler un dysfonctionnement" ]
                , p []
                    [ text "Si vous rencontrez un défaut d’accessibilité vous empêchant d’accéder à un contenu ou une fonctionnalité du site, merci de nous en faire part." ]
                , p []
                    [ text "Si vous n’obtenez pas de réponse rapide de notre part, vous êtes en droit de faire parvenir vos doléances ou une demande de saisine au Défenseur des droits." ]
                , h2 []
                    [ text "En savoir plus" ]
                , p []
                    [ text "Pour en savoir plus sur la politique d’accessibilité numérique de l’État :"
                    , br []
                    , a
                        [ Html.Attributes.href "http://references.modernisation.gouv.fr/accessibilite-numerique"
                        ]
                        [ text "http://references.modernisation.gouv.fr/accessibilite-numerique" ]
                    ]
                , h2 []
                    [ text "Sécurité" ]
                , p []
                    [ text "Le site est protégé par un certificat électronique, matérialisé pour la grande majorité des navigateurs par un cadenas. Cette protection participe à la confidentialité des échanges." ]
                , p []
                    [ text "En aucun cas les services associés à la plateforme ne seront à l’origine d’envoi de courriels pour demander la saisie d’informations personnelles." ]
                ]
            ]
        ]
