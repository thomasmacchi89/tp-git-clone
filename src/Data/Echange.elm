module Data.Echange exposing (Echange(..), EchangeData, EchangeId, TypeEchange(..), compteRendu, createEchange, date, decodeEchange, encodeEchange, encodeTypeEchange, id, modification, themes, titre, typeEchangeToString, type_)

import Api.EntityId exposing (EntityId)
import Data.Demande
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Time


type Echange
    = Echange EchangeData


type EchangeId
    = EchangeId


type alias EchangeData =
    { id : EntityId EchangeId
    , type_ : TypeEchange
    , titre : String
    , compteRendu : String
    , date : Date
    , themes : List Data.Demande.TypeDemande
    , modification : Maybe ( String, Time.Posix )
    }


type TypeEchange
    = Telephone
    | Email
    | Rencontre
    | Courrier


id : Echange -> EntityId EchangeId
id (Echange data) =
    data.id


type_ : Echange -> TypeEchange
type_ (Echange data) =
    data.type_


titre : Echange -> String
titre (Echange data) =
    data.titre


compteRendu : Echange -> String
compteRendu (Echange data) =
    data.compteRendu


date : Echange -> Date
date (Echange data) =
    data.date


themes : Echange -> List Data.Demande.TypeDemande
themes (Echange data) =
    data.themes


modification : Echange -> Maybe ( String, Time.Posix )
modification (Echange data) =
    data.modification


encodeTypeEchange : TypeEchange -> Value
encodeTypeEchange =
    typeEchangeToString >> Encode.string


typeEchangeToString : TypeEchange -> String
typeEchangeToString t =
    case t of
        Telephone ->
            "telephone"

        Email ->
            "email"

        Rencontre ->
            "rencontre"

        Courrier ->
            "courrier"


stringToTypeEchange : String -> Maybe TypeEchange
stringToTypeEchange s =
    case s of
        "telephone" ->
            Just Telephone

        "email" ->
            Just Email

        "rencontre" ->
            Just Rencontre

        "courrier" ->
            Just Courrier

        _ ->
            Nothing


decodeTypeEchange : Decoder TypeEchange
decodeTypeEchange =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToTypeEchange
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Unknown typeEchange " ++ s)
            )


decodeEchange : Decoder Echange
decodeEchange =
    Decode.succeed EchangeData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "typeEchange" decodeTypeEchange)
        |> andMap (Decode.map (Maybe.withDefault "Premier échange") <| Decode.field "titre" <| Decode.nullable Decode.string)
        |> andMap (Decode.field "compteRendu" Decode.string)
        |> andMap (Decode.field "dateEchange" Lib.Date.decodeCalendarDate)
        |> andMap (Decode.field "themes" <| Decode.list Data.Demande.decodeTypeDemande)
        |> andMap decodeModification
        |> Decode.map Echange


decodeModification : Decoder (Maybe ( String, Time.Posix ))
decodeModification =
    Decode.map2 (Maybe.map2 Tuple.pair)
        (optionalNullableField "auteurModif" Decode.string)
        (optionalNullableField "updatedAt" Lib.Date.decodeDateWithTimeFromISOString)


encodeEchange : String -> Echange -> Value
encodeEchange from (Echange echangeData) =
    [ ( "echange"
      , Encode.object
            [ ( "id", Api.EntityId.encodeEntityId echangeData.id )
            , ( "typeEchange", encodeTypeEchange echangeData.type_ )
            , ( "dateEchange", Encode.string <| Date.toIsoString echangeData.date )
            , ( "titre", Encode.string echangeData.titre )
            , ( "compteRendu", Encode.string echangeData.compteRendu )
            , ( "themes", Encode.list Data.Demande.encodeTypeDemande echangeData.themes )
            ]
      )
    , ( "from", Encode.string from )
    ]
        |> Encode.object


createEchange : { id : EntityId EchangeId, type_ : TypeEchange, titre : String, compteRendu : String, date : Date, themes : List Data.Demande.TypeDemande } -> Echange
createEchange r =
    Echange <| EchangeData r.id r.type_ r.titre r.compteRendu r.date r.themes Nothing
