module Data.Role exposing (Role(..), decodeRole, territoire)

import Data.Territoire exposing (Territoire)
import Json.Decode as Decode exposing (Decoder)


type Role
    = Deveco Territoire
    | Superadmin


decodeRole : Decoder Role
decodeRole =
    Decode.field "accountType" Decode.string
        |> Decode.andThen
            (\s ->
                case s of
                    "deveco" ->
                        Decode.field "territoire" Data.Territoire.decodeTerritoire
                            |> Decode.map Deveco

                    "superadmin" ->
                        Decode.succeed Superadmin

                    _ ->
                        Decode.fail <| "role inconnu\u{00A0}: " ++ s
            )


territoire : Role -> Maybe Territoire
territoire role =
    case role of
        Superadmin ->
            Nothing

        Deveco t ->
            Just t
