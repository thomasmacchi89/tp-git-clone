module Data.Local exposing (Local, LocalId, LocalStatut(..), LocalType(..), decodeLocal, encodeLocalStatut, encodeLocalType, localStatutToDisplay, localStatutToString, localTypeToDisplay, localTypeToString, stringToLocalStatut, stringToLocalType)

import Api.EntityId exposing (EntityId)
import Data.Proprietaire exposing (Proprietaire)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.Date
import Time exposing (Posix)


type LocalId
    = LocalId


type alias Local =
    { id : EntityId LocalId
    , titre : String
    , adresse : String
    , ville : String
    , codePostal : String
    , geolocation : String
    , statut : LocalStatut
    , types : List LocalType
    , surface : String
    , loyer : String
    , localisations : List String
    , commentaire : String
    , dateModification : Posix
    , auteurModification : String
    , proprietaires : List Proprietaire
    }


type LocalStatut
    = Occupe
    | Vacant


type LocalType
    = Commerce
    | Bureaux
    | AteliersArtisanaux
    | BatimentsIndustriels
    | Entrepot


decodeLocal : Decoder Local
decodeLocal =
    Decode.succeed Local
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "titre" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "adresse" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "ville" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "codePostal" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "geolocation" Decode.string)
        |> andMap (Decode.field "localStatut" decodeLocalStatut)
        |> andMap (Decode.field "localTypes" <| Decode.list decodeLocalType)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "surface" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "loyer" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "localisations" <| Decode.list Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "-") <| Decode.maybe <| Decode.field "commentaire" Decode.string)
        |> andMap (Decode.field "dateModification" Lib.Date.decodeDateWithTimeFromISOString)
        |> andMap (Decode.field "nomAuteurModification" Decode.string)
        {- make it optional so we don't require it in case we just want to show the Local and no info about the Proprietaires -} |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "proprietaires" <| Decode.list Data.Proprietaire.decodeProprietaire)


decodeLocalStatut : Decoder LocalStatut
decodeLocalStatut =
    Decode.string
        |> Decode.andThen
            (\localStatut ->
                localStatut
                    |> stringToLocalStatut
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Type inconnu " ++ localStatut)
            )


stringToLocalStatut : String -> Maybe LocalStatut
stringToLocalStatut s =
    case s of
        "occupe" ->
            Just Occupe

        "vacant" ->
            Just Vacant

        _ ->
            Nothing


decodeLocalType : Decoder LocalType
decodeLocalType =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToLocalType
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Unknown local type " ++ s)
            )


stringToLocalType : String -> Maybe LocalType
stringToLocalType s =
    case s of
        "commerce" ->
            Just Commerce

        "bureaux" ->
            Just Bureaux

        "ateliersartisanaux" ->
            Just AteliersArtisanaux

        "batimentsindustriels" ->
            Just BatimentsIndustriels

        "entrepot" ->
            Just Entrepot

        _ ->
            Nothing


localStatutToString : LocalStatut -> String
localStatutToString statut =
    case statut of
        Occupe ->
            "occupe"

        Vacant ->
            "vacant"


localStatutToDisplay : LocalStatut -> String
localStatutToDisplay statut =
    case statut of
        Occupe ->
            "Occupé"

        Vacant ->
            "Vacant"


localTypeToString : LocalType -> String
localTypeToString type_ =
    case type_ of
        Commerce ->
            "commerce"

        Bureaux ->
            "bureaux"

        AteliersArtisanaux ->
            "ateliersartisanaux"

        BatimentsIndustriels ->
            "batimentsindustriels"

        Entrepot ->
            "entrepot"


localTypeToDisplay : LocalType -> String
localTypeToDisplay type_ =
    case type_ of
        Commerce ->
            "Commerce"

        Bureaux ->
            "Bureau"

        AteliersArtisanaux ->
            "Artisanat"

        BatimentsIndustriels ->
            "Bât. industriel"

        Entrepot ->
            "Entrepôt"


encodeLocalStatut : LocalStatut -> Value
encodeLocalStatut =
    localStatutToString >> Encode.string


encodeLocalType : LocalType -> Value
encodeLocalType =
    localTypeToString >> Encode.string
