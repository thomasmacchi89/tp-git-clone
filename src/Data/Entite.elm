module Data.Entite exposing (Contact, ContactId, Entite, EntiteId, EntiteType(..), activiteAutre, activitesReelles, adresse, decodeEntite, encodeContact, exogene, getEntreprise, getFutureEnseigne, getParticulier, id, isEntreprise, localisations, motsCles, nom, statut)

import Api.EntityId exposing (EntityId)
import Data.Entreprise exposing (Entreprise, EtatAdministratif)
import Data.Particulier exposing (Particulier)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode


type EntiteId
    = EntiteId


type ContactId
    = ContactId


type alias Entite =
    { id : EntityId EntiteId
    , type_ : EntiteType
    , contacts : List Contact
    , activitesReelles : List String
    , localisations : List String
    , activiteAutre : String
    , motsCles : List String
    , ficheId : Maybe (EntityId ())
    , proprietes : List (EntityId ())
    , futureEnseigne : Maybe String
    }


type EntiteType
    = EntiteE Entreprise
    | EntiteP Particulier


type alias Contact =
    { id : EntityId ContactId
    , fonction : String
    , prenom : String
    , nom : String
    , email : String
    , telephone : String
    }


isEntreprise : Entite -> Bool
isEntreprise { type_ } =
    case type_ of
        EntiteE _ ->
            True

        _ ->
            False


decodeEntite : Decoder Entite
decodeEntite =
    Decode.succeed Entite
        |> andMap (Decode.field "entiteId" Api.EntityId.decodeEntityId)
        |> andMap decodeEntiteType
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "contacts" <| Decode.list decodeContact)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "activitesReelles" <| Decode.list Decode.string)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "localisations" <| Decode.list Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "activiteAutre" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "motsCles" <| Decode.list Decode.string)
        |> andMap (optionalNullableField "fiche" <| Decode.field "id" <| Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "nbProprietes" <| Decode.list <| Api.EntityId.decodeEntityId)
        |> andMap (optionalNullableField "futureEnseigne" Decode.string)


decodeEntiteType : Decoder EntiteType
decodeEntiteType =
    Decode.field "entiteType" Decode.string
        |> Decode.andThen
            (\entiteType ->
                case entiteType of
                    "PM" ->
                        Decode.map EntiteE decodeEntreprise

                    "PP" ->
                        Decode.map EntiteP decodeParticulier

                    _ ->
                        Decode.fail <| "Type inconnu " ++ entiteType
            )


decodeEntreprise : Decoder Entreprise
decodeEntreprise =
    Decode.field "entreprise" Data.Entreprise.decodeEntreprise


decodeContact : Decoder Contact
decodeContact =
    Decode.succeed Contact
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "fonction" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "prenom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "telephone" Decode.string)


encodeContact : Contact -> Encode.Value
encodeContact contact =
    [ ( "fonction", Encode.string contact.fonction )
    , ( "prenom", Encode.string contact.prenom )
    , ( "nom", Encode.string contact.nom )
    , ( "email", Encode.string contact.email )
    , ( "telephone", Encode.string contact.telephone )
    ]
        |> Encode.object


decodeParticulier : Decoder Particulier
decodeParticulier =
    Decode.field "particulier" Data.Particulier.decodeParticulier


id : Entite -> EntityId EntiteId
id entite =
    entite.id


nom : Entite -> String
nom { type_ } =
    case type_ of
        EntiteE entreprise ->
            Data.Entreprise.displayNomEnseigne entreprise

        EntiteP particulier ->
            Data.Particulier.prenom particulier ++ " " ++ Data.Particulier.nom particulier


adresse : Entite -> String
adresse { type_ } =
    case type_ of
        EntiteE entreprise ->
            Data.Entreprise.adresse entreprise

        EntiteP particulier ->
            Data.Particulier.adresse particulier


statut : Entite -> Maybe EtatAdministratif
statut { type_ } =
    case type_ of
        EntiteE data ->
            Just data.etatAdministratif

        EntiteP _ ->
            Nothing


exogene : Entite -> Bool
exogene =
    .type_
        >> (\t ->
                case t of
                    EntiteP _ ->
                        False

                    EntiteE entreprise ->
                        entreprise.exogene
           )


getEntreprise : Entite -> Maybe Entreprise
getEntreprise { type_ } =
    case type_ of
        EntiteP _ ->
            Nothing

        EntiteE entreprise ->
            Just entreprise


getParticulier : Entite -> Maybe Particulier
getParticulier { type_ } =
    case type_ of
        EntiteP particulier ->
            Just particulier

        EntiteE _ ->
            Nothing


getFutureEnseigne : Entite -> Maybe String
getFutureEnseigne { futureEnseigne } =
    futureEnseigne


activitesReelles : Entite -> List String
activitesReelles =
    .activitesReelles


localisations : Entite -> List String
localisations =
    .localisations


motsCles : Entite -> List String
motsCles =
    .motsCles


activiteAutre : Entite -> String
activiteAutre =
    .activiteAutre
