module Data.Adresse exposing (ApiAdresse, ApiStreet, decodeApiFeatures, decodeApiStreet)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type alias ApiAdresse =
    { label : String
    , housenumber : String
    , street : String
    , city : String
    , postcode : String
    , x : Float
    , y : Float
    }


type alias ApiStreet =
    { label : String
    , name : String
    , postcode : String
    , city : String
    }


decodeApiAdresse : Decoder ApiAdresse
decodeApiAdresse =
    Decode.succeed ApiAdresse
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "label" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "housenumber" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "street" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "city" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "postcode" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault 0) <| Decode.maybe <| Decode.field "x" Decode.float)
        |> andMap (Decode.map (Maybe.withDefault 0) <| Decode.maybe <| Decode.field "y" Decode.float)


decodeApiFeatures : Decoder (List ApiAdresse)
decodeApiFeatures =
    Decode.field "features" <|
        Decode.list <|
            Decode.field "properties" <|
                decodeApiAdresse


decodeApiStreet : Decoder (List ApiStreet)
decodeApiStreet =
    Decode.field "features" <|
        Decode.list <|
            Decode.field "properties" <|
                (Decode.succeed ApiStreet
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "label" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "name" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "postcode" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "city" Decode.string)
                )
