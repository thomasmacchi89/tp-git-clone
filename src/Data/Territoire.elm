module Data.Territoire exposing (Territoire, TerritoireId, decodeTerritoire, id, isEpci, nom, typeToDisplay, type_)

import Api.EntityId exposing (EntityId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)


type Territoire
    = Territoire TerritoireData


type TerritoireId
    = TerritoireId


type alias TerritoireData =
    { id : EntityId TerritoireId
    , nom : String
    , type_ : String
    }


id : Territoire -> EntityId TerritoireId
id (Territoire data) =
    data.id


nom : Territoire -> String
nom (Territoire data) =
    data.nom


type_ : Territoire -> String
type_ (Territoire data) =
    data.type_


typeToDisplay : String -> String
typeToDisplay territoryType =
    case territoryType of
        "CA" ->
            "Communauté d'agglomération"

        "CC" ->
            "Communauté de communes"

        "CU" ->
            "Communauté urbaine"

        "ME" ->
            "Métropole"

        "metropole" ->
            "Ville à arrondissements"

        _ ->
            "-"


decodeTerritoire : Decoder Territoire
decodeTerritoire =
    Decode.succeed TerritoireData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "name" Decode.string)
        |> andMap (Decode.field "territoryType" Decode.string)
        |> Decode.map Territoire


isEpci : Territoire -> Bool
isEpci =
    type_ >> typeToDisplay >> (/=) "-"
