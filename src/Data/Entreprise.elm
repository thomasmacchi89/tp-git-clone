module Data.Entreprise exposing (Entreprise, EtatAdministratif(..), adresse, decodeEntreprise, displayNomEnseigne, etatAdministratifToDisplay, etatAdministratifToString)

import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date


type alias Entreprise =
    { siret : String
    , siren : String
    , nom : String
    , adresse : String
    , dateCreationEtablissement : Maybe Date
    , activitePrincipaleUniteLegale : Maybe String
    , categorieActivitePrincipaleUniteLegale : Maybe String
    , etatAdministratif : EtatAdministratif
    , formeJuridique : Maybe String
    , exercices : List ( String, Date )
    , effectifs : Maybe ( String, Date )
    , enseigne : Maybe String
    , exogene : Bool
    , zonagesPrioritaires : List String
    }


type EtatAdministratif
    = Inconnu
    | Actif
    | Inactif


decodeEntreprise : Decoder Entreprise
decodeEntreprise =
    Decode.succeed Entreprise
        |> andMap (Decode.field "siret" Decode.string)
        |> andMap (Decode.map (String.left 9) <| Decode.field "siret" Decode.string)
        |> andMap
            (Decode.map (Maybe.withDefault "") <|
                optionalNullableField "nom" Decode.string
            )
        |> andMap
            (Decode.map (Maybe.withDefault "") <|
                optionalNullableField "adresse" Decode.string
            )
        |> andMap (optionalNullableField "dateCreation" <| Lib.Date.decodeCalendarDate)
        |> andMap decodeNaf
        |> andMap (optionalNullableField "libelleCategorieNaf" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault Inconnu) <| optionalNullableField "etatAdministratif" decodeEtatAdministratif)
        |> andMap (optionalNullableField "formeJuridique" <| Decode.string)
        |> andMap
            (Decode.map (List.filterMap identity) <|
                Decode.map (Maybe.withDefault []) <|
                    optionalNullableField "exercices" <|
                        Decode.list decodeCa
            )
        |> andMap decodeEffectifs
        |> andMap (optionalNullableField "enseigne" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "exogene" Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "qpvs" <| Decode.list <| Decode.field "nomQp" Decode.string)


decodeCa : Decoder (Maybe ( String, Date ))
decodeCa =
    Decode.map2 (Maybe.map2 Tuple.pair)
        (optionalNullableField "ca" <| Decode.string)
        (optionalNullableField "dateCloture" <| Lib.Date.decodeCalendarDate)


decodeEffectifs : Decoder (Maybe ( String, Date ))
decodeEffectifs =
    Decode.map2 (Maybe.map2 Tuple.pair)
        (optionalNullableField "effectifs" <| Decode.string)
        (optionalNullableField "dateEffectifs" <| Lib.Date.decodeCalendarDate)


decodeNaf : Decoder (Maybe String)
decodeNaf =
    Decode.succeed
        (\libelle code ->
            case ( libelle, code ) of
                ( Just l, Just c ) ->
                    l
                        ++ " ("
                        ++ c
                        ++ ")"
                        |> Just

                ( Just l, Nothing ) ->
                    l
                        |> Just

                ( Nothing, Just c ) ->
                    c
                        |> Just

                ( Nothing, Nothing ) ->
                    Nothing
        )
        |> andMap
            (optionalNullableField
                "libelleNaf"
                Decode.string
            )
        |> andMap
            (optionalNullableField
                "codeNaf"
                Decode.string
            )


decodeEtatAdministratif : Decoder EtatAdministratif
decodeEtatAdministratif =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToEtatAdministratif
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "État administratif inconnu :" ++ s)
            )


{-| nom d'enseigne (nom maison-mère) OU nom maison-mère OU nom d'enseigne si identique à la maison-mère
-}
displayNomEnseigne : Entreprise -> String
displayNomEnseigne entreprise =
    case entreprise.enseigne of
        Just "" ->
            entreprise.nom

        Just ens ->
            if ens == entreprise.nom then
                ens

            else
                ens ++ " (" ++ entreprise.nom ++ ")"

        Nothing ->
            entreprise.nom


adresse : Entreprise -> String
adresse =
    .adresse


etatAdministratifToDisplay : EtatAdministratif -> String
etatAdministratifToDisplay etat =
    case etat of
        Actif ->
            "Actif"

        Inactif ->
            "Inactif"

        Inconnu ->
            "Inconnu"


etatAdministratifToString : EtatAdministratif -> String
etatAdministratifToString s =
    case s of
        Actif ->
            "A"

        Inactif ->
            "F"

        Inconnu ->
            "I"


stringToEtatAdministratif : String -> Maybe EtatAdministratif
stringToEtatAdministratif s =
    case s of
        "A" ->
            Just Actif

        "F" ->
            Just Inactif

        "I" ->
            Just Inconnu

        _ ->
            Nothing
