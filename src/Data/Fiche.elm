module Data.Fiche exposing (Fiche, FicheId, activitesReelles, adresse, decodeFiche, encodeContact, entiteId, exogene, futureEnseigne, getEntreprise, getParticulier, id, nom, statut)

import Api.EntityId exposing (EntityId)
import Data.Cloture exposing (Cloture)
import Data.Demande exposing (Demande)
import Data.Echange exposing (Echange)
import Data.Entite as Entite exposing (Entite)
import Data.Entreprise exposing (Entreprise, EtatAdministratif)
import Data.Particulier exposing (Particulier)
import Data.Rappel exposing (Rappel)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode exposing (Value)
import Lib.Date
import Time exposing (Posix)


type FicheId
    = FicheId


type alias Fiche =
    { id : EntityId FicheId
    , entite : Entite
    , demandes : List Demande
    , echanges : List Echange
    , rappels : List Rappel
    , clotures : List Cloture
    , dateModification : Posix
    , auteurModification : String
    , sireneUpdate : Maybe String
    }


decodeFiche : Decoder Fiche
decodeFiche =
    Decode.succeed Fiche
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap Entite.decodeEntite
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "demandes" <| Decode.list Data.Demande.decodeDemande)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "echanges" <| Decode.list Data.Echange.decodeEchange)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "rappels" <| Decode.list Data.Rappel.decodeRappel)
        |> andMap (Decode.map (List.filterMap identity) <| Decode.map (Maybe.withDefault []) <| optionalNullableField "demandes" <| Decode.list Data.Cloture.decodeCloture)
        |> andMap (Decode.field "dateModification" Lib.Date.decodeDateWithTimeFromISOString)
        |> andMap (Decode.field "auteurModification" Decode.string)
        |> andMap (optionalNullableField "sireneUpdate" Decode.string)


encodeContact : Entite.Contact -> Value
encodeContact =
    Entite.encodeContact


id : Fiche -> EntityId FicheId
id =
    .id


nom : Fiche -> String
nom =
    .entite >> Entite.nom


adresse : Fiche -> String
adresse =
    .entite >> Entite.adresse


activitesReelles : Fiche -> List String
activitesReelles =
    .entite >> Entite.activitesReelles


statut : Fiche -> Maybe EtatAdministratif
statut =
    .entite >> Entite.statut


exogene : Fiche -> Bool
exogene =
    .entite >> Entite.exogene


getEntreprise : Fiche -> Maybe Entreprise
getEntreprise =
    .entite >> Entite.getEntreprise


getParticulier : Fiche -> Maybe Particulier
getParticulier =
    .entite >> Entite.getParticulier


futureEnseigne : Fiche -> Maybe String
futureEnseigne =
    .entite >> Entite.getFutureEnseigne


entiteId : Fiche -> EntityId Entite.EntiteId
entiteId =
    .entite >> Entite.id
