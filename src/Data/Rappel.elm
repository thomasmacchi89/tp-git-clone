module Data.Rappel exposing (Rappel(..), RappelData, RappelId, createRappel, date, dateCloture, decodeRappel, encodeRappel, id, titre)

import Api.EntityId exposing (EntityId)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.Date


type Rappel
    = Rappel RappelData


type RappelId
    = RappelId


type alias RappelData =
    { id : EntityId RappelId
    , titre : String
    , date : Date
    , dateCloture : Maybe Date
    }


id : Rappel -> EntityId RappelId
id (Rappel data) =
    data.id


titre : Rappel -> String
titre (Rappel data) =
    data.titre


date : Rappel -> Date
date (Rappel data) =
    data.date


dateCloture : Rappel -> Maybe Date
dateCloture (Rappel data) =
    data.dateCloture


decodeRappel : Decoder Rappel
decodeRappel =
    Decode.succeed RappelData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "titre" Decode.string)
        |> andMap (Decode.field "dateRappel" Lib.Date.decodeCalendarDate)
        |> andMap (optionalNullableField "dateCloture" Lib.Date.decodeCalendarDate)
        |> Decode.map Rappel


encodeRappel : String -> Rappel -> Value
encodeRappel from (Rappel rappelData) =
    [ ( "rappel"
      , [ ( "id", Api.EntityId.encodeEntityId rappelData.id )
        , ( "dateRappel", Encode.string <| Date.toIsoString rappelData.date )
        , ( "titre", Encode.string rappelData.titre )
        , ( "dateCloture", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| Maybe.map Date.toIsoString <| rappelData.dateCloture )
        ]
            |> Encode.object
      )
    , ( "from", Encode.string from )
    ]
        |> Encode.object


createRappel : { id : EntityId RappelId, titre : String, date : Date, dateCloture : Maybe Date } -> Rappel
createRappel =
    Rappel
