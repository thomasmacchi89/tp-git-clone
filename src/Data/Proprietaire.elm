module Data.Proprietaire exposing (Proprietaire, decodeProprietaire)

import Api.EntityId exposing (EntityId)
import Data.Entite exposing (Entite)
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type ProprietaireId
    = ProprietaireId


type alias Proprietaire =
    { id : EntityId ProprietaireId
    , commentaire : String
    , entite : Entite
    }


decodeProprietaire : Decode.Decoder Proprietaire
decodeProprietaire =
    Decode.succeed Proprietaire
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "commentaire" Decode.string)
        |> andMap (Decode.field "entite" Data.Entite.decodeEntite)
