module User exposing (User, UserId, decodeUser, email, id, nom, prenom, role)

import Api.EntityId
import Data.Role exposing (Role)
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)


type User
    = User UserData


type UserId
    = UserId


type alias UserData =
    { email : String
    , role : Role
    , nom : String
    , prenom : String
    , id : Api.EntityId.EntityId UserId
    }


email : User -> String
email (User user) =
    user.email


nom : User -> String
nom (User user) =
    user.nom


prenom : User -> String
prenom (User user) =
    user.prenom


role : User -> Role
role (User user) =
    user.role


id : User -> Api.EntityId.EntityId UserId
id (User user) =
    user.id


decodeUser : Decode.Decoder User
decodeUser =
    Decode.succeed UserData
        |> andMap (Decode.field "email" Decode.string)
        |> andMap Data.Role.decodeRole
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "prenom" Decode.string)
        |> andMap (Decode.field "id" <| Api.EntityId.decodeEntityId)
        |> Decode.map User
