module Api exposing (addProprietaire, authStatus, createCloture, createContact, createEchange, createFiche, createLocal, createRappel, createUtilisateur, deleteProprietaire, editContact, geoTokens, getAdminStatsData, getAdminStatsTerritoires, getAdminTags, getCollaborateurs, getCommunesForTerritoire, getContact, getEntreprise, getEntreprises, getExportEntreprises, getExportFiches, getFiche, getFiches, getFichesFiltersSource, getLocal, getLocaux, getRappels, getStats, getStatsData, getUtilisateurs, getUtilisateursCsv, jwt, login, logout, newTerritoireSearch, rechercheActivite, rechercheActiviteParticulier, rechercheAdresse, rechercheCodeNaf, rechercheLocalisation, rechercheMot, searchEntreprises, searchInseeSiret, updateAccount, updateEchange, updateEntreprise, updateFiche, updateFiches, updateLocal, updatePassword, updatePasswordAdmin, updateQualificationsEntreprises, updateRappel)

import Api.EntityId exposing (EntityId, entityIdToString)
import Data.Echange exposing (EchangeId)
import Data.Entite exposing (ContactId)
import Data.Fiche exposing (FicheId)
import Data.Local exposing (LocalId)
import Data.Rappel exposing (RappelId)
import Data.Territoire exposing (TerritoireId)
import Url.Builder as Builder exposing (QueryParameter)


apiRoot : String
apiRoot =
    "api"


authRoute : String
authRoute =
    "auth"


inseeRoute : String
inseeRoute =
    "insee"


fichesRoute : String
fichesRoute =
    "fiches"


contactsRoute : String
contactsRoute =
    "contacts"


echangesRoute : String
echangesRoute =
    "echanges"


cloturesRoute : String
cloturesRoute =
    "demandes"


rappelsRoute : String
rappelsRoute =
    "rappels"


accountRoute : String
accountRoute =
    "compte"


adminRoute : String
adminRoute =
    "admin"


territoiresRoute : String
territoiresRoute =
    "territoires"


exportRoute : String
exportRoute =
    "export"


statsRoute : String
statsRoute =
    "stats"


locauxRoute : String
locauxRoute =
    "locaux"


apiUrl : List String -> List QueryParameter -> String
apiUrl segments =
    Builder.absolute (apiRoot :: segments)


login : String
login =
    apiUrl [ authRoute, "login" ] []


logout : String
logout =
    apiUrl [ authRoute, "logout" ] []


authStatus : String
authStatus =
    apiUrl [ authRoute, "status" ] []


jwt : String
jwt =
    apiUrl [ authRoute, "jwt" ] []


searchInseeSiret : String -> String
searchInseeSiret search =
    apiUrl [ inseeRoute, "siret" ]
        [ Builder.string "search" search
        ]


getEntreprise : String -> String
getEntreprise siret =
    apiUrl [ inseeRoute, siret ] []


getFiches : String -> String
getFiches query =
    apiUrl [ fichesRoute ] [] ++ "?" ++ query


getFiche : EntityId FicheId -> String
getFiche id =
    apiUrl [ fichesRoute, entityIdToString id ] []


createFiche : String
createFiche =
    apiUrl [ fichesRoute, "creer" ] []


updateFiche : EntityId FicheId -> String
updateFiche id =
    apiUrl [ fichesRoute, entityIdToString id, "modifier" ] []


updateFiches : String -> String
updateFiches query =
    apiUrl [ fichesRoute, "batch" ] [] ++ "?" ++ query


updateQualificationsEntreprises : String -> String
updateQualificationsEntreprises query =
    apiUrl [ inseeRoute, "batch" ] [] ++ "?" ++ query


updateEntreprise : String -> String
updateEntreprise siret =
    apiUrl [ inseeRoute, siret, "modifier" ] []


rechercheAdresse : Maybe String -> Maybe String -> Maybe String -> String -> String
rechercheAdresse type_ cityCode postCode search =
    let
        sanitizedSearch =
            String.replace " " "+" search

        typeQuery =
            type_
                |> Maybe.map (Builder.string "type" >> List.singleton)
                |> Maybe.withDefault []

        cityQuery =
            cityCode
                |> Maybe.map (Builder.string "citycode" >> List.singleton)
                |> Maybe.withDefault []

        postQuery =
            postCode
                |> Maybe.map (Builder.string "postcode" >> List.singleton)
                |> Maybe.withDefault []
    in
    Builder.crossOrigin
        "https://api-adresse.data.gouv.fr"
        [ "search" ]
    <|
        Builder.string "q" sanitizedSearch
            :: (cityQuery ++ postQuery ++ typeQuery)


rechercheActivite : String -> String
rechercheActivite search =
    apiUrl [ "activites" ]
        [ Builder.string "search" search
        ]


rechercheActiviteParticulier : String -> String
rechercheActiviteParticulier search =
    apiUrl [ "activites" ]
        [ Builder.string "search" search
        ]


rechercheLocalisation : String -> String
rechercheLocalisation search =
    apiUrl [ "localisations" ]
        [ Builder.string "search" search
        ]


rechercheMot : String -> String
rechercheMot search =
    apiUrl [ "mots" ]
        [ Builder.string "search" search
        ]


createContact : Maybe String -> Maybe (EntityId FicheId) -> String
createContact siret ficheId =
    apiUrl [ contactsRoute, "creer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


getContact : EntityId ContactId -> String
getContact id =
    apiUrl [ contactsRoute, entityIdToString id ] []


editContact : EntityId ContactId -> String
editContact id =
    apiUrl [ contactsRoute, entityIdToString id, "modifier" ] []


createEchange : Maybe String -> Maybe (EntityId FicheId) -> String
createEchange siret ficheId =
    apiUrl [ echangesRoute, "creer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


updateEchange : EntityId EchangeId -> String
updateEchange id =
    apiUrl [ echangesRoute, entityIdToString id, "modifier" ] []


createCloture : Maybe String -> Maybe (EntityId FicheId) -> String
createCloture siret ficheId =
    apiUrl [ cloturesRoute, "cloturer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


getRappels : String
getRappels =
    apiUrl [ rappelsRoute ] []


createRappel : Maybe String -> Maybe (EntityId FicheId) -> String
createRappel siret ficheId =
    apiUrl [ rappelsRoute, "creer" ] <|
        List.filterMap identity <|
            [ Maybe.map (Builder.string "ficheId") <|
                Maybe.map entityIdToString <|
                    ficheId
            , Maybe.map (Builder.string "siret") <|
                siret
            ]


updateRappel : EntityId RappelId -> String
updateRappel id =
    apiUrl [ rappelsRoute, entityIdToString id, "modifier" ] []


updateAccount : String
updateAccount =
    apiUrl [ accountRoute ] []


updatePassword : String
updatePassword =
    apiUrl [ accountRoute, "password" ] []


getCollaborateurs : String
getCollaborateurs =
    apiUrl [ accountRoute, "collaborateurs" ] []


getStats : String
getStats =
    apiUrl [ accountRoute, "stats" ] []


getFichesFiltersSource : String
getFichesFiltersSource =
    apiUrl [ fichesRoute, "filtersSource" ] []


createUtilisateur : String
createUtilisateur =
    apiUrl [ adminRoute, "utilisateurs" ] []


getUtilisateurs : String
getUtilisateurs =
    apiUrl [ adminRoute, "utilisateurs" ] []


getUtilisateursCsv : String
getUtilisateursCsv =
    apiUrl [ adminRoute, "utilisateurs", "csv" ] []


updatePasswordAdmin : String
updatePasswordAdmin =
    apiUrl [ adminRoute, "password" ] []


newTerritoireSearch : String -> String
newTerritoireSearch search =
    apiUrl [ territoiresRoute, "search" ]
        [ Builder.string "search" search ]


getCommunesForTerritoire : EntityId TerritoireId -> String
getCommunesForTerritoire id =
    apiUrl [ territoiresRoute, entityIdToString id, "communes" ] []


getExportFiches : String -> String
getExportFiches query =
    apiUrl [ exportRoute, "fiches" ] [] ++ "?" ++ query


getExportEntreprises : String -> String
getExportEntreprises query =
    apiUrl [ exportRoute, "entreprises" ] [] ++ "?" ++ query


getStatsData : String -> String
getStatsData query =
    apiUrl [ statsRoute ] [] ++ "?" ++ query


getAdminStatsTerritoires : String
getAdminStatsTerritoires =
    apiUrl [ adminRoute, "stats", "territoires" ] []


getAdminStatsData : String -> String
getAdminStatsData query =
    apiUrl [ adminRoute, "stats" ] [] ++ "?" ++ query


getLocaux : String -> String
getLocaux query =
    apiUrl [ locauxRoute ] [] ++ "?" ++ query


createLocal : String
createLocal =
    apiUrl [ locauxRoute, "creer" ] []


getLocal : EntityId LocalId -> String
getLocal id =
    apiUrl [ locauxRoute, entityIdToString id ] []


updateLocal : EntityId LocalId -> String
updateLocal id =
    apiUrl [ locauxRoute, entityIdToString id, "modifier" ] []


addProprietaire : EntityId LocalId -> String
addProprietaire id =
    apiUrl [ locauxRoute, entityIdToString id, "proprietaire" ] []


deleteProprietaire : EntityId LocalId -> String
deleteProprietaire id =
    apiUrl [ locauxRoute, entityIdToString id, "proprietaire", "remove" ] []


getEntreprises : String -> String
getEntreprises query =
    apiUrl [ inseeRoute, "siret" ] [] ++ "?" ++ query


searchEntreprises : String -> String
searchEntreprises query =
    apiUrl [ inseeRoute ] [] ++ "?" ++ query


rechercheCodeNaf : String -> String
rechercheCodeNaf search =
    apiUrl [ "naf" ]
        [ Builder.string "search" search
        ]


geoTokens : String
geoTokens =
    apiUrl [ accountRoute, "geotokens" ] []


getAdminTags : EntityId TerritoireId -> String
getAdminTags territoryId =
    apiUrl [ adminRoute, "tags", entityIdToString territoryId ] []
