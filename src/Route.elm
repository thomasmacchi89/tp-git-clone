module Route exposing (Route(..), defaultPageForRole, matchActions, matchAdminStats, matchAdminTags, matchAdminUtilisateurs, matchAuthCheck, matchAuthJwtUuid, matchCGU, matchConnexion, matchCreateurEditId, matchCreateurId, matchCreateurNew, matchCreateurs, matchDashboard, matchDeconnexion, matchEtablissementSiret, matchEtablissements, matchFicheNew, matchLocalEditId, matchLocalId, matchLocalNew, matchLocaux, matchMentionsLegales, matchNotFound, matchPolitiqueConfidentialite, matchProfil, matchStats, toRoute, toUrl)

import Api.EntityId exposing (EntityId)
import Data.Fiche exposing (FicheId)
import Data.Local exposing (LocalId)
import Data.Role
import Url exposing (Url)
import Url.Builder as Builder
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, parse, s, string, top)
import Url.Parser.Query as Query


type Route
    = Connexion (Maybe String)
    | Etablissements (Maybe String)
    | Etablissement String
    | EtablissementNew
    | Createurs (Maybe String)
    | Createur (EntityId FicheId)
    | CreateurEdit (EntityId FicheId)
    | CreateurNew
    | Actions
    | Profil
    | Dashboard
    | Stats (Maybe String)
    | AdminUtilisateurs
    | AdminStats (Maybe String)
    | AdminTags
    | Deconnexion
    | Locaux (Maybe String)
    | Local (EntityId LocalId)
    | LocalEdit (EntityId LocalId)
    | LocalNew
    | AuthJwtUuid String (Maybe String)
    | AuthCheck (Maybe String)
    | NotFound String
    | CGU
    | MentionsLegales
    | PolitiqueConfidentialite


route : Url -> Parser (Route -> a) a
route url =
    oneOf
        [ map Dashboard <| top
        , map Connexion <| s "connexion" <?> Query.string "url"
        , map (Etablissements url.query) <| s etablissementsPrefix
        , map EtablissementNew <| s etablissementsPrefix </> s "creer"
        , map Etablissement <| s etablissementsPrefix </> string
        , map CreateurNew <| s createursPrefix </> s "creer"
        , map (Createurs url.query) <| s createursPrefix
        , map CreateurEdit <| s createursPrefix </> Api.EntityId.entityIdParser </> s "modifier"
        , map Createur <| s createursPrefix </> Api.EntityId.entityIdParser
        , map Actions <| s "actions"
        , map Profil <| s "profil"
        , map AdminUtilisateurs <| s "admin" </> s "utilisateurs"
        , map (AdminStats url.query) <| s "admin" </> s "stats"
        , map AdminTags <| s "admin" </> s "tags"
        , map (Locaux url.query) <| s locauxPrefix
        , map LocalNew <| s locauxPrefix </> s "creer"
        , map LocalEdit <| s locauxPrefix </> Api.EntityId.entityIdParser </> s "modifier"
        , map Local <| s locauxPrefix </> Api.EntityId.entityIdParser
        , map Deconnexion <| s "deconnexion"
        , map AuthJwtUuid <| s "auth" </> s "jwt" </> string <?> Query.string "url"
        , map AuthCheck <| s "auth" </> s "check" <?> Query.string "url"
        , map (Stats url.query) <| s "stats"
        , map CGU <| s "cgu"
        , map MentionsLegales <| s "mentions-legales"
        , map PolitiqueConfidentialite <| s "politique-de-confidentialite"
        ]


etablissementsPrefix : String
etablissementsPrefix =
    "etablissements"


createursPrefix : String
createursPrefix =
    "createurs"


locauxPrefix : String
locauxPrefix =
    "locaux"


toRoute : Url -> Route
toRoute url =
    url
        |> parse (route url)
        |> Maybe.withDefault (NotFound <| Url.toString url)


toUrl : Route -> String
toUrl r =
    case r of
        Dashboard ->
            Builder.absolute [] []

        Connexion redirect ->
            Builder.absolute [ "connexion" ] <|
                List.filterMap identity <|
                    [ redirect
                        |> Maybe.map (Builder.string "url")
                    ]

        EtablissementNew ->
            Builder.absolute [ etablissementsPrefix, "creer" ] []

        Createurs query ->
            Builder.absolute [ createursPrefix ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        CreateurNew ->
            Builder.absolute [ createursPrefix, "creer" ] []

        CreateurEdit id ->
            Builder.absolute [ createursPrefix, Api.EntityId.entityIdToString id, "modifier" ] []

        Createur id ->
            Builder.absolute [ createursPrefix, Api.EntityId.entityIdToString id ] []

        Actions ->
            Builder.absolute [ "actions" ] []

        Profil ->
            Builder.absolute [ "profil" ] []

        AdminUtilisateurs ->
            Builder.absolute [ "admin", "utilisateurs" ] []

        AdminTags ->
            Builder.absolute [ "admin", "tags" ] []

        AdminStats query ->
            Builder.absolute [ "admin", "stats" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Deconnexion ->
            Builder.absolute [ "deconnexion" ] []

        Locaux query ->
            Builder.absolute [ locauxPrefix ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        LocalNew ->
            Builder.absolute [ locauxPrefix, "creer" ] []

        LocalEdit id ->
            Builder.absolute [ locauxPrefix, Api.EntityId.entityIdToString id, "modifier" ] []

        Local id ->
            Builder.absolute [ locauxPrefix, Api.EntityId.entityIdToString id ] []

        AuthJwtUuid token redirect ->
            Builder.absolute [ "auth", "jwt", token ] <|
                List.filterMap identity <|
                    [ redirect
                        |> Maybe.map (Builder.string "url")
                    ]

        AuthCheck redirect ->
            Builder.absolute [ "auth", "check" ]
                (redirect
                    |> Maybe.map (Builder.string "url" >> List.singleton)
                    |> Maybe.withDefault []
                )

        Stats query ->
            Builder.absolute [ "stats" ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Etablissements query ->
            Builder.absolute [ etablissementsPrefix ] [] ++ (Maybe.withDefault "" <| Maybe.map ((++) "?") <| query)

        Etablissement siret ->
            Builder.absolute [ etablissementsPrefix, siret ] []

        NotFound url ->
            url

        CGU ->
            Builder.absolute [ "cgu" ] []

        MentionsLegales ->
            Builder.absolute [ "mentions-legales" ] []

        PolitiqueConfidentialite ->
            Builder.absolute [ "politique-de-confidentialite" ] []


defaultPageForRole : Data.Role.Role -> Route
defaultPageForRole role =
    case role of
        Data.Role.Deveco _ ->
            Dashboard

        Data.Role.Superadmin ->
            AdminUtilisateurs


matchAny : Route -> Route -> Maybe ()
matchAny any r =
    if any == r then
        Just ()

    else
        Nothing


matchConnexion : Route -> Maybe (Maybe String)
matchConnexion r =
    case r of
        Connexion redirect ->
            Just redirect

        _ ->
            Nothing


matchEtablissements : Route -> Maybe (Maybe String)
matchEtablissements r =
    case r of
        Etablissements search ->
            Just search

        _ ->
            Nothing


matchEtablissementSiret : Route -> Maybe String
matchEtablissementSiret r =
    case r of
        Etablissement siret ->
            Just siret

        _ ->
            Nothing


matchActions : Route -> Maybe ()
matchActions =
    matchAny Actions


matchFicheNew : Route -> Maybe ()
matchFicheNew =
    matchAny EtablissementNew


matchCreateurs : Route -> Maybe (Maybe String)
matchCreateurs r =
    case r of
        Createurs search ->
            Just search

        _ ->
            Nothing


matchCreateurNew : Route -> Maybe ()
matchCreateurNew =
    matchAny CreateurNew


matchCreateurId : Route -> Maybe (EntityId FicheId)
matchCreateurId r =
    case r of
        Createur id ->
            Just id

        _ ->
            Nothing


matchCreateurEditId : Route -> Maybe (EntityId FicheId)
matchCreateurEditId r =
    case r of
        CreateurEdit id ->
            Just id

        _ ->
            Nothing


matchNotFound : Route -> Maybe String
matchNotFound r =
    case r of
        NotFound url ->
            Just url

        _ ->
            Just <| toUrl r


matchAuthJwtUuid : Route -> Maybe ( String, Maybe String )
matchAuthJwtUuid r =
    case r of
        AuthJwtUuid uuid url ->
            Just ( uuid, url )

        _ ->
            Nothing


matchProfil : Route -> Maybe ()
matchProfil =
    matchAny Profil


matchDashboard : Route -> Maybe ()
matchDashboard =
    matchAny Dashboard


matchAdminUtilisateurs : Route -> Maybe ()
matchAdminUtilisateurs =
    matchAny AdminUtilisateurs


matchAdminTags : Route -> Maybe ()
matchAdminTags =
    matchAny AdminTags


matchAdminStats : Route -> Maybe (Maybe String)
matchAdminStats r =
    case r of
        AdminStats search ->
            Just search

        _ ->
            Nothing


matchDeconnexion : Route -> Maybe ()
matchDeconnexion =
    matchAny Deconnexion


matchLocaux : Route -> Maybe (Maybe String)
matchLocaux r =
    case r of
        Locaux search ->
            Just search

        _ ->
            Nothing


matchLocalNew : Route -> Maybe ()
matchLocalNew =
    matchAny LocalNew


matchLocalId : Route -> Maybe (EntityId LocalId)
matchLocalId r =
    case r of
        Local id ->
            Just id

        _ ->
            Nothing


matchLocalEditId : Route -> Maybe (EntityId LocalId)
matchLocalEditId r =
    case r of
        LocalEdit id ->
            Just id

        _ ->
            Nothing


matchAuthCheck : Route -> Maybe (Maybe String)
matchAuthCheck r =
    case r of
        AuthCheck redirect ->
            Just redirect

        _ ->
            Nothing


matchStats : Route -> Maybe (Maybe String)
matchStats r =
    case r of
        Stats search ->
            Just search

        _ ->
            Nothing


matchCGU : Route -> Maybe ()
matchCGU =
    matchAny CGU


matchMentionsLegales : Route -> Maybe ()
matchMentionsLegales =
    matchAny MentionsLegales


matchPolitiqueConfidentialite : Route -> Maybe ()
matchPolitiqueConfidentialite =
    matchAny PolitiqueConfidentialite
