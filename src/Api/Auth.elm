module Api.Auth exposing (checkAuth)

import Api
import Http
import Json.Decode as Decode
import Shared exposing (Msg, Redirect)
import UI.NewFeatures
import User


checkAuth : Bool -> Redirect -> Cmd Msg
checkAuth stayOnPage redir =
    let
        resultToMsg res =
            case res of
                Ok ( id, category ) ->
                    Shared.LoggedIn id redir category

                Err _ ->
                    if stayOnPage then
                        Shared.NoOp

                    else
                        Shared.LoggedOut redir
    in
    Http.get
        { url = Api.authStatus
        , expect =
            Http.expectJson resultToMsg <|
                Decode.map2 Tuple.pair
                    (Decode.field "user" User.decodeUser)
                    (Decode.field "newFeatures" UI.NewFeatures.decodeCategory)
        }
