import { toggleCrisp } from "./crisp";

if (
	typeof Elm !== "undefined" &&
	Object.prototype.hasOwnProperty.call(Elm, "Main") &&
	Elm.Main &&
	Object.prototype.hasOwnProperty.call(Elm.Main, "init") &&
	Elm.Main.init
) {
	function switchTheme(theme) {
		const options = ["system", "light", "dark"];
		const [html] = document.getElementsByTagName("html");
		if (options.includes(theme)) {
			html.dataset.frScheme = theme;
		}
	}

	const currentTheme = localStorage.getItem("scheme") || "system";

	const appUrl = window.location.origin;

	const flags = {
		now: new Date().getTime(),
		timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
		currentTheme,
		appUrl,
	};
	const elm = Elm.Main.init({ flags });
	elm.ports.switchTheme.subscribe(switchTheme);

	elm.ports.toggleCrisp.subscribe(toggleCrisp);
}
