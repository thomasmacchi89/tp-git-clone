window.customElements.define(
  "currency-display",
  class extends HTMLElement {
    constructor() {
      super();
      this._amount = BigInt(this.getAttribute("amount") || "0");
      this._currency = this.getAttribute("currency");
      this._cultureCode = this.getAttribute("culture-code");
    }
    static get observedAttributes() {
      return ["amount", "currency", "culture-code"];
    }

    attributeChangedCallback(name, prev, next) {
      if (!next) return;
      switch (name) {
        case "amount":
          this._amount = BigInt(next);
          break;
        case "currency":
          this._currency = next;
          break;
        case "culture-code":
          this._cultureCode = next;
          break;
        default:
          break;
      }

      this.render();
    }

    render() {
      let options = {
        style: "currency",
        currency: this._currency,
        maximumFractionDigits: 0,
      };

      try {
        let amount = this._amount
          ? this._amount.toLocaleString(this._cultureCode, options)
          : 0;
        this.innerHTML = `${amount}`;
      } catch (err) {
        console.log({ err });
        // Handle the error.
      }
    }
  }
);
