# Dévéco

## Dépendances

Ce projet nécessite [la dernière version LTS de NodeJS](https://nodejs.org/fr/download/) et le [gestionnaire de paquets Yarn](https://yarnpkg.com/getting-started).

## Commandes

### Lancement de l'application et du serveur de test

```shell
yarn start
```

Si tout s'est bien passé, l'application est consultable sur [http://localhost:3000/](http://localhost:3000/).

### Installation des dépendances

```shell
# installe les dépendances déclarées dans package.json
yarn install
```

### Lancement en mode dev

```shell
# démarre l'application et le serveur de test en même temps
yarn start:dev
```

### Lancement de l'explorateur d'UI

```shell
# lance un serveur qui permet d'explorer les différents composants UI du projet
yarn explorer
```

Si tout s'est bien passé, l'application est consultable sur [http://localhost:4000/](http://localhost:4000/).

### Lancement de la correction automatique

```shell
# signale des problèmes de formatage (via elm-format)
# ou de bonnes pratiques (via elm-review)
# et les résoud si possible
yarn fix
```

### Lancement des tests unitaires

```shell
# lance les tests et les relance à chaque modification de code
yarn test
# lance les tests une seule fois
yarn test:once
# alias de yarn test
yarn test:watch
```

### Lancement des tests e2e

```shell
# lance les tests avec un navigateur "headless" (dans la console)
yarn e2e
# lance les tests avec une interface graphique
yarn e2e:ui
# alias de yarn e2e
yarn e2e:headless
```

### Création de l'application de production

```shell
# crée une application optimisée pour le déploiement
yarn build
```

### Lancement du lint

```shell
# signale des problèmes de formatage (via elm-format)
# ou de revue de code (via elm-review)
# sans les résoudre automatiquement
yarn lint
```

### Lancement du formatage

```shell
# lance seulement elm-format sur le code Elm
yarn format
```

### Lancement de la revue de code

```shell
# lance la revue de code et la relance à chaque modification de code
yarn review
# lance la revue de code une seule fois
yarn review:once
# alias de yarn review
yarn review:watch
```

## Outils utilisés

- L'application est codée en Elm.
- Elle utilise le framework orus-io/elm-spa.
- Vite est utilisé comme bundler.
- Le style est assuré par le DSFR.
- Le layout est obtenu grâce aux classes fournies par Tailwind.

## Outils à mettre en œuvre

- [x] elm-spa : [https://github.com/orus-io/elm-spa](https://github.com/orus-io/elm-spa)
- [x] tests e2e en BDD : [CodeceptJS](https://codecept.io/quickstart/)
- [x] elm-test-rs : [https://github.com/mpizenberg/elm-test-rs](https://github.com/mpizenberg/elm-test-rs)
- [x] elm-review : [https://github.com/jfmengels/elm-review/](https://github.com/jfmengels/elm-review/)
- [x] elm-review shortlist : [https://gist.github.com/JesterXL/c69c192c756f16ff8ab7d38419176e0f](https://gist.github.com/JesterXL/c69c192c756f16ff8ab7d38419176e0f)
- [x] elm-optimize-level2 : [https://github.com/mdgriffith/elm-optimize-level-2](https://github.com/mdgriffith/elm-optimize-level-2)
- [x] minification : [https://guide.elm-lang.org/optimization/asset_size.html#instructions](https://guide.elm-lang.org/optimization/asset_size.html#instructions)
- [x] elm-book : [https://github.com/dtwrks/elm-book](https://github.com/dtwrks/elm-book)
- [x] elm/accessible-html-with-css : [https://github.com/tesk9/accessible-html-with-css/](https://github.com/tesk9/accessible-html-with-css/)
- [ ] R10 ? [https://package.elm-lang.org/packages/rakutentech/r10/latest/](https://package.elm-lang.org/packages/rakutentech/r10/latest/)
- [ ] Http Trinity ? [https://rakutentech.github.io/http-trinity/](https://rakutentech.github.io/http-trinity/)
- [x] Modélisation des entités en Markdown via Mermaid
- [x] passer à tailwind 3 : [https://tailwindcss.com/](https://tailwindcss.com/)
- [ ] étudier la mise en place de Sentry : [https://github.com/MTES-MCT/wikicarbone/commit/12cc8018df3603c0d426bdf4fad59fbb611d4c7d](https://github.com/MTES-MCT/wikicarbone/commit/12cc8018df3603c0d426bdf4fad59fbb611d4c7d)

## Ressources Elm

- Site officiel : [https://guide.elm-lang.org/](https://guide.elm-lang.org/)
- Elm Patterns : [https://sporto.github.io/elm-patterns/](https://sporto.github.io/elm-patterns/)
- Elm Program Test : [https://elm-program-test.netlify.app/](https://elm-program-test.netlify.app/)
- HTML-to-Elm : [https://html-to-elm.com/](https://html-to-elm.com/)
- json2elm : [https://korban.net/elm/json2elm/](https://korban.net/elm/json2elm/)
- elm-spa : [https://www.elm-spa.dev/](https://www.elm-spa.dev/)
- elm-css patterns : [https://elmcsspatterns.io/layout/sidebar](https://elmcsspatterns.io/layout/sidebar)
- Elm Cookbook : [https://orasund.gitbook.io/elm-cookbook/](https://orasund.gitbook.io/elm-cookbook/)
- Elm tests : [https://github.com/elm-explorations/test/](https://github.com/elm-explorations/test/)
- Elm @ Rakuten : [https://engineering.rakuten.today/post/elm-at-rakuten/](https://engineering.rakuten.today/post/elm-at-rakuten/)
- Codecept : [https://codecept.io/basics/](https://codecept.io/basics/)
- Elm-Book : [https://elm-book-in-elm-book.netlify.app/overview](https://elm-book-in-elm-book.netlify.app/overview)

## Modèle de données

```mermaid
erDiagram
  SUPERADMIN ||--|| COMPTE : "a"
  DEVECO ||--|| COMPTE : "a"
  DEVECO ||--|{ EQUIPE : "fait partie de"
  EPCI ||--|{ COMMUNE : "fédère"
  EQUIPE ||--|| TERRITOIRE : "supervise"
  TERRITOIRE ||--o| EPCI : "rattaché à"
  TERRITOIRE ||--o| COMMUNE : "rattaché à"
  EQUIPE ||--|| PORTEFEUILLE : "gère"
  PORTEFEUILLE ||--o{ FICHE : "contient"
  FICHE ||--|| ENTITE_SUIVIE : "liée à"
  ENTITE_SUIVIE ||--o| ENTREPRISE : "est"
  ENTITE_SUIVIE ||--o| PARTICULIER : "est"
  ENTREPRISE ||--|| COMMUNE : "sur le territoire de"
  ENTITE-SUIVIE }o..|o QPV : "dépend de"
  ENTITE-SUIVIE }o..|o IRIS : "dépend de"
  ENTITE-SUIVIE }o--o{ ACTIVITE_REELLE : "exerce"
  ENTITE-SUIVIE }o--o{ LOCALISATION : "se situe à"
  ACTION-CRM }o--|| FICHE : "relative à"
  ACTION-CRM ||--o| RAPPEL : "peut être"
  ACTION-CRM ||--o| ECHANGE : "peut être"
  ACTION-CRM ||--o| CLOTURE : "peut être"
  ECHANGE }o--o{ THEME : "relatif à"
  CLOTURE ||--|| THEME : "clôt"
  EQUIPE ||--|| IMMOBILIER : "gère"
  IMMOBILIER ||--o{ LOCAL : "contient"
  LOCAL ||--o{ PROPRIETAIRE : "possédé par"
  LOCAL ||--o{ LOCATAIRE : "loué à"
  PROPRIETAIRE ||--o| ENTREPRISE : "est"
  PROPRIETAIRE ||--o| PARTICULIER : "est"
  LOCATAIRE ||--o| ENTREPRISE : "est"
  LOCATAIRE ||--o| PARTICULIER : "est"
```
