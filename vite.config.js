import { defineConfig } from "vite";

export default defineConfig({
  plugins: [],
  server: {
    hmr: false,
    port: 3000,
    proxy: {
      "/api": {
        target: "http://127.0.0.1:4000/",
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
  publicDir: "./public",
  esbuild: {
    pure: [
      "F2",
      "F3",
      "F4",
      "F5",
      "F6",
      "F7",
      "F8",
      "F9",
      "A2",
      "A3",
      "A4",
      "A5",
      "A6",
      "A7",
      "A8",
      "A9",
    ],
    minify: true,
  },
  // there is some problematic interaction between terser and elm-optimise-level-2, so disabling for now
  // build: {
  //   minify: false,
  //   terserOptions: {
  //     compress: {
  //       pure_funcs: [
  //         "F2",
  //         "F3",
  //         "F4",
  //         "F5",
  //         "F6",
  //         "F7",
  //         "F8",
  //         "F9",
  //         "A2",
  //         "A3",
  //         "A4",
  //         "A5",
  //         "A6",
  //         "A7",
  //         "A8",
  //         "A9",
  //       ],
  //       pure_getters: true,
  //       keep_fargs: false,
  //       unsafe_comps: true,
  //       unsafe: true,
  //     },
  //     mangle: true,
  //   },
  // },
});
