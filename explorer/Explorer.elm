module Explorer exposing (main)

import BaseUI.Button
import BaseUI.Icon
import ElmBook exposing (Book, book)


main : Book state
main =
    book "Deveco"
        |> ElmBook.withChapterGroups [ ( "UI", [ BaseUI.Button.docs, BaseUI.Icon.docs ] ) ]
