module.exports = {
  content: [
    "./src/**/*.elm",
    "./vendor/**/*.elm",
    "./*.html",
    "./css/styles.css",
  ],
  theme: {
    extend: {
      colors: {
        "france-blue": { DEFAULT: "#000091", 100: "#F2F2F9", 500: "#000091" },
        "marianne-red": "#E1000F",
      },
    },
    screens: {
      sm: "36em",

      md: "48em",

      lg: "62em",

      xl: "78em",
    },
  },
  plugins: [],
};
